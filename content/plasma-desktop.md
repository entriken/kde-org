---
title: "Plasma"
scssFiles:
- /scss/plasma-desktop.scss
plasmais: Plasma is a Desktop
logoplasma: Plasma logo
description: "Plasma is KDE's desktop environment. Simple by default, powerful when needed."
type: plasma-desktop
get: Get Plasma for your device
buy: Buy a device with Plasma
see_announcements: See Announcements
parts:
  - title: Simple by default
    text: Plasma is designed to be easy to use.
    color: blue
    slides:
      - title: Launcher
        laptop: true
        src: /content/plasma-desktop/plasma-launcher.png
        text: The Plasma Launcher lets you quickly and easily launch applications, but it can do much more -- convenient tasks like bookmarking applications, searching for documents as you type, or navigating to common places help you getting straight to the point. With a history of recently started programs and opened files, you can return to where you left off. It even remembers previously entered search terms so you don't have to.
        alt: Screenshot Plasma with the launcher open
      - title: System Tray
        laptop: true
        src: /content/plasma-desktop/plasma-systemtray.png
        alt: Screenshot Plasma with the system tray open
        text: >
          Connect to your WiFi network; change the volume; switch to the next
          song or pause a video; access an external device; change the screen
          layout. All these and a lot more--directly from the system tray.

          To conserve the focus on what you're currently doing, any icon can be
          hidden if you like. Inactive icons hide themselves unless you tell
          them to stay where they are.
        class: bottom-right
      - title: Notifications
        src: /content/plasma-desktop/plasma-notifications.png
        alt: Screenshot Plasma with a notification
        laptop: true
        text: See active tasks and recent actions; read new e-mails; quickly reply to messages; see track changes or low battery notice; check for updates; interact with recently moved files or screenshots. Or enter "Do Not Disturb" mode to concentrate on your work.
        class: bottom-right
      - title: Discover
        src: /content/plasma-desktop/discover.png
        laptop: true
        text: Discover lets you manage the applications installed in your computer. Updating, removing or installing an application is only one click away. Supports [Flatpak](https://flatpak.org/), [Snaps](https://snapcraft.io/store) and the applications available in your Linux distribution.
  - title: Powerful when needed
    text: Plasma is made to stay out of the way as it helps you get things done. But under its light and intuitive surface, it's a powerhouse. So you're free to choose ways of usage right as you need them and when you need them.
    color: red
    slides:
      - title: KRunner
        text: KRunner is the launcher built into the Plasma desktop. While its basic function is to launch programs from a sort of mini-command-line, its functionality can be extended by "runners" to assist the user to accomplish a lot of tasks. From opening folders and files to calculations and currency conversions or even controlling your music player application - there are so many things right at your fingertips.
        alt: Screenshot Plasma with KRunner
        src: /content/plasma-desktop/krunner.png
        class: top center
        laptop: true
      - title: System Settings
        text: "Your Plasma desktop is very flexible and can be configured just how you like it using the System Settings app. Easily manage hardware, software, and workspaces all in one place: Keyboard, Printer, Languages, Desktop Themes, Fonts, Networks..."
        src: /content/plasma-desktop/systemsettings.png
        alt: "Screenshot: Plasma system settings"
        laptop: true
      - title: Professional Applications
        src: /content/plasma-desktop/kdenlive-plasma.png
        laptop: true
        alt: "Screenshot of KDenlive in Plasma"
        text: "[Krita](https://krita.org) is an application used by many artists that gives to everyone the opportunity to draw concept art, textures, matte paintings, illustrations and comics. [Kdenlive](https://kdenlive.org) is a professional tool that enables everyone to do non-linear video editing. [DigiKam](https://www.digikam.org) is an advanced open-source digital photo management application that provides a comprehensive set of tools for importing, managing, editing, and sharing photos and raw files."
  - title: Customizable
    text:  With Plasma the user is king. Not happy with the color scheme? Change it! Want to have your panel on the left edge of the screen? Move it! Don't like the font? Use a different one! Download custom widgets in one click and add them to your desktop or panel. 
    color: green
    slides:
      - title: Dark Theme
        text: Give your eyes some rest by switching Plasma and its applications to a built-in dark theme.
        src: /content/plasma-desktop/theme.png
        alt: Plasma and Kontact with dark theme enabled
        laptop: true
      - title: Widgets
        text: Add widgets to your desktop or panels; move, rotate or resize them; download custom widgets made by the community in one click.
        src: /content/plasma-desktop/plasma-widgets.png
        alt: Screenshot of Plasma widgets
        laptop: true
      - title: Panels
        text: Take complete control over your panels by moving and resizing them, adding new ones, changing the position of widgets within a panel or even adding new widgets to them.
        src:  /content/plasma-desktop/plasma-panels.png
        alt: Screenshot of Plasma panel
        laptop: true
      - title: Community Extensions
        text: KDE has a vast community of people who create application themes, color schemes, widgets, and extensions -- all for free! You can browse their creations in the [KDE Store](https://store.kde.org) or download them directly from the relevant page in System Settings.
        src: /content/plasma-desktop/plasma-latte.png
        laptop: true
        alt: Screenshot of Plasma with Latte Dock
  - title: KDE Connect
    text: Send your vacation pictures; see what music is currently playing on your computer; move your mouse cursor during a presentation; poweroff your computer remotely before going to bed. All these from your Android device.
    color: orange
    download: Download the android extension
    fdroid: Get it on F-Droid
    play: Get it on Google Play
    slides:
      - title: Media Sync
        text: See what's playing on your computer, be it music, videos or YouTube; Pause, play or skip to the next track all from your phone.
        mobile:
          src: /content/plasma-desktop/kde-connect-android-media.jpg
          alt: KDE Connect menu
        desktop:
          src: /content/plasma-desktop/kde-connect-desktop-media.png
          alt: Plasma desktop showing a music player open
      - title: Send Files
        text: Share files from your Android device by simply tapping on the share button and selecting KDE Connect.
        mobile:
          video: /content/plasma-desktop/kde-connect-android-send-files.webm
        desktop:
          video: /content/plasma-desktop/kde-connect-laptop-send-files.webm
      - title: Notifications
        text: Synchronize notification across your devices and quickly answer to messages from your computer.
        mobile:
          src: /content/plasma-desktop/kde-connect-android-notification.png
          alt: KDE Connect menu
        desktop:
          src: /content/plasma-desktop/kde-connect-laptop-notification.png
          alt: Plasma with a notification
        class: bottom left
trademark: Google Play and the Google Play logo are trademarks of Google LLC.


---

Use Plasma to surf the web; keep in touch with colleagues, friends and
family; manage your files, enjoy music and videos; and get creative and
productive at work. Do it all in a beautiful environment that adapts to
your needs, and with the safety, privacy-protection and peace of mind that
the best Free Open Source Software has to offer.
