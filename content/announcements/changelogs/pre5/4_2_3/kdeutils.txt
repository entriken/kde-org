------------------------------------------------------------------------
r947274 | metellius | 2009-03-31 10:28:04 +0000 (Tue, 31 Mar 2009) | 2 lines

Backporting revision 947135
BUG: 188421 When extracting into a subfolder, opening the destination directory should open the subfolder
------------------------------------------------------------------------
r947275 | metellius | 2009-03-31 10:28:09 +0000 (Tue, 31 Mar 2009) | 2 lines

Backporting revision 947136
Avoiding assert on unlocking from another thread
------------------------------------------------------------------------
r948088 | scripty | 2009-04-02 07:45:41 +0000 (Thu, 02 Apr 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r948134 | mlaurent | 2009-04-02 11:21:32 +0000 (Thu, 02 Apr 2009) | 3 lines

Backport:
Remove warning

------------------------------------------------------------------------
r949880 | rkcosta | 2009-04-06 07:13:01 +0000 (Mon, 06 Apr 2009) | 4 lines

Backport of SVN commit 949877: Open URL's in the previewer in an external browser.

CCBUG: 184982

------------------------------------------------------------------------
r952772 | aacid | 2009-04-12 13:53:02 +0000 (Sun, 12 Apr 2009) | 3 lines

Backport 952351 and 952353
Ark should be much more faster opening files now

------------------------------------------------------------------------
r953598 | mlaurent | 2009-04-14 11:45:30 +0000 (Tue, 14 Apr 2009) | 3 lines

Backport:
Fix crash when we has just a space as command

------------------------------------------------------------------------
r953602 | mlaurent | 2009-04-14 11:52:05 +0000 (Tue, 14 Apr 2009) | 4 lines

Backport:
Fix ksystemtray warning about icon


------------------------------------------------------------------------
r954066 | rkcosta | 2009-04-15 02:05:52 +0000 (Wed, 15 Apr 2009) | 2 lines

Backport 948053: emitQuery is not a signal anymore.

------------------------------------------------------------------------
r954085 | rkcosta | 2009-04-15 03:24:15 +0000 (Wed, 15 Apr 2009) | 2 lines

Backport 948055 and 954065: correctly determine if the archive is really single-foldered.

------------------------------------------------------------------------
r954092 | rkcosta | 2009-04-15 04:56:31 +0000 (Wed, 15 Apr 2009) | 4 lines

Backport 954091: do not use fast_mode=true when calling KMimeType::findByUrl as it can return a wrong mimetype for the file, which in turn calls a wrong program to view it.

CCBUG: 185286

------------------------------------------------------------------------
r954094 | rkcosta | 2009-04-15 05:07:17 +0000 (Wed, 15 Apr 2009) | 4 lines

Backport 954093: complement commit 947667 by not including duplicates in supportedWriteMimeTypes.

CCBUG: 160569

------------------------------------------------------------------------
r954132 | scripty | 2009-04-15 08:17:39 +0000 (Wed, 15 Apr 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r954640 | rkcosta | 2009-04-16 06:57:38 +0000 (Thu, 16 Apr 2009) | 2 lines

Backport 954638: call updateActions() in slotLoadingFinished so the Extract button is shown when the archive is loaded.

------------------------------------------------------------------------
r954795 | mlaurent | 2009-04-16 11:14:28 +0000 (Thu, 16 Apr 2009) | 4 lines

Backport:
don't close it when we close dialog


------------------------------------------------------------------------
r955133 | rkcosta | 2009-04-17 05:15:47 +0000 (Fri, 17 Apr 2009) | 8 lines

Backport 955132.

Make Ark follow KDE's click settings: preview a file with a single click
if single clicks should open files and folders, and the same for double
clicks. Based on Dolphin's code.

CCBUG: 188279

------------------------------------------------------------------------
r955135 | rkcosta | 2009-04-17 05:58:38 +0000 (Fri, 17 Apr 2009) | 8 lines

Backport 955134.

First try to determine the file mimetype by its content with
KMimeType::findByFileContent, and only resort to findByPath when the
file does not exist.

CCBUG: 117243

------------------------------------------------------------------------
r955611 | rkcosta | 2009-04-18 07:27:47 +0000 (Sat, 18 Apr 2009) | 2 lines

Remove some dead or unsupported file formats from Ark's supported formats list.

------------------------------------------------------------------------
r955612 | rkcosta | 2009-04-18 07:38:35 +0000 (Sat, 18 Apr 2009) | 6 lines

Backport 955134, 955460, 955461, 955473 and 955609.

Try to determine the file type not only from the file name, but also from its content.

CCBUG: 117243

------------------------------------------------------------------------
r955965 | rkcosta | 2009-04-19 01:39:10 +0000 (Sun, 19 Apr 2009) | 5 lines

Backport 955964.

Only emit itemTriggered for single-clicks for the left mouse button.
This requires borrowing a workaround for Qt-issue 176832 from Dolphin.

------------------------------------------------------------------------
r955987 | scripty | 2009-04-19 07:26:48 +0000 (Sun, 19 Apr 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r956301 | dakon | 2009-04-19 19:51:59 +0000 (Sun, 19 Apr 2009) | 4 lines

Fix setting expiration interval on new keys

BUG:190102

------------------------------------------------------------------------
r957182 | rkcosta | 2009-04-21 14:52:46 +0000 (Tue, 21 Apr 2009) | 7 lines

Backport 957180.

Add support for adding and removing files from non-compressed tar
archives.

CCBUG: 190249

------------------------------------------------------------------------
r957245 | mlaurent | 2009-04-21 17:02:51 +0000 (Tue, 21 Apr 2009) | 2 lines

Add missing i18n

------------------------------------------------------------------------
r957450 | mlaurent | 2009-04-22 07:30:41 +0000 (Wed, 22 Apr 2009) | 3 lines

Backport:
add missing i18n

------------------------------------------------------------------------
r958856 | dakon | 2009-04-24 20:59:18 +0000 (Fri, 24 Apr 2009) | 6 lines

Clean up dealing with persistent model indexes

BUG:189605

Thanks to David Palacio for testing this.

------------------------------------------------------------------------
r960897 | scripty | 2009-04-29 08:18:35 +0000 (Wed, 29 Apr 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
