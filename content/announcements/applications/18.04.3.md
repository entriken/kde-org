---
aliases:
- ../announce-applications-18.04.3
changelog: true
date: 2018-07-12
description: KDE Ships KDE Applications 18.04.3
layout: application
title: KDE Ships KDE Applications 18.04.3
version: 18.04.3
---

{{% i18n_var "July 12, 2018. Today KDE released the third stability update for <a href='%[1]s'>KDE Applications 18.04</a>. This release contains only bugfixes and translation updates, providing a safe and pleasant update for everyone." "../18.04.0" %}}

About 20 recorded bugfixes include improvements to Kontact, Ark, Cantor, Dolphin, Gwenview, KMag, among others.

Improvements include:

- Compatibility with IMAP servers that do not announce their capabilities has been restored
- Ark can now extract ZIP archives which lack proper entries for folders
- KNotes on-screen notes again follow the mouse pointer while being moved