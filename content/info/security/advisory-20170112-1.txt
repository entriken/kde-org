KDE Project Security Advisory
=============================

Title:          Ark: unintended execution of scripts and executable files
Risk Rating:    Important
CVE:            CVE-2017-5330
Versions:       ark >= 15.12
Author:         Elvis Angelaccio <elvis.angelaccio@kde.org>
Date:           12 January 2017

Overview
========

Through a (possibly malicious) tar archive that contains an
executable shell script or binary, it was possible to execute
arbitrary code on target machines.
KRun::runUrl() has a runExecutable argument which defaults to true.
Ark was using this default value and was also not checking
whether an extracted file was executable before passing it to the
runUrl() function.

Impact
======

An attacker can send legitimate tar archives with executable scripts or
binaries disguised as normal files (say, with README or LICENSE as filenames).
The attacker then can trick a user to select those files and click
the Open button in the Ark toolbar, which triggers the affected code.

Workaround
==========

Don't use the File -> Open functionality of Ark.
You can still open archives (Archive->Open) and extract them.

Solution
========

Update to Ark >= 16.12.1

For older releases of Ark, apply the following patches:

Applications/16.08 branch: https://commits.kde.org/ark/49ce94df19607e234525afda5ad4190ce35300c3
Applications/16.04 branch: https://commits.kde.org/ark/6b6da3f2e6ac5ca12b46d208d532948c1dbb8776
Applications/15.12 branch: https://commits.kde.org/ark/e2448360eca1b81eb59fffca9584b0fc5fbd8e5b

Credits
=======

Thanks to Fabian Vogt for reporting this issue, Elvis Angelaccio for
fixing this issue.
