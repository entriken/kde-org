---
aliases:
- ../4.5
date: '2010-08-10'
custom_about: true
custom_contact: true
title: KDE объявляет о выходе платформы разработки, приложений и оболочек рабочего
  стола версии 4.5.0
---

<div class="float-right m-3"> 
<p><a href="https://www.kde.org/announcements/4.5/"><img src="/announcements/4/4.5.0/images/kde45-small.png" class="img-fluid"/></a></p> <br/> <br/>
</div>

<p align="justify">
Сегодня, с выходом новых версий, платформа разработки KDE, 
рабочие окружения Plasma и Plasma Netbook, а также множество приложений получили цифры 4.5.0 
в графе «Версия». Приоритетами для команды разработчиков в этот 
раз стали улучшение удобства использования, производительности 
и стабильности работы ранее представленных технологий и функций.

<h2>Платформа разработки KDE версии 4.5.0 получила прирост 
стабильности и производительности, новый высокоскоростной 
кэш и поддержку WebKit.</h2>

<a href="./platform">
<img src="/announcements/4/4.5.0/images/platform.png" class="app-icon float-left m-3" alt="Платформа разработки KDE 4.5.0"/>
</a>

<p align="justify">
Сегодня команда разработчиков KDE 
объявляет о выходе платформы разработки KDE версии 4.5.0. 
В этом выпуске были значительно улучшены стабильность и 
производительность работы. Новый кэш <b>KSharedDataCache</b> 
был оптимизирован для осуществления быстрого доступа к 
файлам, хранящимся на диске (например, к значкам). 
Новая библиотека интеграции <b>KDE WebKit</b> поддерживает общие настройки сети, 
хранение паролей и многие другие возможности Konqueror. 
<a href="./platform"><b>Прочитать анонс полностью...</b></a>
</p>

<h2>Оболочки рабочего стола Plasma Desktop и Plasma Netbook 4.5.0: 
работать стало удобнее</h2>
<p align="justify">

<a href="./plasma">
<img src="/announcements/4/4.5.0/images/plasma.png" class="app-icon float-left m-3" alt="Оболочки рабочего стола KDE Plasma 4.5.0" />
</a>

Сегодня команда разработчиков
KDE объявляет о выходе оболочек рабочего стола Plasma Desktop
и Plasma Netbook версии 4.5.0.
Теперь рабочий стол Plasma стало ещё удобнее использовать.
Была улучшена обработка задач и уведомлений.
Область уведомления получила обновлённый, визуально более
приятный внешний вид. Также, благодаря интенсивному
использованию протокола уведомлений Freedesktop.org
(впервые был использован в предыдущей версии Plasma),
было улучшено взаимодействие с ней различных приложений.
<a href="./plasma"><b>Прочитать анонс полностью...</b></a>

</p>

<h2>Приложения KDE 4.5.0: повышение 
удобства работы и дополнительные улучшения</h2>
<p align="justify">

<a href="./applications">
<img src="/announcements/4/4.5.0/images/applications.png" class="app-icon float-left m-3" alt="Приложения KDE 4.5.0"/>
</a>

Сегодня команда разработчиков
KDE объявляет о выходе новой версии приложений KDE.
Многие образовательные программы, системные утилиты
и игры теперь стало ещё удобнее использовать.
Кроме того, они получили целый ряд дополнительных улучшений.
В виртуальном глобусе Marble появилась возможность
прокладки маршрутов с помощью сервиса OpenRouteService.
В веб-браузере Konqueror появилась возможность
использования WebKit, благодаря компоненту KWebKit,
который появился в репозитории Extragear.
<a href="./applications"><b>Прочитать анонс полностью...</b></a>

</p>

<div class="text-center">
	<a href="/announcements/4/4.5.0/kde-general45.png">
	<img src="/announcements/4/4.5.0/thumbs/kde-general45.png" class="img-fluid" alt="The KDE Plasma Netbook in 4.5 RC1">
	</a> <br/>
	<em>Выпущены платформа разработки, приложения KDE и оболочки рабочего стола Plasma Desktop и Plasma Netbook версии</em>
</div>
<br/>

<h4>
    Несите благую весть!
</h4>
<p align="justify">
KDE призывает всех <strong>делиться информацией об этом выпуске</strong> 
на социальных сервисах в сети Интернет. 
Высылайте материалы на новостные сайты, распространяйте 
информацию через delicious, digg, reddit, twitter и identi.ca. 
Загружайте снимки экрана на Facebook, Flickr, ipernity и Picasa 
и добавляйте их в соответствующие группы. Создавайте видеоролики 
и загружайте их на YouTube, Blip.tv, Vimeo и другие веб-сайты. 
И не забывайте помечать загруженные материалы 
<em>тегом <strong>kde</strong></em>, чтобы людям было легче их 
найти, а команда разработчиков KDE могла составить отчёт о том, 
насколько полно был освещён анонс нового выпуска программного обеспечения KDE. 
<strong>Расскажите о нас миру!</strong></p>

<p align="justify">
Следите за развитием событий вокруг выпуска 
4.5 через 
<a href="http://buzz.kde.org"><strong>ленту новостей сообщества KDE</strong></a>. 
На этом сайте вы увидите всё, что происходит на 
identi.ca, twitter, youtube, flickr, picasaweb, в блогах и 
других социальных сетях в реальном времени. 
Лента новостей находится по адресу 
<strong><a href="http://buzz.kde.org">buzz.kde.org</a></strong>.
</p>

<div align="center">
<table border="0" cellspacing="2" cellpadding="2">
<tr>
    <td>
        <a href="http://digg.com/search?s=kde45"><img src="/announcements/buttons/digg.gif" alt="Digg" title="Digg" /></a>
    </td>
    <td>
        <a href="http://www.reddit.com/search?q=kde45"><img src="/announcements/buttons/reddit.gif" alt="Reddit" title="Reddit" /></a>
    </td>
    <td>
        <a href="http://twitter.com/#search?q=kde45"><img src="/announcements/buttons/twitter.gif" alt="Twitter" title="Twitter" /></a>
    </td>
    <td>
        <a href="http://identi.ca/search/notice?q=kde45"><img src="/announcements/buttons/identica.gif" alt="Identi.ca" title="Identi.ca" /></a>
    </td>
</tr>
<tr>
    <td>
        <a href="http://www.flickr.com/photos/tags/kde45"><img src="/announcements/buttons/flickr.gif" alt="Flickr" title="Flickr" /></a>
    </td>
    <td>
        <a href="http://www.youtube.com/results?search_query=kde45"><img src="/announcements/buttons/youtube.gif" alt="Youtube" title="Youtube" /></a>
    </td>
    <td>
        <a href="http://www.facebook.com/#!/pages/K-Desktop-Environment/6344818917?ref=ts"><img src="/announcements/buttons/facebook.gif" alt="Facebook" title="Facebook" /></a>
    </td>
    <td>
        <a href="http://delicious.com/tag/kde45"><img src="/announcements/buttons/delicious.gif" alt="del.icio.us" title="del.icio.us" /></a>
    </td>
</tr>
</table>
<span style="font-size: 6pt">
    <a href="http://microbuttons.wordpress.com">микрокнопки</a>
</span>
</div>
