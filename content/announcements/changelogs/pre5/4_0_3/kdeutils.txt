2008-03-16 22:28 +0000 [r786393]  dakon

	* branches/KDE/4.0/kdeutils/kgpg/kgpg.cpp: Don't pass empty
	  arguments to gpg on file decryption BUG:159378

2008-03-16 22:37 +0000 [r786397]  dakon

	* branches/KDE/4.0/kdeutils/kgpg/kgpg.cpp: Fix filename of
	  decrypted files /dir/dir/file.asc was decrypted to /dir/dirfile
	  instead of /dir/dir/file

