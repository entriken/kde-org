---
aliases:
- ../announce-4.3-rc3
date: '2009-07-22'
description: KDE Community Ships Third Release Candidate of New Desktop, Applications
  and Platform.
title: KDE 4.3 RC3 Release Announcement
---

<h2 align="center">
  KDE 4.3 RC3 Codenames "Cay" Out For Testing
</h2>

<p align="justify">
  <strong>
KDE Community Ships Third Release Candidate of KDE 4.3
Free Desktop, Containing Many New Features and Improvements
</strong>
</p>

<p align="justify">
The <a href="http://www.kde.org/">KDE
Community</a> today announced the immediate availability of KDE 4.3 RC3, a release
candidate of the 3rd iteration over the KDE 4 desktop, applications and development
platform.
</p>
<p>
While 4.3 already feels very stable, there still have been quite some changes to the code-base since the second release candidate. In order to have the 4.3.0 release well-tested, the release team decided to put out another release candidate and postpone the final 4.3.0 release by one week. The most notable change is probably a fix for a performance problem in Plasma, where applets would shortly freeze after being resized. This bug has been resolved by delaying the caching of rendered SVG images until after the resizing has finished.
</p>
<p>
KDE 4.3 focuses on polishing and completing the user experience by providing a
modern and beautiful Free working environment.<br />
Please make sure you <a href="http://bugs.kde.org">report</a> any existing issues,
and in case of showstopper bugs, contact the KDE release team.</p>
<h4>Highlights of KDE 4.3 are...</h4>
<ul>
    <li>
    Integration of many new technologies, such as PolicyKit and Geolocation services
    </li>
    <li>
    New Window animation effects, a more usable Run Command popup
    and many new and improved addons in Plasma
    </li>
    <li>
    Many bugfixes and improvements across all applications and more integration of
    features coming with the KDE 4 platform
    </li>
</ul>

Please note that KDE 4.3 RC3 is not suitable for end users. Its sole purpose
is gathering feedback and testing.

<div class="text-center">
	<a href="/announcements/4/4.3-beta1/kcontrol4.png">
	<img src="/announcements/4/4.3-beta1/kcontrol4_thumb.png" class="img-fluid">
	</a> <br/>
	<em>The tree mode and the default icon mode in System Settings</em>
</div>
<br/>

<h4>Highlights of KDE 4 include...</h4>
<ul>
    <li>
    A streamlined hardware integration and multimedia experience thanks to the
    Solid and Phonon libraries
    </li>
    <li>
    A fully themable look taking advantage of modern graphics hardware
    combining the Oxygen artwork with the new Plasma desktop shell
    </li>
    <li>
    An extensive high-level development platform built on top of Qt, available
    in C++, Python, Ruby and other programming languages
    </li>
</ul>

<h3>Changes</h3>

<h4>General</h4>

<ul>
<li>
Policykit integration
</li><li>
Settings module for actions triggered by hardware events
</li><li>
Recent applications, titles and context menues for the classic application launcher
</li><li>
New Tree Mode in System Settings
</li><li>
Revisited user interface of the crash dialog, including backtrace parsing and rating
</li><li>
A network:/ IO Slave to show services such as Zeroconf as view in the file manager
</li><li>
Sorting folders in the file manager first is now optional
</li><li>
Support for the mobipocket format in the document viewer and desktop search
</li>
</ul>

<h4>Plasma Desktop &amp; KWin Window Manager</h4>
<ul>
<li>
KRunner now displays all known syntax in the results area when Help button pressed
</li><li>
Spacers for the panel
</li><li>
Holidays are now displayed in the calendar of the clock
</li><li>
Configurable keyboard shortcuts for Plasma
</li><li>
Grouping support in Extenders
</li><li>
"Open..." in notifications for finished jobs
</li><li>
Entering into directories when hovering over them in the folder view
</li><li>
Keyboard navigation in the zooming user interface and the folder view
</li><li>
Animations of dialogs from the Panel
</li><li>
SVG-themed desktop and panel toolbox
</li><li>
Unit conversion library
</li><li>
Completed wallpaper renderings can now be cached
</li><li>
More complete JavaScript API
</li><li>
Window manager elements now follow the Plasma theme
</li><li>
Fading desktop switcher
</li><li>
Windows sliding behind each other when changing focus
</li>
</ul>

<strong> Plasma Addons</strong>

<ul>
<li>
New Plasmoids: Bubble Monitor (a playful system monitor), Magnifique (Plasma magnifier), Media Player, openDesktop (Social Desktop Plasmoid), Remember The Milk Plasmoid (time management), retro-style system load viewer, Unit converter
</li><li>
New dataengines: Akonadi, Calendar Geolocation, keyboard state, access to Nepomuk metadata, Open Collaboration Services, Picture of the day, support for sun positioning and sunrise/sunset in the time engine
</li><li>
Rich-text editing and text-zooming in the Notes applet
</li><li>
Microblogging Plasmoid now also supports identi.ca
</li><li>
Pastebin applet has been reworked
</li><li>
The System tray can now show / hide icons based on their category
</li><li>
Jobs are now grouped together
</li><li>
Grouped windows' tooltips now also show previews
</li><li>
Lancelot launcher: Improved theming, configurable actions for system buttons, clear document history
</li><li>
Speaking the time from the clock
</li><li>
Weather wallpaper displays a wallpaper matching the current weather
</li><li>
The Virus wallpaper slowly eats your desktop
</li><li>
Mandelbrot fractals as real-time computed wallpaper
</li><li>
Marble Interactive desktop globe can be used as wallpaper
</li>
</ul>

<h4>PIM</h4>
<ul>
<li>
Updated contact list in the instant messenging application Kopete
</li><li>
Export, drag and drop and improved configuration in Alarm notifier
</li><li>
A resource to sync Akonadi with Google Calendar
</li><li>
Support for inserting inline images into emails in KMail
</li>
</ul>

<h4>Games</h4>
<ul>
<li>
New game "Curse of the Mummy" in KGoldrunner, more accurate pause and resume and recording and replaying of games
</li><li>
KPatience can now save the game state on exit to be restored later
</li><li>
New in KDE 4: KTron
</li><li>
Improved AI and asynchronous "thinking" in Bovo
</li><li>
70 new levels in KMahjongg
</li>
</ul>

<h4>Others</h4>
<ul>
<li>
The moon and other Planets can now be displayed using the Marble desktop globe
</li><li>
Support for imperial units, DGML2 and geolocation plugins in Marble
</li><li>
The Juk music player now supports real translucency in track announcements
</li>
<li>
UI improvements and better format handling in the compression tool Ark
</li><li>
New bookmarking tool in Okteta hex editor
</li><li>
KGpg, the GnuPG key manager has been ported to the <a href="http://aseigo.blogspot.com/2009/05/findlibknotificationitem-1.html">new system tray</a>
</li>
</ul>

<div class="text-center">
	<a href="/announcements/4/4.3-beta1/plasma.png">
	<img src="/announcements/4/4.3-beta1/plasma_thumb.png" class="img-fluid">
	</a> <br/>
	<em>Peeking into directories in Plasma and, formatting options in the notes applet and the translatoid</em>
</div>
<br/>

Note that the changelog is usually incomplete, for a complete list of
changes that went into KDE 4.3 RC3, you can browse the Subversion log.

<p />
To find out more about the KDE 4 desktop and applications, please refer to the
<a href="/announcements/4.2/">KDE 4.2.0</a>,
<a href="/announcements/4.1/">KDE 4.1.0</a> and
<a href="/announcements/4.0/">KDE 4.0.0</a> release
notes.
<p />

<h3>
  Installing KDE 4.3 RC3 Binary Packages
</h3>
<p align="justify">
  <em>Packagers</em>.
  Some Linux/UNIX OS vendors have kindly provided binary packages of KDE 4.3 RC3
for some versions of their distribution, and in other cases community volunteers
have done so.
  Some of these binary packages are available for free download from KDE's <a
href="http://download.kde.org/binarydownload.html?url=/unstable/4.2.98/">http://download.kde.org</a>.
  Additional binary packages, as well as updates to the packages now available,
may become available over the coming weeks.
</p>

<p align="justify">
  <a id="package_locations"><em>Package Locations</em></a>.
  For a current list of available binary packages of which the KDE Project has
been informed, please visit the <a href="/info/4.2.98">KDE 4.3 RC3 Info
Page</a>.
</p>

<h3>
  Compiling KDE 4.3 RC3 (a.k.a. 4.2.98)
</h3>
<p align="justify">
  <a id="source_code"></a>
  The complete source code for KDE 4.3 RC3 may be <a
href="http://download.kde.org/unstable/4.2.98/src/">freely downloaded</a>.
Instructions on compiling and installing KDE 4.3 RC3
  are available from the <a href="/info/4.2.98#binary">KDE 4.3 RC3 Info
Page</a>.
</p>

<h3>
  Supporting KDE
</h3>
<p align="justify">
 KDE is a <a href="http://www.gnu.org/philosophy/free-sw.html">Free Software</a>
community that exists and grows only because of the help of many volunteers that
donate their time and effort. KDE is always looking for new volunteers and
contributions, whether it is help with coding, bug fixing or reporting, writing
documentation, translations, promotion, money, etc. All contributions are
gratefully appreciated and eagerly accepted. Please read through the <a
href="/community/donations/">Supporting KDE page</a> for further information. </p>


