---
title: "International Sites"
description: "Support channels for KDE are also available in other languages."
scssFiles:
  - /scss/international.scss
---

<p>This page provides access to a number of KDE web pages in different
languages, countries or regional areas. The idea is to collect useful
information concerning the usage of KDE for specific communities, status of
translations, suggested settings for such users, and similar issues.
</p>
<p>
These pages also are not necessarily involved in the KDE translation
or localization efforts.  Please see the
<b><a href="https://l10n.kde.org">l10n.kde.org</a></b> website for
more information on this topic.
</p>
<p>
If you would like to contribute pages tailored to your language, country or regional
area please contact the KDE.org <a href="&#x6d;ailt&#111;:w&#x65;b&#x6d;&#00097;&#115;&#x74;&#101;&#00114;&#x40;kde.org">webmaster</a>.
</p>

<p class="international-site"><a href="https://br.kde.org/">🇧🇷 Brasil</a></p>
<p class="international-site"><a href="https://kde-china.org">🇨🇳 China</a></p>
<!-- <p class="international-site"><a href="https://www.kde.ie/">🇮🇪 Ireland</a></p> -->
<p class="international-site"><a href="https://il.kde.org/">🇮🇱 Israel</a></p>
<p class="international-site"><a href="https://kdeitalia.it">🇮🇹 Italy</a></p>
<p class="international-site"><a href="https://jp.kde.org/">🇯🇵 Japan</a></p>
<p class="international-site"><a href="https://kde.nl/">🇳🇱 Netherlands</a></p>
<p class="international-site"><a href="https://ro.kde.org">🇷🇴 Romania</a></p>
<p class="international-site"><a href="https://kde.ru">🇷🇺 Russia</a></p>
<p class="international-site"><a href="https://www.kde-espana.org">🇪🇸 Spain</a></p>

<div style="clear:left;"></div>

<p style="font-size: smaller;">
<b>Note:</b><br />

Our policy regarding the sites listed above is simple: If somebody goes through the
trouble of creating a KDE website, then we will include it in this
list. Furthermore, we will <em>not</em> override the site creator's
preference for what to call their geographical area (e.g., calling the
Taiwan site "Taiwan" instead of "China").

</p>
