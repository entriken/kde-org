2006-10-10 09:55 +0000 [r594148]  binner

	* branches/KDE/3.5/kdeaccessibility/kdeaccessibility.lsm: fix for
	  next sed run

2006-10-22 19:37 +0000 [r598183]  cramblitt

	* branches/KDE/3.5/kdeaccessibility/kttsd/plugins/festivalint/voices:
	  CCMAIL:kde-18n-doc@kde.org Possible new strings for mbrola
	  italian voices. To install the voices, look at bug 136131.

2006-11-02 22:29 +0000 [r601337]  cramblitt

	* branches/KDE/3.5/kdeaccessibility/kttsd/plugins/festivalint/kttsd_festivalintplugin.desktop,
	  branches/KDE/3.5/kdeaccessibility/kttsd/plugins/festivalint/voices:
	  CCMAIL:kde-i18n-doc@kde.org CCMAIL:kde-accessibility@kde.org New
	  strings. Add support for new Vietnamese Festival voices at
	  http://sourceforge.net/projects/vietnamesevoice.

2006-11-26 13:35 +0000 [r607957]  lueck

	* branches/KDE/3.5/kdeaccessibility/doc/kmag/index.docbook,
	  branches/KDE/3.5/kdeaccessibility/doc/kmouth/index.docbook:
	  backport from trunk: changed wrong markup guimenuitem -> guimenu
	  kmag 1 fuzzy, kmouth 8 fuzzy CCMAIL:kde-i18n-doc@kde.org

2007-01-08 21:18 +0000 [r621475]  binner

	* branches/KDE/3.5/kdeaccessibility/kmag/kmagzoomview.cpp: fix
	  warning: comparisons like X<=Y<=Z do not have their mathematical
	  meaning

2007-01-15 09:15 +0000 [r623692]  binner

	* branches/KDE/3.5/kdesdk/kdesdk.lsm,
	  branches/KDE/3.5/kdenetwork/kopete/libkopete/kopeteversion.h,
	  branches/KDE/3.5/kdepim/kdepim.lsm,
	  branches/KDE/3.5/kdewebdev/quanta/quanta.lsm,
	  branches/KDE/3.5/kdeaccessibility/kdeaccessibility.lsm,
	  branches/arts/1.5/arts/arts.lsm,
	  branches/KDE/3.5/kdeadmin/kdeadmin.lsm,
	  branches/KDE/3.5/kdeaddons/kdeaddons.lsm,
	  branches/KDE/3.5/kdenetwork/kdenetwork.lsm,
	  branches/KDE/3.5/kdelibs/kdelibs.lsm,
	  branches/KDE/3.5/kdeartwork/kdeartwork.lsm,
	  branches/KDE/3.5/kdewebdev/VERSION,
	  branches/KDE/3.5/kdemultimedia/kdemultimedia.lsm,
	  branches/KDE/3.5/kdebase/kdebase.lsm,
	  branches/KDE/3.5/kdenetwork/kopete/VERSION,
	  branches/KDE/3.5/kdewebdev/kdewebdev.lsm,
	  branches/KDE/3.5/kdegames/kdegames.lsm,
	  branches/KDE/3.5/kdeedu/kdeedu.lsm,
	  branches/KDE/3.5/kdebindings/kdebindings.lsm,
	  branches/KDE/3.5/kdetoys/kdetoys.lsm,
	  branches/KDE/3.5/kdegraphics/kdegraphics.lsm,
	  branches/KDE/3.5/kdeutils/kdeutils.lsm: increase version

