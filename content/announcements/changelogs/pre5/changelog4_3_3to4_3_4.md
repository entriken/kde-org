---
aliases:
- ../changelog4_3_3to4_3_4
hidden: true
title: KDE 4.3.4 Changelog
---

<h2>Changes in KDE 4.3.4</h2>
    <h3 id="kdelibs"><a name="kdelibs">kdelibs</a><span class="allsvnchanges"> [ <a href="4_3_4/kdelibs.txt">all SVN changes</a> ]</span></h3>    
    <div class="module" style="padding-left: 20px;">
      <h4><a name="plasma">plasma</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix ">Fix pixmap caching bug. Improve overall plasma responsiveness. See SVN commit <a href="http://websvn.kde.org/?rev=1055788&amp;view=rev">1055788</a>. </li>
      </ul>
      </div>
      <h4><a name="kio">kio</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix ">Show the bookmark url in the statusbar when the mouse is over the menu item, like in kde3. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=153960">153960</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1048549&amp;view=rev">1048549</a>. </li>
        <li class="bugfix ">Fix crash when killing a job while a rename or skip dialog for it is shown. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=192976">192976</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1055386&amp;view=rev">1055386</a>. </li>
      </ul>
      </div>
    </div>
    <h3 id="kdebase"><a name="kdebase">kdebase</a><span class="allsvnchanges"> [ <a href="4_3_4/kdebase.txt">all SVN changes</a> ]</span></h3>    
    <div class="module" style="padding-left: 20px;">
      <h4><a name="konqueror">konqueror</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix ">Don't show (sometimes) the statusbar under the sidebar. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=100373">100373</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1047334&amp;view=rev">1047334</a>. </li>
        <li class="bugfix ">Fix statusbar sometimes hidden by mistake in filemanagement mode. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=193072">193072</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1048275&amp;view=rev">1048275</a>. </li>
      </ul>
      </div>
      <h4><a name="systemsettings">systemsettings</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix ">Don't offer to move the home directory when changing a path that was pointing to it. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=193057">193057</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1055428&amp;view=rev">1055428</a>. </li>
      </ul>
      </div>
    </div>
    <h3 id="kdeutils"><a name="kdeutils">kdeutils</a><span class="allsvnchanges"> [ <a href="4_3_4/kdeutils.txt">all SVN changes</a> ]</span></h3>    
    <div class="module" style="padding-left: 20px;">
      <h4><a href="http://utils.kde.org/projects/kgpg" name="kgpg">KGpg</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix ">Fix passphrases with non-ascii characters. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=155790">155790</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1045773&amp;view=rev">1045773</a>. </li>
        <li class="bugfix crash">Fix crash after deleting photo id. See SVN commit <a href="http://websvn.kde.org/?rev=1052147&amp;view=rev">1052147</a>. </li>
      </ul>
      </div>
      <h4><a href="http://utils.kde.org/projects/okteta" name="okteta">Okteta</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix ">Doing a edit in the value column did not trigger the modifiedChanged signal. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=213868">213868</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1046919&amp;view=rev">1046919</a>. </li>
        <li class="bugfix ">On the close of modified documents the behaviour for the success of the save was inverted, wrongly closing unsaved documents and keeping saved open. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=213868">213868</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1046929&amp;view=rev">1046929</a>. </li>
      </ul>
      </div>
    </div>
    <h3 id="kdeedu"><a name="kdeedu">kdeedu</a><span class="allsvnchanges"> [ <a href="4_3_4/kdeedu.txt">all SVN changes</a> ]</span></h3>    
    <div class="module" style="padding-left: 20px;">
      <h4><a href="http://edu.kde.org/marble" name="marble">Marble</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix ">Remove repeated debug output. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=184033">184033</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1049442&amp;view=rev">1049442</a>. </li>
        <li class="bugfix ">Fix infinite loop caused by non-scalable fonts. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=189633">189633</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1049584&amp;view=rev">1049584</a>. </li>
        <li class="bugfix ">Show the altitude in the current position (navigation mode). Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=202162">202162</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1049668&amp;view=rev">1049668</a>. </li>
        <li class="bugfix ">Do not search for the same term more than once in a row. Fixes duplicated result entries and crashes. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=210013">210013</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1049435&amp;view=rev">1049435</a>. </li>
      </ul>
      </div>
    </div>
    <h3 id="kdegames"><a name="kdegames">kdegames</a><span class="allsvnchanges"> [ <a href="4_3_4/kdegames.txt">all SVN changes</a> ]</span></h3>    
    <div class="module" style="padding-left: 20px;">
      <h4><a href="http://games.kde.org/game.php?game=ktuberling" name="ktuberling">ktuberling</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix ">Improve saving of files. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=214090">214090</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1047741&amp;view=rev">1047741</a>. </li>
      </ul>
      </div>
    </div>
    <h3 id="kdegraphics"><a name="kdegraphics">kdegraphics</a><span class="allsvnchanges"> [ <a href="4_3_4/kdegraphics.txt">all SVN changes</a> ]</span></h3>    
    <div class="module" style="padding-left: 20px;">
      <h4><a href="http://okular.kde.org" name="okular">Okular</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix ">Stop the search when pressing the [x] button in the find bar. See SVN commit <a href="http://websvn.kde.org/?rev=1045401&amp;view=rev">1045401</a>. </li>
        <li class="bugfix ">Fix potential crash when searching. See SVN commit <a href="http://websvn.kde.org/?rev=1045411&amp;view=rev">1045411</a>. </li>
        <li class="bugfix ">Fix crash on document close/reload. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=212066">212066</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1047675&amp;view=rev">1047675</a>. </li>
        <li class="bugfix ">Fix saving of annotations using the current locale. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=211990">211990</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1050640&amp;view=rev">1050640</a>. </li>
        <li class="bugfix ">Fix changing from single page to facing pages view changing the current viewport. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=215463">215463</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1052139&amp;view=rev">1052139</a>. </li>
      </ul>
      </div>
    </div>
    <h3 id="kdemultimedia"><a name="kdemultimedia">kdemultimedia</a><span class="allsvnchanges"> [ <a href="4_3_4/kdemultimedia.txt">all SVN changes</a> ]</span></h3>    
    <div class="module" style="padding-left: 20px;">
      <h4><a href="http://developer.kde.org/~wheeler/juk.html" name="juk">JuK</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix ">Clear any filtering being used by the Now Playing bar if the Now Playing bar is hidden. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=112238">112238</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1054460&amp;view=rev">1054460</a>. </li>
        <li class="bugfix ">Fix a question-mark icon when dragging tracks in a playlist. See SVN commit <a href="http://websvn.kde.org/?rev=1054467&amp;view=rev">1054467</a>. </li>
      </ul>
      </div>
    </div>
    <h3 id="kdepim"><a name="kdepim">kdepim</a><span class="allsvnchanges"> [ <a href="4_3_4/kdepim.txt">all SVN changes</a> ]</span></h3>    
    <div class="module" style="padding-left: 20px;">
      <h4><a href="http://www.astrojar.org.uk/kalarm" name="kalarm">KAlarm</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Improvements:</em>
      <ul>
          <li class="improvement">In dual screen setup, show alarm in other screen if there is a full screen window (whether active or not). Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=211696">211696</a>.  See SVN commits <a href="http://websvn.kde.org/?rev=1042513&amp;view=rev">1042513</a>, <a href="http://websvn.kde.org/?rev=1043157&amp;view=rev">1043157</a> and <a href="http://websvn.kde.org/?rev=1049671&amp;view=rev">1049671</a>. </li>
      </ul>
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix crash">Fix crashes when editing an alarm using the alarm window Edit button, if alarm window updates or closes. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=213461">213461</a>.  See SVN commits <a href="http://websvn.kde.org/?rev=1050233&amp;view=rev">1050233</a>, <a href="http://websvn.kde.org/?rev=1050335&amp;view=rev">1050335</a>, <a href="http://websvn.kde.org/?rev=1051193&amp;view=rev">1051193</a> and <a href="http://websvn.kde.org/?rev=1052909&amp;view=rev">1052909</a>. </li>
      </ul>
      </div>
    </div>