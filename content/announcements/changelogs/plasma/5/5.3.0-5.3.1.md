---
aliases:
- /announcements/plasma-5.3.0-5.3.1-changelog
hidden: true
plasma: true
title: Plasma 5.3.1 complete changelog
type: fulllog
version: 5.3.1
---

### <a name='bluedevil' href='http://quickgit.kde.org/?p=bluedevil.git'>Bluedevil</a>

- Fileitemactionplugin: Don't use blocking DBus calls. <a href='http://quickgit.kde.org/?p=bluedevil.git&amp;a=commit&amp;h=42e1343843e65efdd1310407b9d3cce41202eafd'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/347113'>#347113</a>
- Applet: Fix showing incorrect device name in connect failed notification. <a href='http://quickgit.kde.org/?p=bluedevil.git&amp;a=commit&amp;h=3af1be56bb7fd36ee8caffbb995371abc54241de'>Commit.</a>
- Update pin-code-database.xml. <a href='http://quickgit.kde.org/?p=bluedevil.git&amp;a=commit&amp;h=76879339ccdd121646ad8d9bba58955483a79a00'>Commit.</a>

### <a name='bluez-qt' href='http://quickgit.kde.org/?p=bluez-qt.git'>bluez-qt</a>

- Don't ignore interface name in DBus properties changed signal. <a href='http://quickgit.kde.org/?p=bluez-qt.git&amp;a=commit&amp;h=4bf933c04f406e3040bd3335c2dde54da14b5c74'>Commit.</a>

### <a name='breeze' href='http://quickgit.kde.org/?p=breeze.git'>Breeze</a>

- Cleanup tests in scrollarea event filter. <a href='http://quickgit.kde.org/?p=breeze.git&amp;a=commit&amp;h=02ceed6ef677618a171aeb8dce1e240a74c0e161'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/347971'>#347971</a>
- Default to Qt::AlignVCenter (instead of Qt::AlignTop) when vertical alignment flag is not set. <a href='http://quickgit.kde.org/?p=breeze.git&amp;a=commit&amp;h=29f05099f5fb950935e65e6f626f2f162e83b383'>Commit.</a> See bug <a href='https://bugs.kde.org/346679'>#346679</a>
- Make sure iconSize is valid before rendering. <a href='http://quickgit.kde.org/?p=breeze.git&amp;a=commit&amp;h=b69d1c16f08d26f689df22b573cd18c52f75f76b'>Commit.</a>
- Sanitize button positioning. <a href='http://quickgit.kde.org/?p=breeze.git&amp;a=commit&amp;h=e88bbeed202801fcfeef6f9f895183a5114ac15d'>Commit.</a>
- Pass iconSize as button's parameter rather than trying to calculate it from geometry and offset. <a href='http://quickgit.kde.org/?p=breeze.git&amp;a=commit&amp;h=98f907edbde8ba7878124c3171d00debae7e2ebb'>Commit.</a>
- Implement SH_ItemView_ActivateItemOnSingleClick in kde4. <a href='http://quickgit.kde.org/?p=breeze.git&amp;a=commit&amp;h=aa34eb031e205c6b21bcfe055523ca9be95b8dac'>Commit.</a>
- Build on ARM. <a href='http://quickgit.kde.org/?p=breeze.git&amp;a=commit&amp;h=96b3201d62485a1d8bd72a04a835868e92e36757'>Commit.</a>

### <a name='kdeplasma-addons' href='http://quickgit.kde.org/?p=kdeplasma-addons.git'>Plasma Addons</a>

- DateTimeRunner: Fix off by 1 error. <a href='http://quickgit.kde.org/?p=kdeplasma-addons.git&amp;a=commit&amp;h=ff619eee229d7d71ecf6cf389dd76a59f1cc7d7d'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/346021'>#346021</a>
- Kimpanel: fix window position when coordinate is outside screen. <a href='http://quickgit.kde.org/?p=kdeplasma-addons.git&amp;a=commit&amp;h=010540f20b09df8e518ca16175995f5c39057c5f'>Commit.</a>

### <a name='kinfocenter' href='http://quickgit.kde.org/?p=kinfocenter.git'>KInfoCenter</a>

- Extract messages from Modules/base into the catalog kcminfo. <a href='http://quickgit.kde.org/?p=kinfocenter.git&amp;a=commit&amp;h=8ed18417609ef64d1c4002913d9d8f5c36275bad'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/123864'>#123864</a>

### <a name='kio-extras' href='http://quickgit.kde.org/?p=kio-extras.git'>KIO Extras</a>

- Don't mangle UDS_TARGET_URL to UDS_LOCAL_PATH in UDSEntries. <a href='http://quickgit.kde.org/?p=kio-extras.git&amp;a=commit&amp;h=fe1f50caaf24c47000938c9ac36a7f9a304f3c96'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/123781'>#123781</a>

### <a name='kwin' href='http://quickgit.kde.org/?p=kwin.git'>KWin</a>

- Fix presentwindows crash on disabling closebuttons. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=ae31aaaa13bef41d2f4583e2ed452b2136c8508e'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/123865'>#123865</a>
- No VBO upload for zero vertex count. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=12358f839a293d7936e12dbabdd1aca953063b36'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/347900'>#347900</a>. Code review <a href='https://git.reviewboard.kde.org/r/123865'>#123865</a>
- [effects] Fix loading of trackmouse effect textures. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=00dd1ad0faed46954a3dce27c0fe07e222c987fd'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/123862'>#123862</a>
- Show Desktop: activateNextClient prefers desktop. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=eb789e43e317fbb25fbd1f51d0edf9800f8c002a'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/123783'>#123783</a>. See bug <a href='https://bugs.kde.org/346837'>#346837</a>. See bug <a href='https://bugs.kde.org/346933'>#346933</a>. See bug <a href='https://bugs.kde.org/347212'>#347212</a>
- Show Desktop: break state with activation (only). <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=43e3b7db285c48efb25966334262d0e80190fd9e'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/123783'>#123783</a>. See bug <a href='https://bugs.kde.org/346837'>#346837</a>. See bug <a href='https://bugs.kde.org/346933'>#346933</a>. See bug <a href='https://bugs.kde.org/347212'>#347212</a>
- Show Desktop: keep desktop group visible. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=a16a489a43aef03e198630f80d8236b56052b6f9'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/123783'>#123783</a>. See bug <a href='https://bugs.kde.org/346837'>#346837</a>. See bug <a href='https://bugs.kde.org/346933'>#346933</a>. See bug <a href='https://bugs.kde.org/347212'>#347212</a>
- Showing Desktop: keep docks visible. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=f3b69b0ed0d3fa863ed4b2233ac5ce6c5fcdd9c3'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/123783'>#123783</a>. Fixes bug <a href='https://bugs.kde.org/346933'>#346933</a>. Fixes bug <a href='https://bugs.kde.org/347212'>#347212</a>. See bug <a href='https://bugs.kde.org/346837'>#346837</a>
- Fix CM reselection. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=e6176ffc020b888f49a1326e2e8bcec5a6b4901a'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/347813'>#347813</a>. Code review <a href='https://git.reviewboard.kde.org/r/123826'>#123826</a>
- Lazy setting of xbc properties on qApp. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=87a3d01a75e2371ae4c74ce71f2a7446a91ca7df'>Commit.</a> See bug <a href='https://bugs.kde.org/346748'>#346748</a>. Code review <a href='https://git.reviewboard.kde.org/r/123777'>#123777</a>
- Stall wobbling while screen is transformed. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=82e0601af97c508580e255759064c25fb02c276d'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/123733'>#123733</a>. Fixes bug <a href='https://bugs.kde.org/338972'>#338972</a>
- Ignore elevation list while screen is locked. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=89195c7e83f4d8be18fede81f14b69e5747b30e2'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/347466'>#347466</a>
- Prefer query Screens::refreshRate(). <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=c08038e0a63f51202762fa72943d5cd3884a82f8'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/347360'>#347360</a>. Code review <a href='https://git.reviewboard.kde.org/r/123693'>#123693</a>
- Add Screens::name(int screen); STUB but for XRandr. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=5779e6844dc3b40c304ebc936b3eb1e37ea8b51a'>Commit.</a>
- Add OutputInfo class to obtain output names. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=b85d5666c13d8946531b98b79b4b91f727cc0583'>Commit.</a>
- Add refreshRate to Screens. STUB but for XRandr!. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=03ab846da82642b92af2d0e24d8bfb50c2f767bf'>Commit.</a>
- Forward resource modes next to crtcs. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=f916cb1aa593991a6062f71774a252ab6d8891a4'>Commit.</a>
- Desk grid: do not recreate DesktopButtonsViews. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=0b334f2a7001c4b5ea66d2a948f865019e944e4b'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/347339'>#347339</a>. Code review <a href='https://git.reviewboard.kde.org/r/123668'>#123668</a>
- Make switchWindow FROM stickies act on current VD. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=56fce0adc7ebd59d8fbfa682fb598eeb82e98541'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/331512'>#331512</a>. Code review <a href='https://git.reviewboard.kde.org/r/123640'>#123640</a>
- UpdateXTime before sending a takeFocus message. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=c2ba7658208ff50eed350cfa6f4dd44f2db77a81'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/347153'>#347153</a>. Code review <a href='https://git.reviewboard.kde.org/r/123639'>#123639</a>
- Window aperture always needs to cancel. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=e0c3b4eed35b6db0f32cb6aefe6f5b4cfd96172d'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/347174'>#347174</a>. Code review <a href='https://git.reviewboard.kde.org/r/123636'>#123636</a>
- Do not switch desktop on resizing windows. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=5d09eaae1ff20887f20e48bbf91fcdb35cd20669'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/123599'>#123599</a>
- Break desktopshowing state from cover &amp; flipswitch. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=7a7d79a1689416f52fc11404b9400696600fa412'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/123550'>#123550</a>. See bug <a href='https://bugs.kde.org/346837'>#346837</a>

### <a name='libkscreen' href='http://quickgit.kde.org/?p=libkscreen.git'>libkscreen</a>

- Handle backend being deleted during GetConfigOperation. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=d062d40582e00c8f34b8e0a1fd1e457d71b70ed9'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/347626'>#347626</a>. Code review <a href='https://git.reviewboard.kde.org/r/123860'>#123860</a>
- BackendLauncher: delete and unload backends before returning from main(). <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=bd679ee72b3ef32832a3700a68405231830ee832'>Commit.</a>
- XRandR: use intermediate screen size when applying config. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=d6233b63da667e0c9296e0738da74930288c8dc5'>Commit.</a>
- Fix crash introduced in previous commit. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=5d3c70f8c7bf91c3bc5f63cd47ff01493c351606'>Commit.</a>
- Fix potential crash when running ConfigOperation in exec() mode. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=cd58a75a6d3d01289894bb03559289f7468ee556'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/346575'>#346575</a>

### <a name='muon' href='http://quickgit.kde.org/?p=muon.git'>Muon</a>

- Make sure the apt notifier gets initialized. <a href='http://quickgit.kde.org/?p=muon.git&amp;a=commit&amp;h=a87de352a5b682c381e8c7ec78de2730e7477b44'>Commit.</a>
- --debug. <a href='http://quickgit.kde.org/?p=muon.git&amp;a=commit&amp;h=70fed2ef8119bf63ff0beca8857248745b6abe7a'>Commit.</a>
- Polish Discover menu configuration. <a href='http://quickgit.kde.org/?p=muon.git&amp;a=commit&amp;h=c3d3c7db7ed85656a775c4c39713f2ca22767ec1'>Commit.</a>
- Improve display of the update button. <a href='http://quickgit.kde.org/?p=muon.git&amp;a=commit&amp;h=7b9ac8cfedf38fc6015f46435a05233988fa2699'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/343210'>#343210</a>
- Use proper icon to identify updates. <a href='http://quickgit.kde.org/?p=muon.git&amp;a=commit&amp;h=0f425b8b7b4d6d154f47019d8ee8ab436f788295'>Commit.</a>
- Only emit about found updates when they change. <a href='http://quickgit.kde.org/?p=muon.git&amp;a=commit&amp;h=217834e6a20d07b7f99415525a27928329cae88c'>Commit.</a> See bug <a href='https://bugs.kde.org/346622'>#346622</a>

### <a name='oxygen' href='http://quickgit.kde.org/?p=oxygen.git'>Oxygen</a>

- Default to Qt::AlignVCenter (instead of Qt::AlignTop) when vertical alignment flag is not set CCBUG: 346679. <a href='http://quickgit.kde.org/?p=oxygen.git&amp;a=commit&amp;h=6250940db28953c16e4ee8d26b2fbf58ffd40e70'>Commit.</a>
- Fix popup menu items getting stray highlighted. <a href='http://quickgit.kde.org/?p=oxygen.git&amp;a=commit&amp;h=b680714d4845aef3ad44c39b6e48e029f000a62d'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/332377'>#332377</a>. Code review <a href='https://git.reviewboard.kde.org/r/123807'>#123807</a>
- Implement SH_ItemView_ActivateItemOnSingleClick. <a href='http://quickgit.kde.org/?p=oxygen.git&amp;a=commit&amp;h=b8f9cecab36094a04f2973e2770e865703f770e1'>Commit.</a>

### <a name='plasma-desktop' href='http://quickgit.kde.org/?p=plasma-desktop.git'>Plasma Desktop</a>

- Require xorg-evdev >= 2.8.99.1. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=0a63e5499aa826ce5092c6e29b956caa13e481c1'>Commit.</a>
- Fix translation of strings in kcm/touchpad.kcfg. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=4d26fab778afac076a6536d8b0e396c3cd254d45'>Commit.</a>
- Fix toolbox positioning. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=d33db5e7128e87b50d2e716cedad5020cf850678'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/347857'>#347857</a>
- Abort window highlight effect before launching present windows. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=d907d03453b8bf1b7b21f0e7ae7185be7095bbcb'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/347395'>#347395</a>
- Remove redundant proxy. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=ccc5076833ec27e8d94b323d86e87d16c0082e49'>Commit.</a>
- Fix force stripes on vertical. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=463a0379af7342f6bd35de24812411d9ace5e9ae'>Commit.</a>
- Add FindEvdev cmake module. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=36fa06eac092c9dbf5470e45918954658fed267b'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/347749'>#347749</a>. Code review <a href='https://git.reviewboard.kde.org/r/123808'>#123808</a>
- KAStats: Properly resetting model when clear is called. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=09810d3df53b71cee923aedf7abe176f7b41b2ce'>Commit.</a>
- Fix reverse scroll in Mouse KCM. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=fdc60272e841e153e314bf52e49eaa791ebbbccb'>Commit.</a>
- Don't close when emptying Recent\* categories. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=140c3ab368c17234dd856765383464e664db7d50'>Commit.</a>
- Adjust margins in vertical panels as horizontal. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=262ecde12d4881f005c9be8fc08aae4a49ba8c71'>Commit.</a>
- Don't add menu actions twice. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=b15e5aa990f1323747cb9bfb41e40e360f4d3610'>Commit.</a>
- Fix 'Add to Desktop' against the Folder containment being unreliable. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=b59ce5e838304213bcedf78f5d267b29d2874c4f'>Commit.</a>
- SQLite supports offset only of limit is specified. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=b8c363200cf075cb54ffea794d2608737a06449a'>Commit.</a>
- Clean up some debug junk. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=6fd372c837950820247cd82188ff01ecfc0ae9a0'>Commit.</a>
- Keeping ResultSet open as little as possible and fixing the count limit. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=b05b5dcdbf135a54066ba3a75f636cdebf0b89c7'>Commit.</a>
- Grab on the right item; fixes regression from 1c38100f while retaining the crash fix. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=0ecf407d87644d648d25c3711d889e00492d2fe5'>Commit.</a>
- Fix missing signal connect + some speed optimizations for KAMD submenu reveals. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=b58c0b32fad13f7a183bb2473323849a5c12e99a'>Commit.</a>
- Fix race condition between PlacesModel and deriving URL from UI state. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=17bf132e958295a00cc956c28b6f379a82fa7a81'>Commit.</a>
- Use TextMetrics for lower bound. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=e55a39707031abb2f1a3feb920a5060627ea6c55'>Commit.</a>
- Ignore press events on scrollbars and make sure smooth scroll is disabled when not autoscrolling. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=5beb7d78fcb2bbce62bd862b5ef87b7ecfc3babb'>Commit.</a>
- Improve visibility of running widget checkmark. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=3347ebe87d6c8706873d65de4fc45ac3c21c0b10'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/123539'>#123539</a>. Fixes bug <a href='https://bugs.kde.org/342112'>#342112</a>
- KActivitiesStats: Obey the item count limit for the ResultModel. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=45e3a86791721beff69e68bd35f9f524b492432a'>Commit.</a>
- Remove use of KCoreAddons.Format; KDirModel now returns preformatted data. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=f7908fa7f090d46f0811589ecfb0643faee01d6b'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/346567'>#346567</a>

### <a name='plasma-mediacenter' href='http://quickgit.kde.org/?p=plasma-mediacenter.git'>Plasma Media Center</a>

- Use correct header for used class. <a href='http://quickgit.kde.org/?p=plasma-mediacenter.git&amp;a=commit&amp;h=0e029d1d377f2873d58d28414671722e23a8ad9c'>Commit.</a>

### <a name='plasma-nm' href='http://quickgit.kde.org/?p=plasma-nm.git'>Plasma Networkmanager (plasma-nm)</a>

- Fix typo in END_TLS_AUTH_TAG. <a href='http://quickgit.kde.org/?p=plasma-nm.git&amp;a=commit&amp;h=01a5893213c70ef7bb72d01f35f91d16b6bb03df'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/347416'>#347416</a>
- Drop WiMAX support for NM 1.2.0+. <a href='http://quickgit.kde.org/?p=plasma-nm.git&amp;a=commit&amp;h=913379a6bd89952a28ff5f88b14f31eb61325a5d'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/123657'>#123657</a>
- Editor: request secrets when "secretkey-flags" is not present in setting. <a href='http://quickgit.kde.org/?p=plasma-nm.git&amp;a=commit&amp;h=dd84e5ba772b726e8bc05a88220481645cc52331'>Commit.</a>
- Add option to show/hide menu bar. <a href='http://quickgit.kde.org/?p=plasma-nm.git&amp;a=commit&amp;h=2e831dccc650b0b33b212f9938f06da57f564328'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/347141'>#347141</a>

### <a name='plasma-workspace' href='http://quickgit.kde.org/?p=plasma-workspace.git'>Plasma Workspace</a>

- [libtaskmanager] Use windowClass from KWindowInfo instead fetching from X each time. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=177881536344d427065851ed485ba0e3516a3dad'>Commit.</a> See bug <a href='https://bugs.kde.org/340583'>#340583</a>
- Adjust the one pixel gap for right aligned panels. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=a36772ea282730b5e2031a2fb88d2d18b1863ff2'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/347873'>#347873</a>
- Process updates scripts even after we load the default layout. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=c2efc8ded52633db16926c0bc52b58b0593c18ff'>Commit.</a>
- Enable translations for devicenotifications dataengine. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=b174a3c92601853bc499d9c2dc62fd8b440cbba8'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/123846'>#123846</a>
- Add missing TRANSLATION_DOMAIN for dataengines keystate, network, rss, weather. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=205f04ca2d0c71cdd4443618cd097e1514280b89'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/123854'>#123854</a>
- Default to desktop sorting. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=a9a35ac2778f737214493435d6f81223c401f8d4'>Commit.</a>
- ++paranoia;. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=ceec66c03626e84eccda26a6d16c17fa50b3577c'>Commit.</a>
- [notifications] Make notifications work properly with --reverse. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=7253c12a2b089176d0354c6a3a88b536befb5653'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/122648'>#122648</a>. Fixes bug <a href='https://bugs.kde.org/343251'>#343251</a>
- [notifications] Clip the NotificationItem to prevent painting outside of its rect. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=29e5f42ba1701a694a731ab3fafef907d561fa4b'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/346419'>#346419</a>
- [digital-clock] Add timezone filtering by region too. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=72041b7c90c3f4e1d1ec6812813dee84952f3f89'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/123740'>#123740</a>. Fixes bug <a href='https://bugs.kde.org/346681'>#346681</a>
- [klipper] Ensure global shortcut actions work. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=56a5947e3707c104fe9170a51bdda308deee4d13'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/345945'>#345945</a>. Code review <a href='https://git.reviewboard.kde.org/r/123727'>#123727</a>
- Guard access to kscreen configuration. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=8e12bd4bdee86fabc3e2985922aae73916cef700'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/346590'>#346590</a>. Code review <a href='https://git.reviewboard.kde.org/r/123648'>#123648</a>CHANGELOGCHANGELOG:Fix crash on Plasma startup caused by a race condition
- [digital-clock] Use KCMShell.open for the formats KCM. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=e6d82aaaa2c30b445e0ccc3a5fa6e124e119dff0'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/347032'>#347032</a>
- Make ConfigFile() reuse the Corona's KSharedConfigPtr when its config file is requested. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=aed980823f6db0497381215a386f8ba4fa273808'>Commit.</a>
- Fix second arg not being used when parent is a ConfigGroup. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=0658de726a5c47f908baff2cb2a3bc335f0c15d3'>Commit.</a>
- [digital-clock] Fix missing function return. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=bbac56c554c7b5569afc39556b41d95bc6b4b255'>Commit.</a>
- [digital-clock] Change the id of the root item. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=ff33964a8ad6b802f8a31dc720f0f35c57f8a86e'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/346841'>#346841</a>
- When switching activities, use only the running ones. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=1e460f3158eb4cf2eb515cb60f98e6370915876a'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/347029'>#347029</a>. Code review <a href='https://git.reviewboard.kde.org/r/123597'>#123597</a>
- [notifications] Clear notification from show queue if it's closed before it's shown. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=af3081abe2337c691cd2d310951ffd20f21da00d'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/342605'>#342605</a>
- [notifications] Always check first if the dispatch timer isn't running already. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=83ec35b632ee495df4b3c6f33f7ca33bca398a8f'>Commit.</a> See bug <a href='https://bugs.kde.org/342605'>#342605</a>
- Fix a crash in PowermanagementEngine::populateApplicationData when the given name is empty. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=08cbba07eb8927ce3a0f864eda8dfe1f8f1a6e44'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/123555'>#123555</a>
- Check for model existence. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=a234c0923767649cc6545866a563ffc4dba58de2'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/346870'>#346870</a>
- Only restart Timer when the dialog is visible. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=710837e3f82e1dc9e3a12f1dcb285c831ae5f74b'>Commit.</a>
- Fix running applet calculation. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=2b7d3c4bc5b126111fec6b3ff3046b16541329a6'>Commit.</a>
- Manually keep track of jobs sources. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=bc5c47537f3bbb706b3fe7af66508f5ef2fadc6e'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/123502'>#123502</a>. Fixes bug <a href='https://bugs.kde.org/346673'>#346673</a>
- Match window switch dialog borders with addwidgets/switch activity. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=f0d0518a70417b35124c0089325b32a6e6bf8f5e'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/345614'>#345614</a>. Code review <a href='https://git.reviewboard.kde.org/r/123506'>#123506</a>
- [notifications] Replace &amp;apos; with ' as &amp;apos; is not supported by StyledText. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=d95c4b850b03776c95efd04baad8c92864c42a3d'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/346710'>#346710</a>
- Disable indeterminate animation for suspended jobs. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=a3b79c165ae69abb9b390a5c657fc1db483a1e0d'>Commit.</a>
- Fix last commit. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=ec22ae8b14c3ccc996068124bedb20ec455076ad'>Commit.</a>
- Fix logic in argument check in plasma-windowed. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=c1fdbe229b85afb8961fae8b87f57a9861d77505'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/346311'>#346311</a>

### <a name='powerdevil' href='http://quickgit.kde.org/?p=powerdevil.git'>Powerdevil</a>

- Consider overall percentage when emitting battery warning on startup. <a href='http://quickgit.kde.org/?p=powerdevil.git&amp;a=commit&amp;h=d3d2ebcfd214a892ac79484cfb8cb6fcbf7fc917'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/123704'>#123704</a>. Fixes bug <a href='https://bugs.kde.org/347470'>#347470</a>