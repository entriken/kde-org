<ul>
<!--
       <li>
       An alpha version of KDE4-based <strong>Arklinux 2008.1</strong> is expected
       shortly after this release, with an expected final release within 3 or 4 weeks.
    </li>
-->

       <li>
       <strong>Debian</strong> KDE 4.2.4 packages are available in the unstable repository.
       </li>

       <li>
       <strong>Fedora</strong> KDE 4.2.4 packages are available:
       <ul>
         <li><a href="https://admin.fedoraproject.org/updates/F11/FEDORA-2009-6481">Fedora 11</a></li>
         <li><a href="https://admin.fedoraproject.org/updates/F10/FEDORA-2009-6483">Fedora 10</a></li>
         <li><a href="https://admin.fedoraproject.org/updates/F9/FEDORA-2009-6142">Fedora 9</a></li>
       </ul>
       </li>

<!--
       <li>
       <strong>Gentoo Linux</strong> provides KDE 4.0.1 builds on
       <a href="http://kde.gentoo.org">http://kde.gentoo.org</a>.
       </li>
-->

       <li>
                <strong>Kubuntu</strong> packages are available for 9.04.
                More details can be found in  the <a href="http://kubuntu.org/news/kde-4.2.4">
                announcement on Kubuntu.org</a>.
        </li>


        <li>
                <strong>Mandriva</strong> already have packages available for 2009.0 and
				2009.1 in their official testing repository. Packages are in
				process of QA testing to become official updates.
                <br />
					 Please edit your medias to enable Main/Testing in drakrpm
					 to enable KDE 4.2.4 upgrade.
        </li>
        <li>
                <strong>openSUSE</strong> packages <a href="http://en.opensuse.org/KDE/KDE4">are available</a> 
                for openSUSE 11.1
                (<a href="http://download.opensuse.org/repositories/KDE:/42/openSUSE_11.1/KDE4-BASIS.ymp">one-click 
                install</a>),
                for openSUSE 11.0
                (<a href="http://download.opensuse.org/repositories/KDE:/42/openSUSE_11.0/KDE4-BASIS.ymp">one-click 
                install</a>) and
                for openSUSE 10.3 
                (<a href="http://download.opensuse.org/repositories/KDE:/42/openSUSE_10.3/KDE4-BASIS.ymp">one-click 
                install</a>).
<!-- and	
		openSUSE Factory 
                (<a href="http://download.opensuse.org/repositories/KDE:/KDE4:/Factory:/Desktop/openSUSE_Factory/KDE4-BASIS.ymp">one-click 
                install</a>).
-->
                A <a href="http://home.kde.org/~binner/kde-four-live/">KDE 
                Four Live CD</a> with these packages is also available.
        </li>
<!--
        <li>
                <strong>Pardus</strong> KDE 4.2 Beta 2 packages are available for Pardus 2008 in <a href="http://paketler.pardus.org.tr/pardus-2008-test/">Pardus test repository</a>. Also
                all source PiSi packages are in <a href="http://svn.pardus.org.tr/pardus/devel/desktop/kde4/base/">Pardus SVN</a>.
        </li>
-->

<!--
        <li>
                <strong>Magic Linux</strong> KDE 4.2.4 packages are available for Magic Linux 2.5.
                See <a href="http://www.linuxfans.org/bbs/thread-182132-1-1.html">the release notes</a>
                for detailed information and
                <a href="http://apt.magiclinux.org/magic/2.5/unstable/RPMS.all/">the FTP tree</a> for
                packages.
        </li>
	-->

	<!--
        <li>
                <strong>Windows</strong> KDE 4.2.4 packages for Microsoft Windows.
                See <a href="http://windows.kde.org">windows.kde.org</a> for details. Download installer and get your kde apps on your windows desktop. Note: KDE on Windows is not in the final state, so many applications can be unsuitable for day to day use yet.
        </li>
	-->
</ul>
