---
aliases:
- ../../plasma-5.14.90
changelog: 5.14.5-5.14.90
date: 2019-01-17
layout: plasma
title: 'KDE Plasma 5.15 Beta: Lightweight, Usable and Productive.'
figure:
  src: /announcements/plasma/5/5.15.0/plasma-5.15-apps.png
---

{{% i18n_date %}}

{{% i18n_var "Today KDE launches the beta release of Plasma %[1]s." "5.15" %}}

For the first release of 2019, the Plasma team has embraced KDE's <a href="https://community.kde.org/Goals/Usability_&_Productivity">Usability &amp; Productivity goal</a>. We have teamed up with the VDG (Visual Design Group) contributors to get feedback on all the papercuts in our software that make your life less smooth, and fixed them to ensure an intuitive and consistent workflow for your daily use.

Plasma 5.15 brings a number of changes to our configuration interfaces, including more options for complex network configurations. Many icons have been added or redesigned. Our integration with third-party technologies like GTK and Firefox has been made even more complete. Discover, our software and add-on installer, has received a metric tonne of improvements to help you stay up-to-date and find the tools you need to get your tasks done.

Please test this beta release and send us bug reports and feedback. The final release will be available in three weeks' time.

## New in Plasma 5.15 Beta

### Plasma Widgets

{{<figure src="/announcements/plasma/5/5.15.0/bluetooth-battery.png" data-toggle="lightbox" alt="Bluetooth Battery Status" caption="Bluetooth Battery Status" width="600px" >}}

- Bluetooth devices now <a data-toggle='lightbox' href='/announcements/plasma/5/5.15.0/bluetooth-battery.png'>show their battery status in the power widget</a>. Note that this cutting-edge feature requires the latest versions of the upower and bluez packages.

- It’s now possible to download and install new <a data-toggle='lightbox' href='/announcements/plasma/5/5.15.0/wallpaper-plugins.png'>wallpaper plugins</a> straight from the wallpaper configuration dialog.

- Filenames on desktop icons now have enough <a data-toggle='lightbox' href='/announcements/plasma/5/5.15.0/desktop-icons2.png'>horizontal space to be legible</a> even when their icons are very tiny, and are <a data-toggle='lightbox' href='/announcements/plasma/5/5.15.0/desktop-icons.png'>easier to read</a> when the wallpaper is very light-colored or visually busy.

- Visually impaired users can now read the icons on the desktop thanks to the newly-implemented screen reader support for desktop icons.

- The Notes widget now has a <a data-toggle='lightbox' href='/announcements/plasma/5/5.15.0/notes-widget.png'>'Transparent with light text' theme</a>.

- It's now possible to configure whether scrolling over the virtual desktop Pager widget will “wrap around” when reaching the end of the virtual desktop list.

- The padding and appearance of <a data-toggle='lightbox' href='/announcements/plasma/5/5.15.0/notification-popup.png'>notification pop-ups</a> have been improved.

- KRunner has received several usability improvements. It now handles duplicates much better, no longer showing <a data-toggle='lightbox' href='/announcements/plasma/5/5.15.0/krunner-bookmarts.png'>duplicate bookmarks from Firefox</a> or duplicate entries when the same file is available in multiple categories. Additionally, the layout of the standalone search widget now matches KRunner's appearance.

- The Devices Notifier is now much smarter. When it's configured to display all disks instead of just removable ones, it will recognize when you try to unmount the root partition and prevent you from doing so.

### Settings

{{<figure src="/announcements/plasma/5/5.15.0/virtual-desktops.png" data-toggle="lightbox" alt="Redesigned Virtual Desktop Settings" caption="Redesigned Virtual Desktop Settings" width="600px" >}}

- System Settings Virtual Desktops page has been redesigned and rewritten for support on Wayland, and now sports greater usability and visual consistency.

- The user interface and layout for the <a data-toggle='lightbox' href='/announcements/plasma/5/5.15.0/systemsettings-digital-clock.png'>Digital Clock</a> and <a data-toggle='lightbox' href='/announcements/plasma/5/5.15.0/systemsettings-folderview-settings.png'>Folder View</a> settings pages have been improved to better match the common style.

- Many <a data-toggle='lightbox' href='/announcements/plasma/5/5.15.0/systemsettings-font.png'>System Settings pages</a> have been tweaked with the goal of standardizing the icons, wording, and placement of the bottom buttons, most notably the “Get New [thing]…” buttons.

- New desktop effects freshly installed from store.kde.org now appear in the list on the System Settings Desktop Effects page.

- The native display resolution is now indicated with a star icon in the System Settings Displays page.

- The <a data-toggle='lightbox' href='/announcements/plasma/5/5.15.0/systemsettings-login.png'>System Settings Login Screen</a> page received plenty of visual improvements. The image preview of the default Breeze theme now reflects its current appearance, the background color of the preview matches the active color scheme, and the sizes and margins were adjusted to ensure that everything fits without being cut off.

- The System Settings Desktop Effects page has been ported to QtQuickControls 2. This fixes a number of issues such as bad fractional scaling appearance, ugly dropdown menu checkboxes, and the window size being too small when opened as a standalone app.

### Cross-Platform Integration

{{<figure src="/announcements/plasma/5/5.15.0/firefox-portals.png" data-toggle="lightbox" alt="Firefox with native KDE open/save dialogs" caption="Firefox with native KDE open/save dialogs" width="600px" >}}

- Firefox 64 can now optionally use native KDE open/save dialogs. This is an optional, bleeding-edge functionality that is not yet included in any distribution. However, it can be enabled by installing the <tt>xdg-desktop-portal</tt> and <tt>xdg-desktop-portal-kde</tt> packages and setting <tt>GTK_USE_PORTAL=1</tt> in Firefox's .desktop file.

- Integration modules <tt>xdg-desktop-portal-kde</tt> and <tt>plasma-integration</tt> now support the Settings portal. This allows sandboxed Flatpak and Snap applications to respect your Plasma configuration - including fonts, icons, widget themes, and color schemes - without requiring read permissions to the kdeglobals configuration file.

- The global scale factor used by high-DPI screens is now respected by GTK and GNOME apps when it’s an integer.

- A wide variety of issues with the Breeze-GTK theme have been resolved, including the inconsistencies between the light and dark variants. We have also made the theme more maintainable, so future improvements will be much easier.

### Discover

{{<figure src="/announcements/plasma/5/5.15.0/discover-release-upgrade.png" data-toggle="lightbox" alt="Distro Release Upgrade Notification" caption="Distro Release Upgrade Notification" width="600px" >}}

- Options for upgrading your distribution are now included in Discover’s Update Notifier widget. The widget will also display a “Restart” button if a restart is recommended after applying all updates, but the user hasn’t actually restarted yet.

- On Discover’s Updates page, it’s now possible to uncheck and re-check all available updates to make it easier to pick and choose the ones you want to apply.

- <a data-toggle='lightbox' href='/announcements/plasma/5/5.15.0/discover-settings.png' data-toggle='lightbox'>Discover’s Settings page</a> has been renamed to “Sources” and now has pushbuttons instead of hamburger menus.

- Distribution repository management in Discover is now more practical and usable, especially when it comes to Ubuntu-based distros.

- Discover now supports app extensions offered with Flatpak packages, and lets you choose which ones to install.

- Handling for local packages has been improved, so Discover can now indicate the dependencies and will show a 'Launch' button after installation.

- When performing a search from the Featured page, Discover now only returns apps in the search results. Add-ons will appear in search results only when a search is initiated from an add-on category.

- Discover’s search on the Installed Apps page now works properly when the Snap backend is installed.

- Handling and presentation of errors arising from misconfigured add-on repos has also been improved.

- Discover now respects your locale preferences when displaying dates and times.

- The “What’s New” section is no longer displayed on app pages when it doesn't contain any relevant information.

- App and Plasma add-ons are now listed in a separate category on Discover’s Updates page.

### Window Management

- The Alt+Tab window switcher now supports screen readers for improved accessibility, and allows you to use the keyboard to switch between items.

- The KWin window manager no longer crashes when a window is minimized via a script.

- Window closing effects are now applied to dialog boxes with a parent window (e.g. an app’s Settings window, or an open/save dialog).

- Plasma configuration windows now raise themselves to the front when they get focus.

### Wayland

- More work has been done on the foundations - the protocols XdgStable, XdgPopups and XdgDecoration are now fully implemented.

- Wayland now supports virtual desktops, and they work in a more fine-grained way than on X11. Users can place a window on any subset of virtual desktops, rather than just on one or all of them.

- Touch drag-and-drop is now supported in Wayland.

### Network Management

{{<figure src="/announcements/plasma/5/5.15.0/wireguard.png" data-toggle="lightbox" alt="WireGuard VPN Tunnels" caption="WireGuard VPN Tunnels" width="600px" >}}

- Plasma now offers support for WireGuard VPN tunnels when the appropriate Network Manager plugin is installed.

- It’s now possible to mark a network connection as “metered”.

### Breeze Icons

Breeze Icons are released with <a href='https://www.kde.org/products/frameworks/'>KDE Frameworks</a> but are extensively used throughout Plasma, so here's a highlight of some of the improvements made over the last three months.

{{<figure src="/announcements/plasma/5/5.15.0/breeze-emblems.png" data-toggle="lightbox" alt="Icon Emblems in Breeze" caption="Icon Emblems in Breeze" width="600px" >}}

- A variety of <a data-toggle='lightbox' href='/announcements/plasma/5/5.15.0/breeze-device.png'>Breeze device and preference icons</a> have been improved, including the multimedia icons and all icons that depict a stylized version of a Plasma wallpaper.
- <a data-toggle='lightbox' href='/announcements/plasma/5/5.15.0/breeze-emblems2.png'>The Breeze emblem</a> and <a data-toggle='lightbox' href='/announcements/plasma/5/5.15.0/breeze-package.png'>package</a> icons have been entirely redesigned, resulting in a better and more consistent visual style, plus better contrast against the icon they’re drawn on top of.

- In new installs, the <a data-toggle='lightbox' href='/announcements/plasma/5/5.15.0/breeze-places.png'>Places panel</a> now displays a better icon for the Network place.

- The <a data-toggle='lightbox' href='/announcements/plasma/5/5.15.0/breeze-vault.png'>Plasma Vault icon</a> now looks much better when using the Breeze Dark theme.

- <a data-toggle='lightbox' href='/announcements/plasma/5/5.15.0/breeze-python.png'>Python bytecode files</a> now get their own icons.

### Other

{{<figure src="/announcements/plasma/5/5.15.0/ksysguard.png" data-toggle="lightbox" alt="KSysGuard’s optional menu bar" caption="KSysGuard’s optional menu bar" width="600px" >}}

- It’s now possible to hide KSysGuard’s menu bar — and it reminds you how to get it back, just like Kate and Gwenview do.

- The <tt>plasma-workspace-wallpapers</tt> package now includes some of the best recent Plasma wallpapers.
