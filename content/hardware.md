---
title: Buy devices with Plasma and KDE Applications
subtitle: "Here you can find a list of devices with KDE Plasma pre-installed that you can buy right now:"
layout: free
sassFiles:
  - /scss/hardware.scss
steamDeck:
  title: Steam Deck
  subtitle: A portable console that runs AAA games
  description: |
    Play your entire gaming library with this portable console that has the flexibility of a PC.
    The Steam Deck runs KDE Plasma optimized to play the latest AAA games and your
    favorite emulators. You can even connect a screen, keyboard, and mouse for a full
    Plasma-based desktop PC experience.
  specification: Specifications
  operating: "Operating system pre-installed:"
  operating_desc: SteamOS 3.0 (Arch Linux based)
  cpu: "CPU:"
  cpu_desc: Custom AMD Ryzen quad-core 3.5GHz CPU
  gpu: "GPU:"
  gpu_desc: Custom 1.6GHz GPU with 8 RDNA 2 Compute Units
  memory: "Memory:"
  memory_desc: "16 GB RAM (LPDDR5)"
  display: "Display:"
  display_desc: 1200 x 800 7” IPS LCD
  storage: "Storage:"
  storage_desc: "Up to 512 GB SSD"
  price: "Price:"
  price_desc: starting at US$399
pinephone:
  title: PinePhone KDE Community Edition
  subtitle: Experience the future of KDE’s open mobile platform
  description: |
    Plasma Mobile and PinePhone provide you with the excitement of experimenting with
    the future of Free and Open Source mobile phones right now.

    The Pinephone is a tinkerer's dream, letting you boot new operating systems with
    ease and tweak, expand, and update the hardware. Plasma Mobile provides you with
    a feature-rich environment with an already well-populated app ecosystem ready for
    convergence. Experiment with software that improves on a daily basis, use calendars,
    sync up with your desktop and other devices using KDE Connect, browse the web,
    read documents, play games, send Instant Messages, and so much more.
  specification: |
    As a regular user, get this phone if you want to experience first hand what the
    future of FLOSS mobile phones will be like. Test it, give us feedback and help
    us push Plasma Mobile to maturity!

    As a developer, get this phone if you want to contribute to Plasma Mobile, one
    of the most active, community-driven projects for mobile phones. It will also
    give you a headstart in creating apps for a budding system that is on the verge
    of taking the mobile arena by storm.
  learn: Learn more about the PinePhone
  alt: Picture of the PinePhone with Plasma Mobile
slimbook:
  title: KDE Slimbook
  subtitle: Powerful Hardware, Sleek Software
  description: |
    The Linux ultrabook with a Ryzen 5700U processor and KDE's full-featured Plasma
    desktop running on KDE Neon and with access to hundreds of Open Source programs and utilities.
  alt: Picture of the two slimbook models
  choose: Choose your size
  slim14: KDE Slimbook 14”
  amd: AMD Ryzen 7 5700 U
  screen14: 14-inch IPS LED display with 1920 by 1080 resolution at 60Hz
  ssd: Up to 2TB storage SSD
  ram: Up to 64GB memory
  watt: 47‑watt‑hour battery
  kg: 1 kg
  slim16: KDE Slimbook 15.6”
  screen16: 15.6-inch IPS LED display with 1920 by 1080 resolution at 60Hz
  watt16: 92‑watt‑hour battery
  kg16: 1.5 kg
  configure: Configure
  full: See the full comparison
kfocus:
  title: Kubuntu Focus Systems
  subtitle: The KDE Linux Systems You've Always Wanted
  description: |
    Officially branded Kubuntu systems
    designed to work out-of-the-box, featuring the beautiful and
    intuitive KDE desktop with industry-standard Ubuntu® 22.04 LTS.
  alt: Kubuntu Focus Systems
  img_url: "https://kfocus.org/img/hosted/kfocus-systems-med-80p.jpg"
  label1: Focus XE
  body1: |
    A high-performance, ultra-mobile 14.0" laptop. Perfect for coding,
    content creation, and devops wherever you go.
  price1: From $845
  label2: Focus M2
  body2: |
    The ultimate trim-and-light 15.6" workstation. Perfect for deep
    learning, rendering, simulations, development, and AAA games.
  price2: From $1895
  label3: Focus NX
  body3: | 
    Big capability in an amazingly compact 4.5" package. Perfect for
    coding, content creation, and devops from your desk, couch, or a nook.
  price3: From $695
  learn_label: Learn More
  learn_url: "https://kfocus.org/"
pinebook:
  title: Pinebook Pro
  description: |
    The Pinebook Pro is meant to deliver solid day-to-day Linux or
    *BSD experience and to be a compelling alternative to mid-ranged
    Chromebooks that people convert into Linux laptops. In contrast to
    most mid-ranged Chromebooks however, the Pinebook Pro comes with
    an IPS 1080p 14″ LCD panel, a premium magnesium alloy shell,
    64/128GB of eMMC storage, a 10,000 mAh capacity battery and the
    modularity / hackability that only an open source project can
    deliver – such as the unpopulated PCIe m.2 NVMe slot. The USB-C
    port on the Pinebook Pro, apart from being able to transmit data
    and charge the unit, is also capable of digital video output
    up-to 4K at 60hz.
  alt: Pinebook pro picture
  specification: Specifications
  operating: "Operating system pre-installed:"
  cpu: "CPU:"
  gpu: "GPU:"
  memory: "Memory:"
  display: "Display:"
  display_desc: 1080p IPS Panel
  storage: "Storage:"
  storage_desc: "64GB of eMMC (Upgradable)"
  price: "Price:"
  price_desc: starting at US$199
hardware_option: Hardware sellers and services with KDE Plasma as an option
hardware_desc: "You can find here a list of hardware sellers and services offering Plasma as an option:"
layout: hardware
---

{{< diagonal-box color="green" href="https://www.shells.com/pricing?special=kde&_a=kdeorg&utm_source=web&utm_medium=lp&utm_campaign=kde-org-hardware" title="Shells" src="/content/hardware/shells-neon.png" >}}

Sign up for KDE Neon on Shells and access all the power and security of KDE's Plasma desktop from anywhere.

Shells lets you transform any device into a powerful, secure Plasma machine, allowing you to work, code, or play on your laptop, smart TV or phone. Unlock the full potential of KDE's full range of software on any device with Shells virtual desktop in the cloud.

<a href="https://www.shells.com/pricing?special=kde&_a=kdeorg&utm_source=web&utm_medium=lp&utm_campaign=kde-org-hardware"><img src="/content/hardware/shells.png" /></a>

<small>Shell makes a donation to KDE when you use this link.</small>

{{< /diagonal-box >}}

{{< diagonal-box color="blue" href="https://slimbook.es/en" title="Slimbook" src="/content/hardware/slimbook.jpg" >}}

SLIMBOOK was born in 2015 with the idea of being the best brand in the computer
market with GNU / Linux (although it is also verified the absolute compatibility
with Microsoft Windows). They assemble computers searching for excellent quality
and warranty service, at a reasonable price. So much so, that in 2018 they were
awarded Best Open Source Service / Solution Provider at the OpenExpo Europe 2018.

{{< /diagonal-box >}}

{{< diagonal-box color="yellow" href="https://starlabs.systems/" title="Star Labs" src="/content/hardware/starlabs.png" >}}

Star Labs offers a range of laptops designed and built specifically for Linux.
Two version of their laptop are offered: the Star Lite Mk III 11-inch and the 
Star LabTop Mk IV 13-inch.

{{< /diagonal-box >}}

{{< diagonal-box color="green" href="https://www.tuxedocomputers.com/en" title="Tuxedo" src="/content/hardware/Tuxedo.png" >}}

Tuxedo builds tailor-made hardware and all this with Linux!

TUXEDO Computers are individually built computers/PCs and notebooks which are fully Linux compatible, i. e. Linux hardware in tailor-made suit.

All TUXEDO devices are delivered in such a way that you only have to unpack, connect and switch them on. All the computers and notebooks are assembled and installed in-house, not outsourced.

They provide you with self-programmed driver packages, support, installation scripts and everything around our hardware, so that every hardware component really works.

{{< /diagonal-box >}}
