KDE Project Security Advisory
=============================

Title:           KTextEditor/Kate: Missing validation of binaries executed via QProcess
Risk Rating:     High
CVE:             CVE-2022-23853
Versions:        KTextEditor <= 5.90.0, Kate <= 21.12.1
Author:          Christoph Cullmann <cullmann@kde.org>
Date:            31 January 2022

Overview
========

KTextEditor and Kate execute binaries without user interaction in a few cases.

Some examples are:

- KTextEditor will try to check on external file modification via invoking the "git" binary if the file is known in the repository with the new content.
- Kate will execute the LSP (language server protocol) server associated with the mime type of the files you open if the LSP plugin is active
- Kate will execute "git", "svn" and other version control tools on document open to retrieve project information if the project plugin is active

KTextEditor & Kate use QProcess for this process invocation.

The typical pattern is like:

QProcess p;
p.setWorkingDirectory(xxx); // in some cases
p.start("binaryname", args);

As the binary name is in most cases passed as a non-absolute executable name, QProcess will execute a "binaryname" named executable
from the working directory (implicit that of the application or explicit that one set by setWorkingDirectory), if no matching executable is found
in the normal search path (PATH).

This allows attackers to put malicious executables with the right name for example in the document directory.
If the wanted executable is not in the search path, the malicious one will be executed.

Impact
======

Opening files might lead to the execution of malicious binaries if they are placed in the right directories.

Workaround
==========

KTextEditor: ensure git is in your PATH
Kate: disable LSP & project plugin

Solution
========

Update to KTextEditor 5.91.0 or Kate 21.12.2.

KTextEditor patches:

https://commits.kde.org/ktexteditor/804e49444c093fe58ec0df2ab436565e50dc147e
https://commits.kde.org/ktexteditor/c80f935c345de2e2fb10635202800839ca9697bf

Kate patches:

https://commits.kde.org/kate/c5d66f3b70ae4778d6162564309aee95f643e7c9 [1]
https://commits.kde.org/kate/7e08a58fb50d28ba96aedd5f5cd79a9479b4a0ad

[1] Not related to this issue these patches are also needed so that one applies
https://commits.kde.org/kate/361dd43e42994829dbdb35e78fb7698d27cbb0e2
https://commits.kde.org/kate/6fc3bf6e5bd540e842e32c4a959c2158c8573be5
https://commits.kde.org/kate/92a9c65e30b4b63b8b116eb5c8dcb1e1a2d867bc

Credits
=======

Thanks to Fabian Vogt for reporting the issue.
Thanks to Christoph Cullmann for fixing the issue.
Thanks to Albert Astals Cid, Fabian Vogt, Mark Nauwelaerts and Waqar Ahmed for helping with the reviews and advisory.
