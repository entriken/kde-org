---
aliases:
- ../announce-frameworks5-beta3
date: '2014-06-05'
description: KDE Ships Third Beta of Frameworks 5.
release: 4.100.0
title: KDE Ships Third Beta of Frameworks 5
---

{{<figure class="text-center img-size-medium" src="/announcements/frameworks/5-tp/KDE_QT.jpg" caption="Collaboration between Qt and KDE" >}}

June 5, 2014. Today KDE makes available the third beta of Frameworks 5. This release is part of a series of releases leading up to the final version planned for July 2014.

### Frameworks 5 Beta 3

This beta features multiple bug fixes, and the finishing touches required to ease the transition for developers to the newest iteration of the KDE Frameworks. This process has included contributions back to Qt5, the modularisation of the kdelibs, and general improvements to the components that developers can use to improve their applications and user experience. This pre-release improves co-installability with kdelibs4 and with future versions of KDE Frameworks (i.e. 6). This is also the first release with translations for Frameworks using the KDE's i18n translation system.
<br /><br />

For information about Frameworks 5, see <a href='http://dot.kde.org/2013/09/25/frameworks-5'>this article on the dot news site</a>. Those interested in following progress can check out the <a href='https://projects.kde.org/projects/frameworks'>git repositories</a>, follow the discussions on the <a href='https://mail.kde.org/mailman/listinfo/kde-frameworks-devel'>KDE Frameworks Development mailing list</a> and contribute patches through <a href='https://git.reviewboard.kde.org/groups/kdeframeworks/'>review board</a>. Policies and the current state of the project and plans are available at the <a href='http://community.kde.org/Frameworks'>Frameworks wiki</a>. Real-time discussions take place on the <a href='irc://#kde-devel@freenode.net'>#kde-devel IRC channel on freenode.net</a>.

## Discuss, Spread the Word and See What's Happening: Tag as &quot;KDE&quot;

KDE encourages people to spread the word on the Social Web. Submit stories to news sites, use channels like delicious, digg, reddit, twitter, identi.ca. Upload screenshots to services like Facebook, Flickr, ipernity and Picasa, and post them to appropriate groups. Create screencasts and upload them to YouTube, Blip.tv, and Vimeo. Please tag posts and uploaded materials with &quot;KDE&quot;. This makes them easy to find, and gives the KDE Promo Team a way to analyze coverage for these releases of KDE software.

You can discuss this news story <a href='http://dot.kde.org/2014/06/05/kde-releases-3rd-beta-frameworks-5'>on the Dot</a>, KDE's news site.

#### Installing frameworks Beta 3 Binary Packages

<em>Packages</em>.
A variety of distributions offers frequently updated packages of Frameworks 5. This includes Arch Linux, AOSC, Fedora, Kubuntu and openSUSE. See <a href='http://community.kde.org/Frameworks/Binary_Packages'>this wikipage</a> for an overview.

#### Compiling frameworks

{{% i18n_var "The complete source code for frameworks %[1]s may be <a href='http://download.kde.org/unstable/frameworks/%[1]s/'>freely downloaded</a>." "4.100.0" %}}

#### {{< i18n "supporting-kde" >}}

{{% i18n "whatiskde" %}}
