<ul>

<!-- SLACKWARE LINUX -->
<li>
  <a href="http://www.slackware.org/">Slackware</a> (Unofficial contribution)
 (<a href="http://download.kde.org/binarydownload.html?url=/unstable/3.3.91/contrib/Slackware/10.0/README">README</a>)
   :
   <ul type="disc">
     <li>
       10.0: <a href="http://download.kde.org/binarydownload.html?url=/unstable/3.3.91/contrib/Slackware/10.0/">Intel i486</a>
     </li>
   </ul>
  <p />
</li>

<!--   SUSE LINUX -->
<li>
  <a href="http://www.novell.com/linux/suse/">SUSE Linux</a>
  (<a href="http://download.kde.org/binarydownload.html?url=/unstable/3.3.91/SuSE/README">README</a>)
      :
  <ul type="disc">
    <li>
        <a href="http://download.kde.org/binarydownload.html?url=/unstable/3.3.91/SuSE/noarch/">Language
        packages</a> (all versions and architectures)
    </li>
    <li>
      9.2:
      <a href="http://download.kde.org/binarydownload.html?url=/unstable/3.3.91/SuSE/ix86/9.2/">Intel i586</a> and
      <a href="http://download.kde.org/binarydownload.html?url=/unstable/3.3.91/SuSE/x86_64/9.2/">AMD x86-64</a>
    </li>
    <li>
      9.1:
      <a href="http://download.kde.org/binarydownload.html?url=/unstable/3.3.91/SuSE/ix86/9.1/">Intel i586</a> and
      <a href="http://download.kde.org/binarydownload.html?url=/unstable/3.3.91/SuSE/x86_64/9.1/">AMD x86-64</a>
    </li>
    <li>
      9.0:
      <a href="http://download.kde.org/binarydownload.html?url=/unstable/3.3.91/SuSE/ix86/9.0/">Intel i586</a> and
      <a href="http://download.kde.org/binarydownload.html?url=/unstable/3.3.91/SuSE/x86_64/9.0/">AMD x86-64</a>
    </li>
    <li>
      8.2:
      <a href="http://download.kde.org/binarydownload.html?url=/unstable/3.3.91/SuSE/ix86/8.2/">Intel i586</a>
    </li>
  </ul>
  <p />
</li>

</ul>
