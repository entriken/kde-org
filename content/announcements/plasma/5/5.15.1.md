---
aliases:
- ../../plasma-5.15.1
changelog: 5.15.0-5.15.1
date: 2019-02-19
layout: plasma
figure:
  src: /announcements/plasma/5/5.15.0/plasma-5.15-apps.png
asBugfix: true
---

- Set parent on newly created fwupd resource. <a href="https://commits.kde.org/discover/baac08a40851699585e80b0a226c4fd683579a7b">Commit.</a> Fixes bug <a href="https://bugs.kde.org/402328">#402328</a>. Phabricator Code review <a href="https://phabricator.kde.org/D18946">D18946</a>
- Fix System Tray popup interactivity after changing item visibility. <a href="https://commits.kde.org/plasma-workspace/6fcf9a5e03ba573fd0bfe30125f4c739b196a989">Commit.</a> Fixes bug <a href="https://bugs.kde.org/393630">#393630</a>. Phabricator Code review <a href="https://phabricator.kde.org/D18805">D18805</a>
- [Digital Clock] Fix 24h tri-state button broken in port to QQC2. <a href="https://commits.kde.org/plasma-workspace/006c4f5f9ee8dfb3d95604a706d01b968c1e1c8a">Commit.</a> Fixes bug <a href="https://bugs.kde.org/404292">#404292</a>
