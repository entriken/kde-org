---
aliases:
  - ../../plasma-5.21.90
authors:
  - SPDX-FileCopyrightText: 2021 Nate Graham <nate@kde.org>
  - SPDX-FileCopyrightText: 2021 Paul Brown <paul.brown@kde.org>
SPDX-License-Identifier: CC-BY-4.0
changelog: 5.21.5-5.21.90
date: 2021-05-13
layout: plasma
title: 'Plasma 5.22 Beta: The Quest for Stability and Usability'
---

{{< laptop src="/announcements/plasma/5/5.22.0/plasma-5.22.png" caption="KDE Plasma 5.22 Beta" >}}

{{% i18n_date %}}

This is the Beta release of Plasma 5.22. To make sure that end-users have the best possible experience with Plasma 5.22, KDE is releasing today this test version of the software. We encourage the more adventurous to test-run it and report problems so that developers may iron out the wrinkles before the final release scheduled for the 8th of June.

Plasma 5.22 is gearing up to be a leap forward with regards to stability and usability. Developers have concentrated their efforts on ironing out hundreds of bugs and removing paper cuts, while also tweaking details. The aim of Plasma 5.22 is to allow users to become even more productive and enjoy a smoother experience when using KDE's Plasma desktop.

**DISCLAIMER:** This is beta software and is released for testing purposes. You are advised to NOT use Plasma 5.22 Beta in a production environment or as your daily desktop. If you do install Plasma 5.22 Beta, you must be prepared to encounter (and [report to the creators](https://bugs.kde.org/)) bugs that may interfere with your day-to-day use of your computer.

Read on to find out more about Plasma 5.22 Beta:

## General Plasma

* Plasma System Monitor replaces KSysguard as the default system monitoring app
* New adaptive panel transparency feature makes the panel and panel widgets more transparent, but turns off transparency entirely when there are any maximized windows. You can also make your panel always transparent or always opaque if you want.
* The new Kickoff no longer suffers from either an irritating delay before switching categories or accidentally switched categories when moving the cursor
* Support for activities on Wayland
* The Global Menu applet now lets you search through menu items on Wayland
* The Task Manager's "Highlight Windows" feature now only highlights windows when hovering over that window's thumbnail in the tooltip, and this behavior has been turned on by default
* Global shortcuts involving non-Latin symbols of non-US keyboards now work properly
* Hugely improved Wayland support in general
* You can now change the text size in sticky note widgets


## System Settings

* The app now opens to a new "Quick Settings" page that shows some of the most commonly/frequently changed settings, and offers a link to the place where you can change the wallpaper
* You can now disable offline updates if you're using a distro that has decided to enable them by default, or enable them if you're using a distro that hasn't
* Improved accessibility and keyboard navigability


## System Tray

* System Tray applets in general are much more consistent in appearance and usability
* The Digital Clock applet's popup has been re-designed for better aesthetics and usability
* You can now force the Digital Clock applet to display the date on the same line as the time even for tall panels
* You can now select the device profile for your audio devices right from the Audio Volume system Tray applet
* You can now call up your clipboard history at any time by pressing Meta+V


## Notifications

* Notifications about files (e.g downloaded files, moved files, etc) now show the app that will open the file if you trigger the "Open" action
* Download notifications now inform you when they've been blocked because you need to tell the browser to really start/continue with the download
* The notification system now automatically activates Do Not Disturb mode by default while you are sharing your screen or screencasting

## KRunner

* KRunner can now display multi-line text for search results, which makes dictionary definitions (e.g. type "define food") useful
* KRunner no longer returns duplicate results from across different runners (e.g. searching for "firefox" no longer offers you the option to launch the app as well as run the command-line `firefox` binary)

## KWin/graphics

* KWin now supports direct scan-out for fullscreen windows run on non-NVIDIA GPUs on Wayland, which improves performance
* KWin now supports variable refresh rate /FreeSync screens on Wayland
* KWin now supports hot-plugging GPUs on Wayland
* KWin now supports setting screens' overscan values on Wayland
* Vertical and horizontal maximization now work on Wayland
* The Present Windows effect is fully activatable on Wayland in all contexts
* On multi-screen setups, new windows now open on the screen where the mouse cursor is located by default
