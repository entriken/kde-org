import logging
import os
import sys
import urllib.request

from pathlib import Path


level = logging.INFO
logging.basicConfig(format='%(levelname)s: %(message)s', level=level)

urllib.request.urlretrieve('https://invent.kde.org/frameworks/kxmlgui/-/raw/master/src/kxmlgui.xsd', 'static/standards/kxmlgui/1.0/kxmlgui.xsd')
urllib.request.urlretrieve('https://invent.kde.org/frameworks/kconfig/-/raw/master/src/kconfig_compiler/kcfg.xsd', 'static/standards/kcfg/1.0/kcfg.xsd')


def is_branch_master():
    head_dir = Path(".") / ".git" / "HEAD"
    with head_dir.open("r") as f: content = f.read().splitlines()
    logging.info(content)
    head_rev = content[0]
    master_dir = Path('.') / '.git' / 'refs' / 'remotes' / 'origin' / 'master'
    with master_dir.open("r") as f: content = f.read().splitlines()
    logging.info(content)
    master_rev = content[0]
    return master_rev == head_rev

if not is_branch_master():
    sys.exit()

os.environ["PACKAGE"] = 'websites-kde-org'
os.system('git clone https://invent.kde.org/websites/hugo-i18n && pip3 install ./hugo-i18n')
os.system('hugoi18n compile po')  # compile translations in folder "po"
os.system('hugoi18n generate')

# We host copies of some pages on www.kde.org for historical reasons
# Using urllib.request results in 403 Forbidden error, so we use a subshell
os.system('curl "https://store.kde.org/content.rdf" -o static/kde-look-content.rdf')
os.system('curl "https://dot.kde.org/rss.xml" -o static/dot/rss.xml')
os.system('cp -f static/dot/rss.xml static/dotkdeorg.rdf')
