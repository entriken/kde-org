---
aliases:
- ../../plasma-5.11.2
changelog: 5.11.1-5.11.2
date: 2017-10-24
layout: plasma
youtube: nMFDrBIA0PM
figure:
  src: /announcements/plasma/5/5.11.0/plasma-5.11.png
  class: text-center mt-4
asBugfix: true
---

- Fix colours not updating in systemsettings. <a href="https://commits.kde.org/systemsettings/5f9243a8bb9f7dccc60fc1514a866095c22801b8">Commit.</a> Phabricator Code review <a href="https://phabricator.kde.org/D8399">D8399</a>
- Default X font DPI to 96 on wayland. <a href="https://commits.kde.org/plasma-desktop/fae658ae90bf855b391061a5332a1a964045e914">Commit.</a> Phabricator Code review <a href="https://phabricator.kde.org/8287">8287</a>
- Kcm baloo: Fix extraction of folder basename for error message. <a href="https://commits.kde.org/plasma-desktop/5a8691900fea2b77ae194f509d17e7368235b4c1">Commit.</a> Phabricator Code review <a href="https://phabricator.kde.org/D8325">D8325</a>
- Properly access the system's GTK settings. <a href="https://commits.kde.org/kde-gtk-config/efa8c4df5b567d382317bd6f375cd1763737ff95">Commit.</a> Fixes bug <a href="https://bugs.kde.org/382291">#382291</a>.
