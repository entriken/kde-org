2006-05-27 12:00 +0000 [r545366]  mueller

	* branches/KDE/3.5/kdegraphics/kpovmodeler/pmtreeview.cpp: assume
	  obj can't be null otherwise all of the rest of the method crashes
	  (various coverity reports)

2006-05-27 12:04 +0000 [r545373]  mueller

	* branches/KDE/3.5/kdegraphics/kpovmodeler/pmvariant.cpp: fix
	  uninitialized variable (CID 2276)

2006-05-28 14:30 +0000 [r545855]  pino

	* branches/KDE/3.5/kdegraphics/kpdf/ui/thumbnaillist.cpp: Keep (if
	  possible) the selected page really selected in the thumbnail view
	  when toggling a search. BUG: 122788

2006-05-29 20:51 +0000 [r546368]  deller

	* branches/KDE/3.5/kdegraphics/kfax/kfax.cpp: fix Bug 128235: kfax
	  crash: multipage TIFF --> zoom out --> hit "PgDown" to go to next
	  page BUG: 128235

2006-06-04 13:57 +0000 [r548026]  reitelbach

	* branches/KDE/3.5/kdegraphics/kcoloredit/Makefile.am: Make the
	  menu from KColorEdit translatable by adding rc.cpp to the
	  messages: target CCMAIL:kde-i18n-doc@kde.org

2006-06-04 15:54 +0000 [r548115]  bram

	* branches/KDE/3.5/kdegraphics/kcoloredit/Makefile.am: But first
	  create the rc.cpp itself. CCMAIL:tr@erdfunkstelle.de

2006-06-05 00:21 +0000 [r548249]  charles

	* branches/KDE/3.5/kdegraphics/kview/photobook/Makefile.am,
	  branches/KDE/3.5/kdegraphics/kview/photobook/photobook.h,
	  branches/KDE/3.5/kdegraphics/kview/photobook/photobook.cpp,
	  branches/KDE/3.5/kdegraphics/kview/photobook/photobookui.rc:
	  -don't empty the list of images when the dir changes -install
	  photobookui.rc into the right directory, causing the keyboard
	  shortcuts to work again So, all: If you make changes to
	  KDirLister and co., please test the changes with all its uses
	  Also, if you move ui.rc files around, you should test that their
	  new location is actually correct BUG: 106747

2006-06-05 18:37 +0000 [r548505]  mlaurent

	* branches/KDE/3.5/kdegraphics/kamera/kcontrol/kamera.desktop: Fix
	  bug #128701

2006-06-10 15:10 +0000 [r549993]  lueck

	* branches/KDE/3.5/kdegraphics/doc/kpdf/index.docbook,
	  branches/KDE/3.5/kdegraphics/doc/kview/index.docbook,
	  branches/KDE/3.5/kdegraphics/doc/kuickshow/index.docbook,
	  branches/KDE/3.5/kdegraphics/doc/kiconedit/index.docbook,
	  branches/KDE/3.5/kdegraphics/doc/kghostview/index.docbook:
	  documentation backport from trunk BUG:96384
	  CCMAIL:kde-doc-english@kde.org CCMAIL:kde-i18n-doc@kde.org

2006-06-10 22:23 +0000 [r550074]  aacid

	* branches/KDE/3.5/kdegraphics/kpdf/xpdf/xpdf/UGString.cc: When any
	  of the chars that we are passing to the UGooString is not
	  pdfencodable, do not encode the string, because we loose
	  information if we do, this fixes rendering of
	  http://publikationen.ub.uni-frankfurt.de/volltexte/2005/890/pdf/TR_abs_g.pdf
	  and other docs with type3 fonts and ligatures

2006-06-11 10:53 +0000 [r550271]  dang

	* branches/KDE/3.5/kdegraphics/kolourpaint/pixmapfx/kppixmapfx.h,
	  branches/KDE/3.5/kdegraphics/kolourpaint/pixmapfx/kppixmapfx.cpp:
	  Remove unused (and buggy) methods, fix comment in
	  kpPixmapFX::paintPixmapAt(). No functional changes but should
	  backport to 3.4, 3.3, 1.2_kde3.

2006-06-11 10:57 +0000 [r550272]  dang

	* branches/KDE/3.5/kdegraphics/kolourpaint/tools/kptoolresizescale.cpp:
	  Fix crash in unexecuteable code path caught by all static
	  analysers. Should really be a Q_ASSERT. No functional changes but
	  should backport to 3.4, 3.3, 1.2_kde3.

2006-06-11 11:45 +0000 [r550279]  dang

	* branches/KDE/3.5/kdegraphics/kolourpaint/NEWS,
	  branches/KDE/3.5/kdegraphics/kolourpaint/README,
	  branches/KDE/3.5/kdegraphics/kolourpaint/VERSION: up ver to
	  1.4.4_relight-pre

2006-06-11 16:14 +0000 [r550397]  aacid

	* branches/KDE/3.5/kdegraphics/kpdf/xpdf/xpdf/UGString.cc: [micro]
	  optimization by Mario Teijeiro Otero

2006-06-12 13:15 +0000 [r550618]  mueller

	* branches/KDE/3.5/kdegraphics/kfile-plugins/exr/configure.in.in:
	  remove duplicated configure check

2006-06-22 06:45 +0000 [r553793]  mueller

	* branches/KDE/3.5/kdegraphics/kamera/kioslave/kamera.cpp: fix
	  comparison with string literal

2006-06-27 09:17 +0000 [r555384]  lunakl

	* branches/KDE/3.5/kdegraphics/ksnapshot/regiongrabber.cpp:
	  WX11BypassWM for the region window, it's a special-purpose window
	  and shouldn't be managed by KWin at all.

2006-06-28 19:18 +0000 [r555943]  aacid

	* branches/KDE/3.5/kdegraphics/kpdf/xpdf/xpdf/DCTStream.cc,
	  branches/KDE/3.5/kdegraphics/kpdf/xpdf/xpdf/DCTStream.h: Reset
	  the jpeg structures on reset. BUGS: 129971 Fix will be on KDE
	  3.5.4

2006-06-30 09:19 +0000 [r556441-556440]  helio

	* branches/KDE/3.5/kdegraphics/kamera/kcontrol/kameradevice.cpp: -
	  Fix crash by testing for valid pointer

	* branches/KDE/3.5/kdegraphics/ksnapshot/ksnapshot.cpp: - Fix
	  increase number of next snapshots. Patch by Laurent Montel

2006-07-01 10:40 +0000 [r556704]  aacid

	* branches/KDE/3.5/kdegraphics/kpdf/part.cpp: Make the watch
	  function work too when opening ps files through ps2pdf BUGS:
	  130000 Ei, that's a nice bug number :-D

2006-07-02 15:15 +0000 [r557163]  pino

	* branches/KDE/3.5/kdegraphics/kdvi/infodialog.cpp: Missing i18n.
	  CCMAIL: kde-i18n-doc@kde.org

2006-07-02 18:07 +0000 [r557211]  aacid

	* branches/KDE/3.5/kdegraphics/kpdf/ui/pageview.cpp,
	  branches/KDE/3.5/kdegraphics/kpdf/part.cpp: Make space scroll by
	  viewport-page, not by document-page Patch by Leo Savernik BUGS:
	  105211

2006-07-03 16:53 +0000 [r557625]  whuss

	* branches/KDE/3.5/kdegraphics/kviewshell/renderedDocumentPage.cpp:
	  Backport of commit 557594: Make the selection of multicolumn text
	  work properly.

2006-07-05 15:33 +0000 [r558573]  lueck

	* branches/KDE/3.5/kdegraphics/doc/ksnapshot/preview.png,
	  branches/KDE/3.5/kdegraphics/doc/ksnapshot/window.png: new
	  screenshots

2006-07-09 12:00 +0000 [r560147]  lueck

	* branches/KDE/3.5/kdegraphics/doc/ksnapshot/index.docbook:
	  documentation backport from trunk CCMAIL:kde-doc-english

2006-07-09 14:25 +0000 [r560199]  aacid

	* branches/KDE/3.5/kdegraphics/kpdf/core/document.cpp: Do not be
	  "too" intelligent and assume a Browse link means html. See how
	  having an example of things not working makes it much easier to
	  fix. BUGS: 122845

2006-07-09 14:31 +0000 [r560202-560201]  aacid

	* branches/KDE/3.5/kdegraphics/kpdf/core/document.cpp: ei you
	  sucker preserve the formatting!

	* branches/KDE/3.5/kdegraphics/kpdf/core/document.cpp: avoid
	  warning

2006-07-11 18:43 +0000 [r561026]  aacid

	* branches/KDE/3.5/kdegraphics/kpdf/core/document.h,
	  branches/KDE/3.5/kdegraphics/kpdf/part.h,
	  branches/KDE/3.5/kdegraphics/kpdf/core/document.cpp,
	  branches/KDE/3.5/kdegraphics/kpdf/part.cpp: finally commit patch
	  by Mary Ellen Foster to implement wish 109078 Incidentally also
	  implements wish 127382 Sorry for having taken so long. FEATURE:
	  109078 FEATURE: 127382

2006-07-12 10:45 +0000 [r561519]  cartman

	* branches/KDE/3.5/kdegraphics/ksvg/impl/libs/libtext2path/configure.in.in:
	  remove PACKAGE= which breaks apidox installation. Note that this
	  configure.in.in could use some cleanup. BUG:130687

2006-07-12 10:56 +0000 [r561521]  dang

	* branches/KDE/3.5/kdegraphics/kolourpaint/NEWS,
	  branches/KDE/3.5/kdegraphics/kolourpaint/README,
	  branches/KDE/3.5/kdegraphics/kolourpaint/VERSION: up ver to
	  1.4.4_relight in light (pun not intended) of freeze

2006-07-16 17:58 +0000 [r563106]  aacid

	* branches/KDE/3.5/kdegraphics/kpdf/xpdf/xpdf/GfxState.cc: don't
	  crash when we can not process a GfxImageColorMap correctly BUGS:
	  130846

2006-07-22 18:41 +0000 [r565222]  aacid

	* branches/KDE/3.5/kdegraphics/kpdf/shell/main.cpp,
	  branches/KDE/3.5/kdegraphics/kpdf/VERSION: version++

2006-07-23 14:02 +0000 [r565471]  coolo

	* branches/KDE/3.5/kdegraphics/kdegraphics.lsm: preparing KDE 3.5.4

