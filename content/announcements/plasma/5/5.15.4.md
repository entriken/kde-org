---
aliases:
- ../../plasma-5.15.4
changelog: 5.15.3-5.15.4
date: 2019-04-02
layout: plasma
figure:
  src: /announcements/plasma/5/5.15.0/plasma-5.15-apps.png
asBugfix: true
---

- [platforms/x11] Force glXSwapBuffers to block with NVIDIA driver. <a href="https://commits.kde.org/kwin/3ce5af5c21fd80e3da231b50c39c3ae357e9f15c">Commit.</a> Phabricator Code review <a href="https://phabricator.kde.org/D19867">D19867</a>
- Don't allow refreshing during updates. <a href="https://commits.kde.org/discover/84f70236f4d98ee53c83ec7fa30a396754e332ad">Commit.</a> Fixes bug <a href="https://bugs.kde.org/403333">#403333</a>
- The crash dialog no longer cuts off text after backtracing. <a href="https://commits.kde.org/drkonqi/cc640cea3e9cc1806000804bbf424cb611622e45">Commit.</a> Fixes bug <a href="https://bugs.kde.org/337319">#337319</a>. Phabricator Code review <a href="https://phabricator.kde.org/D19390">D19390</a>
