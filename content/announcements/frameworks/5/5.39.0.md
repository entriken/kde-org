---
aliases:
- ../../kde-frameworks-5.39.0
date: 2017-10-14
layout: framework
libCount: 70
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---

### Baloo

- Only match real MIME types, not e.g. "raw CD image" (bug 364884)
- Remove pf.path() from container before the reference got screwed up by it.remove()
- Fix tags KIO-slave protocol description
- Consider markdown files to be Documents

### Breeze Icons

- add overflow-menu icon (bug 385171)

### Extra CMake Modules

- Fix python bindings compilation after 7af93dd23873d0b9cdbac192949e7e5114940aa6

### Framework Integration

- Make KStandardGuiItem::discard match QDialogButtonBox::Discard

### KActivitiesStats

- Changed the default query limit to zero
- Added the option to enable model tester

### KCMUtils

- Make KCMultiDialog scrollable (bug 354227)

### KConfig

- Deprecate KStandardShortcut::SaveOptions

### KConfigWidgets

- Deprecate KStandardAction::PasteText and KPasteTextAction

### KCoreAddons

- desktoptojson: Improve legacy service type detection heuristic (bug 384037)

### KDeclarative

- Relicense to LGPL2.1+
- Added openService() method to KRunProxy

### KFileMetaData

- fix crash when more than one instances of ExtractorCollection are destructed

### KGlobalAccel

- Revert "KGlobalAccel: port to KKeyServer's new method symXModXToKeyQt, to fix numpad keys" (bug 384597)

### KIconThemes

- add a method to reset the custom palette
- use qApp-&gt;palette() when no custom one is set
- allocate the proper buffer size
- allow to set a custom palette instead of colorSets
- expose the colorset for the stylesheet

### KInit

- Windows: Fix 'klauncher uses absolute compile time install path for finding kioslave.exe'

### KIO

- kioexec: Watch the file when it has finished copying (bug 384500)
- KFileItemDelegate: Always reserve space for icons (bug 372207)

### Kirigami

- don't instantiate Theme file in BasicTheme
- add a new Forward button
- less contrast to the sheet scrollbar background
- more reliable insert and remove from overflow menu
- better context icon rendering
- more careful to center the action button
- use iconsizes for action buttons
- pixel perfect icon sizes on desktop
- selected effect to fake handle icon
- fix color of handles
- better color for the main action button
- fix context menu for desktop style
- better "more" menu for the toolbar
- a proper menu for the intermediate pages context menu
- add a text field which should bring up a keypad
- don't crash when launched with non existent styles
- ColorSet concept in Theme
- simplify wheel management (bug 384704)
- new example app with desktop/mobile main qml files
- ensure currentIndex is valid
- Generate the appstream metadata of the gallery app
- Look for QtGraphicalEffects, so packagers don't forget it
- Don't include the control over the bottom decoration (bug 384913)
- lighter coloring when listview has no activeFocus
- some support for RTL layouts
- Disable shortcuts when an action is disabled
- create the whole plugin structure in the build directory
- fix accessibility for the gallery main page
- If plasma isn't available, KF5Plasma isn't either. Should fix the CI error

### KNewStuff

- Require Kirigami 2.1 instead of 1.0 for KNewStuffQuick
- Properly create KPixmapSequence
- Don't complain the knsregistry file is not present before it's useful

### KPackage Framework

- kpackage: bundle a copy of servicetypes/kpackage-generic.desktop
- kpackagetool: bundle a copy of servicetypes/kpackage-generic.desktop

### KParts

- KPartsApp template: fix install location of kpart desktop file

### KTextEditor

- Ignore default mark in icon border for single selectable mark
- Use QActionGroup for input mode selection
- Fix missing spell check bar (bug 359682)
- Fix the fall-back "blackness" value for unicode &gt; 255 characters (bug 385336)
- Fix trailing space visualization for RTL lines

### KWayland

- Only send OutputConfig sendApplied / sendFailed to the right resource
- Don't crash if a client (legally) uses deleted global contrast manager
- Support XDG v6

### KWidgetsAddons

- KAcceleratorManager: set icon text on actions to remove CJK markers (bug 377859)
- KSqueezedTextLabel: Squeeze text when changing indent or margin
- Use edit-delete icon for destructive discard action (bug 385158)
- Fix Bug 306944 - Using the mousewheel to increment/decrement the dates (bug 306944)
- KMessageBox: Use question mark icon for question dialogs
- KSqueezedTextLabel: Respect indent, margin and frame width

### KXMLGUI

- Fix KToolBar repaint loop (bug 377859)

### Plasma Framework

- Fix org.kde.plasma.calendar with Qt 5.10
- [FrameSvgItem] Iterate child nodes properly
- [Containment Interface] Don't add containment actions to applet actions on desktop
- Add new component for the greyed out labels in Item Delegates
- Fix FrameSVGItem with the software renderer
- Don't animate IconItem in software mode
- [FrameSvg] Use new-style connect
- possibility to set an attached colorscope to not inherit
- Add extra visual indicator for Checkbox/Radio keyboard focus
- don't recreate a null pixmap
- Pass item to rootObject() since it's now a singleton (bug 384776)
- Don't list tab names twice
- don't accept active focus on tab
- register revision 1 for QQuickItem
- [Plasma Components 3] Fix RTL in some widgets
- Fix invalid id in viewitem
- update mail notification icon for better contrast (bug 365297)

### qqc2-desktop-style

New module:
QtQuickControls 2 style that uses QWidget's QStyle for painting
This makes it possible to achieve an higher degree of consistency between QWidget-based and QML-based apps.

### Solid

- [solid/fstab] Add support for x-gvfs style options in fstab
- [solid/fstab] Swap vendor and product properties, allow i18n of description

### Syntax Highlighting

- Fix invalid itemData references of 57 highlighting files
- Add support for custom search paths for application-specific syntax and theme definitions
- AppArmor: fix DBus rules
- Highlighting indexer: factor out checks for smaller while loop
- ContextChecker: support '!' context switching and fallthroughContext
- Highlighting indexer: check existence of referenced context names
- Relicense qmake highlighting to MIT license
- Let qmake highlighting win over Prolog for .pro files (bug 383349)
- Support clojure's "@" macro with brackets
- Add syntax highlighting for AppArmor Profiles
- Highlighting indexer: Catch invalid a-Z/A-z ranges in regexps
- Fixing incorrectly capitalized ranges in regexps
- add missing reference files for tests, looks ok, I think
- Added Intel HEX file support for the Syntax highlighting database
- Disable spell checking for strings in Sieve scripts

### ThreadWeaver

- Fix memory leak

### Security information

The released code has been GPG-signed using the following key:
pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt;
Primary key fingerprint: 53E6 B47B 45CE A3E0 D5B7 4577 58D0 EE64 8A48 B3BB

You can discuss and share ideas on this release in the comments section of <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>the dot article</a>.
