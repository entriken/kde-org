2007-01-17 13:35 +0000 [r624617]  lunakl

	* branches/KDE/3.5/kdebase/kdesu/kdesud/kdesud.cpp: Don't access
	  invalid array element. Most likely the reason for #138873. BUG:
	  138873

2007-01-17 13:46 +0000 [r624623]  lunakl

	* branches/KDE/3.5/kdebase/kdesu/kdesud/kdesud.cpp: Fix x11
	  connection handling. Quite possibly the reason for #115898.

2007-01-17 15:57 +0000 [r624656]  lunakl

	* branches/KDE/3.5/kdebase/kicker/kicker/core/extensionmanager.cpp,
	  branches/KDE/3.5/kdebase/kdesktop/desktop.cc: This
	  desktopIconsArea stuff is rather broken with multiple monitors,
	  since caring about screen number does not actually seem
	  necessary, just ignore it, should be okay for 3.5.x . Since this
	  code has apparently never made it to trunk, I don't care more. I
	  wonder what was the point of inventing a completely different way
	  of something that exist anyway.

2007-01-18 10:11 +0000 [r624881]  coolo

	* branches/KDE/3.5/kdebase/kioslave/media/mediamanager/halbackend.h:
	  don't confuse the trainee

2007-01-20 09:54 +0000 [r625468]  hermier

	* branches/KDE/3.5/kdebase/kwin/geometry.cpp: Fix for multihead,
	  now maximisation works. CCBUG:94470

2007-01-20 23:40 +0000 [r625744]  knight

	* branches/KDE/3.5/kdebase/konsole/konsole/main.cpp,
	  branches/KDE/3.5/kdebase/konsole/konsole/TEWidget.cpp: Make real
	  transparency accessible in Konsole via a commandline option,
	  instead of a compile-time option. Launch using 'konsole
	  --real-transparency' to enable the effect. This is not enabled
	  automatically because some users have reported a preference for
	  fake over real transparency. In addition I need to find a working
	  test for a compositing manager, otherwise there are some glitches
	  in un-composited desktops.

2007-01-22 22:58 +0000 [r626362]  fredrik

	* branches/KDE/3.5/kdebase/kwin/kompmgr/kompmgr.c: Add support for
	  _NET_WM_CM_Sn manager selections.

2007-01-23 17:28 +0000 [r626552]  helio

	* branches/KDE/3.5/kdebase/kcontrol/background/bgdialog.cpp,
	  branches/KDE/3.5/kdebase/kcontrol/background/bgsettings.cpp: -
	  Using load(defaults) as true never notified renderer about
	  property changes. Now background changes always set dirty for
	  true validating new settings when user hits ok or apply.

2007-01-23 22:25 +0000 [r626615]  dfaure

	* branches/KDE/3.5/kdebase/kioslave/trash/trashimpl.h,
	  branches/KDE/3.5/kdebase/kioslave/trash/trashimpl.cpp: Backport:
	  When a per-partition trash dir is found, like my /d/.Trash-1000,
	  we need to create info and files subdirs into it before we can
	  use it! Detected by a unittest failure here.

2007-01-24 13:41 +0000 [r626727]  lunakl

	* branches/KDE/3.5/kdebase/kwin/tabbox.cpp,
	  branches/KDE/3.5/kdebase/kwin/events.cpp,
	  branches/KDE/3.5/kdebase/kwin/tabbox.h: Revert r427993 (#106013)
	  as it causes #140023. BUG: 140023

2007-01-24 14:37 +0000 [r626750]  mueller

	* branches/KDE/3.5/kdebase/ksysguard/gui/SensorDisplayLib/SignalPlotter.cc,
	  branches/KDE/3.5/kdebase/ksysguard/gui/SensorDisplayLib/SignalPlotter.h:
	  remove min (the usual daily unbreak compilation)

2007-01-25 12:16 +0000 [r627006]  coolo

	* branches/KDE/3.5/kdebase/kioslave/media/configure.in.in,
	  branches/KDE/3.5/kdebase/kioslave/media/kfile-plugin/kfilemediaplugin.cpp:
	  fix compilation without statvfs

2007-01-25 15:28 +0000 [r627054]  mlaurent

	* branches/KDE/3.5/kdebase/kscreensaver/random.cpp: Don't add
	  screensaver when we can exec program

2007-01-25 23:07 +0000 [r627197]  fredrik

	* branches/KDE/3.5/kdebase/kwin/kompmgr/kompmgr.c: Revert r626362,
	  since kwin already manages the selections.

2007-01-26 08:59 +0000 [r627342]  knight

	* branches/KDE/3.5/kdebase/konsole/konsole/session.cpp: A better
	  fix for bug #139225

2007-01-27 02:53 +0000 [r627562]  clee

	* branches/KDE/3.5/kdebase/kcontrol/krdb/krdb.cpp: Make sure to
	  export the right color for menu text, as well.

2007-01-28 22:06 +0000 [r628036]  dhaumann

	* branches/KDE/3.5/kdebase/kate/app/kategrepdialog.cpp: code
	  duplication: two lines above the event filter is already
	  installed...

2007-02-01 11:24 +0000 [r629101]  jkpark

	* branches/KDE/3.5/kdebase/kioslave/tar/tar.cc:
	  kioslave/tar/tar.cc: allow selecting non-local character encoding
	  in tar:/, ar:/, zip:/ protocols

2007-02-01 15:08 +0000 [r629135]  mueller

	* branches/KDE/3.5/kdebase/kwin/workspace.cpp,
	  branches/KDE/3.5/kdebase/kwin/geometry.cpp: add support for
	  xrandr to kwin

2007-02-01 16:08 +0000 [r629152]  binner

	* branches/KDE/3.5/kdebase/kcontrol/filetypes/filetypesview.cpp,
	  branches/KDE/3.5/kdebase/kcontrol/filetypes/typeslistitem.cpp,
	  branches/KDE/3.5/kdebase/kcontrol/filetypes/keditfiletype.cpp:
	  Don't forget to unset configuration mode (requires newest
	  kdelibs) Fixes https://bugzilla.novell.com/show_bug.cgi?id=227717

2007-02-02 10:06 +0000 [r629310]  dfaure

	* branches/KDE/3.5/kdebase/kioslave/settings/kio_settings.cc:
	  Encode name correctly; fixes dropping "Country/Language" onto the
	  desktop

2007-02-02 14:21 +0000 [r629361]  lunakl

	* branches/KDE/3.5/kdebase/kwin/workspace.cpp: Call
	  desktopResized() only when the desktop actually is resized.

2007-02-02 19:35 +0000 [r629444]  lunakl

	* branches/KDE/3.5/kdebase/khotkeys/shared/conditions.h,
	  branches/KDE/3.5/kdebase/khotkeys/kcontrol/condition_list_widget.cpp,
	  branches/KDE/3.5/kdebase/khotkeys/shared/conditions.cpp,
	  branches/KDE/3.5/kdebase/khotkeys/shared/khlistview.cpp: Avoid
	  various braindamage when deleting condition objects. BUG: 139962
	  BUG: 115109

2007-02-02 19:46 +0000 [r629447]  lunakl

	* branches/KDE/3.5/kdebase/khotkeys/shared/settings.cpp: I suppose
	  it's less harm if the voice trigger shortcut is unset by default,
	  especially since it doesn't give conflicts in the other shortcut
	  configuration dialogs. BUG: 117447

2007-02-04 23:16 +0000 [r630298]  craig

	* branches/KDE/3.5/kdebase/kcontrol/kfontinst/kfontinst/XConfig.cpp,
	  branches/KDE/3.5/kdebase/kcontrol/kfontinst/kfontinst/XConfig.h,
	  branches/KDE/3.5/kdebase/kcontrol/kfontinst/kfontinst/Main.cpp:
	  Use mkfontscale/mkfontdir to create fonts.scale/fonts.dir - as
	  opposed to custom code. This should help with bugs such as bug
	  #109551

2007-02-05 10:51 +0000 [r630408]  mlaurent

	* branches/KDE/3.5/kdebase/kcontrol/background/bgdialog.cpp,
	  branches/KDE/3.5/kdebase/kcontrol/background/bgsettings.cpp: Fix
	  apply default value when we click on default button. It will a
	  good idea to backport it, but as Helio never applied his patch
	  for supporting read default value into trunk, all fixes will be
	  lose...

2007-02-05 13:22 +0000 [r630461-630460]  craig

	* branches/KDE/3.5/kdebase/kcontrol/kfontinst/viewpart/KfiPrint.cpp:
	  Dont emit empty 1st page in landscape. In waterfall mode, only
	  draw each text line if sufficient space. Set C locale as per Qt
	  docs. CCBUG:141123

	* branches/KDE/3.5/kdebase/kcontrol/kfontinst/viewpart/KfiPrint.cpp:
	  Oops! Set firstFont=false after first font is printed.

2007-02-06 13:42 +0000 [r630815]  lunakl

	* branches/KDE/3.5/kdebase/kwin/workspace.cpp,
	  branches/KDE/3.5/kdebase/kwin/activation.cpp: Don't accidentally
	  set _NET_ACTIVE_WINDOW to 0 when there is an active window.

2007-02-06 13:59 +0000 [r630819]  craig

	* branches/KDE/3.5/kdebase/kcontrol/kfontinst/kfontinst/FontEngine.cpp,
	  branches/KDE/3.5/kdebase/kcontrol/kfontinst/kfontinst/XConfig.cpp,
	  branches/KDE/3.5/kdebase/kcontrol/kfontinst/kfontinst/Encodings.h
	  (removed),
	  branches/KDE/3.5/kdebase/kcontrol/kfontinst/kfontinst/FontEngine.h,
	  branches/KDE/3.5/kdebase/kcontrol/kfontinst/kfontinst/XConfig.h,
	  branches/KDE/3.5/kdebase/kcontrol/kfontinst/kfontinst/CompressedFile.cpp
	  (removed),
	  branches/KDE/3.5/kdebase/kcontrol/kfontinst/kfontinst/Makefile.am,
	  branches/KDE/3.5/kdebase/kcontrol/kfontinst/kfontinst/CompressedFile.h
	  (removed),
	  branches/KDE/3.5/kdebase/kcontrol/kfontinst/kfontinst/Encodings.cpp
	  (removed): X11 config is generated via mkfontscale/mkfontdir -
	  therefore no need for any bitmap, or X11 encoding, functionality.

2007-02-09 16:42 +0000 [r631968]  dhaumann

	* branches/KDE/3.5/kdebase/kate/app/kateapp.cpp: honor current
	  locale for QCString to QString conversion. Thanks for the patch,
	  looking forward to more :) BUG: 140929

2007-02-11 14:07 +0000 [r632514]  ossi

	* branches/KDE/3.5/kdebase/kdm/kfrontend/genkdmconf.c: backport
	  noclobber override in Xsession csh hack.

2007-02-12 10:50 +0000 [r632792]  dfaure

	* branches/KDE/3.5/kdebase/libkonq/konq_operations.cc,
	  branches/KDE/3.5/kdebase/libkonq/konq_operations.h,
	  branches/KDE/3.5/kdebase/libkonq/konq_undo.cc: Backport r632461
	  It's just too easy to lose files when pressing Ctrl+z by mistake
	  in konqueror or kdesktop, after copying a file. E.g. when the
	  file comes from a removable device, or has been removed
	  meanwhile... Let's ask for confirmation before Undo deletes any
	  file. (Bug #99898)

2007-02-16 16:12 +0000 [r634204]  lunakl

	* branches/KDE/3.5/kdebase/konqueror/konq_mainwindow.cc,
	  branches/KDE/3.5/kdelibs/kded/kded.cpp,
	  branches/KDE/3.5/kdebase/startkde: $KDE_SESSION_UID with the uid
	  of the user starting KDE desktop. Needed e.g. to prevent enabling
	  desktop-wide functionality in kded run via sudo.

2007-02-20 19:57 +0000 [r635743]  aseigo

	* branches/KDE/3.5/kdebase/kicker/kicker/ui/service_mnu.cpp: let
	  translators decide how this gets laid out should they wish to, as
	  per the request of some of them. also, use a '-' by default.
	  nicer than ()s and ' ' doesn't work for enough people. so be it.

2007-02-20 20:04 +0000 [r635745]  aseigo

	* branches/KDE/3.5/kdebase/kcontrol/kicker/menutab.ui: sync the
	  control panel as well. now people who like to bitch at me instead
	  of actually talk reasonably have one less thing to ruin my hour
	  with.

2007-02-20 23:25 +0000 [r635804]  aseigo

	* branches/KDE/3.5/kdebase/kcontrol/kicker/menutab.ui: one more
	  place we mentioned the brackets

2007-02-23 16:59 +0000 [r636638]  klichota

	* branches/KDE/3.5/kdebase/kcontrol/taskbar/kcmtaskbar.cpp,
	  branches/KDE/3.5/kdebase/kicker/taskbar/taskcontainer.cpp,
	  branches/KDE/3.5/kdebase/kcontrol/taskbar/kcmtaskbar.h,
	  branches/KDE/3.5/kdebase/kcontrol/taskbar/kcmtaskbarui.ui,
	  branches/KDE/3.5/kdebase/kicker/taskbar/taskbar.kcfg: Implemented
	  often-requested changing of font colors for taskbar. Details
	  here: http://lists.kde.org/?l=kde-devel&m=117145563500995&w=2
	  FEATURE: 118460 GUI:

2007-02-28 20:26 +0000 [r638096]  dhaumann

	* branches/KDE/3.5/kdebase/kate/app/katefileselector.cpp,
	  branches/KDE/3.5/kdebase/kate/app/katefileselector.h: Patch from
	  Massimo Fidanza <max@massimofidanza.it> Use Extended mode instead
	  of Multi for the file view, as it works better when doubleclick
	  mode is used. Make sure the mode is set after changing view type.
	  Thanks for the patch! BUG: 123700

2007-03-04 08:21 +0000 [r639052]  mlaurent

	* branches/KDE/3.5/kdebase/kicker/applets/media/mediaapplet.cpp:
	  Fix crash

2007-03-06 15:11 +0000 [r640017]  lunakl

	* branches/KDE/3.5/kdebase/ksmserver/shutdown.cpp,
	  branches/KDE/3.5/kdelibs/kdecore/eventsrc: A notification about
	  logout canceled by some application ... usually one of those
	  broken ones like Skype (r627435 from trunk).

2007-03-07 14:40 +0000 [r640285]  lunakl

	* branches/KDE/3.5/kdebase/kcontrol/kicker/positiontab_impl.cpp,
	  branches/KDE/3.5/kdebase/kcontrol/background/bgdialog.cpp:
	  There's no point in involving the window manager in showing
	  tooltips identifying xinerama screens. BUG: 132427

2007-03-07 19:46 +0000 [r640373]  helio

	* branches/KDE/3.5/kdebase/kicker/kicker/ui/k_mnu.cpp: - We can
	  replace iostream + cout for proper kdDebug entry...

2007-03-12 17:59 +0000 [r641829]  lunakl

	* branches/KDE/3.5/kdebase/startkde: Check for return value of
	  lnusertemp. Adjusted patch from #142806.

2007-03-13 12:29 +0000 [r642131]  knight

	* branches/KDE/3.5/kdebase/konsole/konsole/main.cpp: Allow
	  --profile and -e command line options to be used together. If the
	  -e option is specified, the program to execute specified in the
	  .profile file is ignored in favour of the program specified in
	  the -e argument. Fixes BUG: 137031

2007-03-18 15:55 +0000 [r643857]  dhaumann

	* branches/KDE/3.5/kdebase/kate/app/katesession.cpp: mem leak--

2007-03-21 15:20 +0000 [r645027]  lunakl

	* branches/KDE/3.5/kdebase/kxkb/kcmlayout.cpp: Display proper
	  setxkbmap command when including the latin layout.

2007-03-21 16:06 +0000 [r645053]  mueller

	* branches/KDE/3.5/kdebase/kioslave/fish/fish.cpp: this is a
	  pointer, so > 0 doesn't make sense

2007-03-22 14:10 +0000 [r645391-645390]  lunakl

	* branches/KDE/3.5/kdebase/ksmserver/server.cpp,
	  branches/KDE/3.5/kdebase/ksmserver/server.h,
	  branches/KDE/3.5/kdebase/ksmserver/shutdown.cpp: Instead of
	  killing the WM first during shutdown in order to prevent it from
	  updating window-specific settings because of changes done during
	  shutdown, kill it last in order to avoid flicker. KWin will
	  automatically suspend updating window-specific settings during
	  shutdown.

	* branches/KDE/3.5/kdebase/kwin/workspace.cpp,
	  branches/KDE/3.5/kdebase/kwin/sm.cpp,
	  branches/KDE/3.5/kdebase/kwin/client.h,
	  branches/KDE/3.5/kdebase/kwin/workspace.h,
	  branches/KDE/3.5/kdebase/kwin/rules.cpp: Suspend updating of
	  window-specific settings during shutdown, so that KWin doesn't
	  have to be killed as the first one during shutdown.

2007-03-22 17:52 +0000 [r645482]  lunakl

	* branches/KDE/3.5/kdebase/kcontrol/fonts/fonts.cpp,
	  branches/KDE/3.5/kdebase/kcontrol/fonts/fonts.h: It seems people
	  get pretty confused by tri-state checkboxes (well, I don't blame
	  them).

2007-03-29 13:44 +0000 [r647770]  mlaurent

	* branches/KDE/3.5/kdebase/kioslave/media/medianotifier/notificationdialog.cpp:
	  Be sure that all info into dialogbox is shown => resize it with
	  real minimum size

2007-03-30 14:15 +0000 [r648097]  dfaure

	* branches/KDE/3.5/kdebase/kdesktop/lock/lockdlg.cc: Patch by
	  Thibauld Favre <tfavre rift-technologies com> to honour kiosk
	  switch user restriction - thanks! BUG: 143597

2007-04-04 10:16 +0000 [r650339]  lunakl

	* branches/KDE/3.5/kdebase/kcontrol/fonts/Makefile.am,
	  branches/KDE/3.5/kdebase/kcontrol/fonts/fonts.cpp,
	  branches/KDE/3.5/kdebase/kcontrol/fonts/configure.in.in: Check if
	  subpixel hinting is supported.

2007-04-09 17:23 +0000 [r651938]  woebbe

	* branches/KDE/3.5/kdebase/nsplugins/sdk/npapi.h: Don't use long
	  for 32 bit types on LP64 platforms. Now Konqueror can use e.g.
	  flashplugin on x86_64 with nspluginwrapper :-)
	  CCMAIL:gb.public@free.fr

2007-04-10 10:23 +0000 [r652184]  dfaure

	* branches/KDE/3.5/kdebase/kdcop/kdcopview.ui: Use a splitter so
	  that the user can resize the output window/treeview. Patch by
	  "shane richards" <shanerich email com>

2007-04-10 12:18 +0000 [r652214]  lunakl

	* branches/KDE/3.5/kdebase/kcontrol/fonts/fonts.cpp: Looks like
	  subpixel hinting support is still not available everywhere.

2007-04-10 16:17 +0000 [r652286]  ilic

	* branches/KDE/3.5/kdebase/l10n/me/flag.png (added),
	  branches/KDE/3.5/kdebase/l10n/rs/entry.desktop (added),
	  branches/KDE/3.5/kdebase/l10n/yu (removed),
	  branches/KDE/3.5/kdebase/l10n/rs/flag.png (added),
	  branches/KDE/3.5/kdebase/l10n/me (added),
	  branches/KDE/3.5/kdebase/l10n/rs (added),
	  branches/KDE/3.5/kdebase/l10n/me/entry.desktop (added):
	  (backported from trunk 652285) Removed "Serbia and Montenegro"
	  (yu) from l10n country settings, added "Serbia" (rs) and
	  "Montenegro" (me). New codes according to ISO 3166-1.

2007-04-11 14:24 +0000 [r652585-652581]  lunakl

	* branches/KDE/3.5/kdebase/kicker/kicker/ui/service_mnu.cpp: Fix
	  focus with RMB/Put Into Run Dialog. I didn't even know K-Menu
	  could do this.

	* branches/KDE/3.5/kdebase/nsplugins/viewer/qxteventloop.cpp: Fix
	  keyboard events handling.

2007-04-11 14:34 +0000 [r652591]  ossi

	* branches/KDE/3.5/kdebase/kdm/backend/client.c: work around
	  solaris < 10 PAM breakage. will backport immediately. BUG: 135158

2007-04-11 14:41 +0000 [r652600]  lunakl

	* branches/KDE/3.5/kdebase/kwin/client.cpp: Fix timestamp handling
	  on 64bit platforms. BUG: 143545

2007-04-12 18:01 +0000 [r653142]  ossi

	* branches/KDE/3.5/kdebase/kdesktop/lock/lockprocess.cc: give the
	  hack a few seconds to exit. BUG: 81380

2007-04-13 11:20 +0000 [r653460]  mueller

	* branches/KDE/3.5/kdebase/khotkeys/shared/input.cpp,
	  branches/KDE/3.5/kdebase/khotkeys/shared/gestures.h,
	  branches/KDE/3.5/kdebase/khotkeys/shared/voicesignature.h: -
	  avoid aliasing issues - pedantic

2007-04-13 16:17 +0000 [r653561]  ilic

	* branches/KDE/3.5/kdebase/l10n/rs/entry.desktop,
	  branches/KDE/3.5/kdebase/l10n/me/entry.desktop: (backported from
	  trunk 653558) Currency and address templates depend on the chosen
	  language script (sr or sr@Latn).

2007-04-13 19:41 +0000 [r653612]  ilic

	* branches/KDE/3.5/kdebase/l10n/rs/entry.desktop,
	  branches/KDE/3.5/kdebase/l10n/me/entry.desktop: (backported from
	  trunk 653611) Put back the [eo] date formats in rs and me country
	  entries. I may have got the idea why they were there in the first
	  place...

2007-04-14 20:32 +0000 [r653999]  schwarzer

	* branches/KDE/3.5/kdebase/doc/userguide/customizing-desktop.docbook:
	  backport of r652075 and r651570 - added some entities - removed
	  the entities used in url kde-look.org since here "KDE" doesn't
	  need extra accentuation There are no global entities for IceWM or
	  deKorator, so I left them at that. Thanks Natalie for the report.
	  - Added missing content: Wallpapers can be downloaded via GUI
	  button as well as from the website. CCMAIL: kde-i18n-doc@kde.org

2007-04-17 15:44 +0000 [r655041]  hasso

	* branches/KDE/3.5/kdebase/kcontrol/info/memory.cpp,
	  branches/KDE/3.5/kdebase/kcontrol/info/info.cpp: Backport of
	  655032 - make it work on DragonFlyBSD.

2007-04-18 09:33 +0000 [r655417]  hasso

	* branches/KDE/3.5/kdebase/kxkb/x11helper.cpp: Backport of 655406 -
	  make kxkb work on pkgsrc systems.

2007-04-18 13:29 +0000 [r655456]  hasso

	* branches/KDE/3.5/kdebase/ksysguard/ksysguardd/Makefile.am,
	  branches/KDE/3.5/kdebase/ksysguard/configure.in.in,
	  branches/KDE/3.5/kdebase/ksysguard/ksysguardd/configure.in.in,
	  branches/KDE/3.5/kdebase/ksysguard/ksysguardd/FreeBSD/CPU.c,
	  branches/KDE/3.5/kdebase/ksysguard/ksysguardd/FreeBSD/ProcessList.c:
	  Backport of the DragonFlyBSD support for ksysguard from trunk. No
	  any change in any other platform.

2007-04-18 18:46 +0000 [r655554]  hasso

	* branches/KDE/3.5/kdebase/kcontrol/clock/dtime.cpp: Backport of
	  655551 - DragonFlyBSD behaves like any other BSD in this regard.

2007-04-19 21:28 +0000 [r655994]  jriddell

	* branches/KDE/3.5/kdebase/kcontrol/pics/cr48-app-kcmopengl.png:
	  replace opengl icon, it is restricted by copyright and trademark
	  http://opengl.org/about/logos/ replacement by Sune Vuorela, LGPL

2007-04-20 10:44 +0000 [r656117]  coolo

	* branches/KDE/3.5/kdebase/kdesktop/init/directory.desktop:
	  translate the Desktop (already part of desktop_kdebase.pot)

2007-04-20 18:21 +0000 [r656233]  hasso

	* branches/KDE/3.5/kdebase/kcontrol/kfontinst/kfontinst/GetPid.c:
	  Fix compile in DragonFlyBSD 1.9.

2007-04-22 18:05 +0000 [r656915]  mkoller

	* branches/KDE/3.5/kdebase/libkonq/konq_iconviewwidget.cc: BUG:
	  67905 when mouse leaves window, do actions as would be done when
	  mouse moves over an empty viewport area

2007-04-23 12:55 +0000 [r657182]  coolo

	* branches/KDE/3.5/kdebase/kioslave/media/mediamanager/halbackend.h,
	  branches/KDE/3.5/kdebase/kioslave/media/mediamanager/halbackend.cpp:
	  don't handle floppy drives as with old HAL versions - newer ones
	  have a volume on its own, so better use that one (still leaving
	  older logic in place)

2007-04-24 12:14 +0000 [r657557]  lunakl

	* branches/KDE/3.5/kdebase/khotkeys/data/kde32b1.khotkeys,
	  branches/KDE/3.5/kdebase/khotkeys/data/printscreen.khotkeys,
	  branches/KDE/3.5/kdebase/khotkeys/data/konqueror_gestures_kde321.khotkeys:
	  No trailing \n in comments (causes problems for translators).

2007-04-24 13:29 +0000 [r657576]  lunakl

	* branches/KDE/3.5/kdebase/kwin/tabbox.cpp,
	  branches/KDE/3.5/kdebase/kwin/options.h,
	  branches/KDE/3.5/kdebase/kwin/options.cpp: Option to turn off
	  alt+tab outline: kwinrc:[Windows]:TabboxOutline:false
	  FEATURE:134703

2007-04-27 15:10 +0000 [r658521]  lunakl

	* branches/KDE/3.5/kdebase/kwin/data/fsp_workarounds_1: Remove
	  exceptions for Mozilla apps and OOo - they can handle focus
	  stealing prevention somewhat better.

2007-05-01 11:23 +0000 [r660037]  hasso

	* branches/KDE/3.5/kdebase/ksysguard/ksysguardd/FreeBSD/ProcessList.c:
	  Backport 660029.

2007-05-03 16:57 +0000 [r660758]  lunakl

	* branches/KDE/3.5/kdebase/kate/app/katemainwindow.cpp: Don't show
	  the files-have-changed-on-disk dialog more than once.
	  (bugzilla.novell.com #260741)

2007-05-04 09:11 +0000 [r660968]  dfaure

	* branches/KDE/3.5/kdebase/kioslave/trash/trashimpl.cpp: [Bug
	  142148] Directory permission checks for .Trash directory are to
	  restrictive It's actually fine if root-owned .Trash isn't
	  readable/writable by users, users only need to be able to access
	  their own subdir. BUG: 142148

2007-05-08 10:40 +0000 [r662486]  coolo

	* branches/KDE/3.5/kdebase/kioslave/media/mediamanager/halbackend.cpp:
	  only leave out sync for vfat if hal/kernel supports flush

2007-05-08 12:48 +0000 [r662500]  coolo

	* branches/KDE/3.5/kdebase/kioslave/media/mounthelper/kio_media_mounthelper.cpp,
	  branches/KDE/3.5/kdebase/kioslave/media/mediamanager/halbackend.cpp:
	  make sure we unmount the device before we even try to eject - and
	  give correct error message if the device is busy BUG: 143353

2007-05-09 00:15 +0000 [r662728]  perreaul

	* branches/KDE/3.5/kdebase/konsole/konsole/konsole_part.cpp: Be
	  gentle when killing processes spawned by a konsole part. In the
	  past, destroying a konsole part always resulted in a SIGKILL
	  being sent to the child process (usually a shell). This in turn
	  could have negative effects such as preventing the shell from
	  writing its history file. This situation was very painful for
	  users of QuadKonsole. Now a SIGHUP is sent first. We wait up to 1
	  second for the process to terminate before sending a SIGKILL. The
	  method is copied from the non-part konsole.cpp. And they lived
	  happily ever after.

2007-05-09 13:49 +0000 [r662887]  lunakl

	* branches/KDE/3.5/kdebase/kwin/data/kwin_fsp_workarounds_1.upd,
	  branches/KDE/3.5/kdebase/kwin/data/fsp_workarounds_1 (removed),
	  branches/KDE/3.5/kdebase/kwin/data/fsp_workarounds_1.kwinrules
	  (added), branches/KDE/3.5/kdebase/kwin/data/Makefile.am,
	  branches/KDE/3.5/kdebase/kwin/rules.cpp: translate kwin's rules
	  files

2007-05-09 14:47 +0000 [r662906]  lunakl

	* branches/KDE/3.5/kdebase/kwin/workspace.cpp: Remove deleted
	  clients also from the list for 'show desktop'. BUG: 145147

2007-05-10 08:29 +0000 [r663143]  annma

	* branches/KDE/3.5/kdebase/kcontrol/kio/kioslave.kcfg: fix typo

2007-05-10 22:03 +0000 [r663338]  fredrik

	* branches/KDE/3.5/kdebase/kcontrol/krdb/krdb.cpp: Backport
	  r663332.

2007-05-11 15:01 +0000 [r663528]  lunakl

	* branches/KDE/3.5/kdebase/kdesktop/xautolock.cc,
	  branches/KDE/3.5/kdebase/kdesktop/xautolock.h: Sync in
	  klaptopdaemon changes.

2007-05-14 07:15 +0000 [r664515]  binner

	* branches/KDE/3.5/kdesdk/kdesdk.lsm,
	  branches/KDE/3.5/kdenetwork/kopete/libkopete/kopeteversion.h,
	  branches/arts/1.5/arts/configure.in.in,
	  branches/KDE/3.5/kdelibs/README,
	  branches/KDE/3.5/kdelibs/kdecore/ksycoca.h,
	  branches/KDE/3.5/kdepim/kdepim.lsm,
	  branches/KDE/3.5/kdevelop/kdevelop.lsm,
	  branches/KDE/3.5/kdewebdev/quanta/quanta.lsm,
	  branches/KDE/3.5/kdebase/startkde,
	  branches/KDE/3.5/kdeaccessibility/kdeaccessibility.lsm,
	  branches/arts/1.5/arts/arts.lsm,
	  branches/KDE/3.5/kdeadmin/kdeadmin.lsm,
	  branches/KDE/3.5/kdeaddons/kdeaddons.lsm,
	  branches/KDE/3.5/kdelibs/kdelibs.lsm,
	  branches/KDE/3.5/kdeartwork/kdeartwork.lsm,
	  branches/KDE/3.5/kdenetwork/kdenetwork.lsm,
	  branches/KDE/3.5/kdebase/kdebase.lsm,
	  branches/KDE/3.5/kdemultimedia/kdemultimedia.lsm,
	  branches/KDE/3.5/kdelibs/kdecore/kdeversion.h,
	  branches/KDE/3.5/kdegames/kdegames.lsm,
	  branches/KDE/3.5/kdewebdev/kdewebdev.lsm,
	  branches/KDE/3.5/kdeedu/kdeedu.lsm,
	  branches/KDE/3.5/kdebindings/kdebindings.lsm,
	  branches/KDE/3.5/kdebase/konqueror/version.h,
	  branches/KDE/3.5/kdetoys/kdetoys.lsm,
	  branches/KDE/3.5/kdewebdev/quanta/src/quanta.h,
	  branches/KDE/3.5/kdegraphics/kdegraphics.lsm,
	  branches/KDE/3.5/kdeutils/kdeutils.lsm: bump version numbers for
	  3.5.7

2007-05-14 10:09 +0000 [r664555]  dfaure

	* branches/KDE/3.5/kdebase/libkonq/konq_operations.cc: Fix
	  regression introduced by the fix for #99898: don't ask for
	  confirmation (with a weird path) before emptying the trash. BUG:
	  145235

