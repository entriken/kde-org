2005-07-21 02:47 +0000 [r437145-437144]  ggarand

	* branches/KDE/3.4/kdelibs/khtml/html/html_formimpl.cpp: backport
	  #106781 regression fix

	* branches/KDE/3.4/kdelibs/khtml/rendering/render_list.cpp,
	  branches/KDE/3.4/kdelibs/khtml/rendering/render_replaced.cpp,
	  branches/KDE/3.4/kdelibs/khtml/rendering/render_object.cpp:
	  backport vertical-align fixes, widget drawing speedup

2005-07-21 14:40 +0000 [r437321]  lunakl

	* branches/KDE/3.4/kdelibs/khtml/khtmlview.cpp,
	  branches/KDE/3.4/kdelibs/khtml/html/html_formimpl.cpp: Backport
	  setting focus reason for (back)tabbing.

2005-07-21 15:14 +0000 [r437336]  vriezen

	* branches/KDE/3.4/kdelibs/khtml/java/kjava.jar: Regen for bero's
	  "gcj Applet support"

2005-07-21 15:19 +0000 [r437342]  pletourn

	* branches/KDE/3.4/kdelibs/kioslave/ftp/ftp.protocol,
	  branches/KDE/3.4/kdebase/kioslave/sftp/sftp.protocol: Allow
	  rename action in konqueror context menu BUG:105563

2005-07-25 09:54 +0000 [r438539]  carewolf

	* branches/KDE/3.4/kdelibs/khtml/ecma/kjs_events.cpp: Backport of
	  GMail as Firefox fix.

2005-07-27 09:30 +0000 [r439155]  lunakl

	* branches/KDE/3.4/kdelibs/libkscreensaver/kscreensaver.cpp:
	  Backport fix for proper size of screensavers.

2005-07-28 20:04 +0000 [r439745]  mueller

	* branches/KDE/3.4/kdelibs/kio/kio/kmimemagic.cpp,
	  branches/KDE/3.5/kdelibs/kio/kio/kmimemagic.cpp,
	  trunk/KDE/kdelibs/kio/kio/kmimemagic.cpp: fix detection of
	  -fpie/pie executables

2005-07-29 02:30 +0000 [r439838]  mueller

	* branches/KDE/3.4/kdelibs/configure.in.in,
	  branches/KDE/3.5/kdelibs/configure.in.in,
	  trunk/KDE/kdelibs/configure.in.in: move
	  KDE_ENABLE_HIDDEN_VISIBILITY after the Qt check

2005-07-29 02:35 +0000 [r439839]  mueller

	* branches/KDE/3.4/kdelibs/kate/part/katesearch.cpp: backport
	  413244

2005-07-31 17:11 +0000 [r441687]  teske

	* branches/KDE/3.4/kdelibs/kdeui/klistview.cpp: Backport fix for a
	  crash in keditbookmarks

2005-08-01 18:48 +0000 [r442135]  johnflux

	* branches/KDE/3.4/kdelibs/kdeui/ksyntaxhighlighter.cpp: Backport
	  to 3.4 on request by jacubS There was a bug where textedit says
	  too many misspelt words and turns off when you have only written
	  1 or 2 words! So I wrote a patch.. After discussing with the
	  others, we agree a default of 100 words minimum before it can be
	  disabled.

2005-08-10 07:54 +0000 [r444442]  coolo

	* branches/KDE/3.4/kdelibs/kioslave/file/file.cc: if findExe can't
	  find pmount, it's safe to assume sh won't find it either

2005-08-10 10:03 +0000 [r444467]  coolo

	* branches/KDE/3.4/kdelibs/dcop/dcopsignals.cpp: make kded killable
	  when mediamanager is loaded (do not register the
	  dcopsignalconnection twice in the list or removeConnection will
	  die)

2005-08-13 00:40 +0000 [r446290]  jriddell

	* branches/KDE/3.4/kdelibs/pics/crystalsvg/cr64-action-player_play.png
	  (added): Add 64x64 play icon used by kaffeine

2005-08-17 21:05 +0000 [r450293]  mueller

	* branches/KDE/3.4/kdelibs/kabc/formats/binaryformat.cpp: fix
	  export

2005-08-17 21:11 +0000 [r450297]  mueller

	* branches/KDE/3.4/kdelibs/kimgio/psd.h: fix export's

2005-08-19 12:32 +0000 [r450893]  lunakl

	* branches/KDE/3.4/kdelibs/kdeui/kpassivepopup.cpp: Fix xinerama.

2005-08-21 09:30 +0000 [r451596]  coolo

	* branches/KDE/3.4/kdelibs/kio/kio/connection.cpp: backporting fix
	  for handling of EOF in kio_http

2005-08-23 01:48 +0000 [r452327]  mueller

	* branches/KDE/3.4/kdelibs/kioslave/http/kcookiejar/kcookiejar.cpp,
	  branches/KDE/3.4/kdelibs/kioslave/http/kcookiejar/tests/cookie_rfc.test:
	  fix language selector on novell.com

2005-08-26 09:25 +0000 [r453461]  mlaurent

	* branches/KDE/3.4/kdelibs/knewstuff/downloaddialog.cpp: Fix show
	  download. When file is too big or network too slow before we can
	  select other item => when we finished download item activated was
	  not good and we couldn't download it after. => now we disable
	  list during download

2005-08-27 19:23 +0000 [r454042]  gyurco

	* branches/KDE/3.4/kdelibs/kio/misc/kntlm/kntlm.cpp: Backport fix
	  for #110980 (endiannes problem) CCBUG: 110980

2005-08-28 12:30 +0000 [r454243]  gianni

	* branches/KDE/3.4/kdelibs/kdeui/kpushbutton.cpp: fix bug #111642,
	  that make impossible to set a tooltip and a whatsthis in a new
	  KGuiItem widget approved by David CCBUG:111642

2005-08-30 08:34 +0000 [r454903]  montanaro

	* branches/KDE/3.4/kdelibs/kjs/regexp.cpp: Committed fix for bug
	  #110995 (non-pcre regular expressions do not work)

2005-08-30 18:00 +0000 [r455154]  montanaro

	* branches/KDE/3.4/kdelibs/kjs/regexp.cpp: Backport from head, no
	  need to call regcomp twice

2005-09-05 19:50 +0000 [r457394]  gyurco

	* branches/KDE/3.4/kdelibs/kio/misc/kntlm/kntlm.cpp: Disable
	  (NT)LMv2, until the issues are solved. CCBUG: 93454

2005-09-07 14:03 +0000 [r458197]  mueller

	* branches/KDE/3.4/kdelibs/kio/magic: improve detection of tiff's

2005-09-17 11:12 +0000 [r461335]  cas

	* branches/KDE/3.4/kdelibs/kate/data/clipper.xml: Update Clipper
	  syntax file

2005-09-28 14:49 +0000 [r464903]  aseigo

	* branches/KDE/3.4/kdelibs/kdeui/kpushbutton.cpp: don't tooltip by
	  default on kpushbuttons. prevents problems with kdesktop and
	  looks nicer anyways BUG:113489

2005-10-01 04:32 +0000 [r465875]  tibirna

	* branches/KDE/3.4/kdelibs/kdeprint/specials.desktop: backport
	  r463433 (bug 65623)

2005-10-01 04:43 +0000 [r465876]  tibirna

	* branches/KDE/3.4/kdelibs/kdeprint/management/kmwdrivertest.cpp,
	  branches/KDE/3.4/kdelibs/kdeprint/management/kmwizardpage.h,
	  branches/KDE/3.4/kdelibs/kdeprint/management/kmwizardpage.cpp,
	  branches/KDE/3.4/kdelibs/kdeprint/management/kmwizard.cpp:
	  backport r465077 fix bug 107283

2005-10-01 04:54 +0000 [r465877]  tibirna

	* branches/KDE/3.4/kdelibs/kdeprint/kpqtpage.cpp: backport r464359
	  fix bug 97694

2005-10-01 05:12 +0000 [r465883]  rodda

	* branches/KDE/3.4/kdelibs/kate/part/katecodecompletion.cpp:
	  Backport r465878: Properly adjust the argument hint when the
	  cursor is on screens other than the first screen

2005-10-03 16:27 +0000 [r466896]  staikos

	* branches/KDE/3.4/kdelibs/kwallet/backend/sha1.cc: backport patch
	  for 64bit crash

2005-10-05 01:02 +0000 [r467362]  mattr

	* branches/KDE/3.4/kdelibs/kjs/date_object.cpp,
	  branches/KDE/3.4/kdelibs/kjs/testkjs.cpp,
	  branches/KDE/3.4/kdelibs/kjs/date_object.h,
	  branches/KDE/3.4/kdelibs/kjs/ustring.cpp,
	  branches/KDE/3.4/kdelibs/kjs/internal.cpp,
	  branches/KDE/3.4/kdelibs/kjs/string_object.cpp,
	  branches/KDE/3.4/kdelibs/kjs/value.cpp,
	  branches/KDE/3.4/kdelibs/kjs/number_object.cpp: Backport the
	  recent fixes from the KDE 3.5 branches

2005-10-05 13:42 +0000 [r467507]  coolo

	* branches/KDE/3.4/kdelibs/kdecore/kdeversion.h,
	  branches/KDE/3.4/kdelibs/README: 3.4.3

2005-10-05 13:47 +0000 [r467529-467519]  coolo

	* branches/KDE/3.4/kdelibs/kdelibs.lsm: 3.4.3

	* branches/KDE/3.4/kdelibs/kdeui/kdepackages.h: new packages

