---
aliases:
- ../../plasma-5.15.3
changelog: 5.15.2-5.15.3
date: 2019-03-12
layout: plasma
figure:
  src: /announcements/plasma/5/5.15.0/plasma-5.15-apps.png
asBugfix: true
---

- Single-clicks correctly activate modules again when the system is in double-click mode. <a href="https://commits.kde.org/kinfocenter/dbaf5ba7e8335f3a9c4577f137a7b1a23c6de33b">Commit.</a> Fixes bug <a href="https://bugs.kde.org/405373">#405373</a>. Phabricator Code review <a href="https://phabricator.kde.org/D19703">D19703</a>
- [Task Manager] Fix sorting of tasks on last desktop in sort-by-desktop mode. <a href="https://commits.kde.org/plasma-workspace/53290043796c90207c5b7b6aad4a70f030514a97">Commit.</a> Phabricator Code review <a href="https://phabricator.kde.org/D19259">D19259</a>
- [OSD] Fix animation stutter. <a href="https://commits.kde.org/plasma-workspace/9a7de4e02399880a0e649bad32a40ad820fc87eb">Commit.</a> Phabricator Code review <a href="https://phabricator.kde.org/D19566">D19566</a>
