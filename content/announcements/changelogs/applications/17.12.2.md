---
aliases:
- ../../fulllog_applications-17.12.2
hidden: true
title: KDE Applications 17.12.2 Full Log Page
type: fulllog
version: 17.12.2
---

<h3><a name='akonadi' href='https://cgit.kde.org/akonadi.git'>akonadi</a> <a href='#akonadi' onclick='toggle("ulakonadi", this)'>[Show]</a></h3>
<ul id='ulakonadi' style='display: none'>
<li>Fix ItemModifyJob for Item's with only clearPayload flag set. <a href='http://commits.kde.org/akonadi/a2a85090cce30676c05e11fedb3088806e80f7e5'>Commit.</a> </li>
</ul>
<h3><a name='akonadi-calendar' href='https://cgit.kde.org/akonadi-calendar.git'>akonadi-calendar</a> <a href='#akonadi-calendar' onclick='toggle("ulakonadi-calendar", this)'>[Show]</a></h3>
<ul id='ulakonadi-calendar' style='display: none'>
<li>Use the chosen organizer identity when emailing attendees. <a href='http://commits.kde.org/akonadi-calendar/4172a6ed055330aa201fa673256de95e86ed84ed'>Commit.</a> </li>
</ul>
<h3><a name='akonadi-search' href='https://cgit.kde.org/akonadi-search.git'>akonadi-search</a> <a href='#akonadi-search' onclick='toggle("ulakonadi-search", this)'>[Show]</a></h3>
<ul id='ulakonadi-search' style='display: none'>
<li>Handle DatabaseModifiedError when querying email contacts. <a href='http://commits.kde.org/akonadi-search/d43b22a5132bb41981fa1594da0f97455f370383'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/369056'>#369056</a></li>
<li>Handle DatabaseCorruptError exception in ContactCompleter. <a href='http://commits.kde.org/akonadi-search/94e608dffa8112c94cf88c16e67afaea70b40d7b'>Commit.</a> </li>
</ul>
<h3><a name='ark' href='https://cgit.kde.org/ark.git'>ark</a> <a href='#ark' onclick='toggle("ulark", this)'>[Show]</a></h3>
<ul id='ulark' style='display: none'>
<li>CreateDialog: remove trailing whitespace after last separator. <a href='http://commits.kde.org/ark/cec095d69f1de4c6c66493c4ebb191c0ab1b6e3c'>Commit.</a> </li>
<li>Libarchive: check isInterruptionRequested() in copyData(). <a href='http://commits.kde.org/ark/54ba50ec71998ae6856cb5baf777f72a4d0e33ba'>Commit.</a> </li>
</ul>
<h3><a name='dolphin' href='https://cgit.kde.org/dolphin.git'>dolphin</a> <a href='#dolphin' onclick='toggle("uldolphin", this)'>[Show]</a></h3>
<ul id='uldolphin' style='display: none'>
<li>Let the terminal panel dock to any part of the window. <a href='http://commits.kde.org/dolphin/108e10ca15be6662fa02f56665cc78808ab243e7'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/362593'>#362593</a></li>
<li>Folderspanel's context-menu option "Limit to Home Directory" is always visible. <a href='http://commits.kde.org/dolphin/64d2fd29819fa46c293e8c726c7df2ff00b332b3'>Commit.</a> </li>
<li>Folderspanel context-menu option "Limit to Home Directory" should be always visible. <a href='http://commits.kde.org/dolphin/3cb3d58fbf0a5d3736579ad7a52a2a46ab9c1d29'>Commit.</a> </li>
</ul>
<h3><a name='eventviews' href='https://cgit.kde.org/eventviews.git'>eventviews</a> <a href='#eventviews' onclick='toggle("uleventviews", this)'>[Show]</a></h3>
<ul id='uleventviews' style='display: none'>
<li>Timelabels.cpp - fix invalid times when using multiple timezones. <a href='http://commits.kde.org/eventviews/73db0afccec6e54b4b8683cb3493fbd2c1c7f503'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/369365'>#369365</a></li>
</ul>
<h3><a name='gwenview' href='https://cgit.kde.org/gwenview.git'>gwenview</a> <a href='#gwenview' onclick='toggle("ulgwenview", this)'>[Show]</a></h3>
<ul id='ulgwenview' style='display: none'>
<li>Resize top bar in fullscreen view mode on toggling sidebar. <a href='http://commits.kde.org/gwenview/b3e66abe28c2a85f0891f9212403ff6648a8e2eb'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/387784'>#387784</a></li>
<li>Disable auto saving window size in fullscreen mode. <a href='http://commits.kde.org/gwenview/74509b4af0009f818c288659bde16ae81e6afdf5'>Commit.</a> </li>
</ul>
<h3><a name='incidenceeditor' href='https://cgit.kde.org/incidenceeditor.git'>incidenceeditor</a> <a href='#incidenceeditor' onclick='toggle("ulincidenceeditor", this)'>[Show]</a></h3>
<ul id='ulincidenceeditor' style='display: none'>
<li>Fix use-after-free. <a href='http://commits.kde.org/incidenceeditor/c1f5f69291226fb08d1d744059243f71b91fbacb'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/356053'>#356053</a></li>
</ul>
<h3><a name='juk' href='https://cgit.kde.org/juk.git'>juk</a> <a href='#juk' onclick='toggle("uljuk", this)'>[Show]</a></h3>
<ul id='uljuk' style='display: none'>
<li>Immediately update dynamic/search playlist items upon a change. <a href='http://commits.kde.org/juk/e8c2dbbfce00765335d079bca86abc7e2147f3e6'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/389176'>#389176</a></li>
</ul>
<h3><a name='k3b' href='https://cgit.kde.org/k3b.git'>k3b</a> <a href='#k3b' onclick='toggle("ulk3b", this)'>[Show]</a></h3>
<ul id='ulk3b' style='display: none'>
<li>Ffmpeg: add checks in metadata to string conversion. <a href='http://commits.kde.org/k3b/8969b3735ae1e8d0703a0553381aa685926e8976'>Commit.</a> </li>
</ul>
<h3><a name='kalzium' href='https://cgit.kde.org/kalzium.git'>kalzium</a> <a href='#kalzium' onclick='toggle("ulkalzium", this)'>[Show]</a></h3>
<ul id='ulkalzium' style='display: none'>
<li>KF5NewStuff is only used in moleculeview, conditional on Eigen3 and AvogadroLibs. <a href='http://commits.kde.org/kalzium/dfc9659af7aa94f9801eff171e9bf31792f0bc5a'>Commit.</a> </li>
</ul>
<h3><a name='kate' href='https://cgit.kde.org/kate.git'>kate</a> <a href='#kate' onclick='toggle("ulkate", this)'>[Show]</a></h3>
<ul id='ulkate' style='display: none'>
<li>Revert "Clear konsole command-line before directory change". <a href='http://commits.kde.org/kate/6303cc4a0788d84190db74951fc2db639456a8bc'>Commit.</a> </li>
<li>Make connection unique to not trigger flood of slot calls. <a href='http://commits.kde.org/kate/b04adca2b2bf2460e1ce51377f6d18e6182cc94f'>Commit.</a> </li>
<li>Fix a couple of regressions in the conversion to modern connect(). <a href='http://commits.kde.org/kate/db520750bd82c81e6fed847f8d5a32390a82ecb8'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/388752'>#388752</a></li>
</ul>
<h3><a name='kbreakout' href='https://cgit.kde.org/kbreakout.git'>kbreakout</a> <a href='#kbreakout' onclick='toggle("ulkbreakout", this)'>[Show]</a></h3>
<ul id='ulkbreakout' style='display: none'>
<li>Fix slowdown issue. <a href='http://commits.kde.org/kbreakout/e636c174c83f133259fd34ecc8f7fe689604db17'>Commit.</a> </li>
</ul>
<h3><a name='kcalcore' href='https://cgit.kde.org/kcalcore.git'>kcalcore</a> <a href='#kcalcore' onclick='toggle("ulkcalcore", this)'>[Show]</a></h3>
<ul id='ulkcalcore' style='display: none'>
<li>Remove unused unistd.h include. <a href='http://commits.kde.org/kcalcore/fd8d45c0f77b6817e43819db957d3e0852cb5f4d'>Commit.</a> </li>
<li>Define strncasecmp and strcasecmp on Windows. <a href='http://commits.kde.org/kcalcore/196b24b967e113b892370645c46cbf577f4a3a20'>Commit.</a> </li>
<li>Remove strings.h include. <a href='http://commits.kde.org/kcalcore/07520bfe1d9781f106f1477b55f320471fa7e7dd'>Commit.</a> </li>
<li>Icalformat: remove QByteArray->QString->QByteArray roundtrip. <a href='http://commits.kde.org/kcalcore/72909253ae446a22f73ba8aa2759a57e264ec0b2'>Commit.</a> </li>
</ul>
<h3><a name='kde-dev-scripts' href='https://cgit.kde.org/kde-dev-scripts.git'>kde-dev-scripts</a> <a href='#kde-dev-scripts' onclick='toggle("ulkde-dev-scripts", this)'>[Show]</a></h3>
<ul id='ulkde-dev-scripts' style='display: none'>
<li>Makeobj: fix syntax error when pwd contains a space. <a href='http://commits.kde.org/kde-dev-scripts/fddee611f9879ae2011a05d50543230c75d58b49'>Commit.</a> </li>
</ul>
<h3><a name='kdegraphics-thumbnailers' href='https://cgit.kde.org/kdegraphics-thumbnailers.git'>kdegraphics-thumbnailers</a> <a href='#kdegraphics-thumbnailers' onclick='toggle("ulkdegraphics-thumbnailers", this)'>[Show]</a></h3>
<ul id='ulkdegraphics-thumbnailers' style='display: none'>
<li>Gs thumbnailer: Make it work when gs spits out warning messages. <a href='http://commits.kde.org/kdegraphics-thumbnailers/cfc2e2c32b5525691b5ba1b6eaf91071e4a0c8be'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/388288'>#388288</a></li>
</ul>
<h3><a name='kdenlive' href='https://cgit.kde.org/kdenlive.git'>kdenlive</a> <a href='#kdenlive' onclick='toggle("ulkdenlive", this)'>[Show]</a></h3>
<ul id='ulkdenlive' style='display: none'>
<li>Fix compatibility with MLT >= 6.6.0. <a href='http://commits.kde.org/kdenlive/92b88c61ac552b74fa9e8a8fcf2ed1bf100cdf4a'>Commit.</a> </li>
<li>Fix build with Qt 5.6. <a href='http://commits.kde.org/kdenlive/94c420c85b55d5fabeebc38984f0afcbe3cc9e93'>Commit.</a> </li>
<li>Fix backwards seeking not stoping when reaching clip start. <a href='http://commits.kde.org/kdenlive/ea185dfce692382abe3cb4ab3450db7774237553'>Commit.</a> </li>
<li>Add release version number to AppData. <a href='http://commits.kde.org/kdenlive/3bc52cf369958258b5d3483c52b13a3bfac54f3f'>Commit.</a> </li>
<li>Don't query available widget styles on each startup. <a href='http://commits.kde.org/kdenlive/41a9199800c370b8077083ab04d39d567e660da7'>Commit.</a> </li>
<li>Fix webM encoding (auto replace vorbis by libvorbis if installed). <a href='http://commits.kde.org/kdenlive/5de2633cc59424eb64968534d01ebe4e9186830f'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/388503'>#388503</a></li>
</ul>
<h3><a name='kdepim-addons' href='https://cgit.kde.org/kdepim-addons.git'>kdepim-addons</a> <a href='#kdepim-addons' onclick='toggle("ulkdepim-addons", this)'>[Show]</a></h3>
<ul id='ulkdepim-addons' style='display: none'>
<li>It uses https. <a href='http://commits.kde.org/kdepim-addons/4a8930d4fd1c2e9a309ccc06ef1d8ecd31be53a6'>Commit.</a> </li>
<li>This service uses https now. <a href='http://commits.kde.org/kdepim-addons/8d19518e48964d36d5e3763c13dfd5ae281106a9'>Commit.</a> </li>
</ul>
<h3><a name='kdepim-runtime' href='https://cgit.kde.org/kdepim-runtime.git'>kdepim-runtime</a> <a href='#kdepim-runtime' onclick='toggle("ulkdepim-runtime", this)'>[Show]</a></h3>
<ul id='ulkdepim-runtime' style='display: none'>
<li>Maildir: upload empty email bodies as a whitespace. <a href='http://commits.kde.org/kdepim-runtime/275508c4142ec4748101a63633481a947cc67484'>Commit.</a> </li>
<li>Don't insert invalid google accounts into the access list. <a href='http://commits.kde.org/kdepim-runtime/ea2ec0926f8ecd6101122e582830a45b14ee95ed'>Commit.</a> </li>
<li>Fix duplicated entries in Google Calendar and Tasks resource settings dialog. <a href='http://commits.kde.org/kdepim-runtime/4ff8c2c0d02be91b7b524b933fa86a7ca5943e54'>Commit.</a> </li>
</ul>
<h3><a name='kget' href='https://cgit.kde.org/kget.git'>kget</a> <a href='#kget' onclick='toggle("ulkget", this)'>[Show]</a></h3>
<ul id='ulkget' style='display: none'>
<li>TrayIcon: Explicitly show/hide the main window if requested. <a href='http://commits.kde.org/kget/ccefe1842cb34a530fa18cd878b7a7f56654d99c'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/389663'>#389663</a></li>
<li>[newTransferDialog] Don't explicitly call checkInput() with KIO 5.42+. <a href='http://commits.kde.org/kget/166d94303066b701847847ae8d4ef9427ab27ce5'>Commit.</a> </li>
</ul>
<h3><a name='kleopatra' href='https://cgit.kde.org/kleopatra.git'>kleopatra</a> <a href='#kleopatra' onclick='toggle("ulkleopatra", this)'>[Show]</a></h3>
<ul id='ulkleopatra' style='display: none'>
<li>Fix crash if compliance is not known to gnupg. <a href='http://commits.kde.org/kleopatra/8e417cbfd52f88c4f114595b68e3f17a08c85d02'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/389291'>#389291</a></li>
</ul>
<h3><a name='kmail' href='https://cgit.kde.org/kmail.git'>kmail</a> <a href='#kmail' onclick='toggle("ulkmail", this)'>[Show]</a></h3>
<ul id='ulkmail' style='display: none'>
<li>Fix Bug 388953 - Apply filters on folder doesn't work anymore. <a href='http://commits.kde.org/kmail/e0c1bf6570537086628d455cab22c802d663344d'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/388953'>#388953</a></li>
</ul>
<h3><a name='kmailtransport' href='https://cgit.kde.org/kmailtransport.git'>kmailtransport</a> <a href='#kmailtransport' onclick='toggle("ulkmailtransport", this)'>[Show]</a></h3>
<ul id='ulkmailtransport' style='display: none'>
<li>Fix Bug 388573 - Draft message is confused about mail transport. <a href='http://commits.kde.org/kmailtransport/424f0273dbebd115da6812e0b680fb911f282cd4'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/388573'>#388573</a></li>
</ul>
<h3><a name='kmime' href='https://cgit.kde.org/kmime.git'>kmime</a> <a href='#kmime' onclick='toggle("ulkmime", this)'>[Show]</a></h3>
<ul id='ulkmime' style='display: none'>
<li>Fix bug in parsing dates that fall in the DST "non existant hour". <a href='http://commits.kde.org/kmime/f6dea81ae8b2c306ac60b136fd44a5fd4537e17b'>Commit.</a> </li>
</ul>
<h3><a name='kmix' href='https://cgit.kde.org/kmix.git'>kmix</a> <a href='#kmix' onclick='toggle("ulkmix", this)'>[Show]</a></h3>
<ul id='ulkmix' style='display: none'>
<li>Silence noise: don't use qCWarning for debug output. <a href='http://commits.kde.org/kmix/34795341b7f17a1410e449d7bda22c86fe1b1bae'>Commit.</a> </li>
<li>Prevent window flickering on startup. <a href='http://commits.kde.org/kmix/54e7703d998ec2bc5ee8f553b1bd9e34ccb513ce'>Commit.</a> </li>
</ul>
<h3><a name='knotes' href='https://cgit.kde.org/knotes.git'>knotes</a> <a href='#knotes' onclick='toggle("ulknotes", this)'>[Show]</a></h3>
<ul id='ulknotes' style='display: none'>
<li>Fix strech position. <a href='http://commits.kde.org/knotes/790cada267a7d7adcf51d803bbd7159bedd6af4c'>Commit.</a> </li>
</ul>
<h3><a name='ksmtp' href='https://cgit.kde.org/ksmtp.git'>ksmtp</a> <a href='#ksmtp' onclick='toggle("ulksmtp", this)'>[Show]</a></h3>
<ul id='ulksmtp' style='display: none'>
<li>Fix autotests. <a href='http://commits.kde.org/ksmtp/d498a6ac0c3524f739998d61a4ca93dac2d2f882'>Commit.</a> </li>
<li>SMTP: fix truncated attachment if it contains a line with a single dot. <a href='http://commits.kde.org/ksmtp/6194f93a6af6bfd79bef04611e411b0b760aff49'>Commit.</a> </li>
</ul>
<h3><a name='libkgapi' href='https://cgit.kde.org/libkgapi.git'>libkgapi</a> <a href='#libkgapi' onclick='toggle("ullibkgapi", this)'>[Show]</a></h3>
<ul id='ullibkgapi' style='display: none'>
<li>Fix oauth focus. <a href='http://commits.kde.org/libkgapi/1ad3e82403e2f01e9b44e5e7daa2324c22d66603'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/389263'>#389263</a></li>
<li>Reenable setting the authentication URL. <a href='http://commits.kde.org/libkgapi/c693d40f84e20720a1c2cf5f50934b0436e0ab60'>Commit.</a> </li>
<li>Retrieve OAuth2 token with HTTP socket. <a href='http://commits.kde.org/libkgapi/b87061d781adf31b0bf4b319a4f50e3aff1ec3b2'>Commit.</a> </li>
<li>Check for the correct "kind" strings. <a href='http://commits.kde.org/libkgapi/367a933fc7939f34fe6737ee733d68c4921e9237'>Commit.</a> </li>
<li>Fix token page URL. <a href='http://commits.kde.org/libkgapi/fa572d93cfa463f61432dd92239e747f9642fbae'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/388483'>#388483</a></li>
</ul>
<h3><a name='libksane' href='https://cgit.kde.org/libksane.git'>libksane</a> <a href='#libksane' onclick='toggle("ullibksane", this)'>[Show]</a></h3>
<ul id='ullibksane' style='display: none'>
<li>Fix translations from Sane. <a href='http://commits.kde.org/libksane/fc30bcf15d0d2a58dc1c693e713f508c41f8b28e'>Commit.</a> </li>
</ul>
<h3><a name='messagelib' href='https://cgit.kde.org/messagelib.git'>messagelib</a> <a href='#messagelib' onclick='toggle("ulmessagelib", this)'>[Show]</a></h3>
<ul id='ulmessagelib' style='display: none'>
<li>Add normal text too. So we can switch from html to normal text too. <a href='http://commits.kde.org/messagelib/d777497837e2c2e062774490b93917b3f2214262'>Commit.</a> </li>
</ul>
<h3><a name='okular' href='https://cgit.kde.org/okular.git'>okular</a> <a href='#okular' onclick='toggle("ulokular", this)'>[Show]</a></h3>
<ul id='ulokular' style='display: none'>
<li>Add pdf.gz file save test. <a href='http://commits.kde.org/okular/4e678cdc33f629cb8226cdbe9562864b50608843'>Commit.</a> </li>
<li>Fix saving annotations to gzipped files. <a href='http://commits.kde.org/okular/ad1160be1b370ca512a741c1a12f9eff0ca5848c'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/338742'>#338742</a></li>
<li>Fix crash on save when we had edited forms. <a href='http://commits.kde.org/okular/f656f1b23f7036831c04f9af6cb9eeceb5d3222c'>Commit.</a> </li>
<li>Parttest: make the mouse selection be a few steps. <a href='http://commits.kde.org/okular/8a0f70cb165a1f2fdf0fc831b09e4db42584b5d7'>Commit.</a> </li>
<li>Use correct arrow cursor for Annotations' close buttons. <a href='http://commits.kde.org/okular/1e80804c1b603bc7849dadc74661f6447b1571d7'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/384381'>#384381</a></li>
<li>Improve parttest a bit. <a href='http://commits.kde.org/okular/60daf5c2d87bae0bfcf628eda774f5c6ec7fc22c'>Commit.</a> </li>
<li>Fix regression due to more QTemporaryFile behaviour changes. <a href='http://commits.kde.org/okular/acd3c81e23464ff66dc67c01d62971c01e70a2eb'>Commit.</a> </li>
<li>Fix crash when exporting. <a href='http://commits.kde.org/okular/4a80d3f963b9acf901ed98db7f24fd354e8e986f'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/389216'>#389216</a></li>
<li>Djvu: Fix printing. <a href='http://commits.kde.org/okular/dcf544f8264967ac4cee90452714afa58dfd8a53'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/388514'>#388514</a></li>
</ul>
<h3><a name='print-manager' href='https://cgit.kde.org/print-manager.git'>print-manager</a> <a href='#print-manager' onclick='toggle("ulprint-manager", this)'>[Show]</a></h3>
<ul id='ulprint-manager' style='display: none'>
<li>Initialize the translations of the ppd. <a href='http://commits.kde.org/print-manager/893222767251a4b22953afe6f845f03d421d4fa9'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/387494'>#387494</a></li>
</ul>
<h3><a name='umbrello' href='https://cgit.kde.org/umbrello.git'>umbrello</a> <a href='#umbrello' onclick='toggle("ulumbrello", this)'>[Show]</a></h3>
<ul id='ulumbrello' style='display: none'>
<li>Fix writing '44' as true boolean value into xmi file for attributes usesdiagramfillcolor and usesdiagramusefillcolor. <a href='http://commits.kde.org/umbrello/0202da0eaa71d03b7f71e390bf21a66749baaae4'>Commit.</a> </li>
<li>Reorder 'useDiagram' related variables alphabetical. <a href='http://commits.kde.org/umbrello/3e4f3b1137cf3afe4421d1c5db6b95fdeebe1218'>Commit.</a> </li>
<li>Fix 'Naming of new datatypes'. <a href='http://commits.kde.org/umbrello/530af55f967a7db866ad2ce4fc880015d45a850e'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/389353'>#389353</a></li>
<li>Fix 'Font is always bold when model is opened, although default font is regular style'. <a href='http://commits.kde.org/umbrello/34dc002381252db1f4e7c4cce0ef50131b0663f5'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/389357'>#389357</a></li>
<li>Fix 'Bug in Source Code Editor'. <a href='http://commits.kde.org/umbrello/7547fc9a1dbf522c5d4fbeb2b1ab9e0c4e3566e2'>Commit.</a> </li>
<li>Extract messages from umbrelloui.rc.cmake. <a href='http://commits.kde.org/umbrello/51bddb3f55102393017207d4d290b7759d14ddda'>Commit.</a> </li>
</ul>