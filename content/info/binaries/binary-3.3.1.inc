<ul>
<!-- CONECTIVA LINUX -->
<li><a href="http://www.conectiva.com/">Conectiva Linux</a>:
  <ul type="disc">
    <li>
      CL10:
      <a href="http://download.kde.org/binarydownload.html?url=/stable/3.3.1/Conectiva/cl10/">Intel i386</a>
    </li>
  </ul>
</li>

<!--   REDHAT LINUX -->
<li>
  <a href="http://www.redhat.com/">Red Hat</a>
  :
  <ul type="disc">
    <li>
        <a href="http://download.kde.org/binarydownload.html?url=/stable/3.3.1/RedHat/Fedora2/noarch/">Language
        packages</a> (all versions and architectures)
    </li>
    <li>
      Fedora 2: <a href="http://download.kde.org/binarydownload.html?url=/stable/3.3.1/RedHat/Fedora2/i386/">Intel i386</a>
    </li>
  </ul>
</li>

<!-- SLACKWARE LINUX -->
<li>
  <a href="http://www.slackware.org/">Slackware</a> (Unofficial contribution)
 (<a href="http://download.kde.org/binarydownload.html?url=/stable/3.3.1/contrib/Slackware/10.0/README">README</a>)
   :
   <ul type="disc">
    <li>
        <a href="http://download.kde.org/binarydownload.html?url=/stable/3.3.1/contrib/Slackware/noarch/">Language packages</a>
    </li>
     <li>
       10.0: <a href="http://download.kde.org/binarydownload.html?url=/stable/3.3.1/contrib/Slackware/10.0/">Intel i486</a>
     </li>
   </ul>
</li>

<!-- SOLARIS -->
<li>
  <a href="http://www.sun.com/solaris/">SUN Solaris</a> (Unofficial contribution):
  <ul type="disc">
    <li>
      8: <a href="http://download.kde.org/binarydownload.html?url=/stable/3.3.1/contrib/Solaris/FORTE/8/">Sparc (FORTE)</a>
    </li>
    <li>
      9: <a href="http://download.kde.org/binarydownload.html?url=/stable/3.3.1/contrib/Solaris/FORTE/9/">Sparc (FORTE)</a>
    </li>
    <li>
      9: <a href="http://download.kde.org/binarydownload.html?url=/stable/3.3.1/contrib/Solaris/GCC/9/">Sparc (GCC)</a>
    </li>
  </ul>
</li>


<!--   SUSE LINUX -->
<li>
  <a href="http://www.suse.com/">SuSE Linux</a>
  (<a href="http://download.kde.org/binarydownload.html?url=/stable/3.3.1/SuSE/README">README</a>)
  :
  <ul type="disc">
    <li>
        <a href="http://download.kde.org/binarydownload.html?url=/stable/3.3.1/SuSE/noarch/">Language
        packages</a> (all versions and architectures)
    </li>
    <li>
      9.1:
      <a href="http://download.kde.org/binarydownload.html?url=/stable/3.3.1/SuSE/ix86/9.1/">Intel i586</a> and
      <a href="http://download.kde.org/binarydownload.html?url=/stable/3.3.1/SuSE/x86_64/9.1/">AMD x86-64</a>
    </li>
    <li>
      9.0:
      <a href="http://download.kde.org/binarydownload.html?url=/stable/3.3.1/SuSE/ix86/9.0/">Intel i586</a> and
      <a href="http://download.kde.org/binarydownload.html?url=/stable/3.3.1/SuSE/x86_64/9.0/">AMD x86-64</a>
    </li>
    <li>
      8.2:
      <a href="http://download.kde.org/binarydownload.html?url=/stable/3.3.1/SuSE/ix86/8.2/">Intel i586</a>
    </li>
  </ul>
</li>

<!-- YOPER LINUX -->
<li>
  <a href="http://www.yoper.com/">Yoper</a>:
  <ul type="disc">
    <li>
      V2:
      <a href="http://download.kde.org/binarydownload.html?url=/stable/3.3.1/Yoper/">Intel i686 rpm</a>
    </li>
  </ul>
</li>

</ul>
