---
aliases:
- ../../kde-frameworks-5.46.0
date: 2018-05-12
layout: framework
libCount: 70
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---

### Baloo

- Avoid infinite loops when fetching the URL from DocumentUrlDB (bug 393181)
- Add Baloo DBus signals for moved or removed files
- Install pri file for qmake support &amp; document it in metainfo.yaml
- baloodb: Add clean command
- balooshow: Colorize only when attached to terminal
- Remove FSUtils::getDirectoryFileSystem
- Avoid hardcoding of filesystems supporting CoW
- Allow disabling of CoW to fail when not supported by filesystem
- databasesanitizer: Use flags for filtering
- Fix merging of terms in the AdvancedQueryParser
- Use QStorageInfo instead of a homegrown implementation
- sanitizer: Improve device listing
- Immediately apply termInConstruction when term is complete
- Handle adjacent special characters correctly (bug 392620)
- Add test case for parsing of double opening '((' (bug 392620)
- Use statbuf consistently

### Breeze Icons

- Add plasma-browser-integration system tray icon
- Add Virt-manager icon thanks to ndavis
- Add video-card-inactive
- overflow-menu as view-more-symbolic, and horizontal
- Use the more appropriate "two sliders" icon for "configure"

### Extra CMake Modules

- Include FeatureSummary before calling set_package_properties
- Don't install plugins within lib on android
- Make it possible to build several apk out of a project
- Check if the application androiddeployqt package has a main() symbol

### KCompletion

- [KLineEdit] Use Qt's built-in clear button functionality
- Fix KCompletionBox on wayland

### KCoreAddons

- [KUser] Check whether .face.icon is actually readable before returning it
- Make KJob signals public so Qt5 connect syntax can work

### KDeclarative

- Load NV graphics reset based on config
- [KUserProxy] Adjust to accounts service (bug 384107)
- Plasma mobile optimizations
- Make room for footer and header
- new resize policy (bug 391910)
- support actions visibility
- Support nvidia reset notifications in QtQuickViews

### KDED

- Add platform detection and adjustment to kded (automatic setting of $QT_QPA_PLATFORM)

### KFileMetaData

- Add description and purpose to Xattr dep
- extractors: Hide warnings from system headers
- fix detection of taglib when compiling for Android
- Install pri file for qmake support &amp; document it in metainfo.yaml
- Make concatenated strings wrappable
- ffmpegextractor: Silence deprecation warnings
- taglibextractor: Fix empty genre bug
- handle more tags in taglibextractor

### KHolidays

- holidays/plan2/holiday_sk_sk - Teacher's Day fix (bug 393245)

### KI18n

- [API dox] New UI marker @info:placeholder
- [API dox] New UI marker @item:valuesuffix
- Don't need to run previous iterations commands again (bug 393141)

### KImageFormats

- [XCF/GIMP loader] Raise maximimum allowed image size to 32767x32767 on 64 bit platforms (bug 391970)

### KIO

- Thumbnail smooth scaling in filepicker (bug 345578)
- KFileWidget: Perfectly align filename widget with icon view
- KFileWidget: Save places panel width also after hiding panel
- KFileWidget: Prevent places panel width from growing 1px iteratively
- KFileWidget: Disable zoom buttons once reached minimum or maximum
- KFileWidget: Set minimum size for zoom slider
- Don't select file extension
- concatPaths: process empty path1 correctly
- Improve grid icon layout in filepicker dialog (bug 334099)
- Hide KUrlNavigatorProtocolCombo if there is just one protocol supported
- Only show supported schemes in KUrlNavigatorProtocolCombo
- Filepicker reads thumbs preview from Dolphin settings (bug 318493)
- Add Desktop and Downloads to the default list of Places
- KRecentDocument now stores QGuiApplication::desktopFileName instead of applicationName
- [KUrlNavigatorButton] Also don't stat MTP
- getxattr takes 6 parameters in macOS (bug 393304)
- Add a "Reload" menu item to KDirOperator's context menu (bug 199994)
- Save the dialog view settings even when canceling (bug 209559)
- [KFileWidget] Hardcode example user name
- Don't show top "Open With" app for folders; only for files
- Detect incorrect parameter in findProtocol
- Use text "Other Application..." in "Open With" submenu
- Correctly encode URL of thumbnails (bug 393015)
- Tweak column widths in tree view of file open/save dialogs (bug 96638)

### Kirigami

- Don't warn when using Page {} outside of a pageStack
- Rework InlineMessages to address a number of issues
- fix on Qt 5.11
- base on units for toolbutton size
- color close icon on hover
- show a margin under the footer when needed
- fix isMobile
- also fade on open/close anim
- include the dbus stuff only on unix-non android, non apple
- watch the tabletMode from KWin
- on desktop mode show actions on hover (bug 364383)
- handle in the top toolbar
- use a gray close button
- less applicationwindow dependency
- less warnings without applicationwindow
- work correctly without applicationWindow
- Don't have a non-integral size on separators
- Don't show the actions if they are disabled
- checkable FormLayout items
- use different icons in the color set example
- include icons only on android
- make it work with Qt 5.7

### KNewStuff

- Fix double margins around DownloadDialog
- Fix hints in UI files about subclasses of custom widgets
- Don't offer qml plugin as a link target

### KPackage Framework

- use KDE_INSTALL_DATADIR instead of FULL_DATADIR
- Add donate urls to test data

### KPeople

- Fix PersonSortFilterProxyModel filtering

### Kross

- Also make installation of translated docs optional

### KRunner

- DBus runner servicename wildcard support

### KTextEditor

- optimization of KTextEditor::DocumentPrivate::views()
- [ktexteditor] much faster positionFromCursor
- Implement single click on line number to select line of text
- Fix missing bold/italic/... markup with modern Qt versions (&gt;= 5.9)

### Plasma Framework

- Fix not shown event marker in calendar with air &amp; oxygen themes
- Use "Configure %1..." for text of applet configure action
- [Button Styles] Fill height and vertical align (bug 393388)
- add video-card-inactive icon for system tray
- correct look for flat buttons
- [Containment Interface] Don't enter edit mode when immutable
- make sure largespacing is perfect multiple of small
- call addContainment with proper paramenters
- Don't show the background if Button.flat
- ensure the containment we created has the activity we asked for
- add a version containmentForScreen with activity
- Don't alter memory management to hide an item (bug 391642)

### Purpose

- Make sure we give some vertical space to configuration plugins
- Port KDEConnect plugin config to QQC2
- Port AlternativesView to QQC2

### QQC2StyleBridge

- export layout paddings from qstyle, start from Control
- [ComboBox] Fix mouse wheel handling
- make the mousearea not interfere with controls
- fix acceptableInput

### Solid

- Update mount point after mount operations (bug 370975)
- Invalidate property cache when an interface is removed
- Avoid creating duplicate property entries in the cache
- [UDisks] Optimize several property checks
- [UDisks] Correct handling of removable file systems (bug 389479)

### Sonnet

- Fix remove enable/disable button
- Fix enable/disable add button
- Look into subdirectories for dictionaries

### Syntax Highlighting

- Update project URL
- 'Headline' is a comment, so base it on dsComment
- Add highlighting for GDB command listings and gdbinit files
- Add syntax highlighting for Logcat

### Security information

The released code has been GPG-signed using the following key:
pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt;
Primary key fingerprint: 53E6 B47B 45CE A3E0 D5B7 4577 58D0 EE64 8A48 B3BB

You can discuss and share ideas on this release in the comments section of <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>the dot article</a>.
