---
aliases:
- ../../plasma-5.14.0
changelog: 5.13.5-5.14.0
date: 2018-10-09
layout: plasma
title: 'KDE Plasma 5.14: Reliable, Light and Innovative.'
figure:
  src: /announcements/plasma/5/5.14.0/plasma-5.14.png
---

{{% i18n_date %}}

{{% i18n_var "Today KDE launches the first release of Plasma %[1]s." "5.14" %}}

Plasma is KDE's lightweight and full featured Linux desktop. For the last three months we have been adding features and fixing bugs and now invite you to install Plasma 5.14.

A lot of work has gone into improving Discover, Plasma's software manager, and, among other things, we have added a Firmware Update feature and many subtle user interface improvements to give it a smoother feel. We have also rewritten many effects in our window manager KWin and improved it for slicker animations in your work day. Other improvements we have made include a new Display Configuration widget which is useful when giving presentations.

## New in Plasma 5.14

### New Features

{{<figure src="/announcements/plasma/5/5.14.0/kscreen-presentation-plasmoid.png" alt="Display Configuration Widget" caption="Display Configuration Widget" width="600px" >}}

- There's a new Display Configuration widget for screen management which is useful for presentations.
- The Audio Volume widget now has a built in speaker test feature moved from Phonon settings.
- The Network widget now works for SSH VPN tunnels again.
- Switching primary monitor when plugging in or unplugging monitors is now smoother.
- The lock screen now handles user-switching for better usability and security.
- You can now import existing encrypted files from a Plasma Vault.
- The Task Manager implements better compatibility with LibreOffice.

{{<figure src="/announcements/plasma/5/5.14.0/system-monitor-tools.png" alt="System Monitor Tools" caption="System Monitor Tools" width="600px" >}}

- The System Monitor now has a 'Tools' menu full of launchers to handy utilities.
- The Kickoff application menu now switches tabs instantly on hover.

{{<figure src="/announcements/plasma/5/5.14.0/panel-widget-options-old.png" alt="Old Panel Widget Edit Menu" width="600px" >}}

{{<figure src="/announcements/plasma/5/5.14.0/panel-widget-options-new.png" alt="New Slicker Panel Widget Edit Menu" caption="Panel Widget Edit Menu Old and New Style" width="600px" >}}

- Widget and panels get consistent icons and other user interface improvements.

{{<figure src="/announcements/plasma/5/5.14.0/logout-warn.png" alt="Logout Warning" caption="Logout Warning" width="600px" >}}

- Plasma now warns on logout when other users are logged in.
- The Breeze widget theme has improved shadows.
- <a href='https://blog.broulik.de/2018/03/gtk-global-menu/'>The Global menu now supports GTK applications.</a> This was a 'tech preview' in 5.13, but it now works out of the box in 5.14.

{{<figure src="/announcements/plasma/5/5.14.0/discover.png" alt="Plasma Discover" caption="Plasma Discover" width="600px" >}}

### Plasma Discover

Discover, our software and add-on installer, has more features and improves its look and feel.

- Discover gained <i>fwupd</i> support, allowing it to upgrade your computer's firmware.
- It gained support for Snap channels.
- Discover can now display and sort apps by release date.
- You can now see an app's package dependencies.
- When Discover is asked to install a standalone Flatpak file but the Flatpak backend is not installed, it now offers to first install the backend for you.
- Discover now tells you when a package update will replace some packages with other ones.
- We have added numerous minor user interface improvements: update button are disabled while checking for updates, there is visual consistency between settings and the update pages, updates are sorted by completion percentage, we have improved the layout of updates page and updates notifier plasmoid, etc..
- We have improved reliability and stability through a bunch of bug fixes.

{{< video src="/announcements/plasma/5/5.14.0/kwin-glide-effect.mp4" caption="Improved KWin Glide Effect" >}}

### KWin and Wayland:

- We fixed copy-paste between GTK and non-GTK apps on Wayland.
- We fixed non-centered task switchers on Wayland.
- We have improved pointer constraints.
- There are two new interfaces, XdgShell and XdgOutput, for integrating more apps with the desktop.
- We have considerably improved and polished KWin effects throughout, including completely rewriting the Dim Inactive effect, adding a new scale effect, rewriting the Glide effect, and more.

### Bugfixes

We fixed many bugs, including:

- Blurred backgrounds behind desktop context menus are no longer visually corrupted.
- It's no longer possible to accidentally drag-and-drop task manager buttons into app windows.
