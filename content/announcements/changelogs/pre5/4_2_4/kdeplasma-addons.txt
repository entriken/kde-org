------------------------------------------------------------------------
r962556 | fabo | 2009-05-02 14:48:48 +0000 (Sat, 02 May 2009) | 5 lines

Backport to KDE 4.2.x
don't paint on the cache image
add svgz to file filters
BUG: 190320

------------------------------------------------------------------------
r962593 | lueck | 2009-05-02 16:03:00 +0000 (Sat, 02 May 2009) | 1 line

use the right catalog to fetch the existing translations
------------------------------------------------------------------------
r966865 | jriddell | 2009-05-12 01:15:01 +0000 (Tue, 12 May 2009) | 1 line

make it compile with Phonon built from Qt
------------------------------------------------------------------------
r966937 | jriddell | 2009-05-12 10:11:43 +0000 (Tue, 12 May 2009) | 1 line

revert phonon header change, Qt creates Global now, http://qt.gitorious.org/qt/qt/commit/5299240db14579960358edeebfc72fcef905af13
------------------------------------------------------------------------
r968028 | aacid | 2009-05-14 18:57:47 +0000 (Thu, 14 May 2009) | 2 lines

set the buddy otherwise the & is shown as & instead as of accelerator

------------------------------------------------------------------------
r968157 | scripty | 2009-05-15 07:45:27 +0000 (Fri, 15 May 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r972516 | scripty | 2009-05-25 07:51:42 +0000 (Mon, 25 May 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r973296 | cfeck | 2009-05-26 19:07:42 +0000 (Tue, 26 May 2009) | 4 lines

Fix QPixmap usage in contacts runner (backport r973290)

CCBUG: 181838

------------------------------------------------------------------------
r973454 | scripty | 2009-05-27 08:47:16 +0000 (Wed, 27 May 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
