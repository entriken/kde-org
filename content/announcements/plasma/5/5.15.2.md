---
aliases:
- ../../plasma-5.15.2
changelog: 5.15.1-5.15.2
date: 2019-02-26
layout: plasma
figure:
  src: /announcements/plasma/5/5.15.0/plasma-5.15-apps.png
asBugfix: true
---

- The 'Module Help' button gets enabled when help is available. <a href="https://commits.kde.org/kinfocenter/585b92af8f0e415ffb8c2cb6a4712a8fe01fbbc4">Commit.</a> Fixes bug <a href="https://bugs.kde.org/392597">#392597</a>. Phabricator Code review <a href="https://phabricator.kde.org/D19187">D19187</a>
- [about-distro] let distributions choose VERSION_ID or VERSION. <a href="https://commits.kde.org/kinfocenter/99f10f1e5580f373600478746016c796ac55e3f9">Commit.</a> Phabricator Code review <a href="https://phabricator.kde.org/D19226">D19226</a>
- xdg-desktop-portal-kde: Fix selection of multiple files. <a href="https://commits.kde.org/xdg-desktop-portal-kde/8ef6eb3f7a950327262881a5f3b21fac1d3064c6">Commit.</a> Fixes bug <a href="https://bugs.kde.org/404739">#404739</a>
