-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA1


KDE Security Advisory: plain text password exposure
Original Release Date: 2004-12-09
URL: http://www.kde.org/info/security/advisory-20041209-1.txt

0. References

        http://www.sec-consult.com/index.php?id=118


1. Systems affected:

        All KDE 3.2.x releases, KDE 3.3.0, KDE 3.3.1 and KDE 3.3.2.


2. Overview:

        Daniel Fabian notified the KDE security team about a possible
        privacy issue in KDE. When creating a link to a remote file
        from various applications including Konqueror, the resulting
        URL may contain the authentication credentials used to access
        that remote resource. This includes, but is not limited to
        browsing SMB ("Samba") shares. Further investigation revealed
        unnecessary exposure of authentication credentials by the
        SMB ("Samba") protocol handler. 

        The link reference file, which is a file with the extension
        ".desktop", is a plain text configuration file that is created
        with default access permissions, depending on the users' umask
        this could include world read permission. Usually the URL saved
        in this .desktop file only contains the password if the user
        manually entered it this way. The SMB protocol handler however
        unnecessarily exposes authentication credentials by always
        including this information in the URL that it generates.

        The KDE team provides patches which will unconditionally
        remove the password from the authentication credentials
        before creating the link reference file and that fix the SMB
        protocol handler to not unnecessarily include passwords
        in URLs Authentication credentials can then be stored in
        KWallet instead.


3. Impact:

        A user may inadvertly expose passwords provided for SMB shares
        or other passwords that were entered as part of an URL.


4. Solution:

        Users should verify that links to remote files do not contain
        password information by right-clicking the link and selecting
        the "Properties" option and then selecting the "URL" tab.

        The KDE 3.3.2 release contains most fixes already, therefore
        the patch set to apply to KDE 3.3.2 is less than for other
        KDE versions.

        Source code patches have been made available which fix these
        vulnerabilities. Contact your OS vendor / binary package provider
        for information about how to obtain updated binary packages.


5. Patch:

        Patches for KDE 3.3.1 are available from 
        ftp://ftp.kde.org/pub/kde/security_patches :

        501852d12f82aebe7eb73ec5d96c9e6d  post-3.3.1-kdebase-smb.diff
        5b9c1738f2de3f00533e376eb64c7137  post-3.3.1-kdelibs-khtml.diff
        f287c900c637af2452c7a554f2df166f  post-3.3.1-kdelibs-kio.diff


        Patch for KDE 3.3.2 is available from
        ftp://ftp.kde.org/pub/kde/security_patches :

        d3658e90acec6ff140463ed2fd0e7736  post-3.3.2-kdelibs-kio.diff


        Patches for KDE 3.2.3 are available from
        ftp://ftp.kde.org/pub/kde/security_patches :

        d080d9acf4d2abc5f91ccec8fc463568  post-3.2.3-kdebase-smb.diff
        d79d1717b4bc0b3891bacaaf37deade0  post-3.2.3-kdelibs-khtml.diff
        94e76ec98cd58ce27cad8f886d241986  post-3.2.3-kdelibs-kio.diff




-----BEGIN PGP SIGNATURE-----
Version: GnuPG v1.2.5 (GNU/Linux)

iD8DBQFBt618vsXr+iuy1UoRArYpAJ9WwYla1w0zwLZ5h5aC+loKcsYl2wCcCx0y
VXT0cntKNdpheNgZcKGYnug=
=bTjQ
-----END PGP SIGNATURE-----
