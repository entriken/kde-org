---
title: "Getting KDE Software"
description: "Learn how to install KDE Software on your device."
layout: list
---

As KDE produces Free Software, you can download and install KDE programs
in most cases free of charge. On this page you can read about
various ways to get KDE software.

## General Information

KDE software consists of a large number of individual applications,
Plasma, a desktop workspace to run these applications, and
KDE Frameworks, a set of libraries they are build upon. You can run KDE applications
just fine on any desktop environment.
KDE applications are built to integrate well with your
system's components. By using also Plasma, you get
even better integration of your applications with the working
environment while lowering system resource needs.

Plasma runs fine on relatively recent PCs, but it also works
great on older machines with more constrained resources.

To install our software on Linux pick a distro and grab the packages you want.
See <a href="https://community.kde.org/Get_KDE_Software_on_Your_Linux_Distro">Get
KDE Software on your Linux Distro</a>.

### KDE Plasma - Our Desktop

Installing Plasma on your system is easy if you use the major Linux
distributions; it's included in nearly all of them! A partial list can be found here:
<a href="https://community.kde.org/Distributions"><b>Distributions Shipping KDE Software</b></a>

There are instructions available for
<strong><a href="https://techbase.kde.org/Getting_Started/Build">building
the source code</a></strong>. Building from source code is not recommended
for normal users as it requires knowledge of software development
tools.

### KDE Applications

KDE applications run fine on most Linux,
BSD systems. In these cases, the recommended way to
install a KDE application is to use your operating system's
software management tool to install it. It comes included
with most Free operating systems.

### KDE Frameworks

The KDE Frameworks are a set of libraries built on the Qt framework, providing everything from simple utility classes to integrated solutions for common requirements of desktop applications.
You can download them at <a href="https://download.kde.org/stable/frameworks/">https://download.kde.org/stable/frameworks/</a> or read the <a href="https://develop.kde.org/products/frameworks/">online documentation</a>.

## Windows and macOS

An installer for some KDE applications on Windows is available from <a href="https://community.kde.org/Windows">the KDE on Windows project</a>.

For Apple macOS systems, some applications have been made available by their developers. A list of these can be found on the <a href="https://community.kde.org/Mac">Community Wiki</a>.

## Android

KDE provides some software for Android under the <a href="https://play.google.com/store/apps/dev?id=4758894585905287660">KDE Community account on the Google Play Store</a>.
