---
aliases:
- ../changelog4_7_2to4_7_3
hidden: true
title: KDE 4.7.3 Changelog
---

<h2>Changes in KDE 4.7.3</h2>
    <h3 id="kdelibs"><a name="kdelibs">kdelibs</a><span class="allsvnchanges"> [ <a href="4_7_3/kdelibs.txt">all SVN changes</a> ]</span></h3>    
    <div class="module" style="padding-left: 20px;">
      <h4><a name="solid">solid</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix normal">Do not repeat the Vendor name if the model starts with it. See Git commit <a href="http://commits.kde.org/solid/2e500b5f5edd177c8af187c9bfe077815e65b6e7">2e500b5</a>. </li>
      </ul>
      </div>
    </div>
    <h3 id="kdeworkspace"><a name="kdeworkspace">kdeworkspace</a><span class="allsvnchanges"> [ <a href="4_7_3/kdeworkspace.txt">all SVN changes</a> ]</span></h3>    
    <div class="module" style="padding-left: 20px;">
        <h4><a name="kwin">Kwin</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Improvements:</em>
      <ul>
          <li class="improvement">Check for EGL_image OR EGL_image_base + EGL_image_pixmap. See Git commit <a href="http://commits.kde.org/kde-workspace/4085ebdc2d9c849f27c812bf180aa9adb42f2901">4085ebd</a>. </li>
          <li class="improvement">Opt-out of flipping with GLES. See Git commit <a href="http://commits.kde.org/kde-workspace/f0d8fce3450935c773dc49f70a232f53436eef36">f0d8fce</a>. </li>
          <li class="improvement">Save a branch in the main shader. See Git commit <a href="http://commits.kde.org/kde-workspace/c10419b6f85af4a6d2c7539c594abc7224535c03">c10419b</a>. </li>
          <li class="improvement">Add support for EGL_NV_post_sub_buffer See Git commit <a href="http://commits.kde.org/kde-workspace/c8306ed8e1c220c3a49c452da43c1eb165151905">c8306ed</a>. </li>
      </ul>
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix normal">Fix xrender scale+shadowed toplevel geometry. See Git commit <a href="http://commits.kde.org/kde-workspace/5980946806a09ce44ad2015ac7f6be56b6b68f88">5980946</a>. </li>
        <li class="bugfix normal">Don't react on mouse release events when the release happens outside the button area in Aurorae Decoration Theme Engine. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=283236">283236</a>.  See Git commit <a href="http://commits.kde.org/kde-workspace/d4b83c3b37d3671ce0707adce26a4c1483a5b406">d4b83c3</a>. </li>
        <li class="bugfix normal">Ensure that always one shader is on the ShaderStack with GL2. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=283609">283609</a>.  See Git commit <a href="http://commits.kde.org/kde-workspace/cab2fee5c934ade31d457d470b042b578f74b9d8">cab2fee</a>. </li>
        <li class="bugfix normal">Generate texture coordinates for limited NPOT support. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=269576">269576</a>.  See Git commit <a href="http://commits.kde.org/kde-workspace/b22f64f95509c0b9f343c64e60bfcbe2b08d0a0a">b22f64f</a>. </li>
      </ul>
      </div>
    </div>
    <h3 id="kdeedu"><a name="kdeedu">kdeedu</a><span class="allsvnchanges"> [ <a href="4_7_3/kdeedu.txt">all SVN changes</a> ]</span></h3>    
    <div class="module" style="padding-left: 20px;">
      <h4><a href="http://edu.kde.org/applications/all/kgeography" name="kgeography">KGeography</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix normal">Update Libya flag. See Git commit <a href="http://commits.kde.org/KGeography/12f9f4778a5a52005fad897ec5d1ac9a43f484cc">12f9f47</a>. </li>
      </ul>
      </div>
    </div>
    <h3 id="kdegraphics"><a name="kdegraphics">kdegraphics</a><span class="allsvnchanges"> [ <a href="4_7_3/kdegraphics.txt">all SVN changes</a> ]</span></h3>    
    <div class="module" style="padding-left: 20px;">
      <h4><a href="http://okular.kde.org" name="okular">Okular</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix normal">Fix compilation issue that made the plucker backend not compile silently. See Git commit <a href="http://commits.kde.org/Okular/41c572463c33e711ebbb094b9989868a9564e467">41c5724</a>. </li>
        <li class="bugfix normal">Fix creation of the Table Of Contents tree for some DVI files. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=283447">283447</a>.  See Git commit <a href="http://commits.kde.org/Okular/83df127ac057d98e6e4f67e07074675c8830198a">83df127</a>. </li>
        <li class="bugfix normal">Fix antialiasing problem when reloading some files. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=284464">284464</a>.  See Git commit <a href="http://commits.kde.org/Okular/eb705417fac06ad7a29b9f05f00c825ded886334">eb70541</a>. </li>
      </ul>
      </div>
    </div>
    <h3 id="kdeutils"><a name="kdeutils">kdeutils</a><span class="allsvnchanges"> [ <a href="4_7_3/kdeutils.txt">all SVN changes</a> ]</span></h3>    
    <div class="module" style="padding-left: 20px;">
      <h4><a href="http://utils.kde.org/projects/kgpg" name="kgpg">KGpg</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix crash">Check for GnuPG errors on startup. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=269459">269459</a>.  See Git commit <a href="http://commits.kde.org/kgpg/db0b4afa84317182f48b6d8c3f603b0c96f5aff3">db0b4af</a>. </li>
        <li class="bugfix normal">Ask for confirmation before overwriting a file when exporting a key. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=283215">283215</a>.  See Git commit <a href="http://commits.kde.org/kgpg/c97530d36fc24221b8ce99abd54b79ebfff495fb">c97530d</a>. </li>
        <li class="bugfix crash">Fix crash when generating a new key fails. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=283357">283357</a>.  See Git commit <a href="http://commits.kde.org/kgpg/98d7865250cc8ce3016101d54facdbe01c12983d">98d7865</a>. </li>
        <li class="bugfix normal">Fix confusing message when generating a new key fails. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=285081">285081</a>.  See Git commit <a href="http://commits.kde.org/kgpg/b416fabb00fb53e5f4c0774247b640dd3f6a014d">b416fab</a>. </li>
        <li class="bugfix normal">Make buttons to change key expiration and passphrase work again. See Git commit <a href="http://commits.kde.org/kgpg/aa99500f40bb25337b28a1056020b26cfe6a9972">aa99500</a>. </li>
      </ul>
      </div>
    </div>