---
aliases:
- ../announce-applications-18.08.1
changelog: true
date: 2018-09-06
description: KDE Ships KDE Applications 18.08.1
layout: application
title: KDE Ships KDE Applications 18.08.1
version: 18.08.1
---

{{% i18n_var "September 6, 2018. Today KDE released the first stability update for <a href='%[1]s'>KDE Applications 18.08</a>. This release contains only bugfixes and translation updates, providing a safe and pleasant update for everyone." "../18.08.0" %}}

More than a dozen recorded bugfixes include improvements to Kontact, Cantor, Gwenview, Okular, Umbrello, among others.

Improvements include:

- The KIO-MTP component no longer crashes when the device is already accessed by a different application
- Sending mails in KMail now uses the password when specified via password prompt
- Okular now remembers the sidebar mode after saving PDF documents