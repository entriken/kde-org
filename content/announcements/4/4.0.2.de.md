---
aliases:
- ../announce-4.0.2.de
date: '2008-03-05'
title: KDE 4.0.2 – Pressemitteilung
---

<h3 align="center">
  Das KDE-Projekt veröffentlicht die zweite Übersetzungs- und Wartungsversion der führenden, freien Arbeitsumgebung.
</h3>

<p align="justify">
  Die Gemeinschaft der KDE-Entwickler veröffentlicht die zweite Übersetzungs- und Wartungsversion der 4.0-Entwicklungsreihe, mit neuen Funktionen für die Plasma-Oberfläche und zahlreichen Fehlerbehebungen, Geschwindigkeitsverbesserungen sowie Aktualisierungen der Übersetzungen.
</p>

<p align="justify">
  Die <a href="http://www.kde.org/">Gemeinschaft der KDE-Entwickler</a> gab heute die sofortige Verfügbarkeit der KDE-Version 4.0.2, der zweiten Fehlerbereinigungs- und Wartungsversion der am weitesten entwickelten und leistungsfähigsten, freien Arbeitsumgebung, bekannt. KDE 4.0.2 bring von Haus aus eine Standard-Arbeitsoberfläche und viele weitere Pakete mit sich, wie zum Beispiel Pakete zur Administration, Netzwerken, Lernprogramme, Werkzeuge, Multimedia, Spiele, Grafik, Webentwicklung und noch viele mehr. Die durch zahlreiche Preise ausgezeichneten Werkzeuge und Programme von KDE sind in mehr als 49 verschiedenen Sprachen verfügbar.
</p>
<p align="justify">
 KDE, inklusive all seiner Bibliotheken und Anwendungen, ist für jedermann gratis unter freien Lizenzen verfügbar (&bdquo;Open Source&ldquo;). KDE kann im Quelltext und in verschiedenen Binärformaten von <a
href="http://download.kde.org/stable/4.0.2/">http://download.kde.org</a> heruntergeladen, oder auf <a href="http://www.kde.org/download/cdrom">CD-ROM</a> bestellt werden, oder es ist bereits beim Kauf einer der <a href="http://www.kde.org/download/distributions">führenden GNU/Linux- und UNIX-Distributionen</a>, die derzeit verfügbar sind, enthalten.
</p>

<h4>
  <a id="changes">Verbesserungen</a>
</h4>
<p align="justify">

KDE 4.0.2 ist eine so genannte Wartungsversion, welche Behebungen von Fehlern die mittels dem <a href="http://bugs.kde.org/">KDE-System zur Fehlerverfolgung</a> berichtet wurden, sowie Verbesserungen von bereits existierenden und neue Übersetzungen mit sich bringt. Für KDE 4.0.2 wurde vom KDE-Dokumentations- und KDE-Übersetzungsteam eine Ausnahme bezüglich der gewöhnlichen Richtlinien im Bezug auf das Verändern von für Benutzer sichtbare Strings in den Anwendungen gemacht, so dass die Plasma-Entwickler einige neue Funktionen in diese Version von KDE einfließen lassen konnten.
<br />
Verbesserungen in dieser Version sind keinesfalls beschränkt auf, beinhalten aber unter anderem:

</p>
<ul>
    <li>
    Neue Funktionen in Plasma. Die Größe und Position der Kontrollleiste kann nun eingestellt werden, die Sichtbarkeit von zahlreichen Optionen wurde verbessert um es für neue Benutzer einfacher zu machen zu verstehen wie Plasma funktioniert und was es bietet.
    </li>
    <li>
    Die KDE-Übersetzungen in Farsi und ins Isländische wurden in der Version 4.0.2 weiter verbessert, sowie auch zahlreiche andere Übersetzungen. Insgesamt ist KDE 4.0.2 in mehr als 49 verschiedenen Sprachen verfügbar.
    </li>
    <li>
    Probleme bei der Darstellung wurden im System zur Darstellung von Webseiten (KHTML) behoben. KHTML unterstützt nun noch mehr Webseiten, indem es mehr Dokumente verarbeiten kann welche nicht in korrektem HTML4 geschrieben wurden.
    </li>
</ul>

<p align="justify">
Zahlreiche Probleme bezüglich der Stabilität der Anwendungen wurden in Kopete – dem Instant-Messenger von KDE – und Okular – dem Dokumentbetrachter – behoben.</p>

<p align="justify">
 Für eine detaillierte Liste aller Verbesserungen seit der Version 4.0.1 des letzten Monats, werfen Sie bitte einen Blick auf <a href="/announcements/changelogs/changelog4_0_1to4_0_2">Liste der Änderungen in KDE 4.0.2</a>.
</p>

<p align="justify">
 Zahlreiche, zusätzliche Informationen über die Verbesserungen der 4.0.x-Veröffentlichungsreihe von KDE finden Sie in der <a href="/announcements/4.0/">Pressemitteilung zu KDE 4.0</a> und in der <a href="/announcements/4/4.0.1.de">Pressemitteilung zu KDE 4.0.1</a>.
</p>

<h4>
 Installation von Binärpaketen von KDE 4.0.2
</h4>
<p align="justify">
  <em>Paketersteller</em>.
  Einige Linux- und Unix-Betriebssystemhersteller haben freundlicherweise Binärpakete von KDE 4.0.2 für ihre Distributionen erstellt, in anderen Fällen haben dies Freiwillige erledigt. 
  Einige dieser Binärpakete stehen gratis zum Herunterladen auf  <a
href="http://download.kde.org/binarydownload.html?url=/stable/4.0.2/">http://download.kde.org</a> bereit.
  Zusätzliche Binärpakete, sowie Aktualisierungen der derzeit verfügbaren Pakete werden voraussichtlich im Laufe der nächsten Wochen verfügbar sein.
</p>

<p align="justify">
  <a id="package_locations"><em>Bezugsquellen</em></a>.
 Für eine Liste der derzeit verfügbaren Binärpakete, über deren Verfügbarkeit das KDE-Projekt in Kenntnis gesetzt wurde, werfen Sie bitte einen Blick auf die <a href="/info/4.0.2">KDE-4.0.2-Informationsseite</a>.
</p>

<h4>
  KDE 4.0.2 kompilieren
</h4>
<p align="justify">
  <a id="source_code"></a><em>Quelltext</em>.
  Der vollständige Quelltext von KDE 4.0.2 kann <a
href="http://download.kde.org/stable/4.0.2/src/">gratis heruntergeladen</a> werden.
Anweisungen zum Kompilieren und Installieren von KDE 4.0.2 sind auf der <a href="/info/4.0.2#binary">KDE-4.0.2-Informationsseite</a>.
</p>

<h4>
  KDE unterstützen
</h4>
<p align="justify">
 KDE ist ein <a href="http://www.gnu.org/philosophy/free-sw.html">freies Softwareprojekt</a>, dass nur existiert und wächst, weil unzählige Freiwillige ihre Zeit und Arbeit investieren. KDE ist immer auf der Suche nach neuen Freiwilligen, die etwas beitragen möchten. Unabhängig davon ob es ums Programmieren, das Beheben oder Berichten von Fehlern, das Schreiben von Dokumentationen, Übersetzungen, Werbung und Öffentlichkeitsarbeit, Geld, usw. geht. Jegliche Beiträge sind willkommen und werden mit Freuden entgegengenommen. Bitte lesen Sie die Seite <a
href="/community/donations/">&bdquo;KDE unterstützen&ldquo;</a> für nähere Informationen. </p>

<p align="justify">
Wir würden uns freuen bald von Ihnen zu hören!
</p>

<h4>Über KDE 4</h4>
<p>
KDE 4.0 ist die innovative, freie Arbeitsumgebung mit zahlreichen Anwendungen, sowohl für den täglichen Gebrauch, als auch für spezielle Zwecke. Plasma ist die neue Benutzerschnittstelle für die Arbeitsoberfläche von KDE 4 und bietet eine intuitive Oberfläche um mit der Arbeitsoberfläche und Anwendungen zu interagieren. Der Konqueror-Webbrowser integriert das Internet in die Arbeitsumgebung. Der Dolphin-Dateimanager, der Okular-Dokumentbetrachter und das „Systemeinstellungen“-Kontrollzentrum komplettieren den Standard-Desktop. <br />
KDE basiert auf den KDE-Bibliotheken, welche einfachen Zugriff auf Netzwerkressourcen mittels der KIO-Technologie und erweiterte, grafische Effekte durch die Möglichkeiten von Qt4 bieten. Phonon und Solid, welche auch Teil der KDE-Bibliotheken sind, erweitern alle KDE-Anwendungen um ein Multimedia-System und ermöglichen bessere Integration von Hardware.
</p>


