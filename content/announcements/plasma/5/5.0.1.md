---
aliases:
- ../../plasma-5.0.1
date: '2014-08-12'
description: KDE Ships First Bugfix Release of Plasma 5.
layout: plasma
title: KDE Ships First Bugfix Release of Plasma 5
---

{{<figure src="/announcements/plasma/5/5.0.1/plasma-5.0.1.png" class="text-center" alt="Plasma 5" width="600px" >}}

August 12, 2014. Today KDE releases the first bugfix update to Plasma 5. <a href='http://kde.org/announcements/plasma5.0/index.php'>Plasma 5</a> was released a month ago with many feature refinements and streamlining the existing codebase of KDE's popular desktop for developers to work on for the years to come.

This release, versioned 5.0.1, adds a month's worth of new translations and fixes from KDE's contributors. The bugfixes are typically small but important such as fixing text which couldn't be translated, using the correct icons and fixing overlapping files with KDELibs 4 software.
