------------------------------------------------------------------------
r1064497 | scripty | 2009-12-21 04:23:39 +0000 (Mon, 21 Dec 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1065303 | asimha | 2009-12-22 23:45:54 +0000 (Tue, 22 Dec 2009) | 7 lines

Removing saturn moons feature from 4.3 branch as it contained
plagiarized code that amounted to copyright infringement.

FEATURE
CCMAIL: kstars-devel@kde.org


------------------------------------------------------------------------
r1072386 | scripty | 2010-01-10 04:13:40 +0000 (Sun, 10 Jan 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1073709 | jmhoffmann | 2010-01-12 18:11:39 +0000 (Tue, 12 Jan 2010) | 2 lines

Bump version number for KDE 4.3.5 release.

------------------------------------------------------------------------
r1074985 | scripty | 2010-01-15 04:03:23 +0000 (Fri, 15 Jan 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
