---
title: "Support & Information"
layout: support
columns:
- title: Documentation
  rows:
    - title: Handbook
      text: "[Program documentation](https://docs.kde.org/) for many KDE applications"
      url: "https://docs.kde.org/"
    - title: KDE for System Administrators
      text: The KDE for System Administrators document provides system administrators who are deploying KDE software in their organisation with all the information they need concerning KDE software.
      url: "https://develop.kde.org/deploy/"
    - title: KDE Userbase
      text: "Information on many KDE applications and products. [Userbase](https://userbase.kde.org) is the home for KDE users and enthusiasts. It provides information for end users on how to use KDE applications."
      url: "https://userbase.kde.org/"
- title: Community Support
  text: 'These <a href="https://userbase.kde.org/Asking_Questions">tips on asking questions</a> should help you get useful answers.'
  rows:
    - title: KDE Forums
      text: Chat with other KDE users who may be able to help
      url: 'https://forum.kde.org/'
    - title: Chat Channels
      text: 'Community support is available in <a href="https://webchat.kde.org/#/room/#kde:kde.org">the KDE Matrix Room</a>. See the <a href="https://community.kde.org/Matrix#Rooms">List of KDE Matrix Rooms</a> for more specific channels.'
      url: 'https://community.kde.org/Matrix'
    - title: Mailing Lists
      text: 'Often a direct way to get in touch with the development community'
      url: '/support/mailinglists/'

- title: International
  rows:
    - title: International Pages
      text: 'If you are interested in viewing the KDE Home page and other attached pages in a language other than English, you can choose from the list at <a href="/support/international">KDE International</a>.'
      url: '/support/international'

- title: Security and Bug Reporting
  rows:
    - title: Report an Issue or Bug
      text: Developers work best fixing bugs if accurate and detailed information is provided
      url: "https://community.kde.org/Get_Involved/Issue_Reporting"
    - title: Security Advisories
      text: Information for users and administrators with important security information. It is highly advised you keep your software up-to-date.
      url: "/info/security/"

- title: KDE e.V. Trusted IT Consulting Firms
  rows:
    - text: |
        In KDE we take pride in being a free software community and having an open, free and fair development process.

        However, we also understand that sometimes companies and institutions main priority is not learning the processes of our community and they just want issue solved or a feature implemented.

        For this reason, we are offering a list of consultants that have expertise in dealing with the KDE Community and we know will help get your project landed in KDE as best as possible.
      url: https://ev.kde.org/consultants/
---

On this page, you can find different ways to find user support for KDE
software provided by the community.
