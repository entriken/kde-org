---
title : "KDE 2.2 Info Page"
publishDate: 2001-08-15 00:01:00
---


KDE 2.2 was released on August 15, 2001. Read the
<a href="/announcements/announce-2.2">official announcement</a>.


For a list of changes since KDE 2.1, see the
<a href="/announcements/changelogs/changelog2_1to2_2">list of changes</a>

For a high-level overview of the features of KDE, see the
<a href="/info">KDE info page</a>

For a graphical tutorial on using KDE 2, see this
<a href="https://www.linux-mandrake.com/en/demos/Tutorial/">tutorial page</a> from Linux Mandrake

This page will be updated to reflect changes in the status of
2.2 so check back for new information.

## FAQ

See the <a href="faq.php">KDE FAQ</a> for any specific
questions you may have.  Questions about Konqueror should be directed to the
<a href="https://konqueror.kde.org/faq/">Konqueror FAQ</a> and sound related
questions are answered in the <a href="https://www.arts-project.org/doc/handbook/faq.html">Arts FAQ</a>

## Download and Installation

See the links listed in the <a
href="/announcements/announce-2.2.php">announcement</a>. The KDE
<a href="/documentation/faq/install.html">Installation FAQ</a>
provides generic instruction about installation issues.

If you want to compile from sources we offer
<a href="http://developer.kde.org/build/">instructions</a> and help for common
problems in the <a href="http://developer.kde.org/build/compilationfaq.html">Compilation
FAQ</a>.

## Updates

<a href="/info/2.2.1.php">KDE 2.2.1</a> has been 
released as of September 19th, 2001 -- see the 
<a href="/announcements/announce-2.2.1.php">official KDE 2.2.1 announcement</a> for details. Users are encouraged to upgrade.  
This page will no longer be updated.

## Security Issues

<b>NOTE:</b>This section is no longer maintained. Please refer to the
<a href="2.2.2.php">2.2.2 Info page</a> instead.

## Bugs

This is a list of grave bugs and common pitfalls
surfacing after the release date:

<ul>
        <li>Bug: Renaming text files on the Desktop using KDesktop
            (the right mouse button menu) will destroy the contents of
            the file.  Workaround: Use anything but KDesktop to rename
            text files on the Desktop (fixed for KDE 2.2.1)</li>
        <li>Bug: Form submission in KHTML (Konqueror) is broken. No known
            work around. (fixed for KDE 2.2.1)</li>
	<li>Bug: KPersonalizer may turn on the window manager option "Unshade on hover"
            by mistake. You can turn this off again in the Control Center
            under Look&amp;Feel -> Window Behavior -> Advanced -> Shading ->
            Enable Hover.</li>
        <li>Package problem: KDE and/or Konqueror may fail to start due
            to problems with FAM on RedHat. Solution: make sure to have
            fam-2.6.4-10.i386.rpm and xinetd-2.3.0-1.71.i386.rpm or a later
            version.</li>
        <li>Package problem: KDE packages seem to depend on rpm-4.0.3-0.91 
            (RedHat?) however, this version of rpm is reported to break 
            "Red-Carpet". Workaround: leave rpm-4.0.2-8 installed; that is
            rpm -ivh rpm-4.0.3-0.91 to permit two rpms on your system.</li>
        <li>Package problem: SuSE packages labeled "experimental-i686" make
            KHTML (Konqueror) crash when javascript is enabled. To solve this 
            problem, uninstall these packages. The affected packages 
            have been removed from our ftp-site.</li>
	<li>See the <a href="http://kmail.kde.org">KMail homepage</a> for
	information about a problem with compacting folders.</li>
</ul>

Please check the <a href="http://bugs.kde.org">bug database</a>
before filing any bug reports. Also check for possible updates that
might fix your problem.

## Developer Info

If you need help porting your application to KDE 2.x see the <a
href="http://websvn.kde.org/*checkout*/branches/KDE/2.2/kdelibs/KDE2PORTING.html">
porting guide</a>. We suggest to port to KDE 3.x instead. 

There is also info on the <a
href="http://developer.kde.org/documentation/library/kdeqt/kde2arch/index.html">architecture</a>
and the <a
href="http://developer.kde.org/documentation/library/2.2-api/classref/index.html">
programming interface of KDE 2.2</a>.

<!-- END CONTENT -->
<?php
  include "footer.inc";
?>
