2005-08-20 12:41 +0000 [r451326]  amantia

	* branches/KDE/3.4/kdewebdev/quanta/project/project.cpp,
	  branches/KDE/3.4/kdewebdev/quanta/ChangeLog: Backport: Do not
	  close a file when it is removed from a project. Fixes the crash
	  when deleting a file which is part of a project from the tab
	  context menu.

2005-08-22 22:31 +0000 [r452261]  amantia

	* branches/KDE/3.4/kdewebdev/quanta/treeviews/basetreeview.cpp,
	  branches/KDE/3.4/kdewebdev/quanta/src/quanta_init.cpp: Backport
	  two bugfixes made by Jens: -show filesystem permissions also for
	  remote locations in the properties dialog - open also the unsaved
	  backup files after crash of Quanta

2005-09-04 12:09 +0000 [r457006]  jens

	* branches/KDE/3.4/kdewebdev/quanta/data/dtep/xhtml/ul.tag,
	  branches/KDE/3.4/kdewebdev/quanta/data/dtep/html-strict/u.tag,
	  branches/KDE/3.4/kdewebdev/quanta/data/dtep/html/l.tag,
	  branches/KDE/3.4/kdewebdev/quanta/data/dtep/xhtml/li.tag,
	  branches/KDE/3.4/kdewebdev/quanta/data/dtep/xhtml-frameset/ul.tag,
	  branches/KDE/3.4/kdewebdev/quanta/data/dtep/xhtml-frameset/li.tag,
	  branches/KDE/3.4/kdewebdev/quanta/data/dtep/html-strict/l.tag,
	  branches/KDE/3.4/kdewebdev/quanta/data/dtep/html/u.tag: backport
	  of fix for bug #112006

2005-09-21 18:43 +0000 [r462685]  amantia

	* branches/KDE/3.4/kdewebdev/quanta/parsers/tag.cpp,
	  branches/KDE/3.4/kdewebdev/quanta/treeviews/tagattributetree.cpp,
	  branches/KDE/3.4/kdewebdev/quanta/treeviews/scripttreeview.cpp,
	  branches/KDE/3.4/kdewebdev/quanta/parts/kafka/kafkacommon.cpp,
	  branches/KDE/3.4/kdewebdev/lib/compatibility/knewstuff/downloaddialog.h,
	  branches/KDE/3.4/kdewebdev/quanta/parts/kafka/kafkahtmlpart.cpp,
	  branches/KDE/3.4/kdewebdev/quanta/parts/kafka/wkafkapart.cpp,
	  branches/KDE/3.4/kdewebdev/quanta/treeviews/basetreeview.cpp,
	  branches/KDE/3.4/kdewebdev/quanta/parsers/node.cpp,
	  branches/KDE/3.4/kdewebdev/quanta/dialogs/actionconfigdialog.cpp,
	  branches/KDE/3.4/kdewebdev/quanta/src/document.cpp,
	  branches/KDE/3.4/kdewebdev/quanta/ChangeLog,
	  branches/KDE/3.4/kdewebdev/quanta/project/projectprivate.cpp,
	  branches/KDE/3.4/kdewebdev/quanta/utility/quantacommon.h,
	  branches/KDE/3.4/kdewebdev/lib/compatibility/knewstuff/uploaddialog.h,
	  branches/KDE/3.4/kdewebdev/quanta/src/quantaview.cpp,
	  branches/KDE/3.4/kdewebdev/quanta/treeviews/docfolder.cpp,
	  branches/KDE/3.4/kdewebdev/quanta/parts/kafka/htmlenhancer.cpp,
	  branches/KDE/3.4/kdewebdev/quanta/src/quanta.cpp,
	  branches/KDE/3.4/kdewebdev/quanta/quanta.kdevelop,
	  branches/KDE/3.4/kdewebdev/lib/compatibility/knewstuff/providerdialog.h,
	  branches/KDE/3.4/kdewebdev/quanta/components/tableeditor/tableeditor.cpp,
	  branches/KDE/3.4/kdewebdev/quanta/parsers/dtd/dtdparser.cpp,
	  branches/KDE/3.4/kdewebdev/lib/compatibility/knewstuff/knewstuff.h,
	  branches/KDE/3.4/kdewebdev/quanta/src/quanta.h,
	  branches/KDE/3.4/kdewebdev/quanta/plugins/quantaplugin.cpp:
	  Backport lots of fixes from 3.5 branch in case some distributions
	  want to package a KDE 3.4.2. Make it clear in the version string
	  that this is not 3.4.2.

2005-09-21 18:49 +0000 [r462690]  amantia

	* branches/KDE/3.4/kdewebdev/quanta/project/projectupload.cpp,
	  branches/KDE/3.4/kdewebdev/quanta/ChangeLog,
	  branches/KDE/3.4/kdewebdev/quanta/project/projectprivate.cpp:
	  Backport: replace a leading ~ in an upload profile with the users
	  home folder and avoid a hang

2005-09-30 06:38 +0000 [r465571]  amantia

	* branches/KDE/3.4/kdewebdev/kdewebdev.lsm,
	  branches/KDE/3.4/kdewebdev/quanta/quanta.kdevelop,
	  branches/KDE/3.4/kdewebdev/quanta/quanta.lsm,
	  branches/KDE/3.4/kdewebdev/quanta/src/quanta.h,
	  branches/KDE/3.4/kdewebdev/quanta/ChangeLog: Prepare for 3.4.3

2005-10-01 13:15 +0000 [r466066]  amantia

	* branches/KDE/3.4/kdewebdev/quanta/parts/kafka/kafkahtmlpart.cpp,
	  branches/KDE/3.4/kdewebdev/quanta/ChangeLog: Backport fix for
	  #112733.

2005-10-01 15:38 +0000 [r466116]  amantia

	* branches/KDE/3.4/kdewebdev/quanta/parts/kafka/tagactionset.cpp:
	  Backport fix for 108501.

2005-10-02 18:48 +0000 [r466469]  coolo

	* branches/KDE/3.4/kdewebdev/quanta/parts/kafka/htmldocumentpropertiesui.ui:
	  compile with qt 3.3.5

2005-10-05 10:29 +0000 [r467448]  amantia

	* branches/KDE/3.4/kdewebdev/quanta/data/dtep/html/p.tag,
	  branches/KDE/3.4/kdewebdev/quanta/data/dtep/html/d.tag,
	  branches/KDE/3.4/kdewebdev/quanta/ChangeLog: Backport: Show DT
	  tags in VPL. Fix list of children for DT.

