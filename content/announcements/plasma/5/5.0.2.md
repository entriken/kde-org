---
aliases:
- ../../plasma-5.0.2
date: '2014-09-17'
description: KDE Ships Second Bugfix Release of Plasma 5.
layout: plasma
title: KDE Ships Second Bugfix Release of Plasma 5
---

{{<figure src="/announcements/plasma/5/5.0.1/plasma-5.0.1.png" class="text-center" alt="Plasma 5" width="600px" >}}

September 17, 2014. Today KDE releases the second bugfix update to Plasma 5. <a href='http://kde.org/announcements/plasma5.0/index.php'>Plasma 5</a> was released two months ago with many feature refinements and streamlining the existing codebase of KDE's popular desktop for developers to work on for the years to come.

{{% i18n_var "This release, versioned %[1]s, adds a month's worth of new translations and fixes from KDE's contributors.  The bugfixes are typically small but important such as fixing text which couldn't be translated, using the correct icons and fixing overlapping files with KDELibs 4 software.  It also adds a month's hard work of translations to make support in other languages even more complete." "plasma-5.0.2" %}}
