---
aliases:
- ../4.0
- ../4.0.ro
date: '2008-01-11'
layout: single
title: KDE 4.0 Lansat
---

<h3 align="center">
   Proiectul KDE Lansează a patra versiune majoră a Desktop-ului Linux
</h3>
<p align="justify">
  <strong>
    Cu a patra versiune majoră, comunitatea KDE marchează începutul erei KDE 4.
  </strong>
</p>

<p>
Comunitatea KDE este încântată să anunţe imediata disponibilitate a
  <a href=/announcements/4.0/>KDE 4.0</a>. Această lansare
  semnificativă, marchează două lucruri: sfârşitul unor lungi şi intensive
  cicluri de dezvoltare care au ca rezultat KDE 4 şi reprezintă începutul erei
  KDE 4.
</p>

<div class="text-center">
<a href="/announcements/4/4.0/desktop.png">
<img src="/announcements/4/4.0/desktop_thumb.png" class="img-fluid">
</a> <br/>
<em>Desktop-ul KDE 4.0</em>
</div>
<br/>

<p>
<b>Librăriilor</b> KDE 4 li s-au adus îmbunătăţiri pe aproape toate
  planurile. Cadrul de dezvoltare Phonon asigură suport multimedia
  independent de platformă pentru toate aplicaţiile KDE, în timp ce "Solid"
  face posibilă interacţiunea cu dispozitivele (amovibile) mai uşor, asigurând
  unelte pentru un mai bun management al consumului de energie.
<p />
<b>Desktop-ul</b> KDE 4 a căpătat nişte funcţii noi de o deosebită
  importanţă. Mediul de lucru "Plasma" oferă o nouă interfaţă, aceasta
  incluzând un panou, meniu, widget-uri pe desktop, şi o funcţie de tablou de
  bord (<i>dashboard</i>). KWin, managerul de ferestre KDE,
  acum suportă efecte grafice avansate pentru a uşura interacţiunea cu
  ferestrele.
<p />
Multe <b>Aplicaţii KDE</b> au fost îmbunătăţite de asemenea. Actualizări
  vizuale prin grafica vectorială, schimbări în librăriile de bază, îmbunătăţiri
  la interfaţa utilizatorului, trăsături noi, chiar şi aplicaţii noi.
  Okular, vizualizator de documente şi Dolphin, manager de fisiere
  sunt numai două aplicaţii care subliniază noile tehnologii ale lui KDE
  4.
<p />
<img src="/announcements/4/4.0/images/oxybann.png" align="right" hspace="10"/>
<b>Tema Oxygen</b> asigură un aer nou pentru mediul de lucru. Aproape
  toate părţile vizibile de către utilizator al mediului de lucru KDE şi
  al aplicaţiilor au fost îmbunătăţite. Frumusetea şi logica sunt doar două
  dintre conceptele de bază din spatele Oxygen.
</p>

<h3>Desktop</h3>
<ul>
	<li>Plasma este noul model de spaţiu de lucru. Plasma pune la
    dispoziţie un panou, un meniu şi alte posibilităţi de a interacţiona cu
    desktop-ul şi aplicaţiile
	</li>
	<li>KWin, managerul de festre al KDE suportă acum funcţii avansate
    3D. Accelerarea hardware se ocupă mai bine de interacţiunea
    utilizator-ferestre
	</li>
	<li> Oxygen este noua temă KDE 4.0. Acest concept îţi oferă
    simplitate, uşurinţă în utilizare şi este plăcut ochiului.
	</li>
</ul>
Citeşte mai multe despre noua interfaţă a spaţiului de lucru KDE în <a href="./guide">Ghidul vizual KDE
4.0</a>.

<h3>Aplicaţii</h3>
<ul>
	<li>Konqueror este browserul web, uşor de utilizat, bine integrat şi care
    oferă suport pentru noile standarde, cum ar fi CSS3.</li>
	<li>Dolphin este noul manager de fişiere KDE. Dezvoltarea sa a
    plecat de la ideea uşurinţei în utilizare însă ţinându-se cont şi de
    necesităţile avansate ale utilizatorilor. Asta face din Dolphin un
    program excelent.
	</li>
	<li>În "Setările de sistem" s-a introdus o nouă interfaţă de control.
    KSysGuard, programul de monitorizare ajută acum mult mai uşor la
    supervizarea consumului de resurse şi a activităţii sistemului.
	</li>
	<li>Okular, vizualizatorul de documente disponibil în KDE 4.0
    suportă o multitudine de fişiere. Este unul din programele dezvoltate în
    colaborare cu&nbsp; <a href=http://openusability.org/>Proiectul
    OpenUsability</a>.
	</li>
	<li>Programele din pachetul educaţional au fost printre primele aplicaţii
    portate şi dezvoltate utilizându-se noua tehnologie KDE 4.0.
    Kalzium, tabelul periodic al elementelor şi Marble Desktop
    Globe se pot numi vârfurile de lance ale acestor programe destinate
    sectorului educaţional. Citeşte mai multe în
    <a href=/announcements/4/4.0/education>Ghidul
    aplicaţiilor grafice</a>.
	</li>
	<li>O mulţime de jocuri KDE au fost actualizate. KMine, o replică
    a faimosului minesweeper sau KPat sunt doar câteva exemple. Datorită
    facilităţilor graficii vectoriale, jocurile au devenit independente de
    rezoluţia folosită.
	</li>
</ul>
Despre alte aplicaţii găsiţi mai multe detalii în <a href="./applications">Ghidul vizual KDE
4.0</a>.

<div class="text-center">
<a href="/announcements/4/4.0/dolphin-systemsettings-kickoff.png">
<img src="/announcements/4/4.0/dolphin-systemsettings-kickoff_thumb.png" class="img-fluid">
</a> <br/>
<em>Managerul de fişiere, setările de sistem şi meniul în acţiune</em>
</div>
<br/>

<h3>Librării</h3>
<p>
<ul>
	<li> Phonon oferă aplicaţiilor multimedia capabilităţi precum redare audio
    şi video. În interior, Phonon utilizează diferite subprograme
    (backend-uri), comutabile în timpul utilizării. Backend-ul de bază pentru
    KDE 4.0 va fi Xine, care oferă un suport excelent pentru
    diferite formate. Phonon mai dă voie utilizatorului să aleagă
    dispozitivul de ieşire în funcţie de tipul de fişier multimedia.
	</li>
	<li>Cadrul "Solid" de integrare hardware, integrează unităţile fixe şi
    amovibile în aplicaţiile KDE. "Solid" se ocupă de sistemul de
    management al energiei, manipulează conectivitatea reţelei şi integrarea cu
    aparatele Bluetooth. În interior, "Solid" combină punctele forte
    din HAL, NetworkManager şi BlueZ, dar aceste componente
    se pot schimba fără a închide aplicaţiile, pentru a asigura o portabilitate
    maximă.
	</li>
	<li>KHTML este motorul de redare folosit de Konqueror, navigatorul
    web. KHTML este un motor simplu si usor, dar care suportă multe
    standarde moderne cum ar fi CSS 3. KHTML a fost de
    asemenea&nbsp; primul motor de interpretare care a trecut faimosul test
    Acid 2.
	</li>
	<li>Librăria ThreadWeaver, care vine cu kdelibs, asigură o
    interfaţă de inalt nivel pentru a folosi mai bine sistemele multi-core de
    azi, făcând ca aplicaţiile KDE mai fluente şi folosind mai bine
    resursele sistemului.
	</li>
	<li> Fiind construit pe librăria Qt4 de la Trolltech, KDE
    4.0 se poate folosi de capabilităţi vizuale avansate şi de dimensiunea
    mică a acestei librări. Kdelibs asigură o extensie remarcabilă a
    librăriei Qt, adăugând multe funcţionalităţi de înalt nivel şi
    comoditate pentru programatori şi dezvoltatori.
	</li>
</ul>
</p>
<p>Mai multe informaţii despre librăriile KDE găsiţi în<a href="http://techbase.kde.org">KDE TechBase</a></p>

<h4>Încearcă un ghid asistat...</h4>
<p>
<a href=/announcements/4/4.0/guide>Ghidul vizual
  KDE 4.0</a> îţi pune la dispoziţie o privire de ansamblu asupra noutăţilor şi
  îmbunătăţirilor tehnologice ale KDE 4.0. Presărat cu multe capturi de
  ecran, îţi arată modificările aduse acestei noi versiuni KDE şi îţi
  demonstrează capacităţile noilor tehnologii venite în sprijinul utilizatorului
  final. Noile funcţii ale
  <a href=/announcements/4/4.0/desktop>mediului de lucru</a>
  te ajută cu primii paşi, iar
  <a href=/announcements/4/4.0/applications>aplicaţii</a>
  precum Setări de sistem, Okular, vizualizatorul de documente sau
  managerul de fişiere Dolphin sunt introduse.
  <a href=/announcements/4/4.0/education>Pachetele
  educaţionale</a> se găsesc şi ele alături de
  <a href=/announcements/4/4.0/games>Jocuri</a> în KDE
  4.0.
</p>

<h4>Încearcă...</h4>
<p>
Pentru cei interesaţi în obţinerea pachetelor pentru a testa şi a contribui,
  mai multe distribuţii au anunţat că vor avea disponibile pachete KDE
  4.0 imediat după lansare. Lista curentă, completă poate fi găsită pe
  <a href=http://www.kde.org/info/4.0>KDE 4.0 Info Page</a>, unde puteţi
  găsi legături către codul sursă, informaţii referitoare la compilare,
  securitate si alte lucruri.
</p>
<p>
Următoarele distribuţii au anunţat disponibilitatea pachetelor sau CD-uri Live
  KDE 4.0:
<ul>
  <li>
    O versiune alfa bazată pe KDE4 a distribuţiei <b>Arklinux 2008.1</b> este
    aşteptată imediat după această lansare, versiunea finală urmând a fi lansată
    în 3 sau 4 săptămâni.
  </li>
  <li>
    Pachetele KDE 4.0 pentru <b>Debian</b> sunt disponibile în ramura
    experimentală. Platforma KDE de dezvoltare va fi disponibilă chiar începând
    cu <i>Lenny</i>. Urmăriţi anunţurile din partea
    <a href=http://pkg-kde.alioth.debian.org/>echipei KDE Debian</a>. De
    asemenea sunt zvonuri că există în plan un Live CD.
  </li>
  <li>
    <b>Fedora</b> va dispune de KDE 4.0 în Fedora 9, ce va fi
    <a href=http://fedoraproject.org/wiki/Releases/9 id=ov8w title=lansata>lansată</a>
    în aprilie, versiunile alfa începând a fi disponibile cu data de 24
    ianuarie. Pachetele KDE 4.0 se găsesc în versiunea pre-alfa&nbsp; a
    depozitului
    <a href=http://fedoraproject.org/wiki/Releases/Rawhide>Rawhide</a>.
  </li>
  <li>
    <b>Gentoo Linux</b> pune la dispoziţie build-uri KDE 4.0 pe
    <a href=http://kde.gentoo.org/>http://kde.gentoo.org</a>.
  </li>
  <li>
    Pachete<b> Kubuntu</b> vor fi incluse în "Hardy Heron" (8.04) si vor fi
    disponibile de asemenea ca update-uri pentru "Gutsy Gibbon" (7.10). Pentru a
    putea încerca KDE 4.0 este disponibil un Live CD. Mai multe detalii puteti
    găsi în cadrul
    <a href=http://kubuntu.org/announcements/kde-4.0>anunţului disponibil pe
    Kubuntu.org</a>.
  </li>
  <li>
    <b>Mandriva</b> va furniza pachete pentru
    <a href=http://download.kde.org/binarydownload.html?url=/stable/4.0.0/Mandriva/>2008.0</a>
    şi intenţionează să pună la dispoziţie un Live CD cu ultima versiune
    începând cu 2008.1.
  </li>
  <li>
    Pachetele<b> openSUSE</b> <a href=http://en.opensuse.org/KDE/KDE4>sunt
    disponibile</a> pentru openSUSE 10.3
    (<a href=http://download.opensuse.org/repositories/KDE:/KDE4:/STABLE:/Desktop/openSUSE_10.3/KDE4-BASIS.ymp>one-click
    install</a>), openSUSE Factory
    (<a href=http://download.opensuse.org/repositories/KDE:/KDE4:/STABLE:/Desktop/openSUSE_Factory/KDE4-BASIS.ymp>one-click
    install</a>) şi openSUSE 10.2. Un
    <a href=http://home.kde.org/%7Ebinner/kde-four-live/>Live CD KDE Four</a> cu
    aceste pachete este de asemenea disponibil. KDE 4.0 va face parte din
    viitoarea versiune openSUSE 11.0.
  </li>
</ul>

</p>

<h2>Despre KDE 4</h2>
<p>
KDE 4.0 este un produs inovativ al Free Software ca mediu de lucru ce
  conţine o mulţime de aplicaţii gratuite pentru utilizarea de zi cu zi cât şi
  pentru scopuri bine determinate, specifice anumitor arii de lucru.
  Plasma este noul mediu desktop dezvoltat pentru KDE 4 care
  asigură o interfaţă intuitivă ce facilitează interacţiunea cu mediul de lucru
  şi aplicaţii. Konqueror, navigatorul web integrează mediul online cu
  spaţiul de lucru. Managerul de fişiere Dolphin, vizualizatorul de
  documente Okular şi Centrul de control al setărilor de sistem
  completează un set complet de <i>mediu de lucru</i>.<br>
  KDE este construit în interacţiune cu librăriile KDE care asigură un
  acces uşor la resursele din reţea şi nu numai, prin intermediul interfeţei
  KIO şi a capacităţilor avansate oferite de Qt4. Phonon şi
  Solid, la rândul lor făcând parte din librăriile KDE, adaugă un
  cadru de lucru multimedia şi o mai bună coordonare a tuturor aplicaţiilor
  KDE cu resursele hardware.
</p>
