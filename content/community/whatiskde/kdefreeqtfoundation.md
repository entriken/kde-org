---
title: "KDE Free Qt Foundation"
hidden: true
---

The KDE Free Qt Foundation is an organization with the purpose of
<b>securing the availability of the [Qt toolkit](https://www.qt.io/)</b>

- for the development of Free Software and
- in particular for the development of [KDE software](https://kde.org).

The Foundation has <a href="#update_2015">license agreements</a>
with <a href="http:/www.qt.io/">The Qt Company</a>,
<a href="http:/www.digia.com/">Digia</a> and <a href="http://www.nokia.com/">Nokia</a>.
The agreements ensure that the Qt will continue to be available as Free Software.
Should The Qt Company discontinue the development of the Qt Free Edition under
the required licenses, then the Foundation has the right to release Qt under a BSD-style license or under
other open source licenses. The agreements stay valid in case of a buy-out, a merger or bankruptcy.

## Licenses and Platforms

The most recent agreement covers all releases of all Qt versions for the following platforms:

- Core Platforms: X11 (i.e. Desktop Linux) and Android – support cannot be dropped.
- Additional Platforms: Microsoft Windows, Microsoft Windows Phone, Apple MacOS and Apple iOS – as long as they are supported at all by The Qt Company.
- The Foundation has the right to update to supported successor platforms (e.g. from X11 to Wayland) without additional negotiations with The Qt Company.

The following license terms are required:

- All parts of Qt are available under the GPLv3 or under a compatible license. Most parts are also available under the LGPLv3 and under the GPLv2.
- The core libraries of Qt (Essentials) and all existing LGPL-licensed Qt add-ons must continue to be available under the LGPLv3.
- GPLv2-compatibility must kept for all existing GPLv2-licensed Qt code and for the core part of Qt (Essentials)
  and for the Qt applications. Future versions of the GPL are supported “if approved by the KDE Free Qt Foundation”.
- Applications included in Qt (e.g. Qt Creator) must be licensed under GPLv2 and GPLv3. Two GPL exceptions clarify:
<ol>* generated code is not license-restricted, and* 2. bridges to third party applications are still possible.</ol>

- New add-ons for Qt must use GPLv3 and may optionally also offer GPLv2 and/or LGPLv3.
  The stricter rules above continue to apply if existing functionality is replaced by new modules.

If these license terms are not yet present at the time of the Qt release, then they must be applied within a timeframe of not more than 12 months.

The agreement also protects the free license of all publicly developed code – even when it has not yet been included in a Qt release.

## Board Members

The board of the Foundation consists of two members for The Qt Company and two
members for KDE e.V. Decisions of the Foundation are taken by vote of the board
members. In case of a tie the votes of the KDE representatives decide.

The KDE e.V. appointed as board members of the KDE Free Qt
Foundation the following people:

- Albert Astals Cid

- Olaf Schmidt-Wischhöfer

The current voting members for The Qt Company are:

- Lars Knoll

- Tuukka Turunen

The Trolltech founders are advising members without voting right:

- Eirik Eng

- Haavard Nord

## Statutes

<a href="../KDEFreeQt_Statutes_091111_final.pdf">Statutes of the KDE Free Qt Foundation</a> (72 KiB)</b>

## History

The Foundation was originally founded by <a href="http://en.wikipedia.org/wiki/Trolltech">Trolltech</a>
and the <a href="http://ev.kde.org">KDE e.V.</a> in 1998. After <a href="http://www.nokia.com/">Nokia</a>
bought Trolltech, the statutes were updated accordingly.
In <a href="#update_2004">May 2004</a>, a first update to the agreement was made.
This agreement addressed the purpose in a more precise and complete way. The
intention and basic content however, were not changed.
In <a href="#update_2009">July 2009</a>, the agreement was again updated to respond
both to the relicensing of Qt to the LGPL license and to the merger of Trolltech into Nokia.
In November 2009, the statutes of the Foundation where also updated to better fit the new
situation.

In October 2011, Nokia launched the <a href="http://www.qt-project.org/">Qt Project</a>,
allowing both companies and individuals to contribute to the development of Qt under
open governance terms. To ensure the fairness of the contribution terms to individual
developers, two lawyers were contracted by the KDE Free Qt Foundation. Nokia took
their feedback into account and made is thus possible for the KDE Free Qt Foundation
to support both the launch of the Qt Project and the wording improvements in the
<a href="../QtContributionLicenseAgreement-1-1.pdf">Qt Contribution License Agreement 1.1</a>:
“We fully support the work being done with the Qt Project. An openly governed Qt is in the best
interests of all Qt developers. The Open Governance structure of the Qt Project empowers
developers to influence the direction and the pace of Qt development. Stakeholders in the
future of Qt, such as KDE, can now contribute according to their own priorities and take
ownership over areas of Qt that are of particular importance to them.”
(Olaf Schmidt-Wischhöfer and Martin Konold, the two board members from KDE)

In <a href="#update_2012">September 2012</a>, <a href="http://www.digia.com/">Digia</a>
bought Qt from Nokia and signed a letter of commitment to be bound by any and all terms of agreement,
and then continued to <a href="#update_2013">sign a revised agreement</a> with the foundation.
Later Digia founded The Qt Company, which signed a similar <a href="#update_2015a">letter of commitment</a>
and negotiated an <a href="#update_2015">update to the agreement</a>.

### Announcements

<div class="table-wrapper">
<table>
<tr>
  <td valign="top"><em>January 13, 2016:</em></td>
  <td>
    <a href="https://dot.kde.org/2016/01/13/qt-guaranteed-stay-free-and-open-%E2%80%93-legal-update">
    Qt is Guaranteed to Stay Free and Open – Legal Update</a>
  </td>
</tr>
<tr>
  <td valign="top"><em>July 23, 2004:</em></td>
  <td>
    <a href="../kdefreeqt_announcement_20040723">
    Trolltech and KDE Free Qt Foundation Announce Amended and Restated Software
    License Agreement</a>
  </td>
</tr>
<tr>
  <td valign="top"><em>June 1998:</em></td>
  <td>
    <a href="../kdefreeqt_announcement">Original announcement about the
    creation of the KDE Free Qt Foundation</a>
  </td>
</tr>
</table>
</div>

### <a name="update_2015">Fifth Agreement (2015)</a>

The foundation has signed a new <a href="../Software_License_Agreement_2015.pdf">agreement with The Qt Company</a> (11 MiB; <a href="../Software_License_Agreement_2015_Text.pdf">as text</a> 301 KiB).

### <a name="update_2015a">Notification Letter (2015)</a>

Below is the Noticaton Letter signed by The Qt Company on March 23, 2015:

<a href="../Notification_about_Qt_Asset_Transfer.pdf">Notification Letter about Qt Asset Transfer 2015</a> (345 KiB).

### <a name="update_2013">Fourth Agreement (2013)</a>

In April 2013, <a href="../Software_License_agreement_2013.pdf">an agreement between the foundation and Digia</a> (1.5 Mi B) was signed.

### <a name="update_2012">Letter of Commitment (2012)</a>

Below is the Letter of Commitment signed by Digia on September 17, 2012:

<a href="../KDE_CommitmentLetter_120917.pdf">Digia’s Letter of Commitment</a> (2.6 MiB)

### <a name="update_2009">Third Agreement (2009)</a>

Below is a list of the pages of the document containing the agreement
between the KDE Free Qt Foundation and Nokia from 2009:

<b>Software License Agreement:
<a href="../images/nokia-agreement-1.jpg">page 1</a>
<a href="../images/nokia-agreement-2.jpg">page 2</a>
<a href="../images/nokia-agreement-3.jpg">page 3</a>
<a href="../images/nokia-agreement-4.jpg">page 4</a>
<a href="../images/nokia-agreement-5.jpg">page 5</a></b><br/>
<b>BSD License (Exhibit A):
<a href="../images/nokia-agreement-6.jpg">page 6</a></b><br/>
<b>Open Souce Definition (Exhibit B):
<a href="../images/nokia-agreement-7.jpg">page 7</a>
<a href="../images/nokia-agreement-8.jpg">page 8</a></b><br/>
<b>Nokia Qt LGPL Exception (Exhibit C):
<a href="../images/nokia-agreement-9.jpg">page 9</a></b><br/>

### <a name="update_2004">Second Agreement (2004)</a>

Below is a list of the pages of the document containing the second agreement
between the KDE Free Qt Foundation and Trolltech from May 2004:

<b>The Agreement:
<a href="../images/agreement1.png">page 1</a>,
<a href="../images/agreement2.png">page 2</a>,
<a href="../images/agreement3.png">page 3</a>,
<a href="../images/agreement4.png">page 4</a>
</b><br/>
<a href="../images/agreement5.png"><b>BSD-style License
(Exhibit A)</b></a><br/>
<b>The Open Source Definition (Exhibit B):
<a href="../images/agreement6.png">page 1</a>,
<a href="../images/agreement7.png">page 2</a></b>

### <a name="agreement">Original Agreement (1998)</a>

Below is a list of the pages of the document containing the original agreement
between the KDE Free Qt Foundation and Trolltech from June 1998:

<b><a href="../images/kdefreeqt0.png">Title page</a></b><br />
<b>The Agreement: <a href="../images/kdefreeqt1.png">page
1</a>, <a href="../images/kdefreeqt2.png">page 2</a></b><br />
<b>Qt Free Edition license (Exhibit 1): <a
href="../images/kdefreeqt3.png">page 3</a>, <a
href="../images/kdefreeqt4.png">page 4</a></b><br />
<b>Original KDE Free Qt Foundation Statutes (Exhibit 2): <a
href="../images/kdefreeqt5.png">page 5</a>, <a
href="../images/kdefreeqt6.png">page 6</a></b><br />
<a href="../images/kdefreeqt7.png"><b>BSD-style License
(Exhibit 3)</b></a>

### <a name="cla">Contribution License Agreement 1.1 (2010)</a>

<a href="../QtContributionLicenseAgreement-1-1.pdf">Qt Contribution License Agreement 1.1</a> (153 KiB)
