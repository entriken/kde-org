<?php
  $page_title = "KDE 3.1 Compilation Requirements";
  $site_root = "../../";
  include "header.inc";
?>

<h4>Compilers</h4>
<p align="justify">
  <em>Compiler Requirements</em>.
  KDE is designed to be cross-platform and hence to compile with a large
  variety of GNU/Linux - UNIX compilers.  However, KDE is advancing very rapidly
  and the ability of native compilers on various UNIX systems to compile
  KDE depends on users of those systems
  <a href="http://bugs.kde.org/">reporting</a> compile problems to
  the responsible developers.
</p>

<p align="justify">
  In addition, C++ support by <a href="http://gcc.gnu.org/">gcc/egcs</a>,
  the most popular KDE compiler, has been advancing rapidly, and has also
  recently undergone a major redesign.  As a result, KDE will not compile
  properly with older versions of gcc or most newer releases.
</p>

<p align="justify">
  In particular, gcc versions earlier than gcc-2.95, such as egcs-1.1.2
  or gcc-2.7.2, may not properly compile some components of KDE 3.1.
  In addition, some components of KDE 3.1, such as
  <a href="#aRts">aRts</a>, generally will not compile with unpatched
  CVS versions of <a href="http://gcc.gnu.org/gcc-3.1/gcc-3.1.html">gcc</a>
  3.1.x
  While there have been reports of successful KDE compilations with
  the so-called gcc-2.96 and gcc-3.1 (cvs), the KDE project at this time
  continues to recommend the use of gcc-2.95-* or a version of gcc
  which shipped with a stable Linux distribution and which was used
  successfully to compile a stable KDE for that distribution.
</p>

    <div align="center">
      <a href="#Basic">Basic</a>&nbsp;&nbsp;<a href="#Help">Help</a>&nbsp;&nbsp;<a href="#Hardware">Hardware</a>&nbsp;&nbsp;<a href="#Networking">Networking</a>&nbsp;&nbsp;<a href="#Browsing">Browsing</a>&nbsp;&nbsp;<a href="#Security">Security</a>&nbsp;&nbsp;<a href="#Graphics">Graphics</a>&nbsp;&nbsp;<a href="#Multimedia">Multimedia</a>&nbsp;&nbsp;<a href="#Development">Development</a>
    </div>
    <br />

    <table border="1" cellpadding="4" cellspacing="0">
      <tr>
        <td align="center" colspan="5">Basic</td>
      </tr>

      <tr>
        <td align="center"><strong>Package</strong></td>

        <td align="center"><strong>Level</strong></td>

        <td align="center" width="25%"><strong>Description</strong></td>

        <td align="center" width="25%"><strong>Explanation</strong></td>

        <td align="center" width="25%"><strong>Modules</strong></td>
      </tr>

      <tr valign="top">
        <td><a href="http://www.trolltech.com/">Qt &gt;= 3.1.0</a></td>

        <td bgcolor="#ff8c8c">Required</td>

        <td>Qt is the C++ cross-platform GUI toolkit upon which the great majority of KDE is built.</td>

        <td>Qt is essential for the use of nearly each KDE module.</td>

        <td>kdelibs, kdebase, kdeaddons, kdeadmin, kdeartwork, kdebindings, kdeedu, kdegames, kdegraphics, kdemultimedia, kdenetwork, kdepim, kdesdk, kdetoys, kdeutils, koffice, kdevelop</td>
      </tr>

      <tr valign="top">
        <td><a href="http://www.xfree86.org/">X Server</a></td>

        <td bgcolor="#ff8c8c">Required</td>

        <td>An X Server provides the underlying display technology on UNIX systems. The KDE Project recommends the XFree86 server.</td>

        <td>An X server is required for almost each KDE module.</td>

        <td>kdelibs, kdebase, kdeaddons, kdeadmin, kdeartwork, kdebindings, kdeedu, kdegames, kdegraphics, kdemultimedia, kdenetwork, kdepim, kdesdk, kdetoys, kdeutils, koffice, kdevelop</td>
      </tr>

      <tr valign="top">
        <td><a href="http://www.xfree86.org/">X Render Extension</a></td>

        <td bgcolor="#c5ff7f">Optional</td>

        <td>The X Render Extension provides anti-aliased fonts for X Servers.</td>

        <td>Nearly each KDE application benefits from anti-aliased fonts.</td>

        <td>kdelibs, kdebase, kdeaddons, kdeadmin, kdeartwork, kdebindings, kdeedu, kdegames, kdegraphics, kdemultimedia, kdenetwork, kdepim, kdesdk, kdetoys, kdeutils, koffice, kdevelop</td>
      </tr>

      <tr valign="top">
        <td><a href="http://www.xfree86.org/">X DPMS</a></td>

        <td bgcolor="#ffff8e">Recommended</td>

        <td>The DPMS provides energy-saving power management for most modern monitors.</td>

        <td>KDE enables you to configure your display's DPMS settings.</td>

        <td>kdebase</td>
      </tr>

      <tr valign="top">
        <td><a href="http://www.xfree86.org/">X Video Extension</a></td>

        <td bgcolor="#c5ff7f">Optional</td>

        <td>The X Video Extension (xv) provides hardware-accelerated video playback for X Servers.</td>

        <td>The video subsytem will have much improved performance if the X Video Extension is available.</td>

        <td>kdemultimedia</td>
      </tr>

      <tr valign="top">
        <td><a href="http://www.cs.wisc.edu/~ghost/index.html">Ghostscript</a></td>

        <td bgcolor="#ffff8e">Recommended</td>

        <td>Ghostscript provides PostScript&reg; and PDF (Portable Document Format) support for KDE. PostScript is necessary for printing.</td>

        <td>Almost all of KDE benefits from GhostScript due to the printing support it provides. Version 6.53 or later is recommended.</td>

        <td>kdebase, kdeaddons, kdeedu, kdegraphics, kdemultimedia, kdenetwork, kdepim, kdeutils, koffice, kdevelop</td>
      </tr>

      <tr valign="top">
        <td><a href="http://">Database Server</a></td>

        <td bgcolor="#ffff8e">Recommended</td>

        <td>Databases provide a scalable method for organizing, accessing and modifying data quickly.</td>

        <td>Various applications require or work better with a database server. While the database requirements vary from application to application, the most common requirements are <a href="http://www.mysql.com/">MySQL</a> and/or <a href="http://www.postgresql.org/">PostgreSQL</a> and/or <a href="http://genix.net/unixODBC/">unixODBC</a>.</td>

        <td>kdepim</td>
      </tr>

      <tr valign="top">
        <td><a href="http://www.sleepycat.com/">Berkely DB II</a></td>

        <td bgcolor="#ffff8e">Recommended</td>

        <td>Berkely DB provides a fast, scalable, in-process database engine.</td>

        <td>KBabel, the KDE translation tool, can use DB II.</td>

        <td>kdesdk</td>
      </tr>

      <tr valign="top">
        <td><a href="http://www.python.org/">Python</a></td>

        <td bgcolor="#c5ff7f">Optional</td>

        <td>Python is a modern, object-oriented scripting language.</td>

        <td>Some KOffice components use Python as a scripting engine. In addition, Python bindings to KDE are available for KDE development using Python.</td>

        <td>kdebindings, koffice</td>
      </tr>

      <tr valign="top">
        <td><a href="http://www.perl.com/">Perl</a></td>

        <td bgcolor="#ffff8e">Recommended</td>

        <td>Perl is a modern scripting language.</td>

        <td>KSirc can be scripted in Perl. In addition, the KDE upgrade tool uses Perl to update configuration files when upgrading KDE.</td>

        <td>kdebase, kdenetwork, kdevelop</td>
      </tr>

      <tr valign="top">
        <td><a href="http://www.digistar.com/bzip2/">bzip2</a></td>

        <td bgcolor="#ff8c8c">Required</td>

        <td>Bzip2 is a patent-free compression and decompression library. It provides greater compression, but is substantially slower, than gzip.</td>

        <td>Some KDE applications enable compressing or decompressing bzip'd files.</td>

        <td>kdelibs, kdeadmin, kdeutils</td>
      </tr>

      <tr valign="top">
        <td><a href="http://www.dante.de/">TeX</a></td>

        <td bgcolor="#c5ff7f">Optional</td>

        <td>TeX, together with LaTeX, provide the standard, high-quality UNIX publishing system.</td>

        <td>Some KDE applications can import or export TeX documents.</td>

        <td>kdegraphics, koffice</td>
      </tr>

      <tr valign="top">
        <td><a href="http://www.xfree86.org/">X Xinerama Extension</a></td>

        <td bgcolor="#c5ff7f">Optional</td>

        <td>The X Xinerama extension provides support for multi-headed (multi-monitor) setups.</td>

        <td>The KDE window manager supports the use of multiple monitors.</td>

        <td>kdebase</td>
      </tr>

      <tr valign="top">
        <td><a href="http://www.gzip.org/zlib/">zlib &gt;=1.1</a></td>

        <td bgcolor="#ff8c8c">Required</td>

        <td>A compression/decompression library.</td>

        <td>zlib is required by Qt, though Qt provides its own copy if one is not installed on your system. Various other KDE applications make use of zlib to compress or decompress data.</td>

        <td>kdelibs, kdebase, arts, kdeaddons, kdeadmin, kdeartwork, kdebindings, kdeedu, kdegames, kdegraphics, kdemultimedia, kdenetwork, kdepim, kdesdk, kdetoys, kdeutils, koffice, kdevelop</td>
      </tr>
      <tr>
        <td align="center" colspan="5">Help</td>
      </tr>

      <tr>
        <td align="center"><strong>Package</strong></td>

        <td align="center"><strong>Level</strong></td>

        <td align="center"><strong>Description</strong></td>

        <td align="center"><strong>Explanation</strong></td>

        <td align="center"><strong>Modules</strong></td>
      </tr>

      <tr valign="top">
        <td><a href="http://www.xmlsoft.org/">libxml2 &gt;= 2.4.8</a></td>

        <td bgcolor="#ffff8e">Recommended</td>

        <td>Libxml is an XML library. XML is a metalanguage to design markup languages, such as HTML.</td>

        <td>Libxml is used for reading KDE documentation. Note that in some cases earlier versions of libxml2 may work, though versions 2.4.26 through 2.4.28 have a known bug which prevents their use in KDE.</td>

        <td>kdelibs, kdebase, arts, kdeaddons, kdeadmin, kdeartwork, kdebindings, kdeedu, kdegames, kdegraphics, kdemultimedia, kdenetwork, kdepim, kdesdk, kdetoys, kdeutils, koffice, kdevelop</td>
      </tr>

      <tr valign="top">
        <td><a href="http://xmlsoft.org/XSLT/">libxslt &gt;= 1.0.7</a></td>

        <td bgcolor="#ffff8e">Recommended</td>

        <td>Libxslt is an XSLT library. XSLT is itself an XML language, but one which is used to define XML transformations (<em>i.e.</em>, from one XML language to another).</td>

        <td>Libxslt is needed to read KDE documentation.</td>

        <td>kdelibs, kdebase, arts, kdeaddons, kdeadmin, kdeartwork, kdebindings, kdeedu, kdegames, kdegraphics, kdemultimedia, kdenetwork, kdepim, kdesdk, kdetoys, kdeutils, koffice, kdevelop</td>
      </tr>

      <tr valign="top">
        <td><a href="http://www.htdig.org/">ht dig</a></td>

        <td bgcolor="#ffff8e">Recommended</td>

        <td>ht://dig is a document indexing and search facility.</td>

        <td>KDevelop uses ht://dig to index and search documentation.</td>

        <td>kdevelop</td>
      </tr>
      <tr>
        <td align="center" colspan="5">Hardware</td>
      </tr>

      <tr>
        <td align="center"><strong>Package</strong></td>

        <td align="center"><strong>Level</strong></td>

        <td align="center"><strong>Description</strong></td>

        <td align="center"><strong>Explanation</strong></td>

        <td align="center"><strong>Modules</strong></td>
      </tr>

      <tr valign="top">
        <td><a href="http://www.cups.org/">CUPS &gt;= 1.1.9</a></td>

        <td bgcolor="#c5ff7f">Optional</td>

        <td>CUPS (the Common Unix Printing System) is a modern printing architecture for UNIX systems.</td>

        <td>CUPS provides enhanced printing administration, printing options and usability to all KDE applications. KDE provides tools to configure CUPS.</td>

        <td>kdelibs, kdebase</td>
      </tr>

      <tr valign="top">
        <td><a href="http://www.gphoto.org/">gPhoto 2 &gt;= 2.0.1</a></td>

        <td bgcolor="#c5ff7f">Optional</td>

        <td>GPhoto is a set of digital camera software applications for Unix-like systems.</td>

        <td>Some KDE applications and services which can work with images on digital cameras require gPhoto 2.</td>

        <td>kdeaddons, kdegraphics</td>
      </tr>

      <tr valign="top">
        <td><a href="http://www.mostang.com/sane/">SANE</a></td>

        <td bgcolor="#c5ff7f">Optional</td>

        <td>SANE is an application programming interface (API) that provides standardized access to any raster image scanner hardware.</td>

        <td>KDE applications which work with scanners require SANE.</td>

        <td>kdelibs, kdegraphics</td>
      </tr>

      <tr valign="top">
        <td><a href="http://secure.netroedge.com/~lm78/">lm_sensors</a></td>

        <td bgcolor="#c5ff7f">Optional</td>

        <td>Lm_sensors provides some essential Linux monitoring tools for systems containing hardware health monitoring hardware, such as the LM78 and LM75.</td>

        <td>KDE applications which enable you to monitor your system's motherboard require lm_sensors to perform the monitoring function.</td>

        <td>kdeadmin</td>
      </tr>

      <tr valign="top">
        <td><a href="http://mtools.linux.lu/">mtools</a></td>

        <td bgcolor="#c5ff7f">Optional</td>

        <td>Mtools is a collection of utilities to access MS-DOS disks from Unix without mounting them. It supports Win'95 style long file names, OS/2 Xdf disks and 2m disks (store up to 1992k on a high density 3 1/2 disk).</td>

        <td>Using mtools you can access a floppy drive from Konqueror without first mounting the device.</td>

        <td>kdebase</td>
      </tr>

      <tr valign="top">
        <td><a href="https://www.jlogday.com/code/libmal/">libmal &gt;=0.20</a></td>

        <td bgcolor="#c5ff7f">Optional</td>

        <td>Provides the functionality to synchronize PalmOS handhelds with MAL servers like AvantGo.</td>

        <td>The AvantGo conduit of KPilot depends on this library to do the actual sync with AvantGo. If the library or the header files are not available on the building machine, the AvantGo conduit (malconduit) will not be compiled at all.</td>

        <td>kdepim</td>
      </tr>
      <tr>
        <td align="center" colspan="5">Networking</td>
      </tr>

      <tr>
        <td align="center"><strong>Package</strong></td>

        <td align="center"><strong>Level</strong></td>

        <td align="center"><strong>Description</strong></td>

        <td align="center"><strong>Explanation</strong></td>

        <td align="center"><strong>Modules</strong></td>
      </tr>

      <tr valign="top">
        <td><a href="ftp://cs.anu.edu.au/pub/software/ppp/">pppd</a></td>

        <td bgcolor="#c5ff7f">Optional</td>

        <td>Pppd is a PPP (Point-to-Point Protocol) daemon. PPP is a common serial protocol for connecting to the Internet over a computer modem.</td>

        <td>Kppp uses pppd to connect to the Internet using a computer POTS modem.</td>

        <td>kdenetwork</td>
      </tr>

      <tr valign="top">
        <td><a href="http://nicolas.brodu.free.fr/libsmb/">libsmb</a></td>

        <td bgcolor="#c5ff7f">Optional</td>

        <td>Libsmb is a library which provides access to Windows SMB shares. It provides client (both download and upload) services but not server services.</td>

        <td>Some KDE applications use libsmb to access Windows shares.</td>

        <td>kdenetwork</td>
      </tr>

      <tr valign="top">
        <td><a href="http://www.openldap.org/">libldap</a></td>

        <td bgcolor="#c5ff7f">Optional</td>

        <td>Libldap implments LDAP (the Lightweight Directory Access Protocol). LDAP provides access to X.500 directory services, both stand-alone and as part of a distributed directory service.</td>

        <td>The KDE Address Book and some PIM applications can access directory and user information from an LDAP server with libldap.</td>

        <td>kdelibs, kdepim</td>
      </tr>

      <tr valign="top">
        <td><a href="http://oss.sgi.com/projects/fam/">FAM</a></td>

        <td bgcolor="#ffff8e">Recommended</td>

        <td>FAM is a server which tracks changes to the filesystem and relays these changes to interested applications efficiently by either consolidating polling for all applications or, with kernel-level support, obtaining kernel notifications of file system changes.</td>

        <td>The KDE subsytem relies on monitoring changes in the filesystem so that the GUI provides an up-to-date view. FAM makes this process greatly more efficient.</td>

        <td>kdelibs, kdebase</td>
      </tr>

      <tr valign="top">
        <td><a href="http://www.openslp.org/">OpenSLP</a></td>

        <td bgcolor="#c5ff7f">Optional</td>

        <td>SLP (Service Location Protocol) implementation. Allows browsing and searching for network services.</td>

        <td>Used by the Desktop Sharing server and the Remote Desktop Connection client to find servers. For the client an installation of the library is sufficient, on servers the slpd daemon must run.</td>

        <td>kdenetwork</td>
      </tr>
      <tr valign="top">
        <td><a href="http://www.pilot-link.org/">Pilot-Link</a></td>

        <td bgcolor="#c5ff7f">Recommended</td>

        <td>pilot-link is a library that provides means to communicate with
        Palm OS (tm) devices such as the Palm Pilot, Handspring Visor, and Sony Clie.
        </td>

        <td>
        pilot-link is needed by KPilot, the KDE-to-Palm synchronization
        </td>

        <td>kdepim</td>
      </tr>
      <tr>
        <td align="center" colspan="5">Browsing</td>
      </tr>

      <tr>
        <td align="center"><strong>Package</strong></td>

        <td align="center"><strong>Level</strong></td>

        <td align="center"><strong>Description</strong></td>

        <td align="center"><strong>Explanation</strong></td>

        <td align="center"><strong>Modules</strong></td>
      </tr>

      <tr valign="top">
        <td><a href="http://java.sun.com/">Java &gt;= 1.3</a></td>

        <td bgcolor="#ffff8e">Recommended</td>

        <td>Java is a scripting / programming language expressly designed for use in the distributed environment of the Internet. Since Netscape integrated Java into its browser, Java has become a popular scripting language for websites.</td>

        <td>Some websites require the use of Java for some or all of their services. In addition, Java bindings exist for KDE which enables writing KDE applications in the Java language.</td>

        <td>kdebase, kdebindings</td>
      </tr>

      <tr valign="top">
        <td><a href="http://www.lesstif.org/">Lesstif</a></td>

        <td bgcolor="#c5ff7f">Optional</td>

        <td>Lesstif provides a free implementation of the Motif API. Motif was once the most popular GUI toolkit for UNIX systems.</td>

        <td>The KDE webbrowser Konqueror can use Netscape plugins, such as RealAudio and Flash. Many of these plugins use the Motif API, the API which was used by Netscape on UNIX systems.</td>

        <td>kdebase</td>
      </tr>

      <tr valign="top">
        <td><a href="http://www.winehq.org/">WINE</a></td>

        <td bgcolor="#c5ff7f">Optional</td>

        <td>WINE is an implementation of the Windows Win32 and Win16 APIs on top of X and UNIX. It can be used to execute an ever-increasing number of applications written for Windows on GNU/Linux and UNIX systems.</td>

        <td>The KDE web browser Konqueror can use some ActiveX controls, such as the Quicktime video player, with WINE.</td>

        <td>kdebase</td>
      </tr>

      <tr valign="top">
        <td><a href="http://www.codeweavers.com/products/crossover/">Crossover Plugin</a></td>

        <td bgcolor="#c5ff7f">Optional</td>

        <td>CrossOver Plugin, an extension of WINE, lets you use many Windows ActiveX plugins, such as QuickTime, ShockWave Director and Windows Media Player 6.4, directly from within your Linux browser.</td>

        <td>The KDE web browser Konqueror can use CrossOver Plugin to support some proprietary but common web technologies.</td>

        <td>kdebase</td>
      </tr>
      <tr valign="top">
        <td><a href="http://www.pcre.org/">PCRE</a></td>

        <td bgcolor="#ffff8e">Recommended</td>

        <td>The PCRE (Perl Compatible Regular Expressions) library is a set of functions that implement regular expression pattern
        matching using the same syntax and semantics as Perl 5.</td>

        <td>PCRE improves the regular-expression support in the KDE JavaScript engine (KJS), which is used by the KDE HTML
        rendering engine (KHTML). </td>

        <td>kdelibs</td>
      </tr>
      <tr>
        <td align="center" colspan="5">Security</td>
      </tr>

      <tr>
        <td align="center"><strong>Package</strong></td>

        <td align="center"><strong>Level</strong></td>

        <td align="center"><strong>Description</strong></td>

        <td align="center"><strong>Explanation</strong></td>

        <td align="center"><strong>Modules</strong></td>
      </tr>

      <tr valign="top">
        <td><a href="http://www.openssl.org/">OpenSSL &gt;= 0.9.6</a></td>

        <td bgcolor="#ffff8e">Recommended</td>

        <td>OpenSSL is a robust, commercial-grade, full-featured and Open Source toolkit implementing the Secure Sockets Layer (SSL v2/v3) and Transport Layer Security (TLS v1) protocols as well as a full-strength general purpose cryptography library.</td>

        <td>OpenSSL is used for the great bulk of secure communications, include secure web browsing via HTTPS, in KDE.</td>

        <td>kdelibs, kdebase, kdenetwork, kdepim</td>
      </tr>

      <tr valign="top">
        <td><a href="http://www.gnupg.org/">GnuPG &gt;= 1.0.6</a></td>

        <td bgcolor="#ffff8e">Recommended</td>

        <td>GnuPG is a complete and free replacement for PGP (Pretty Good Privacy). PGP uses a popular technique for encrypting and decrypting e-mail as well as for sending an encrypted digital signature that lets the receiver verify the sender's identity and know that the message was not modified en route.</td>

        <td>The KDE mail client KMail can use GnuPG to encrypt and decrypt emails as well as to authenticate the identify of the author of an email. Version 1.2.1 or higher is strongly recommended (and required for CryptPLug support), but 1.0.6 should suffice. Note that if you use CryptPlug, you may want to update to 1.2.2 as soon as it's released as it contains an important fix for CryptPlugs, namely recognizing when the user cancels pinentry.</td>

        <td>kdenetwork</td>
      </tr>

      <tr valign="top">
        <td><a href="http://java.sun.com/products/jsse/">JSSE</a></td>

        <td bgcolor="#ffff8e">Recommended</td>

        <td>JSSE (Java Secure Socket Extension) is a set of Java packages that enable secure Internet communications by Java applets.</td>

        <td>Some Java-enabled websites, particularly banking sites, require JSSE for full access to their services. The KDE web browser Konqueror will use JSSE if it is available on the system and properly configured. If you do not do online banking JSSE is optional.</td>

        <td>kdebase</td>
      </tr>

      <tr valign="top">
        <td><a href="http://www.kernel.org/pub/linux/libs/pam/">PAM</a></td>

        <td bgcolor="#c5ff7f">Optional</td>

        <td>PAM (Pluggable Authentication Modules) is a flexible system for authenticating users.</td>

        <td>Various KDE applications can use the PAM modules for user authentication.</td>

        <td>kdebase</td>
      </tr>

      <tr valign="top">
        <td><a href="http://www.gnupg.org/aegypten/">CryptPlug &gt;= 0.3.15</a></td>

        <td bgcolor="#ffff8e">Recommended</td>

        <td>Plugins for KMail OpenPGP/MIME and S/MIME support. CryptPlug itself depends on a "few" other libraries and supporting programs, see http://kmail.kde.org/kmail-pgpmime-howto.html and http://www.gnupg.org/aegypten/development.en.html</td>

        <td>Plugins for KMail OpenPGP/MIME and S/MIME support. Not needed for traditional "clearsigning" OpenPGP support, which is built-in to KMail.</td>

        <td>kdenetwork</td>
      </tr>
      <tr>
        <td align="center" colspan="5">Graphics</td>
      </tr>

      <tr>
        <td align="center"><strong>Package</strong></td>

        <td align="center"><strong>Level</strong></td>

        <td align="center"><strong>Description</strong></td>

        <td align="center"><strong>Explanation</strong></td>

        <td align="center"><strong>Modules</strong></td>
      </tr>

      <tr valign="top">
        <td><a href="http://mesa3d.sourceforge.net/">OpenGL</a></td>

        <td bgcolor="#ffff8e">Recommended</td>

        <td>OpenGL is an industry-standard API for developing portable, interactive 2D and 3D graphics applications.</td>

        <td>Various applications, from graphics applications and modellers to screensavers and video players, make use of the 3d hardware acceleration available through the OpenGL API.</td>

        <td>kdelibs, kdebase, kdeaddons, kdegraphics, kdemultimedia</td>
      </tr>

      <tr valign="top">
        <td><a href="http://www.libsdl.org/index.php">SDL</a></td>

        <td bgcolor="#c5ff7f">Optional</td>

        <td>SDL (Simple DirectMedia Layer) is a cross-platform multimedia library designed to provide fast access to the graphics framebuffer and audio device.</td>

        <td>Some <a href="http://noatun.kde.org/">Noatun</a> plugins require SDL.</td>

        <td>kdemultimedia</td>
      </tr>

      <tr valign="top">
        <td><a href="http://www.libtiff.org/">libtiff</a></td>

        <td bgcolor="#c5ff7f">Optional</td>

        <td>Libtiff provides support for the Tag Image File Format (TIFF), a widely used format for storing image data, particular for facsimile transmissions.</td>

        <td>Libtiff is needed for KDE applications which send or receive faxes.</td>

        <td>kdebase, kdegraphics</td>
      </tr>

      <tr valign="top">
        <td><a href="http://www.libpng.org/pub/png/libpng.html">libpng</a></td>

        <td bgcolor="#ff8c8c">Required</td>

        <td>Libpng is the official PNG (Portable Network Graphics) reference library.</td>

        <td>PNG images are the standard image format for KDE. Libpng is required to view and save such images. Note that most image handling is done by Qt, which will provide its own copy of libpng if one is not installed on the system.</td>

        <td>kdelibs, kdebase</td>
      </tr>

      <tr valign="top">
        <td><a href="ftp://ftp.uu.net/graphics/jpeg/">libjpeg</a></td>

        <td bgcolor="#ffff8e">Recommended</td>

        <td>Libjpeg is a library which implements JPEG image compression and decompression. JPEG is a standardized lossy compression method for full-color and gray-scale images, such as photographs, and is commonly useed on the Internet.</td>

        <td>KDE applications can view and save JPEG images using libjpeg.</td>

        <td>kdelibs, kdebase, kdegraphics, kdemultimedia, koffice</td>
      </tr>

      <tr valign="top">
        <td><a href="http://www.libmng.com/">libmng</a></td>

        <td bgcolor="#c5ff7f">Optional</td>

        <td>Libmng is the standard library implementation of MNG (Multiple-image Network Graphics). MNG, built on PNG, provides animated images, similar to animated GIF images.</td>

        <td>While not nearly as ubiquitous as animated GIFs, libmng is catching on as an animated image technology for the Internet.</td>

        <td>kdelibs, kdebase, kdegraphics</td>
      </tr>

      <tr valign="top">
        <td><a href="http://www.freetype.org/">freetype &gt;= 2.0.0</a></td>

        <td bgcolor="#ffff8e">Recommended</td>

        <td>FreeType 2 is a software font engine.</td>

        <td>Freetype 2 provides enhanced anti-aliased font handling and manipulation for the entire KDE desktop.</td>

        <td>kdelibs, kdebase, kdeadmin, kdeedu, kdegames, kdegraphics, kdemultimedia, kdenetwork, kdepim, kdeutils, koffice, kdevelop</td>
      </tr>

      <tr valign="top">
        <td><a href="http://www.foolabs.com/xpdf/">PDFInfo</a></td>

        <td bgcolor="#c5ff7f">Optional</td>

        <td>PDFInfo is a PDF (Portable Document Format) document information extractor.</td>

        <td>Some KDE applications can browse or provide enhanced information about PDF files using PDFInfo.</td>

        <td>kdebase, kdegraphics</td>
      </tr>

      <tr valign="top">
        <td><a href="ftp://ftp.gnu.org/pub/gnu/gift">GIFT &gt;= 0.1.9</a></td>

        <td bgcolor="#c5ff7f">Optional</td>

        <td>The GNU Image Finding Tool allows for searching images by example. E.g. right-click on an image in the filemanager and select "Search for similar Images". See <a href="http://www.fer-de-lance.org/">http://www.fer-de-lance.org/</a> for more information.</td>

        <td>Implemented as a KPart and kioslave, it is typically used with Konqueror, but it can also be used by other applications.</td>

        <td>kdegraphics</td>
      </tr>

      <tr valign="top">
        <td><a href="http://www.levien.com/libart/">libart &gt;= 2.3.8</a></td>

        <td bgcolor="#ffff8e">Recommended</td>

        <td>Libart is a library for high-performance 2D graphics, including anti-aliasing.</td>

        <td>Libart is required for KDE's SVG icon engine.</td>

        <td>kdelibs</td>
      </tr>
      <tr>
        <td align="center" colspan="5">Multimedia</td>
      </tr>

      <tr>
        <td align="center"><strong>Package</strong></td>

        <td align="center"><strong>Level</strong></td>

        <td align="center"><strong>Description</strong></td>

        <td align="center"><strong>Explanation</strong></td>

        <td align="center"><strong>Modules</strong></td>
      </tr>

      <tr valign="top">
        <td><a href="http://www.alsa-project.org/">ALSA</a></td>

        <td bgcolor="#ffff8e">Recommended</td>

        <td>ALSA (Advanced Linux Sound Architecture) provides audio and MIDI functionality for Linux.</td>

        <td>ALSA provides advanced audio support for various KDE multimedia and audio applications.</td>

        <td>kdemultimedia</td>
      </tr>

      <tr valign="top">
        <td><a href="http://www.fokus.gmd.de/research/cc/glone/employees/joerg.schilling/private/cdrecord.html">cdrtools</a></td>

        <td bgcolor="#c5ff7f">Optional</td>

        <td>cdrtools (f/k/a cdrecord) creates home-burned CDs with a CDR/CDRW recorder.</td>

        <td>Cdrtools is commonly required for burning CDs.</td>

        <td>kdebase, kdeaddons, kdemultimedia</td>
      </tr>

      <tr valign="top">
        <td><a href="http://www.xiph.org/paranoia/">cdparanoia</a></td>

        <td bgcolor="#c5ff7f">Optional</td>

        <td>Cdparanoia extracts audio from compact discs (audio CDs) directly as data, with no analog step between, and convers the data to various standard formats.</td>

        <td>Cdparanoia is used by most KDE CD "rippers", inclding the audiocd:// IO slave.</td>

        <td>kdebase, kdemultimedia</td>
      </tr>

      <tr valign="top">
        <td><a href="http://lame.sourceforge.net/">L.A.M.E.</a></td>

        <td bgcolor="#c5ff7f">Optional</td>

        <td>L.A.M.E. (LAME Ain't an Mp3 Encoder) is an MP3 encoder.</td>

        <td>L.A.M.E. is needed to save digital music in the MP3 format.</td>

        <td>kdemultimedia</td>
      </tr>

      <tr valign="top">
        <td><a href="http://www.xiph.org/ogg/index.html">Ogg Vorbis</a></td>

        <td bgcolor="#ffff8e">Recommended</td>

        <td>Ogg Vorbis is a patent-clear, fully open general purpose audio encoding format standard that rivals or surpasses the 'upcoming' generation of proprietary coders (AAC and TwinVQ, also known as VQF).</td>

        <td>Ogg Vorbis is needed to listen to digital music in the Ogg Vorbis format or to save digial music using this excellent compression technology.</td>

        <td>kdebase, arts, kdeaddons, kdemultimedia</td>
      </tr>

      <tr valign="top">
        <td><a href="http://webs1152.im1.net/~dsweet/XAnim/">XAnim</a></td>

        <td bgcolor="#c5ff7f">Optional</td>

        <td>XAnim provides a large number of (older) video codecs.</td>

        <td>Aktion! can use XAnim to play some (older) videos.</td>

        <td>kdemultimedia</td>
      </tr>

      <tr valign="top">
        <td><a href="http://freeware.sgi.com/source/audiofile/">libaudiofile</a></td>

        <td bgcolor="#ffff8e">Recommended</td>

        <td>Libaudio implements a library for the .WAV audio format.</td>

        <td>Libaudio is needed to play or save .WAV audio files.</td>

        <td>kdelibs, kdemultimedia</td>
      </tr>

      <tr valign="top">
        <td><a href="http://xinehq.de/">XINE</a></td>

        <td bgcolor="#ffff8e">Recommended</td>

        <td>XINE is a multimedia system for playback of CDs, DVDs and VCDs and for decoding multimedia streams or files such as .AVI, .MOV, .WMV and .MP3.</td>

        <td>The KDE multimedia system is switching to XINE for basic video support as of KDE 3.1.</td>

        <td>kdemultimedia</td>
      </tr>
      <tr>
        <td align="center" colspan="5">Development</td>
      </tr>

      <tr>
        <td align="center"><strong>Package</strong></td>

        <td align="center"><strong>Level</strong></td>

        <td align="center"><strong>Description</strong></td>

        <td align="center"><strong>Explanation</strong></td>

        <td align="center"><strong>Modules</strong></td>
      </tr>

      <tr valign="top">
        <td><a href="http://subversion.tigris.org/">Subversion</a></td>

        <td bgcolor="#c5ff7f">Optional</td>

        <td>Subversion is a revision control system like CVS.</td>

        <td>KDevelop 3.0 can use subversion to maintain a revision control repository for projects.</td>

        <td>kdevelop</td>
      </tr>

      <tr valign="top">
        <td><a href="http://www.cvshome.org/">CVS</a></td>

        <td bgcolor="#c5ff7f">Optional</td>

        <td>The Concurrent Versioning System is a very popular revision control system, typically used in software project development.</td>

        <td>CVS is needed by KDevelop and certain other applications to access CVS repositories.</td>

        <td>kdesdk, kdevelop</td>
      </tr>

      <tr valign="top">
        <td><a href="http://www.perforce.com/">Perforce</a></td>

        <td bgcolor="#c5ff7f">Optional</td>

        <td>Perforce is a revision control system like CVS.</td>

        <td>KDevelop 3.0 can use the <code>p4</code> command line tool to enable Perforce as the backend revision control repository for projects.</td>

        <td>kdevelop</td>
      </tr>

      <tr valign="top">
        <td><a href="http://ctags.sourceforge.net/">Exuberant Ctags</a></td>

        <td bgcolor="#ffff8e">Recommended</td>

        <td>Ctags indexes source code files that allows these items to be quickly and easily located by a text editor or other utility.</td>

        <td>KDevelop uses ctags, if available, to provide improved source code navigation.</td>

        <td>kdevelop</td>
      </tr>

      <tr valign="top">
        <td><a href="http://www.doxygen.org/">doxygen</a></td>

        <td bgcolor="#ffff8e">Recommended</td>

        <td>Doxygen is a cross-platform, <a href="http://java.sun.com/products/jdk/javadoc/">JavaDoc</a>-like documentation system for C, C++ and IDL (Corba, Microsoft&reg; and KDE DCOP flavors).</td>

        <td>Doxygen is used to generate the kdelibs API documentation. If you want to develop software using KDE you should install doxygen.</td>

        <td>kdebindings, kdesdk, kdevelop</td>
      </tr>
    </table>

<?php
  include "footer.inc"
?>
