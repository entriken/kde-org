---
aliases:
- ../../kde-frameworks-5.17.0
date: 2015-12-12
layout: framework
libCount: 60
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---

### Baloo

- Fix date filter used by timeline://
- BalooCtl: Return after commands
- Clean up and armour Baloo::Database::open(), handle more crash conditions
- Add check in Database::open(OpenDatabase) to fail if db doesn't exist

### Breeze Icons

- Many icons added or improved
- use stylesheets in breeze icons (bug 126166)
- BUG: 355902 fix and changed system-lock-screen (bug 355902 fix and changed system-lock-screen)
- Add 24px dialog-information for GTK apps (bug 355204)

### Extra CMake Modules

- Don't warn when SVG(Z) icons are provided with multiple sizes/level of detail
- Make sure we load translations on the main thread. (bug 346188)
- Overhaul the ECM build system.
- Make it possible to enable Clazy on any KDE project
- Do not find XCB's XINPUT library by default.
- Clean export dir before generating an APK again
- Use quickgit for Git repository URL.

### Framework Integration

- Add plasmoid installation failed to plasma_workspace.notifyrc

### KActivities

- Fixed a lock on the first start of the daemon
- Moving QAction creation to the main thread. (bug 351485)
- Sometimes clang-format makes a bad decision (bug 355495)
- Killing potential synchronization issues
- Use org.qtproject instead of com.trolltech
- Removing the usage of libkactivities from the plugins
- KAStats config removed from the API
- Added linking and unlinking to ResultModel

### KDE Doxygen Tools

- Make kgenframeworksapidox more robust.

### KArchive

- Fix KCompressionDevice::seek(), called when creating a KTar on top of a KCompressionDevice.

### KCoreAddons

- KAboutData: Allow https:// and other URL schemas in homepage. (bug 355508)
- Repair MimeType property when using kcoreaddons_desktop_to_json()

### KDeclarative

- Port KDeclarative to use KI18n directly
- DragArea delegateImage can now be a string from which an icon is automatically created
- Add new CalendarEvents library

### KDED

- Unset SESSION_MANAGER envvar instead of setting it empty

### KDELibs 4 Support

- Fix some i18n calls.

### KFileMetaData

- Mark m4a as readable by taglib

### KIO

- Cookie dialogue: make it work as intended
- Fix filename suggestion changing to something random when changing save-as mimetype.
- Register DBus name for kioexec (bug 353037)
- Update KProtocolManager after configuration change.

### KItemModels

- Fix KSelectionProxyModel usage in QTableView (bug 352369)
- Fix resetting or changing the source model of a KRecursiveFilterProxyModel.

### KNewStuff

- registerServicesByGroupingNames can define default more items
- Make KMoreToolsMenuFactory::createMenuFromGroupingNames lazy

### KTextEditor

- Add syntax highlighting for TaskJuggler and PL/I
- Make it possible to disable keyword-completion via the config interface.
- Resize the tree when the completion model got reset.

### KWallet Framework

- Correctly handle the case where the user deactivated us

### KWidgetsAddons

- Fix a small artifact of KRatingWidget on hi-dpi.
- Refactor and fix the feature introduced in bug 171343 (bug 171343)

### KXMLGUI

- Don't call QCoreApplication::setQuitLockEnabled(true) on init.

### Plasma Framework

- Add basic plasmoid as example for developerguide
- Add a couple of plasmoid templates for kapptemplate/kdevelop
- [calendar] Delay the model reset until the view is ready (bug 355943)
- Don't reposition while hiding. (bug 354352)
- [IconItem] Don't crash on null KIconLoader theme (bug 355577)
- Dropping image files onto a panel will no longer offer to set them as wallpaper for the panel
- Dropping a .plasmoid file onto a panel or the desktop will install and add it
- remove the now unused platformstatus kded module (bug 348840)
- allow paste on password fields
- fix positioning of edit menu, add a button to select
- [calendar] Use ui language for getting the month name (bug 353715)
- [calendar] Sort the events by their type too
- [calendar] Move the plugin library to KDeclarative
- [calendar] qmlRegisterUncreatableType needs a bit more arguments
- Allow adding config categories dynamically
- [calendar] Move the plugins handling to a separate class
- Allow plugins to supply event data to Calendar applet (bug 349676)
- check for slot existence before connecting or disconnecting (bug 354751)
- [plasmaquick] Don't link OpenGL explicitly
- [plasmaquick] Drop XCB::COMPOSITE and DAMAGE dependency

You can discuss and share ideas on this release in the comments section of <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>the dot article</a>.
