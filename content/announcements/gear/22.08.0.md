---
description: KDE Gear ⚙️ 22.08 brings improvements and new features to dozens of apps
authors:
  - SPDX-FileCopyrightText: 2022 Paul Brown <paul.brown@kde.org>
  - SPDX-FileCopyrightText: 2022 Aniqa Khokhar <aniqa.khokhar@kde.org>
  - SPDX-FileCopyrightText: 2022 Áron Kovács <aronkvh@gmail.com>
  - SPDX-FileCopyrightText: 2022 Guilherme MS. <guimarcalsilva@gmail.com>
SPDX-License-Identifier: CC-BY-4.0
date: 2022-08-18
hero_image: hero.webp
images:
  - /announcements/gear/22.08.0/hero.webp
layout: gear
scssFiles:
- /scss/gear-22-08.scss
highlights:
  title: Highlights
  items:
  - header: Kate/KWrite
    text: Multi-cursor support in KDE's text editors
    hl_video:
      poster: https://cdn.kde.org/promo/Announcements/Apps/22.08/KWrite.png
      mp4: https://cdn.kde.org/promo/Announcements/Apps/22.08/KWrite.mp4
      webm: https://cdn.kde.org/promo/Announcements/Apps/22.08/KWrite.webm
    hl_class: konsole
  - header: Elisa
    text: A music player also made for touchscreens
    hl_video:
      poster: https://cdn.kde.org/promo/Announcements/Apps/22.08/Elisa.png
      mp4: https://cdn.kde.org/promo/Announcements/Apps/22.08/Elisa.mp4
      webm: https://cdn.kde.org/promo/Announcements/Apps/22.08/Elisa.webm
    hl_class: kdenlive
draft: false
---
# KDE Gear ⚙️ 22.08 has Landed!

KDE Gear ⚙️ is the collection of KDE apps, frameworks and libraries that all release new versions at the same time. Version 22.08 brings updates for KDE programs for working, developing your creativity and enjoying your free time without having to submit yourself to extortionate licenses, intrusive advertising, or surrender your privacy.

Discover the most important changes added in the last four months to software designed to make your life better!

# <a id="whatsnew"></a>What's New

{{< highlight-grid >}}

## [Spectacle](https://apps.kde.org/spectacle/)

Spectacle is KDE's popular screen-capturing app, that gives you image annotating features, allows you to push your screencaps directly to social media, online image hosting services, instant messaging apps, and much more.

To help you up your screen-capturing game, Spectacle now [displays the keyboard shortcuts](https://bugs.kde.org/show_bug.cgi?id=389778) with the different capture modes right there, in the capture mode selection combobox.

And once you have your image, [Spectacle resizes itself in annotation mode](https://bugs.kde.org/show_bug.cgi?id=429833), so you don't have to manually make the window big every time you want to annotate a big image. Once you're finished, Spectacle will return to its original size do you can carry on working as normal.

{{< video src-webm="https://cdn.kde.org/promo/Announcements/Apps/22.08/Spectacle.webm" src="https://cdn.kde.org/promo/Announcements/Apps/22.08/Spectacle.mp4" poster="https://cdn.kde.org/promo/Announcements/Apps/22.08/Spectacle.png" fig_class="mx-auto max-width-800" >}}

## [Kalendar](https://apps.kde.org/kalendar/)

Kalendar is a modern calendar and life-organizer that can get your appointments and list of daily chores from many sources... And, [among many other features](https://carlschwan.eu/2022/07/04/kde-pim-in-may-and-june/#kalendar), the new version [includes contacts](https://carlschwan.eu/2022/06/14/kalendar-contact-book-current-progress/)! As with calendars, you can add address books from a wide variety of sources and view them from a desktop/panel widget. Kalendar can also generate a QR Code for a contact in case you want to share it to a mobile device.

In other news, the calendar view has been enhanced and you can now see subtasks and parent tasks in the task sidebar, making it easier to navigate between them.

## [KDE Itinerary](https://apps.kde.org/itinerary/)

Itinerary is KDE's travel assistant that helps you plan your trip; manages your tickets, boarding passes and even health certificates for you, and guides you through airports, train and bus stations, and other transport hubs.

Itinerary developers [have been very busy](https://volkerkrause.eu/2022/06/04/kde-itinerary-april-may-2022.html) [over the last few months](https://volkerkrause.eu/2022/07/30/kde-itinerary-june-july-2022.html) and have added an integrated barcode scanner that allows you to import paper tickets into the app, as well as support for flat rate tickets or discount program cards.

{{< video src-webm="https://cdn.kde.org/promo/Announcements/Apps/22.08/Itinerary.webm" src="https://cdn.kde.org/promo/Announcements/Apps/22.08/Itinerary.mp4" poster="https://cdn.kde.org/promo/Announcements/Apps/22.08/Itinerary.png" fig_class="mx-auto max-width-800" >}}

To make your travelling easier, train and bus trips can be imported from online journey queries, and, if you are travelling to a meet-up or work do, events can be imported from and exported to the system calendar.

Feel like a detour on a multi-stage trip? No problem: the alternate route option is now available for a single part of a journey.

**IMPORTANT:** If you are using Itinerary on Android, please note that new updates are **NOT** available in Google's Play Store any more. Download KDE Itinerary from [KDE's F-Droid repo](https://community.kde.org/Android/FDroid) to keep up to date.

## [Kate](https://kate-editor.org/)/[KWrite](https://apps.kde.org/kwrite/)

Kate and KWrite are KDE's all-important and feature-rich text editors. While KWrite is simple and straightforward, ideal for quick edits without distractions, Kate packs a whole development environment under its hood and includes, multiple panels for a command line terminal, markup preview and project structure, integration with Git, and much more.

Both of Kate and KWrite have recently incorporated a nifty new feature that allows you to place several cursors throughout a document and add text or code in various places simultaneously.

{{< video src-webm="https://cdn.kde.org/promo/Announcements/Apps/22.08/KWrite.webm" src="https://cdn.kde.org/promo/Announcements/Apps/22.08/KWrite.mp4" poster="https://cdn.kde.org/promo/Announcements/Apps/22.08/KWrite.png" fig_class="mx-auto max-width-800" >}}

KWrite has added another interesting new feature in that it now supports tabs and screen splitting so you can open several documents at the same time.

Meanwhile, Kate has become easier to use as it [now shows its toolbar by default](https://invent.kde.org/utilities/kate/-/merge_requests/733) and its menu bar [has been re-arranged to make each option less huge and intimidating](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/353). In particular, there is now a new "Selection" menu that holds actions which will be applied only to whatever is selected.

## [Filelight](https://apps.kde.org/filelight/)

Filelight let's you see clearly how much room you have in your storage and find out what is taking up so much space! Developers have overhauled Filelight's appearance to modernize its look and, in the process, made the code easier to maintain by [porting it to QtQuick](https://invent.kde.org/utilities/filelight/-/merge_requests/27).

{{< video src-webm="https://cdn.kde.org/promo/Announcements/Apps/22.08/Filelight.webm" src="https://cdn.kde.org/promo/Announcements/Apps/22.08/Filelight.mp4" poster="https://cdn.kde.org/promo/Announcements/Apps/22.08/Filelight.png" fig_class="mx-auto max-width-800" >}}

Another improvement is that the text in Filelight’s tooltips [is no longer clipped away at the ends](https://bugs.kde.org/show_bug.cgi?id=431447).

## [Partition Manager](https://apps.kde.org/partitionmanager/)

...And talking of storage, Partition Manager, KDE's tool for creating, moving and resizing partitions on your disks, [has corrected some glitches that affected text in the information panel](https://invent.kde.org/system/partitionmanager/-/merge_requests/20), and [now shows human-readable text for the amount of time that a disk has been powered on](https://bugs.kde.org/show_bug.cgi?id=449386).

## [Dolphin](https://apps.kde.org/dolphin/)

Dolphin is KDE's powerful file manager that lets you move, copy, remove files and directories, and connect to remote hosts over SAMBA, SSH, FTP, and more.

{{< video src-webm="https://cdn.kde.org/promo/Announcements/Apps/22.08/Dolphin_new.webm" src="https://cdn.kde.org/promo/Announcements/Apps/22.08/Dolphin_new.mp4" poster="https://cdn.kde.org/promo/Announcements/Apps/22.08/Dolphin_new.png" fig_class="mx-auto max-width-800" >}}

Dolphin [now lets you sort files also by file extension if you prefer that](https://invent.kde.org/system/dolphin/-/merge_requests/38) and you can [remove individual items from the "Recent Files" and "Recent Locations" lists in Dolphin, file dialogs, and other places](https://bugs.kde.org/show_bug.cgi?id=447312). Also, when you cancel loading a slow folder in Dolphin, the message that appears in the middle of the window does not say "Folder is empty" anymore, but says ["Loading canceled" instead](https://invent.kde.org/system/dolphin/-/merge_requests/416).

## [Elisa](https://apps.kde.org/elisa/)

Elisa is KDE's elegant and compact music player, and has improved its accessibility features in this version by making the playlist sidebar [once again keyboard-navigable](https://invent.kde.org/multimedia/elisa/-/merge_requests/357). In version 22.08 it works even better than ever before, as you can reach and trigger all controls for each item in it.

Furthermore, if you are using a touchscreen, tapping a song in Elisa’s playlist [will play it immediately rather than simply selecting it](https://bugs.kde.org/show_bug.cgi?id=454343). We have also made the [the items on the playlist taller and more finger-friendly](https://invent.kde.org/multimedia/elisa/-/commit/74c3cb393dcce8b8a9b450c465cc59a6e599bb46), making it easier to use.

{{< video src-webm="https://cdn.kde.org/promo/Announcements/Apps/22.08/Elisa.webm" src="https://cdn.kde.org/promo/Announcements/Apps/22.08/Elisa.mp4" poster="https://cdn.kde.org/promo/Announcements/Apps/22.08/Elisa.png" fig_class="mx-auto max-width-800" >}}

As for your music collection, you can now choose to [disable the automatic scan on startup, and only run it manually](https://invent.kde.org/multimedia/elisa/-/merge_requests/256), and [sort tracks view by "Date Modified"](https://invent.kde.org/multimedia/elisa/-/merge_requests/359) — useful if you want to play things you've recently added or changed. Finally, Elisa's "Files" view [starts at](https://bugs.kde.org/show_bug.cgi?id=420895) root in version 22.08, so you can easily access music that isn't in your home folder.

## General

Apart for new versions of apps, KDE Gear delivers updates to the underlying frameworks that power the apps. That means that upgrades in a framework will pop up also in apps that depend on it.

Like Dolphin, Gwenview, and Spectacle now use the XDG Portals interface for dragged-and-dropped files. This allows them to successfully drop files into sandboxed apps without punching a hole in the sandbox by giving it access to your entire home folder or the system’s temp folder.

And in the Plasma Wayland session, a wide variety of single-window KDE apps now have their existing windows brought to the front when re-launched from Kickoff, KRunner, etc.

## And all This Too...

* Ark [now checks to make sure that there will be sufficient free space in the place when you’re trying to un-archive something before it starts](https://bugs.kde.org/show_bug.cgi?id=393959)
* Clicking on a Konsole notification about a particular session [now takes you to that session in Konsole](https://invent.kde.org/utilities/konsole/-/merge_requests/581)
* Skanpage [now supports exporting searchable PDFs using optical character recognition!](https://invent.kde.org/utilities/skanpage/-/merge_requests/24)
* [Gwenview](https://apps.kde.org/gwenview/), KDE's popular image-viewer, [adds annotations](https://bugs.kde.org/show_bug.cgi?id=408551) to its image-editing functions. The annotation feature is the same as the one used in Spectacle, so you will probably already be familiar with it.

---

### [Full changelog here](/announcements/changelogs/gear/22.08.0/).

## KDE is All About the Apps

One of the Goals of KDE is to be [All About the Apps](https://kde.org/goals/). This means the KDE Community takes more charge of releasing our own software and delivering it directly to you. Although we fully support distributions that ship our software, KDE Gear 22.04 apps will also be available on these Linux app stores shortly:

{{< img class="text-center img-fluid" src="/images/snapcraft.png" caption="Snapcraft" style="width: 600px" href="https://snapcraft.io/publisher/kde" >}}

{{< img class="text-center img-fluid" src="/images/flathub.png" caption="Flathub" style="width: 600px" href="https://flathub.org/apps/search/kde" >}}

If you'd like to help us get more KDE applications into the app stores, support more app stores and get the apps better integrated into our development process, come say hi in our [All About the Apps chat room](https://webchat.kde.org/#/room/#kde-all-about-apps:kde.org).

![Screenshots of many applications](many_apps.webp)
