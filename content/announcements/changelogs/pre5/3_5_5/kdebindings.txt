2006-08-08 07:12 +0000 [r570941]  mueller

	* branches/KDE/3.5/kdebindings/qtruby/rubylib/qtruby/handlers.cpp:
	  also try to marshall strings with NUL bytes

2006-08-09 10:30 +0000 [r571342]  mueller

	* trunk/extragear/utils/Makefile.am.in,
	  trunk/extragear/graphics/Makefile.am.in,
	  branches/KDE/3.5/kdeaddons/Makefile.am.in,
	  branches/KDE/3.5/kdebase/Makefile.am.in,
	  branches/KDE/3.5/kdeedu/Makefile.am.in,
	  branches/KDE/3.5/kdesdk/Makefile.am.in,
	  branches/koffice/1.5/koffice/Makefile.am.in,
	  branches/KDE/3.5/kdepim/Makefile.am.in,
	  branches/KDE/3.5/kdeaccessibility/Makefile.am.in,
	  branches/KDE/3.5/kdeadmin/Makefile.am.in,
	  branches/KDE/3.5/kdenetwork/Makefile.am.in,
	  branches/KDE/3.5/kdelibs/Makefile.am.in,
	  branches/KDE/3.5/kdeartwork/Makefile.am.in,
	  branches/arts/1.5/arts/Makefile.am.in,
	  trunk/extragear/pim/Makefile.am.in,
	  branches/KDE/3.5/kdemultimedia/Makefile.am.in,
	  branches/KDE/3.5/kdegames/Makefile.am.in,
	  trunk/playground/sysadmin/Makefile.am.in,
	  trunk/extragear/network/Makefile.am.in,
	  trunk/extragear/libs/Makefile.am.in,
	  branches/KDE/3.5/kdebindings/Makefile.am.in,
	  branches/KDE/3.5/kdetoys/Makefile.am.in,
	  trunk/extragear/multimedia/Makefile.am.in,
	  branches/KDE/3.5/kdeutils/Makefile.am.in,
	  branches/KDE/3.5/kdegraphics/Makefile.am.in: update automake
	  version

2006-08-10 21:03 +0000 [r571840]  rdale

	* branches/KDE/3.5/kdebindings/qtruby/rubylib/qtruby/handlers.cpp,
	  branches/KDE/3.5/kdebindings/qtruby/ChangeLog: * The Ruby VALUE
	  to 'uchar *' marshaller wasn't working correctly if the Ruby
	  string contained nulls. Fixed by Dirk Mueller (thanks) and also
	  applied for 'char *' types.

2006-08-14 16:04 +0000 [r573029]  mueller

	* branches/KDE/3.5/kdebindings/kdejava/koala/kdejava/KDEJavaSlot.h:
	  fix typo

2006-08-18 11:52 +0000 [r574198]  rdale

	* branches/KDE/3.5/kdebindings/korundum/rubylib/korundum/kdehandlers.cpp,
	  branches/KDE/3.5/kdebindings/korundum/ChangeLog: * Added a
	  KActionPtrList marshaller. Fixes problem reported by eskatos on
	  the #qtruby irc channel.

2006-08-18 13:49 +0000 [r574238]  rdale

	* branches/KDE/3.5/kdebindings/korundum/rubylib/korundum/kdehandlers.cpp:
	  * Another attempt at adding a KActionListPtr marshaller

2006-08-28 19:23 +0000 [r578251]  ctennis

	* branches/KDE/3.5/kdebindings/qtruby/rubylib/qtruby/handlers.cpp:
	  small memory leak fix (backported)

2006-08-29 12:07 +0000 [r578465]  ctennis

	* branches/KDE/3.5/kdebindings/korundum/rubylib/korundum/Korundum.cpp:
	  This closes what looks to be a large portion (but not all) of the
	  memory leak I'm seeing in Korundum's DCOP slots/signals. CCMAIL:
	  kde-bindings@kde.org

2006-08-29 12:16 +0000 [r578473]  ctennis

	* branches/KDE/3.5/kdebindings/korundum/rubylib/korundum/Korundum.cpp:
	  Fix another memory leak. CCMAIL: kde-bindings@kde.org

2006-08-29 13:00 +0000 [r578487]  rdale

	* branches/KDE/3.5/kdebindings/qtruby/rubylib/qtruby/Qt.cpp,
	  branches/KDE/3.5/kdebindings/qtruby/ChangeLog: * Backported some
	  memory leak fixes from Qt4 QtRuby CCMAIL: kde-bindings@kde.org

2006-08-29 14:01 +0000 [r578511]  ctennis

	* branches/KDE/3.5/kdebindings/qtruby/rubylib/qtruby/Qt.cpp: Clears
	  up a valgrind mem leak on destruction. CCMAIL:
	  kde-bindings@kde.org

2006-09-01 11:52 +0000 [r579654]  ctennis

	* branches/KDE/3.5/kdebindings/qtruby/rubylib/qtruby/Qt.cpp: Remove
	  this. It didn't actually fix the lost memory since they were
	  created with strdup (which uses malloc) and the autodeletion uses
	  the delete operator

2006-09-13 17:29 +0000 [r583859]  rdale

	* branches/KDE/3.5/kdebindings/qtruby/rubylib/qtruby/lib/Qt/qtruby.rb,
	  branches/KDE/3.5/kdebindings/qtruby/rubylib/qtruby/Qt.cpp,
	  branches/KDE/3.5/kdebindings/qtruby/ChangeLog: * Blocks can now
	  be used as slots in Qt::Object.connect() calls. There are two
	  versions, a singleton method and an instance method. * Here is an
	  example of the class method connect() call with a block as a
	  target: app = Qt::Application.new(ARGV) quit =
	  Qt::PushButton.new('Quit') Qt::Object.connect(quit,
	  SIGNAL('clicked()'), app) { puts 'quit clicked' } The block is
	  executed in the context of the target instance, 'app' in this
	  case. * And the instance method form: class MyButton < Qt::Button
	  def initialize(text) super(text) connect(self,
	  SIGNAL('clicked()')) { puts 'button clicked' } end ... The block
	  is executed in the context of self - the instance making the
	  connect() call. * The Rails version of the Ruby 1.9 method
	  Object#instance_exec was used to invoke the block. Thanks to
	  kelko on the #kde-ruby channel for the idea and the code, and
	  #rickdangerous for further discussion. CCMAIL:
	  kde-bindings@kde.org

2006-09-14 10:34 +0000 [r584205]  rdale

	* branches/KDE/3.5/kdebindings/qtruby/rubylib/qtruby/lib/Qt/qtruby.rb,
	  branches/KDE/3.5/kdebindings/qtruby/rubylib/qtruby/Qt.cpp,
	  branches/KDE/3.5/kdebindings/qtruby/ChangeLog: * Added a new
	  variant of connect, which takes a SIGNAL as an argument, along
	  with a block. For example: quit.connect(SIGNAL(:clicked)) { puts
	  'quit pressed' } The block is called in the context of where the
	  connect call was made, and 'self' needn't be a Qt::Object. It is
	  similar to the signal_connect() method in ruby-gnome. This was
	  suggested by rickdangerous on the #qtruby irc channel. CCMAIL:
	  kde-bindings@kde.org

2006-09-17 19:56 +0000 [r585700]  rdale

	* branches/KDE/3.5/kdebindings/qtruby/rubylib/qtruby/handlers.cpp,
	  branches/KDE/3.5/kdebindings/qtruby/ChangeLog: * Fixed bug
	  reported by Caleb Tennis where temporary QString const arguments
	  were only being deleting after marshalling if they weren't const
	  types. CCMAIL: kde-bindings@kde.org

2006-09-19 12:34 +0000 [r586385]  rdale

	* branches/KDE/3.5/kdebindings/qtruby/rubylib/qtruby/lib/Qt/qtruby.rb,
	  branches/KDE/3.5/kdebindings/qtruby/rubylib/qtruby/Qt.cpp,
	  branches/KDE/3.5/kdebindings/qtruby/ChangeLog: * Upped the QtRuby
	  version to 1.0.13 for the KDE 3.5.5 release * Fixed a crash when
	  a slot was inherited by a subclass, and the subclass had no slots
	  or signals of its own CCMAIL: kde-bindings@kde.org

2006-09-28 06:39 +0000 [r589485]  sedwards

	* branches/KDE/3.5/kdebindings/python/pykde/sip/kdeui/klistview.sip,
	  branches/KDE/3.5/kdebindings/python/sip/sipgen/gencode.c,
	  branches/KDE/3.5/kdebindings/python/sip/siplib/objmap.c,
	  branches/KDE/3.5/kdebindings/python/sip/siplib/qtlib.cpp,
	  branches/KDE/3.5/kdebindings/python/pykde/sip/khtml/khtmlview.sip,
	  branches/KDE/3.5/kdebindings/python/pykde/sip/kfile/kfilebookmark.sip,
	  branches/KDE/3.5/kdebindings/python/pykde/sip/kfile/kfileviewitem.sip,
	  branches/KDE/3.5/kdebindings/python/sip/siplib/siplib.c: * Fixed
	  up to work with Python 2.5 * Removed references to the old compat
	  header qlist.h.

2006-10-02 11:07 +0000 [r591327]  coolo

	* branches/KDE/3.5/kdebindings/kdebindings.lsm: updates for 3.5.5
