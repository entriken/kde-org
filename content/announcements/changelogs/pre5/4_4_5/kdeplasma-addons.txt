------------------------------------------------------------------------
r1134656 | bettio | 2010-06-05 08:10:21 +1200 (Sat, 05 Jun 2010) | 2 lines

We don't have anymore Hail wallpaper so it has been replaced with Lightning.

------------------------------------------------------------------------
r1135271 | scripty | 2010-06-07 13:57:02 +1200 (Mon, 07 Jun 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1135663 | mfuchs | 2010-06-08 09:51:29 +1200 (Tue, 08 Jun 2010) | 3 lines

Adds method to check if an url is redirected.
Also adds a way to check of which version the api is.
BUG:207648
------------------------------------------------------------------------
r1135992 | nlecureuil | 2010-06-09 03:50:12 +1200 (Wed, 09 Jun 2010) | 7 lines

Backport commits:
- 1135635
- 1135657

BUG: 214943
FIXED-IN: 4.4.5

------------------------------------------------------------------------
r1136131 | scripty | 2010-06-09 14:04:54 +1200 (Wed, 09 Jun 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1136657 | annma | 2010-06-10 23:35:05 +1200 (Thu, 10 Jun 2010) | 3 lines

Add bmp and tiff support
CCBUG=241292

------------------------------------------------------------------------
r1137818 | mfuchs | 2010-06-14 23:35:31 +1200 (Mon, 14 Jun 2010) | 2 lines

First always turn off configurationRequired if data comes in.
BUG:241040
------------------------------------------------------------------------
r1138423 | scripty | 2010-06-16 14:08:58 +1200 (Wed, 16 Jun 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1138691 | nlecureuil | 2010-06-17 01:49:31 +1200 (Thu, 17 Jun 2010) | 2 lines

Use existing icon

------------------------------------------------------------------------
r1139537 | annma | 2010-06-19 03:23:31 +1200 (Sat, 19 Jun 2010) | 2 lines

backport of 1139532: display "Full Moon" string

------------------------------------------------------------------------
r1139938 | annma | 2010-06-20 02:31:34 +1200 (Sun, 20 Jun 2010) | 3 lines

fix pastebin.com by backporting some trunk code as it works in trunk. Ivan, this will be in next 4.4.5 which should be out the 29th June. Thanks for the report and the follow-up!
BUG=242157

------------------------------------------------------------------------
r1140159 | scripty | 2010-06-20 14:09:07 +1200 (Sun, 20 Jun 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1141035 | scripty | 2010-06-22 14:47:15 +1200 (Tue, 22 Jun 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1141605 | scripty | 2010-06-23 14:08:06 +1200 (Wed, 23 Jun 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1141669 | wstephens | 2010-06-23 19:43:25 +1200 (Wed, 23 Jun 2010) | 3 lines

Backport r1141365, adding OAuth support to the Twitter dataengine
BUG: 242048

------------------------------------------------------------------------
