---
title : "KDE 3.0.5b Info Page"
publishDate: 2003-04-09 00:01:00
unmaintained: true
---

<p>
KDE 3.0.5b was released on April 9th, 2003 in response to the
<a href="/info/security/advisory-20030409-1.txt">PS/PDF handling vulnerability</a>.
</p>

<h2>Security Issues</h2>

<p>Please report possible problems to <a href="mai&#00108;to&#0058;s&#0101;&#00099;u&#x72;&#105;ty&#0064;&#107;de&#x2e;&#111;&#x72;g">s&#101;curi&#116;y&#064;&#x6b;d&#101;.&#00111;&#114;&#x67;</a>.</p>

<ul>
<li>
The telnet, rlogin, ssh and mailto URI handlers in KDE do not check for '-' at the beginning of the hostname
passed, which makes it possible to pass an option to the programs started by the handlers.
Read the <a href="security/advisory-20040517-1.txt">detailed advisory</a>.
All versions of KDE up to KDE 3.2.2 are affected.
</li>
</ul>

<h2>FAQ</h2>

See the <a href="faq.php">KDE FAQ</a> for any specific
questions you may have.  Questions about Konqueror should be directed
<a href="http://konqueror.kde.org/faq/">to the Konqueror FAQ</a> and sound related
questions are answered in the
<a href="http://www.arts-project.org/doc/handbook/faq.html">FAQ of the aRts Project</a>

<h2>Download and Installation</h2>

<u>Source code</u>

<a href="/info/sources/source-3.0.5b">Source 3.0.5b</a>

<p>
  These source packages have been digitally signed with
  <a href="http://www.gnupg.org/">GnuPG</a> using the KDE Archives PGP Key
  (available from the
  <a href="http://www.kde.org/signature.html">KDE Signature page</a>
  or public key servers).
</p>

<p>
  The translation package has been split into individual language
  packages so you can
  <a href="http://download.kde.org/stable/3.0.5b/src/kde-i18n/">download</a> only the
  translations you need.
</p>

<u>Binary packages</u>

<p>
Binary packages can be found under
<a href="http://download.kde.org/stable/3.0.5b/">http://download.kde.org/stable/3.0.5b/</a>
or in the equivalent directory at one of the KDE
<a href="/mirrors/ftp.php">FTP mirrors</a>.
</p>

<p>
Additional binary packages might become available in the coming weeks,
as well as updates to the current packages.
</p>

<h2>Updates</h2>

<h2>Security Issues</h2>

<p>Please report possible problems to <a href="m&#97;ilt&#111;&#0058;se&#x63;u&#114;&#x69;ty&#x40;&#x6b;d&#x65;.&#111;&#x72;&#x67;">s&#0101;cu&#114;i&#116;y&#00064;&#x6b;&#100;&#101;.org</a>.</p>

<h2><a name="bugs">Bugs</a></h2>

<p>This is a list of grave bugs and common pitfalls
surfacing after the release date:</p>

<ul>
<li>
A HTTP authentication credentials leak via the a "Referrer" was discovered by George Staikos
in Konqueror. If the HTTP authentication credentials were part of the URL they would be possibly sent
in the referer header to a 3rd party web site.
Read the <a href="security/advisory-20030729-1.txt">detailed advisory</a>. KDE 3.1.3 and newer
are not vulnerable.
</li>
</ul>

<h2>Developer Info</h2>

If you need help porting your application to KDE 3.x see the <a
href="http://websvn.kde.org/*checkout*/branches/KDE/3.5/kdelibs/KDE3PORTING.html">
porting guide</a> or subscribe to the
<a href="http://mail.kde.org/mailman/listinfo/kde-devel">KDE Devel Mailinglist</a>
to ask specific questions about porting your applications.

<p>There is also info on the <a
href="http://developer.kde.org/documentation/library/kdeqt/kde3arch/index.html">architecture</a>
and the <a href="http://developer.kde.org/documentation/library/3.0-api/classref/index.html">
programming interface of KDE 3.0</a>.
</p>

