---
date: 2021-07-27
changelog: 5.22.3-5.22.4
layout: plasma
youtube: HdirtBXW8bI
asBugfix: true
draft: false
---

+ Discover: Address the keyboard shortcut tooltip. [Commit.](http://commits.kde.org/discover/a7fcb85803b151f5e36c399453fa0bc739853911) Fixes bug [#438916](https://bugs.kde.org/438916)
+ Sort Unsplash POTD image categories alphabetically. [Commit.](http://commits.kde.org/kdeplasma-addons/ed7c2c218ee3cae72115f7e66ee57ee2878bdda8) Fixes bug [#422971](https://bugs.kde.org/422971)
+ Info Center: Unbreak about CPU value when solid is missing a product string. [Commit.](http://commits.kde.org/kinfocenter/4cfd889c6fe3aa27b8eb99ebc104d4eb2fb39b10) Fixes bug [#439464](https://bugs.kde.org/439464)
