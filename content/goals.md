---
title: "KDE's Goals"
summary: KDE's Goals for the Next Couple of Years.
---

<article>
    <p>KDE is a huge and diverse community working on hundreds of projects, from our flagship Plasma desktop to knowledge bases like WikitoLearn and including teams which help cross-project such as our Visual Design Group or Promotion team.</p>
    <p>To guide our work we have selected 3 goals to focus on over the next couple of years.</p>
    <div class="text-center mt-3">
        <a class="button ms-1 me-1 mb-3" href="https://dot.kde.org/2019/09/07/kde-decides-three-new-challenges-wayland-consistency-and-apps">Read the Announcement</a>
        <a class="button ms-1 me-1" href="https://community.kde.org/Goals">Goals wiki page</a>
    </div>
</article>

<article class="mt-4">
    <div class="row">
        <div class="col-3">
            <img src="/images/all-about-apps.png" alt="" class="img-fluid">
        </div>
        <div class="col-9">
            <h2>KDE is All About the Apps</h2>
            <p>KDE has over 200 applications and countless addons, plugins and Plasmoids. We offer useful services to them as projects such as Git hosting, teams of translators, designers and artists, some release management if they are part of KDE Applications. But much of the support we offer has fallen short, until recently there wasn't even an up to date website listing them all. There is no method of taking decisions on how these support services are provided and proposed changes often stall such as when the kde.org/applications did for a year.</p>
            <p> With new packaging formats we will now be able to release apps directly to users on Linux, our main target platform. This means we need to review our processes and up our game in many areas because no more will we be able to rely on 3rd parties to finish off our software and make it useful and attractive to users, we need to do that ourselves. </p>
            <p>KDE has many fabulous apps, let's take them out to the world! </p>
        </div>
    </div>
    <div class="text-center mt-3">
        <a class="button mb-3 ms-1 me-1" href="https://phabricator.kde.org/project/board/313/">Workboard</a>
        <a class="button mb-3 ms-1 me-1" href="https://community.kde.org/Goals/All_about_the_Apps">Goal wiki page</a>
        <a class="button ms-1 me-1" href="https://webchat.kde.org/#/room/#kde-all-about-apps:kde.org">Chat Room</a>
    </div>
</article>

<article class="mt-4">
    <div class="row">
        <div class="col-9">
            <h2>Wayland</h2>
            <p>Wayland has been of great interest and effort to the KDE community as it aligns with our values and goals to produce software that is secure, lightweight and beautiful. See more about Wayland on our Wayland page.</p>
            <p>This goal will prioritize tracking down and fixing the issues that keep our software from achieving feature parity with the venerable X Window system. Our focus will be our own software — KWin, Plasma and Apps — but will also be about helping the wider free software community to advance the Wayland feature set.</p>
            <p>This is an ambitious goal. It cannot be a small team effort, and we invite the wider community to participate. </p>
        </div>
        <div class="col-3">
            <img src="/images/wayland.png" alt="Wayland logo" class="img-fluid" />
        </div>
    </div>
    <div class="text-center mt-3">
        <a class="button" href="https://community.kde.org/Goals/Wayland">Goal wiki page</a>
    </div>
</article>

<article class="mt-4">
    <div>
        <h2>Consistency</h2>
        <p>"Consistency" here means having a single solution for a particular problem rather than many different ones. This applies to application design elements, feature implementations, website structure and style, and the KDE ecosystem as a whole that, unfortunately, often suffers from redundancy.</p>
        <p>Benefits of consistency include:</p>
        <ul>
            <li>Increased software usability: users recognize patterns that are common across KDE applications, which makes each one easier to learn and master.</li>
            <li>Stronger branding: the use of consistent visual elements throughout KDE software allows users to quickly recognize KDE products.</li>
            <li>Better discoverability: users are able to quickly locate useful information on our websites and choose software that helps them achieve their goals.</li>
            <li>Better maintainability: reduced code redundancy simplifies making changes to the codebase.</li>
            <li>Reduced complexity of writing new software: high-quality reusable components are available, eliminating the desire to reimplement.</li>
        </ul>
        <div class="text-center mt-3">
            <a class="button mb-3 me-1 ms-1" href="https://phabricator.kde.org/project/view/312/">Workboard</a>
            <a class="button mb-3 me-1 ms-1" href="https://community.kde.org/Goals/Consistency">Goal wiki page</a>
            <a class="button me-1 ms-1" href="https://webchat.kde.org/#/room/%23consistency:kde.org">Chat Room</a>
        </div>
    </div>
</article>
