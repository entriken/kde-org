------------------------------------------------------------------------
r1043000 | mueller | 2009-10-31 13:09:22 +0000 (Sat, 31 Oct 2009) | 1 line

backport r1042999
------------------------------------------------------------------------
r1045930 | tmcguire | 2009-11-06 23:20:57 +0000 (Fri, 06 Nov 2009) | 13 lines

Backport r1045886 by tmcguire from trunk to the 4.3 branch:

Merged revisions 1041824 via svnmerge from 
svn+ssh://tmcguire@svn.kde.org/home/kde/branches/kdepim/enterprise4/kdepimlibs

........
  r1041824 | mutz | 2009-10-28 16:22:40 +0100 (Wed, 28 Oct 2009) | 1 line
  
  QIODeviceDataProvider::read: implement blocking behaviour. Fixes QProcess io devices
........
SVN_MERGE


------------------------------------------------------------------------
r1045931 | tmcguire | 2009-11-06 23:21:47 +0000 (Fri, 06 Nov 2009) | 13 lines

Backport r1045888 by tmcguire from trunk to the 4.3 branch:

Merged revisions 1042721 via svnmerge from 
svn+ssh://tmcguire@svn.kde.org/home/kde/branches/kdepim/enterprise4/kdepimlibs

........
  r1042721 | mutz | 2009-10-30 15:34:28 +0100 (Fri, 30 Oct 2009) | 1 line
  
  QIODeviceDataProvider: use QProcess-specific API to detect error cases to get some poor-mans-error-handling until we fix this on a higher level
........
SVN_MERGE


------------------------------------------------------------------------
r1045932 | tmcguire | 2009-11-06 23:22:12 +0000 (Fri, 06 Nov 2009) | 13 lines

Backport r1045889 by tmcguire from trunk to the 4.3 branch:

Merged revisions 1042766 via svnmerge from 
svn+ssh://tmcguire@svn.kde.org/home/kde/branches/kdepim/enterprise4/kdepimlibs

........
  r1042766 | mutz | 2009-10-30 17:13:47 +0100 (Fri, 30 Oct 2009) | 1 line
  
  QIODeviceDataProvider: fix overzealous use of comma operator
........
SVN_MERGE


------------------------------------------------------------------------
r1047360 | scripty | 2009-11-11 04:02:30 +0000 (Wed, 11 Nov 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1047640 | winterz | 2009-11-11 17:26:26 +0000 (Wed, 11 Nov 2009) | 7 lines

Backport r1047638 by winterz from trunk to the 4.3 branch:

updated holidays, from Xavier. Thanks.
MERGE: 4.3



------------------------------------------------------------------------
r1048056 | winterz | 2009-11-12 17:28:42 +0000 (Thu, 12 Nov 2009) | 6 lines

Backport r1048055 by winterz from trunk to the 4.3 branch:

fix holiday for Epifanía, per Xavier
MERGE: 4.3


------------------------------------------------------------------------
r1048619 | cgiboudeaux | 2009-11-13 16:03:20 +0000 (Fri, 13 Nov 2009) | 9 lines

Backport r1041334 from trunk to 4.3:

Public holiday for Luxembourg.

Thanks to Georges Toth (gtoth at trypill org) for the data.

(a German translation would be welcome)


------------------------------------------------------------------------
r1051105 | krake | 2009-11-18 21:32:10 +0000 (Wed, 18 Nov 2009) | 4 lines

Backport of revision 1050785

Return all the capabilities as upper case, so that we don't have to deal with casing issues in client code.

------------------------------------------------------------------------
r1051244 | scripty | 2009-11-19 04:16:19 +0000 (Thu, 19 Nov 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1055145 | mueller | 2009-11-27 12:39:06 +0000 (Fri, 27 Nov 2009) | 2 lines

bump version

------------------------------------------------------------------------
