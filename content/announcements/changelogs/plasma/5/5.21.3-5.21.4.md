---
title: Plasma 5.21.4 complete changelog
version: 5.21.4
hidden: true
plasma: true
type: fulllog
---
{{< details title="Bluedevil" href="https://commits.kde.org/bluedevil" >}}
+ [applet] Fix tooltip showing wrong name for connected device. [Commit.](http://commits.kde.org/bluedevil/ad327dd06e4536d872eb59c670a0e7f915d71f33) Fixes bug [#422691](https://bugs.kde.org/422691)
{{< /details >}}

{{< details title="KScreen" href="https://commits.kde.org/kscreen" >}}
+ Prefer "21:9" over "64:27" aspect ratio. [Commit.](http://commits.kde.org/kscreen/20cf8b99a2b23dbbb535a67dce3f8c4e8ffb729f) 
{{< /details >}}

{{< details title="kwayland-server" href="https://commits.kde.org/kwayland-server" >}}
+ Send current primary selection after introducing focused surface. [Commit.](http://commits.kde.org/kwayland-server/ebdab57fce842ebc4e350216c43eaae8a760deac) 
+ Fix management of keymap files. [Commit.](http://commits.kde.org/kwayland-server/1c9e21b2c39f58a3cc7314ae4e3921c3fec04dd0) 
+ Fix a typo. [Commit.](http://commits.kde.org/kwayland-server/5ba30dcadb73baa6fe66dd0350ffe766d9f8da3d) 
{{< /details >}}

{{< details title="KWin" href="https://commits.kde.org/kwin" >}}
+ Platforms/drm: Fix crash in EglGbmBackend::presentOnOutput(). [Commit.](http://commits.kde.org/kwin/5a57eeafc840e97e14b4ed465a60f02dae57d6d1) 
+ Fix crash on hotplugging displays while switched to another TTY. [Commit.](http://commits.kde.org/kwin/991aa206d970112b576d3d328b541213799066b3) Fixes bug [#435388](https://bugs.kde.org/435388)
{{< /details >}}

{{< details title="libksysguard" href="https://commits.kde.org/libksysguard" >}}
+ Use lld instead of Ld as format specifier. [Commit.](http://commits.kde.org/libksysguard/ea8fc81dd776fd757ee4f5d83b766ab7c56bc0cc) Fixes bug [#418142](https://bugs.kde.org/418142)
+ Emit also dataChanged when sensor colors are changed. [Commit.](http://commits.kde.org/libksysguard/fa3cdaa0e07d7a7db8fbc4c4b6105f3ac2d82517) 
+ Assign a new color when configuring sensor colors. [Commit.](http://commits.kde.org/libksysguard/265337b6901f9170e8151412a5033c71fc79d00e) Fixes bug [#434515](https://bugs.kde.org/434515)
+ SensorFaceController: Save on destruction if the face requests it. [Commit.](http://commits.kde.org/libksysguard/cf77c8ceddfdd098be46ee63c9be4405659f8e45) Fixes bug [#433768](https://bugs.kde.org/433768). Fixes bug [#433536](https://bugs.kde.org/433536). Fixes bug [#434005](https://bugs.kde.org/434005)
{{< /details >}}

{{< details title="Milou" href="https://commits.kde.org/milou" >}}
+ Set RunnerManager runnerWindow variable if it is available. [Commit.](http://commits.kde.org/milou/113480f605b29e3becf1278e94ccfc19014e3804) See bug [#433173](https://bugs.kde.org/433173)
{{< /details >}}

{{< details title="Plasma Desktop" href="https://commits.kde.org/plasma-desktop" >}}
+ Fixup! fix broken keyboard configurations with single layout on Wayland. [Commit.](http://commits.kde.org/plasma-desktop/ff875141a263321ba41a38a45ff0f57c6d63535f) 
+ Fix broken keyboard configurations with single layout on Wayland. [Commit.](http://commits.kde.org/plasma-desktop/5e89a55e26b004fc097fd3b544823e88003db066) Fixes bug [#433576](https://bugs.kde.org/433576)
+ Fix zero badges (eg when downloading small files with Firefox). [Commit.](http://commits.kde.org/plasma-desktop/b4e1688b8f576ea218cbfd91267f136a4ceae7d7) 
+ [applets/kickoff] Make section headers more section headery. [Commit.](http://commits.kde.org/plasma-desktop/5f664cb171a399b599c6ab5198c92e9d9456d009) Fixes bug [#434669](https://bugs.kde.org/434669)
{{< /details >}}

{{< details title="Plasma Firewall" href="https://commits.kde.org/plasma-firewall" >}}
+ Add a message and disable the firewall on forced failure. [Commit.](http://commits.kde.org/plasma-firewall/1f9732e34d3a3dd42834c1e9014188bc17b3fe9e) 
+ Fix crash while re-entering the firewall KCM. [Commit.](http://commits.kde.org/plasma-firewall/c963f227c5cb6dec7f53735d730e2d0a8d36abf8) 
{{< /details >}}

{{< details title="Plasma Networkmanager (plasma-nm)" href="https://commits.kde.org/plasma-nm" >}}
+ Fix bug with openfortivpn that do not support 2fa. [Commit.](http://commits.kde.org/plasma-nm/13bc2023b8a7c3070cc45b83b3120197de0e69b1) Fixes bug [#434940](https://bugs.kde.org/434940)
{{< /details >}}

{{< details title="Plasma Audio Volume Control" href="https://commits.kde.org/plasma-pa" >}}
+ Round volume to avoid increasing or decreasing by more than stepSize. [Commit.](http://commits.kde.org/plasma-pa/b0f7cfccf80d00d9e76efa05d02bc2fc94b4318d) Fixes bug [#434769](https://bugs.kde.org/434769)
{{< /details >}}

{{< details title="Plasma Phone Components" href="https://commits.kde.org/plasma-phone-components" >}}
+ Lockscreen: Accept pin on numpad enter. [Commit.](http://commits.kde.org/plasma-phone-components/0dafdc266b9748e6d1023be6caa40daa460a3fea) 
+ Fix typo. [Commit.](http://commits.kde.org/plasma-phone-components/727b9d83804db3129f2155e40395ae7a2f26a324) 
{{< /details >}}

{{< details title="Plasma Systemmonitor" href="https://commits.kde.org/plasma-systemmonitor" >}}
+ Fix scrollbar overlapping configuration page. [Commit.](http://commits.kde.org/plasma-systemmonitor/954282f9f618c08e23ac6d312ec79e8213aa2ee1) Fixes bug [#434461](https://bugs.kde.org/434461)
{{< /details >}}

{{< details title="plasma-vault" href="https://commits.kde.org/plasma-vault" >}}
+ [applet] Allow subtitle to wrap. [Commit.](http://commits.kde.org/plasma-vault/0ea3805ed67176d9a0c3cf4f9ea13abde14c9ced) 
{{< /details >}}

{{< details title="Plasma Workspace" href="https://commits.kde.org/plasma-workspace" >}}
+ Make sure that *m_twinSpacer is iniitialized in panelspacer. [Commit.](http://commits.kde.org/plasma-workspace/1f244ed71f4992e6d18d255798a03c19dcbfb057) Fixes bug [#422914](https://bugs.kde.org/422914)
+ Comment was interpreted as part of Restart. [Commit.](http://commits.kde.org/plasma-workspace/f3718ffc4b1a88ecc10a16b1fe2dbc31b2403a9f) 
+ [applets/digital-clock] Fix timezone placeholder message positioning. [Commit.](http://commits.kde.org/plasma-workspace/a4abd73cdba460ff6f7a60abc7f9dfd0d6039456) 
+ Icons kcm: Clear pending deletions when Defaults button is clicked. [Commit.](http://commits.kde.org/plasma-workspace/c442becff0c8275e7c203994d55a2ba32b476f9f) 
+ Remove pointless widget. [Commit.](http://commits.kde.org/plasma-workspace/cc1f8881bb551daf50d2829aa8a0cf8fae65dde9) Fixes bug [#434910](https://bugs.kde.org/434910)
+ Krunner: Restore history related property and methods for compatibility with third party themes. [Commit.](http://commits.kde.org/plasma-workspace/dc57d322e585d977ec6b193bd6399f2d060690b9) Fixes bug [#433173](https://bugs.kde.org/433173)
+ Make bottom frame again visible. [Commit.](http://commits.kde.org/plasma-workspace/d522ebbae5b4b225129197e1ddd3ccee01e490ea) Fixes bug [#434645](https://bugs.kde.org/434645)
+ Fix color scheme preview. [Commit.](http://commits.kde.org/plasma-workspace/286edc6768e90d07947fad50db911f0c72e3759f) Fixes bug [#434493](https://bugs.kde.org/434493)
+ Save layout when corona startup is completed. [Commit.](http://commits.kde.org/plasma-workspace/dbdf3b1873aa8c96760b4015adb145bf0ec098a1) Fixes bug [#433799](https://bugs.kde.org/433799)
+ Klipper Waylandclipboard: force offer of specific mimetype to fix pasting to gtk applications. [Commit.](http://commits.kde.org/plasma-workspace/65c6955148d8d975fe1f7352f6249be9ac584027) 
{{< /details >}}

{{< details title="SDDM KCM" href="https://commits.kde.org/sddm-kcm" >}}
+ Fix broken commit to fix build. [Commit.](http://commits.kde.org/sddm-kcm/454062e9e813a923b8a4318792ddef75b0c32f49) 
+ Use default app font if there's no "font" entry. [Commit.](http://commits.kde.org/sddm-kcm/ed60574f34c9b0dc2f9c4949c6edfa5496e12a71) 
{{< /details >}}

