---
title : "KDE 3.0.1 Info Page"
unmaintained: true
publishDate: 2002-05-22 00:01:00
---

KDE 3.0.1 was released on May 22nd, 2002.
Read the <a href="/announcements/announce-3.0.1">official announcement</a>.

## FAQ

See the <a href="faq.php">KDE FAQ</a> for any specific
questions you may have.  Questions about Konqueror should be directed
<a href="http://konqueror.kde.org/faq/">to the Konqueror FAQ</a> and sound related
questions are answered <a href="http://www.arts-project.org/doc/handbook/faq.html">in the FAQ of the aRts Project</a>

<h2>Download and Installation</h2>

See the links listed in the <a
href="/announcements/announce-3.0.1.php">announcement</a>. The KDE
<a href="/documentation/faq/install.html">FAQ</a>
provides generic instruction about installation issues.

<p>
Binary packages can be found under
<a href="http://download.kde.org/stable/3.0.1/">http://download.kde.org/stable/3.0.1/</a>
or in the equivalent directory at one of the KDE
<a href="/mirrors/ftp.php">FTP mirrors</a>.
Additional binary packages might become available in the coming weeks,
as well as updates to the current packages.
</p>

<h2>Updates</h2>

<ul>
<li>
<p>A compile failure has been found in the release kde-i18n tarball. 
   The tarball has been fixed and replaced on ftp (24 May 2002).The
   same problem was in kde-i18n-fr language-specific tarball. 

   Please redownload the tarball in case you hit this problem. 
</p></li>
</ul>

<h2>Security Issues</h2>

<ul>
<li>
<p> Konqueror fails to correctly initialize the site domains for sub-(i)frames
    and may as a result allow access to forein cookies. 
</p>
<p>
It is strongly recommended to upgrade at least kdelibs to KDE 3.0.3a in which
this bug is fixed. 
</p>
<p><a href="ftp://ftp.kde.org/pub/kde/security_patches/post-3.0.3-kdelibs-khtml.diff">A patch</a>
   is also available for download to address this particular problem.
</p>
</li>
 <li>
 <p>KDE's SSL implementation fails to check the basic constraints on certificates 
 and as a result may accept certificates as valid that were signed by an 
 issuer who was not authorized to do so.
 </p>
</li>
<li>
<p>	Konqueror fails to detect the "secure" flag in HTTP cookies and as
        a result may send secure cookies back to the originating site over
        an unencrypted network connection.</p>
<p>
It is strongly recommended to upgrade at least kdelibs to KDE 3.0.3a in which
this bug is fixed.
</p>
<p><a href="http://download.kde.org/stable/3.0.1/src/post-3.0-kdelibs-kcookiejar.diff">A patch</a>
   is also available for download to address this particular problem.
</p>
</li>
<li>
<p>KDE's SSL implementation fails to check the basic constraints on certificates 
and as a result may accept certificates as valid that were signed by an 
issuer who was not authorized to do so.
</p>
<p>
Due to this, users of Konqueror and other SSL enabled KDE software may fall victim to a 
malicious man-in-the-middle attack without noticing. In such case the user 
will be under the impression that there is a secure connection with a trusted 
site while in fact a different site has been connected to.
</p>
<p>
It is strongly recommended to upgrade at least kdelibs to KDE 3.0.3 in which
this bug is fixed.
</p>
</li>
<li>
<p>A format string vulnerability was found in the commonly used
   talkd implementation, which ktalk(d) uses.</p>
<p><a href="http://download.kde.org/stable/3.0.1/src/post-3.0.1-kdenetwork.diff">A patch</a>
   is available for download to address this particular problem.
</p>
<p>
   <strong>The use of ktalk(d) is strongly discouraged in any security
   relevant area. Use it with care, and never make it accessible outside your
   local, trusted network.</strong>
</p></li>
<li>
<p>KHTML, the html rendering component of Konqueror, allowed webpages to
   initialize the file upload box with a filename. This could cause unwanted
   submit of the file to the remote host. </p>
<p><a href="http://download.kde.org/stable/3.0.1/src/post-3.0.1-kdelibs.diff">A patch</a>
   is available for download to address this problem.
</p>
</li>
<li>A Denial of Service vulnerability has been found in the
<a href="http://www.arts-project.org/">aRts</a> soundserver. All versions of
KDE 2.2.x and KDE 3.0.x are affected. If you allow untrusted users to login,    it is recommended to remove the sUID bit of the <tt>artswrapper</tt> application. To achieve this, please
run the following command in the directory <tt>artswrapper</tt> is installed in:
<pre>
  chmod u-s artswrapper
</pre>
</li>
<li>
<p>Several buffer overflows have been found in code KGhostview shared from
    other postscript viewers. Read the <a href="security/advisory-20021008-1.txt">detailed 
    advisory</a>. Update to KDE 3.0.4 is recommended. 
</p>
<p><a href="ftp://ftp.kde.org/pub/kde/security_patches/post-3.0.3-kdegraphics-kghostview.diff">A patch</a>
   is also available for download to address this particular problem.
</p>
</li>
<li>
<p>A path traversal exploit has been found in kpf. 
    Read the <a href="security/advisory-20021008-2.txt">detailed 
    advisory</a>. Update to KDE 3.0.4 is recommended. 
</p>
<p><a href="ftp://ftp.kde.org/pub/kde/security_patches/post-3.0.3-kdenetwork-kpf.diff">A patch</a>
   is also available for download to address this particular problem.
</p>
</li>
<li>Several vulnerabilites have been found in LISa/resLISa and the rlan:// protocol, 
 including the possibility to escalate the privileges to root via a remote attack. See the 
<a href="http://www.kde.org/info/security/advisory-20021111-1.txt">detailed advisory</a> for
 an explanation and instructions for immediate workaround. 
<a href="ftp://ftp.kde.org/pub/kde/security_patches/post-3.0.4-kdenetwork-lanbrowsing.diff">
A patch</a> is available for download. <b> The use of LISa/resLISa is strongly discouraged
in any security relevant area. Never make it available outside your local, trusted network.</b> </li>
<li>the rlogin protocol implementation in KIO allows remote command execution. See the
<a href="http://www.kde.org/info/security/advisory-20021111-2.txt">detailed advisory</a> for
an explanation and instructions for immediate workaround.
<a href="ftp://ftp.kde.org/pub/kde/security_patches/post-3.0.4-kdelibs-kio-misc.diff">A patch</a>
is available for download. </li>

<li>
<p>Several shell escaping vulnerabilities have been found throughout KDE which allow a remote attacker to execute commands as the local user.
   Read the <a href="security/advisory-20021220-1.txt">detailed
   advisory</a>. It is strongly recommended to update to KDE 3.0.5a.
</p>
</li>
<li>
Several problems with KDE's use of Ghostscript where discovered that allow the execution of
arbitrary commands contained in PostScript (PS) or PDF files with the privileges of the victim.
Read the <a href="security/advisory-20030409-1.txt">detailed advisory</a>. 
It is strongly recommended to update to <a href="3.0.5b.php">KDE 3.0.5b</a>
</li>
<li>
A HTTP authentication credentials leak via the a "Referrer" was discovered by George Staikos
in Konqueror. If the HTTP authentication credentials were part of the URL they would be possibly sent
in the referer header to a 3rd party web site.
Read the <a href="security/advisory-20030729-1.txt">detailed advisory</a>. KDE 3.1.3 and newer
are not vulnerable.
</li>
</ul>

<h2><a name="bugs">Bugs</a></h2>

<p>This is a list of grave bugs and common pitfalls
surfacing after the release date:</p>

<ul>
<li>Support for &lt;OBJECT&gt; tags in webpages has been accidently broken.
    The KHTML patch above, which is strongly recommended, 
    corrects this problem as well.</li>
</ul>

<p>Please check the <a href="http://bugs.kde.org">bug database</a>
before filing any bug reports. Also check for possible updates on this pag
that might describe or fix your problem.</p>

<h2>Developer Info</h2>

If you need help porting your application to KDE 3.x see the <a
href="http://websvn.kde.org/*checkout*/branches/KDE/3.5/kdelibs/KDE3PORTING.html">
porting guide</a> or subscribe to the 
<a href="http://mail.kde.org/mailman/listinfo/kde-devel">KDE Devel Mailinglist</a> 
to ask specific questions about porting your applications.

<p>There is also info on the <a
href="http://developer.kde.org/documentation/library/kdeqt/kde3arch/index.html">architecture</a>
and the <a href="http://developer.kde.org/documentation/library/3.0-api/classref/index.html">
programming interface of KDE 3.0</a>.
</p>

<!-- END CONTENT -->
<?php
?>
