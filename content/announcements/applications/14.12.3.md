---
aliases:
- ../announce-applications-14.12.3
changelog: true
date: '2015-03-03'
description: KDE Ships Applications 14.12.3.
layout: application
title: KDE Ships KDE Applications 14.12.3
version: 14.12.3
---

{{% i18n_var "March 3, 2015. Today KDE released the third stability update for <a href='%[1]s'>KDE Applications 14.12</a>. This release contains only bugfixes and translation updates, providing a safe and pleasant update for everyone." "../14.12.0" %}}

With 19 recorded bugfixes it also includes improvements to the anagram game Kanagram, Umbrello UML Modeller, the document viewer Okular and the geometry application Kig.

{{% i18n_var "This release also includes Long Term Support versions of Plasma Workspaces %[1]s, KDE Development Platform %[2]s and the Kontact Suite %[2]s." "4.11.17" "4.14.6" %}}