---
aliases:
- ../announce-3.5.9
date: '2008-02-19'
description: KDE Community Ships Ninth Maintenance Update for Fourth Major Version
  for Leading Free Software Desktop.
title: KDE 3.5.9 Release Announcement
---

<p>FOR IMMEDIATE RELEASE</p>

<h3 align="center">
   KDE Community Ships Ninth Translation and Service Release for Leading Free
   Software Desktop
</h3>

<p align="justify">
  <strong>
    KDE 3.5.9 features translations in 65 languages, improvements to KDE PIM
    suite and other applications.
  </strong>
</p>

<p align="justify">
  The <a href="http://www.kde.org/">KDE
  Community</a> today announced the immediate availability of KDE 3.5.9,
  a maintenance release for the latest generation of the most advanced and
  powerful <em>free</em> desktop for GNU/Linux and other UNIXes. 
  The most important changes have been made to the KDE-PIM applications,
  including the KMail email client, KOrganizer, a planning application and
  other components.
</p>

<p align="justify">
The KDE-PIM enterprise branch, that has already been shipped by some distributions
has found its way into the stable branch of KDE. While KDE 3 has been in bugfix mode
for quite a while now, the Release Team has decided to merge the enterprise branch
and release it as KDE-PIM proper in 3.5.9 since no regressions are known and
it makes packagers' lives easier.
<p />
New features and bugfixes in this branch include, but are not limited to:
    <ul>
        <li>
			A <em>Favorite</em> email folders view in KMail, as well as drag and drop
			support for folders
        </li>
        <li>
			Easier scheduling in KOrganizer through various improvements in the
			user interface
        </li>
        <li>
			Improvements in KAlarm and KAddressbook	
        </li>
    </ul>
 
Many more of those can be found on 
<a href="http://techbase.kde.org/Projects/PIM/Features_3.5.9">Techbase</a>. More 
bugfixes contained in this release can be found in the 
<a href="http://www.kde.org/announcements/changelogs/changelog3_5_8to3_5_9">Changelog</a>.

</p>
<?php // Changed stuff from 3.5.8 until here. ?>

<p align="justify">
  For a more detailed list of improvements since the
  <a href="http://www.kde.org/announcements/announce-3.5.8">KDE 3.5.8 release</a>
  on the 16th October 2007, please refer to the
  <a href="http://www.kde.org/announcements/changelogs/changelog3_5_8to3_5_9">KDE 3.5.9 Changelog</a>.
</p>

<p align="justify">
  KDE 3.5.9 ships with a basic desktop and fifteen other packages (PIM,
  administration, network, edutainment, utilities, multimedia, games,
  artwork, web development and more). KDE's award-winning tools and
  applications are available in <strong>65 languages</strong>.
</p>

<h4>
  Distributions shipping KDE
</h4>
<p align="justify">
  Most of the Linux distributions and UNIX operating systems do not immediately
  incorporate new KDE releases, but they will integrate KDE 3.5.9 packages in
  their next releases. Check 
  <a href="http://www.kde.org/download/distributions">this list</a> to see
  which distributions are shipping KDE.
</p>

<h4>
  Installing KDE 3.5.9 Binary Packages
</h4>
<p align="justify">
  <em>Package Creators</em>.
  Some operating system vendors have kindly provided binary packages of
  KDE 3.5.9 for some versions of their distribution, and in other cases
  community volunteers have done so.
  Some of these binary packages are available for free download from KDE's
  download server at
  <a href="http://download.kde.org/binarydownload.html?url=/stable/3.5.9/">http://download.kde.org</a>.
  Additional binary packages, as well as updates to the packages now
  available, may become available over the coming weeks.
</p>

<p align="justify">
  <a id="package_locations"><em>Package Locations</em></a>.
  For a current list of available binary packages of which the KDE
  Project has been informed, please visit the
  <a href="/info/3.5.9">KDE 3.5.9 Info Page</a>.
</p>

<h4>
  Compiling KDE 3.5.9
</h4>
<p align="justify">
  <a id="source_code"></a><em>Source Code</em>.
  The complete source code for KDE 3.5.9 may be
  <a href="http://download.kde.org/stable/3.5.9/src/">freely
  downloaded</a>.  Instructions on compiling and installing KDE 3.5.9
  are available from the <a href="/info/3.5.9">KDE
  3.5.9 Info Page</a>.
</p>

<h4>
  Supporting KDE
</h4>
<p align="justify">
KDE is a <a href="http://www.gnu.org/philosophy/free-sw.html">Free Software</a> 
project that exists and grows only because of the
help of many volunteers that donate their time and effort. KDE
is always looking for new volunteers and contributions, whether it's
help with coding, bug fixing or reporting, writing documentation,
translations, promotion, money, etc. All contributions are gratefully
appreciated and eagerly accepted. Please read through the <a href="/community/donations/">Supporting
KDE page</a> for further information. </p>

<p align="justify">
We look forward to hearing from you soon!
</p>
