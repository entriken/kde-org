---
aliases:
- ../announce-applications-14.12.1
changelog: true
date: '2015-01-08'
description: KDE Ships KDE Applications 14.12.1.
layout: application
title: KDE Ships KDE Applications 14.12.1
version: 14.12.1
---

{{% i18n_var "January 13, 2015. Today KDE released the first stability update for <a href='%[1]s'>KDE Applications 14.12</a>. This release contains only bugfixes and translation updates, providing a safe and pleasant update for everyone." "../14.12.0" %}}

More than 50 recorded bugfixes include improvements to the archiving tool Ark, Umbrello UML Modeller, the document viewer Okular, the pronunciation learning application Artikulate and remote desktop client KRDC.

{{% i18n_var "This release also includes Long Term Support versions of Plasma Workspaces %[1]s, KDE Development Platform %[2]s and the Kontact Suite %[2]s." "4.11.15" "4.14.4" %}}
