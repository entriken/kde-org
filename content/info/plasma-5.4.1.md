---
version: "5.4.1"
title: "Plasma 5.4.1 Source Information Page"
errata:
    link: https://community.kde.org/Plasma/5.4_Errata
    name: 5.4 Errata
type: info/plasma5
---


This is a bugfix release of Plasma, featuring Plasma Desktop and
other essential software for your computer.  Details in the <a
href="/announcements/plasma-5.4.1">Plasma 5.4.1 announcement</a>.
