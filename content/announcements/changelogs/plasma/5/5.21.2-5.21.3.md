---
title: Plasma 5.21.3 complete changelog
version: 5.21.3
hidden: true
plasma: true
type: fulllog
---
{{< details title="Bluedevil" href="https://commits.kde.org/bluedevil" >}}
+ Manually bind width of list item. [Commit.](http://commits.kde.org/bluedevil/506b311d0bc7505cb37ea6f6b725a288af3a0159) 
+ [applet] Enable Bluetooth checkbox should be always enabled. [Commit.](http://commits.kde.org/bluedevil/4b4b0dd119b7bff79ece33bb253d746660cdb936) Fixes bug [#433232](https://bugs.kde.org/433232)
{{< /details >}}

{{< details title="Breeze" href="https://commits.kde.org/breeze" >}}
+ Correct global theme metadata. [Commit.](http://commits.kde.org/breeze/8b4e8250c8cd3f8916c7463652744b40fb95995b) Fixes bug [#414417](https://bugs.kde.org/414417)
+ In case of conflict in i18n, keep the version of the branch "ours". [Commit.](http://commits.kde.org/breeze/a6a3b2f5c7e761ebff6858147c9ac8cf181224c0) 
{{< /details >}}

{{< details title="breeze-gtk" href="https://commits.kde.org/breeze-gtk" >}}
+ Adjust menu heights to properly match Breeze QStyle, second time. [Commit.](http://commits.kde.org/breeze-gtk/7be4c04e4dca2ba61b5822d8d50b7d3ee3a87702) Fixes bug [#433158](https://bugs.kde.org/433158)
+ Gtk3, gtk4: unscrew libhandy's height. [Commit.](http://commits.kde.org/breeze-gtk/72b568275a5ef464f133a62db875740a89598b49) Fixes bug [#430081](https://bugs.kde.org/430081)
{{< /details >}}

{{< details title="Discover" href="https://commits.kde.org/discover" >}}
+ Only turn http[s] links into clickable links. [Commit.](http://commits.kde.org/discover/94478827aab63d2e2321f0ca9ec5553718798e60) 
+ Do not fiddle with the search field when its not visible. [Commit.](http://commits.kde.org/discover/f8d266a1b25e176053df36803175e9b3711fc18e) 
+ Updates: wrap the text when it doesn't fit. [Commit.](http://commits.kde.org/discover/edd04de389a6bc95263738d7e2eaca319bd2cc46) 
{{< /details >}}

{{< details title="Dr Konqi" href="https://commits.kde.org/drkonqi" >}}
+ Map kdeinit5. [Commit.](http://commits.kde.org/drkonqi/9e88bfe8734f632e2d6611f750e0a3ac373355be) Fixes bug [#433483](https://bugs.kde.org/433483)
+ Wire up productInfoError to fall back to 'kde' product. [Commit.](http://commits.kde.org/drkonqi/cad5d0773675a134ffa674cb51f0b0475ef2e8b6) 
+ Don't assert that there is 1 product. [Commit.](http://commits.kde.org/drkonqi/b273ff4301ca35c2299a2c2e5f5c9c73b8347f4a) 
+ Input focus is no longer dancing tango with two text fields. [Commit.](http://commits.kde.org/drkonqi/ba0a804232be13767450297c327e32213d953d68) Fixes bug [#433480](https://bugs.kde.org/433480)
+ Refine warning message. [Commit.](http://commits.kde.org/drkonqi/e2143fa5077211255fd373b0dc5cb6bb623c6281) 
+ Log why debugger entered failure state. [Commit.](http://commits.kde.org/drkonqi/073da75bf0fd7e9317b990d354ed89d2fccf1985) 
{{< /details >}}

{{< details title="Plasma Addons" href="https://commits.kde.org/kdeplasma-addons" >}}
+ Fix outdated API key for flickr provider. [Commit.](http://commits.kde.org/kdeplasma-addons/905c21875e866ab59cc338ccff9ecd1c5ca84f21) See bug [#427566](https://bugs.kde.org/427566)
{{< /details >}}

{{< details title="KDE Hotkeys" href="https://commits.kde.org/khotkeys" >}}
+ In case of conflict in i18n, keep the version of the branch "ours". [Commit.](http://commits.kde.org/khotkeys/274e640ab0543cbb32279b36a50eb9e84acec916) 
{{< /details >}}

{{< details title="Info Center" href="https://commits.kde.org/kinfocenter" >}}
+ Force-resize columns in the wayland module. [Commit.](http://commits.kde.org/kinfocenter/7727b3f8c810e4dde63daf3935e545f9c8dcd65e) Fixes bug [#433327](https://bugs.kde.org/433327)
{{< /details >}}

{{< details title="KSysGuard" href="https://commits.kde.org/ksysguard" >}}
+ Add missing #include <array>. [Commit.](http://commits.kde.org/ksysguard/6e5cc475cc4e882c41ff4c01c0a4091ff59610a0) Fixes bug [#433517](https://bugs.kde.org/433517)
+ Add providerName to power plugin. [Commit.](http://commits.kde.org/ksysguard/66fcaf9cff9537dc78f38689d5ba55f549c48a42) 
+ Use correct unit for charge rate. [Commit.](http://commits.kde.org/ksysguard/75936e9967d7b7ccfdb0529f03e60a4745c5b09f) 
{{< /details >}}

{{< details title="kwayland-server" href="https://commits.kde.org/kwayland-server" >}}
+ Update pressed keys even if there is no focused surface. [Commit.](http://commits.kde.org/kwayland-server/f4da515d3f9707afadaa95135f32430c0f3d153b) 
{{< /details >}}

{{< details title="KWin" href="https://commits.kde.org/kwin" >}}
+ In case of conflict in i18n, keep the version of the branch "ours". [Commit.](http://commits.kde.org/kwin/784b17f61ac9186b2c509fce05988975ebeabe76) 
+ Inputmethod: Use the object to connect to slots. [Commit.](http://commits.kde.org/kwin/310594a6c50c2e2fabcdbe42a3f6333c0ae82ab9) 
+ Wayland: Do not force qtvirtualkeyboard. [Commit.](http://commits.kde.org/kwin/a83a7bcc8a6ce6494dca53ee9aa2ff2cf84d3187) 
+ In case of conflict in i18n, keep the version of the branch "ours". [Commit.](http://commits.kde.org/kwin/f1f59e40fc39ec84fd12eb6be74e407add4e4017) 
+ Inputmethod: If the client hides itself, act accordingly. [Commit.](http://commits.kde.org/kwin/f1229f119ac7ad650fb912713d09cfbb44af33ab) 
+ In case of conflict in i18n, keep the version of the branch "ours". [Commit.](http://commits.kde.org/kwin/28411f228b91893fffcd1cb277ff55760de13864) 
{{< /details >}}

{{< details title="libksysguard" href="https://commits.kde.org/libksysguard" >}}
+ Avoid emitting configurationChanged during save. [Commit.](http://commits.kde.org/libksysguard/ace8d824a5d2b9793c1d8c59d0191005fb38e4e9) Fixes bug [#422672](https://bugs.kde.org/422672)
+ SensorDataModel: Do not insert columns that are out of range. [Commit.](http://commits.kde.org/libksysguard/a5db29c6f0ed0dcd1b375b770d6f318f8e9e174c) Fixes bug [#433064](https://bugs.kde.org/433064)
+ Properly initialize the parent widget to nullptr in ProcessController. [Commit.](http://commits.kde.org/libksysguard/77e7b53cd8ad19eac09078c092943f519797c4b6) Fixes bug [#434074](https://bugs.kde.org/434074)
+ Also emit showTitleChanged when reloading the config. [Commit.](http://commits.kde.org/libksysguard/24afd59161d1cffa1ac2c3168fc62b4658eaa3e7) Fixes bug [#433954](https://bugs.kde.org/433954)
+ Fix build. [Commit.](http://commits.kde.org/libksysguard/6fc9d7eeab7633500743df9577aa41ec168bc59b) 
+ Move CGroup pid fetching callback to the controller. [Commit.](http://commits.kde.org/libksysguard/1ce85053e45c95ef43f9a0543e2710f33b81e10b) Fixes bug [#430615](https://bugs.kde.org/430615)
{{< /details >}}

{{< details title="Oxygen" href="https://commits.kde.org/oxygen" >}}
+ In case of conflict in i18n, keep the version of the branch "ours". [Commit.](http://commits.kde.org/oxygen/a97010e30a77e21101e853519dbaf048a29cc836) 
{{< /details >}}

{{< details title="Plasma Browser Integration" href="https://commits.kde.org/plasma-browser-integration" >}}
+ In case of conflict in i18n, keep the version of the branch "ours". [Commit.](http://commits.kde.org/plasma-browser-integration/b743c2f61bcc04c723c98ad5b4dd6f1d323906bc) 
{{< /details >}}

{{< details title="Plasma Desktop" href="https://commits.kde.org/plasma-desktop" >}}
+ [kcms/joystick] Fix minor leak. [Commit.](http://commits.kde.org/plasma-desktop/1913d9f5b4700c349ff0460ec0390673aac76f31) Fixes bug [#385894](https://bugs.kde.org/385894)
+ Require KF5Codecs. [Commit.](http://commits.kde.org/plasma-desktop/7936caed63458cd08bbb9f4e836e22a8805c40b5) 
+ [kcms/keyboard] Allow faster keyboard repeat rates. [Commit.](http://commits.kde.org/plasma-desktop/f41eb4ff616751ba75999691decc6a202ea9ba9e) Fixes bug [#286760](https://bugs.kde.org/286760)
+ Kcm/autostart: Add missing <optional> include. [Commit.](http://commits.kde.org/plasma-desktop/5396e4a8adbdf622dc9bb95546d57eb830167f5d) 
+ Pass Qt::MatchExactly when calling QAbstractItemModel::match for strings. [Commit.](http://commits.kde.org/plasma-desktop/494878b381b497ff671a3ead137af5271ff64498) 
+ In case of conflict in i18n, keep the version of the branch "ours". [Commit.](http://commits.kde.org/plasma-desktop/ca1914d481a70d3c63ce62eced5f5d1c8b28b025) 
{{< /details >}}

{{< details title="Plasma Firewall" href="https://commits.kde.org/plasma-firewall" >}}
+ In case of conflict in i18n, keep the version of the branch "ours". [Commit.](http://commits.kde.org/plasma-firewall/811be4469deffc7c5ceb66f903c7b444f1f565fc) 
{{< /details >}}

{{< details title="Plasma Networkmanager (plasma-nm)" href="https://commits.kde.org/plasma-nm" >}}
+ In case of conflict in i18n, keep the version of the branch "ours". [Commit.](http://commits.kde.org/plasma-nm/b22fe1a7911081ee09aac299a86493c327737e93) 
+ [applet] Remove broken call to closeSearch(). [Commit.](http://commits.kde.org/plasma-nm/67330ba4525d97079260b04c8aabad1515a6c792) 
+ [applet] Manually bind width of list item. [Commit.](http://commits.kde.org/plasma-nm/1b286e2f8e5409bb82eaedfcacff33383bcd2bd9) 
+ In case of conflict in i18n, keep the version of the branch "ours". [Commit.](http://commits.kde.org/plasma-nm/ed53adb4159989442165162d1808b7889932ec12) 
{{< /details >}}

{{< details title="Plasma Phone Components" href="https://commits.kde.org/plasma-phone-components" >}}
+ Fix panel clock from not respecting 12/24 hour settings. [Commit.](http://commits.kde.org/plasma-phone-components/8b92cf7efc110f525c0631708302835ef5a7b264) 
+ In case of conflict in i18n, keep the version of the branch "ours". [Commit.](http://commits.kde.org/plasma-phone-components/83b87f4f63d0a6f50576ba1b5f0783e2a5b14fd8) 
+ Avoid potential double dbus start. [Commit.](http://commits.kde.org/plasma-phone-components/654be92f56919524d8d1218e8de4bbaa4a882459) 
+ Remove code pushed by mistake. [Commit.](http://commits.kde.org/plasma-phone-components/2f1ae11f6f11d5e1f32b6608c7ab90b00a8ac10e) 
+ Apporder reflects favorites. [Commit.](http://commits.kde.org/plasma-phone-components/919689c8cdab2f6ee9639ab64f1a304225490abc) 
{{< /details >}}

{{< details title="Plasma Systemmonitor" href="https://commits.kde.org/plasma-systemmonitor" >}}
+ The checkbox can be bigger than the text. [Commit.](http://commits.kde.org/plasma-systemmonitor/93b6c1d5d1d73abfb8259b52afd15b7163681fce) Fixes bug [#434009](https://bugs.kde.org/434009)
+ Ensure correct paddings. [Commit.](http://commits.kde.org/plasma-systemmonitor/9c8ed3adbb4454d1e40a7041690fabeab7e8cfa4) Fixes bug [#434007](https://bugs.kde.org/434007)
+ Display existing search text in search field for application/process pages. [Commit.](http://commits.kde.org/plasma-systemmonitor/ef09d30da772be5e410b5f50767033ff19c43bfe) Fixes bug [#433324](https://bugs.kde.org/433324)
+ Set "ForceSaveOnDestroy" to true for Applications and Processes faces. [Commit.](http://commits.kde.org/plasma-systemmonitor/d3ff9e92d2c4c2e52acf1c885a92c468b610949b) 
+ Do not call a non-existing function. [Commit.](http://commits.kde.org/plasma-systemmonitor/bc994c071ed8826893b51664ab026f8ee7b2e038) Fixes bug [#433706](https://bugs.kde.org/433706)
+ Avoid showing "Processes: 0" in applications sidebar whilst loading. [Commit.](http://commits.kde.org/plasma-systemmonitor/4dc2734acaab6ea9ff95cc665e95af771b782fe8) Fixes bug [#433326](https://bugs.kde.org/433326)
+ Properly change colorSet. [Commit.](http://commits.kde.org/plasma-systemmonitor/af33c076ee83fcf2e7c219c60529fc70d052c5f6) Fixes bug [#434006](https://bugs.kde.org/434006)
+ Fix some visual bugs of the KillDialog. [Commit.](http://commits.kde.org/plasma-systemmonitor/747d0076d92e02dfcb318fbe67d92f1203b469d3) Fixes bug [#433751](https://bugs.kde.org/433751)
+ Fix initial size of page download dialog. [Commit.](http://commits.kde.org/plasma-systemmonitor/2c36107c8a6d1747d809e08df3bdcdf6a5691729) Fixes bug [#433726](https://bugs.kde.org/433726)
{{< /details >}}

{{< details title="plasma-thunderbolt" href="https://commits.kde.org/plasma-thunderbolt" >}}
+ In case of conflict in i18n, keep the version of the branch "ours". [Commit.](http://commits.kde.org/plasma-thunderbolt/ce1482b9e6d40e8d3cf22a90fd42b854279a7996) 
+ Include <thread> for std::this_thread::sleep_for. [Commit.](http://commits.kde.org/plasma-thunderbolt/34640e7e777b198b06d73f71149c0ee44fddb1fc) 
{{< /details >}}

{{< details title="plasma-vault" href="https://commits.kde.org/plasma-vault" >}}
+ Manually bind width of list item. [Commit.](http://commits.kde.org/plasma-vault/a55dca50b749e82fba2943773443c75012b2d07e) 
{{< /details >}}

{{< details title="Plasma Workspace" href="https://commits.kde.org/plasma-workspace" >}}
+ [Image wallpaper] Make the Positioning combobox as equally wide as the other comboboxes. [Commit.](http://commits.kde.org/plasma-workspace/caa27dcfcf4b8cbf3b709cfb9ddbac9712c53be2) 
+ [Notifications] Emit dataChanged in setData call. [Commit.](http://commits.kde.org/plasma-workspace/73c104ac393583599fc7fecc3a8934ca392cb35a) See bug [#429862](https://bugs.kde.org/429862)
+ [applets/devicenotifier] Manually bind width of list item. [Commit.](http://commits.kde.org/plasma-workspace/5a04c0f944fc2f93f6fbd659e2596c5410e89f79) 
+ Re-add Force Font DPI spinbox on Wayland. [Commit.](http://commits.kde.org/plasma-workspace/1278ebee2a6e72de2b3600d2baa61e57378f3d9f) Fixes bug [#433115](https://bugs.kde.org/433115)
+ Proper form factor filtering. [Commit.](http://commits.kde.org/plasma-workspace/78f885a17682356355b2368b095bf9290c49127d) Fixes bug [#433983](https://bugs.kde.org/433983)
+ Pass Qt::MatchExactly when calling QAbstractItemModel::match for strings. [Commit.](http://commits.kde.org/plasma-workspace/4140cf289b626f0a49a32d23d6ecd0ebcd97edb9) 
+ Adapt more KCMs to using Kirigami.ActionToolbar for their footer actions. [Commit.](http://commits.kde.org/plasma-workspace/e849cdbd605a7eeee05f10265ea44abbdc208349) 
+ Fix query of StartPlasma::hasSystemdService. [Commit.](http://commits.kde.org/plasma-workspace/bbe0f91070ac7235066b28e57f9971ac1a4f821e) Fixes bug [#433333](https://bugs.kde.org/433333)
+ In case of conflict in i18n, keep the version of the branch "ours". [Commit.](http://commits.kde.org/plasma-workspace/ccf0d65bf311fd09bae0be7acd11989bd29de24a) 
+ [lookandfeel/ActionButton] Fix broken focus effect. [Commit.](http://commits.kde.org/plasma-workspace/8fd7b38b66de36ad8b673f3ddb88f7810c4291cc) Fixes bug [#433755](https://bugs.kde.org/433755)
+ Use separate face controller for appearance config. [Commit.](http://commits.kde.org/plasma-workspace/e94b3449eb9e0a7af3aabd9dc4da2395fdba136a) Fixes bug [#424458](https://bugs.kde.org/424458)
{{< /details >}}

{{< details title="plasma-workspace-wallpapers" href="https://commits.kde.org/plasma-workspace-wallpapers" >}}
+ In case of conflict in i18n, keep the version of the branch "ours". [Commit.](http://commits.kde.org/plasma-workspace-wallpapers/68fa10ebdab852f6f279133ad1afcf2b4ad12107) 
{{< /details >}}

{{< details title="qqc2-breeze-style" href="https://commits.kde.org/qqc2-breeze-style" >}}
+ Revert "[Units] Add sizeForLabels". [Commit.](http://commits.kde.org/qqc2-breeze-style/b63b4f315ca2f69b086257b95531b5be13a5fb20) 
+ [Units] Add sizeForLabels. [Commit.](http://commits.kde.org/qqc2-breeze-style/e3fceef1b2dc60fa6f1d27576c06bdf103c77769) 
+ [RangeSlider] Lower Kirigami version to 2.14. [Commit.](http://commits.kde.org/qqc2-breeze-style/7d00373015a25d6a9c7c429479cc89f2f5fcd76b) 
{{< /details >}}

{{< details title="SDDM KCM" href="https://commits.kde.org/sddm-kcm" >}}
+ Use Kirigami.ActionToolBar to auto-resize the SDDM KCM's footer. [Commit.](http://commits.kde.org/sddm-kcm/7b039a4567a6904c8282c4a3057b66771e23095f) 
{{< /details >}}

{{< details title="System Settings" href="https://commits.kde.org/systemsettings" >}}
+ [sidebar view] Set header KCM icon size correctly. [Commit.](http://commits.kde.org/systemsettings/9498e76350528889874dfe19a1ad47318b258766) 
+ In case of conflict in i18n, keep the version of the branch "ours". [Commit.](http://commits.kde.org/systemsettings/aae34ce278ed5c3a52c16cf57c662f2e09441628) 
+ In case of conflict in i18n, keep the version of the branch "ours". [Commit.](http://commits.kde.org/systemsettings/ec60b7230f3b6368772ef11326827fa7651fa272) 
+ Underscores are not allowed in action names. [Commit.](http://commits.kde.org/systemsettings/a0b4a391bb8f0447c357922afe69241bd47fc41e) Fixes bug [#433109](https://bugs.kde.org/433109)
{{< /details >}}

