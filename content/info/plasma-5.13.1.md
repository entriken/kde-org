---
version: "5.13.1"
title: "KDE Plasma 5.13.1, Bugfix Release"
errata:
    link: https://community.kde.org/Plasma/5.13_Errata
    name: 5.13 Errata
type: info/plasma5
signer: Jonathan Riddell
signing_fingerprint: EC94D18F7F05997E
---

This is a Bugfix release of KDE Plasma, featuring Plasma Desktop and
other essential software for your computer.  Details in the [Plasma 5.13.1 announcement](/announcements/plasma-5.13.1).
