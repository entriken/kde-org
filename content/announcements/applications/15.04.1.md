---
aliases:
- ../announce-applications-15.04.1
changelog: true
date: '2015-05-12'
description: KDE Ships Applications 15.04.1.
layout: application
title: KDE Ships KDE Applications 15.04.1
version: 15.04.1
---

{{% i18n_var "May 12, 2015. Today KDE released the first stability update for <a href='%[1]s'>KDE Applications 15.04</a>. This release contains only bugfixes and translation updates, providing a safe and pleasant update for everyone." "../15.04.0" %}}

More than 50 recorded bugfixes include improvements to kdelibs, kdepim, kdenlive, okular, marble and umbrello.

{{% i18n_var "This release also includes Long Term Support versions of Plasma Workspaces %[1]s, KDE Development Platform %[2]s and the Kontact Suite %[2]s." "4.11.19" "4.14.8" %}}