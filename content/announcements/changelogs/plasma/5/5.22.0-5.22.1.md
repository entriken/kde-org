---
title: Plasma 5.22.1 complete changelog
version: 5.22.1
hidden: true
plasma: true
type: fulllog
---
{{< details title="Discover" href="https://commits.kde.org/discover" >}}
+ Flatpak: fix warning upon updates detection. [Commit.](http://commits.kde.org/discover/1f3e45fcb9386a35f3848eda5e72b146966f99c6) 
+ Hide URL buttons for invalid metadata. [Commit.](http://commits.kde.org/discover/6865ec109f0209ccd4b31acf3777b012a544bd27) 
{{< /details >}}

{{< details title="Plasma Addons" href="https://commits.kde.org/kdeplasma-addons" >}}
+ [applet/notes] Don't focus buttons on click. [Commit.](http://commits.kde.org/kdeplasma-addons/343967d79417392e397a0989ccc4d40ab5f9054d) Fixes bug [#437828](https://bugs.kde.org/437828)
{{< /details >}}

{{< details title="kscreenlocker" href="https://commits.kde.org/kscreenlocker" >}}
+ [kcm] Fix icon name in metadata.desktop. [Commit.](http://commits.kde.org/kscreenlocker/1806144fa64581c804df32adcd0536e5934c57da) 
{{< /details >}}

{{< details title="KWin" href="https://commits.kde.org/kwin" >}}
+ Platforms/drm: on NVidia do pageflips with QPainter. [Commit.](http://commits.kde.org/kwin/6561b58d8164978e4b61d129aed6dba84e6a1eea) See bug [#438363](https://bugs.kde.org/438363)
+ Platforms/drm: support NVidia as secondary GPU with CPU copy. [Commit.](http://commits.kde.org/kwin/5363451eae46aa87c4fffb27a197c2902f67e817) Fixes bug [#431062](https://bugs.kde.org/431062)
+ Kcm/kwinrules: Adapt spinbox width to actual text. [Commit.](http://commits.kde.org/kwin/79ff49f4927d0d3fbecfeabc5962c51b82c3254b) Fixes bug [#438193](https://bugs.kde.org/438193)
+ [xwl] Create a new datasource on offer changes. [Commit.](http://commits.kde.org/kwin/d0a5fb07da38c09fff56e5ccf71f27b5c0c36765) 
+ Platforms/drm: only allocate two dumb buffers for the swapchain. [Commit.](http://commits.kde.org/kwin/9ce14c04b1fe7f0bb3d35c81ba5c3a28c0dcf001) 
+ Remove unused include. [Commit.](http://commits.kde.org/kwin/120c98c6ada4f2b38a8ce30679688fda73995e86) 
+ Platforms/drm: fix modifiers detection. [Commit.](http://commits.kde.org/kwin/18c0525422a000247b5d487af77deb9428be0c0c) See bug [#437893](https://bugs.kde.org/437893)
+ [tabbox] fix non-working global shortcuts overrides. [Commit.](http://commits.kde.org/kwin/c04b2aa367a94a09f8ab036841fd9802a0e55434) Fixes bug [#359141](https://bugs.kde.org/359141)
{{< /details >}}

{{< details title="libksysguard" href="https://commits.kde.org/libksysguard" >}}
+ Remove sensors before adding. [Commit.](http://commits.kde.org/libksysguard/9dc314d860abeb7c320364ae5494611c26555363) Fixes bug [#438354](https://bugs.kde.org/438354)
{{< /details >}}

{{< details title="Plasma Desktop" href="https://commits.kde.org/plasma-desktop" >}}
+ Make config category delegates always fill sidebar. [Commit.](http://commits.kde.org/plasma-desktop/5a656cba1a417c5c19d9810874b810288700e96d) 
{{< /details >}}

{{< details title="plasma-disks" href="https://commits.kde.org/plasma-disks" >}}
+ Add pipeline for reuse compliance. [Commit.](http://commits.kde.org/plasma-disks/f26ea6707c82839235b6678ebf2720861fa3e504) 
{{< /details >}}

{{< details title="Plasma Nano" href="https://commits.kde.org/plasma-nano" >}}
+ Make sure the config dialog is maximized. [Commit.](http://commits.kde.org/plasma-nano/31501eeb2e6bc14b8536158f2790c03a96f6949e) 
{{< /details >}}

{{< details title="Plasma Networkmanager (plasma-nm)" href="https://commits.kde.org/plasma-nm" >}}
+ Add missing QFile include. [Commit.](http://commits.kde.org/plasma-nm/55ab9bfd83fb74d14e26cc1a3d8f257bc47c45ea) 
{{< /details >}}

{{< details title="Plasma Phone Components" href="https://commits.kde.org/plasma-phone-components" >}}
+ Lockscreen: handle all cases for lockscreen keyboard focus. [Commit.](http://commits.kde.org/plasma-phone-components/6667ecbfdd92c6a6ccb5f61dc5cfe778a1f73d47) 
+ Lockscreen: explicitly tell keyboard to open rather than relying on focus. [Commit.](http://commits.kde.org/plasma-phone-components/026b8ac03714df1a845600409afc47bff78bc38c) 
+ Panel: remove unnecessary color changes for header panel when quicksettings open. [Commit.](http://commits.kde.org/plasma-phone-components/154a899f3efc90bf7c9eb53f3d63de97f09fe3dc) 
{{< /details >}}

{{< details title="Plasma Systemmonitor" href="https://commits.kde.org/plasma-systemmonitor" >}}
+ Clear layers when navigating. [Commit.](http://commits.kde.org/plasma-systemmonitor/f06cf1e0383f085fc6e451d9b2055d80c118c19a) 
+ Switch away from removed pages. [Commit.](http://commits.kde.org/plasma-systemmonitor/d96d748fd7d97c439f7dc8226155b4b38036b47d) Fixes bug [#437499](https://bugs.kde.org/437499)
+ Do not reset the start page if the model changes. [Commit.](http://commits.kde.org/plasma-systemmonitor/5ef9aa2297a84ec8448d8662d2300de90fea92e3) 
+ Fix binding loop and null derefence warnings in EditablePage::heightForContent. [Commit.](http://commits.kde.org/plasma-systemmonitor/6f8d7f5ca1e4308c1fc338d45818a14d9f2cfaaf) 
+ Fix GCC warning about copying an element from a container. [Commit.](http://commits.kde.org/plasma-systemmonitor/6e222f63e5c19d84f3fb9ef5c7125d5dde20ad7f) 
+ Use entryEvent signal for reacting to GHNS changes. [Commit.](http://commits.kde.org/plasma-systemmonitor/4d1bebb35ee933e8dbd322463ea8eed2712ad517) See bug [#438336](https://bugs.kde.org/438336)
+ Push NewStuff pages to layers, not the stack. [Commit.](http://commits.kde.org/plasma-systemmonitor/55fe372c6bdfabdd958795344ef4cf15ff548790) Fixes bug [#437961](https://bugs.kde.org/437961)
{{< /details >}}

{{< details title="Plasma Workspace" href="https://commits.kde.org/plasma-workspace" >}}
+ [applets/devicenotifier] Don't show empty header most of the time. [Commit.](http://commits.kde.org/plasma-workspace/7cffd42b480d5a1dd43703a797104f2dddb567ca) Fixes bug [#438351](https://bugs.kde.org/438351)
+ [kcms/autostart] Avoid empty application icon. [Commit.](http://commits.kde.org/plasma-workspace/a657df4232e637f5560a6ec5138fb066565310d7) 
+ [libtaskmanager/x11] Fix transient windows bug. [Commit.](http://commits.kde.org/plasma-workspace/2a1f9e4489687fa276c20bddf05d3eedb0994b00) Fixes bug [#438222](https://bugs.kde.org/438222)
+ Krunerglobalshortcuts: Fix migration from old component. [Commit.](http://commits.kde.org/plasma-workspace/924bdf5e27850b33282e1b4f09d9c27fbb4cfd55) 
+ Recent Documents: Fix missing actions for results. [Commit.](http://commits.kde.org/plasma-workspace/23577747e93fbb1dfb91d213c78d1f45823de487) Fixes bug [#437462](https://bugs.kde.org/437462)
+ [kcms/autostart] Keep capitalization of desktop file names. [Commit.](http://commits.kde.org/plasma-workspace/3080b2801cea3437772b2a68e07866f18618d538) Fixes bug [#438406](https://bugs.kde.org/438406)
+ Point bbcukmet to new location API. [Commit.](http://commits.kde.org/plasma-workspace/398762cf8763d56c5b6d6ce21ff29d30f3bb98d3) Fixes bug [#430643](https://bugs.kde.org/430643)
+ Krunnerglobalshortcuts: Prevent actions from becoming inactive. [Commit.](http://commits.kde.org/plasma-workspace/42b1039ed32c62e1d1750135cc5509caea6fe6b8) See bug [#437364](https://bugs.kde.org/437364)
+ Fix kcmfontinst install destination. [Commit.](http://commits.kde.org/plasma-workspace/f90d4d5548dfe2d75e83b8da91af9be0e261286b) Fixes bug [#436306](https://bugs.kde.org/436306)
{{< /details >}}

{{< details title="plasma-workspace-wallpapers" href="https://commits.kde.org/plasma-workspace-wallpapers" >}}
+ Add milky way. [Commit.](http://commits.kde.org/plasma-workspace-wallpapers/4f8e26400b0d04bde60adf99f55e2b34cc6c97ab) Fixes bug [#438349](https://bugs.kde.org/438349)
{{< /details >}}

{{< details title="System Settings" href="https://commits.kde.org/systemsettings" >}}
+ [sidebar] Add missing subcategory header spacing for widescreen view. [Commit.](http://commits.kde.org/systemsettings/4576284c627725083e2795df74929903ec1dbd91) Fixes bug [#438377](https://bugs.kde.org/438377)
+ Only update the global header reacting to the active page. [Commit.](http://commits.kde.org/systemsettings/0ea21d35d988a36b937b7bc0772561b47077da7b) Fixes bug [#437088](https://bugs.kde.org/437088)
{{< /details >}}

