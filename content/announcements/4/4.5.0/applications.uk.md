---
title: "Програми KDE 4.5.0: зручність та маршрутизація"
hidden: true
---

<p>
KDE, міжнародна технологічна спільнота, щаслива повідомити про те, що випущено набір програм KDE версії 4.5. Які б програми не входили до цього набору — ігри, освітні програми, офісні програми чи інші корисні програми — ці програми стали потужнішими та простішими у користуванні.
</p>

<div class="text-center">
	<a href="/announcements/4/4.5.0/apps-overview.png">
	<img src="/announcements/4/4.5.0/thumbs/apps-overview.png" class="img-fluid" >
	</a> <br/>
	<em>Математичні програми, програми для редагування текстів та розваг</em>
</div>
<br/>

<p>
Програми KDE було покращено за багатьма параметрами. У цьому оголошенні ми зупинимося лише на деяких внесених змінах.
</p>
<p>
    <ul>

   <li>Команда створення ігор KDE попрацювала над реалізацією нових можливостей та створенням нової гри: <b>Kajongg</b>, <a href="http://en.wikipedia.org/wiki/Mahjong">оригінальна версія гри у маджонґ для чотирьох гравців</a>. Цю гру було написано цілковито мовою Python, отже це перша програма з набору програм KDE, написана мовою Python. У новій версії гри <a href="http://www.kde.org/applications/games/konquest/">Konquest</a>, гри у підкорення галактики, користувачі зможуть створювати і змінювати карти гри власноруч. Серед інших покращень простіше налаштування у <a href="http://www.kde.org/applications/games/palapeli">грі у складанки Palapeli</a>, новий набір рівнів KGoldRunner («Знищення»), та краща інтеграція підтримки файлів <a href="http://en.wikipedia.org/wiki/Smart_Game_Format">SGF</a> у <a href="http://www.kde.org/applications/games/kigo/">Kigo</a>.
    </li>
    <li>У <b>навчальних програмах</b> ви також знайдете декілька нових можливостей. Найбільших змін зазнала програма допомоги у вивченні мов, <a href="http://www.kde.org/applications/education/parley/">Parley</a>, у ній передбачено новий інтерфейс вправляння та підтримку дієвідмін.
    </li>
    <li><a href="http://www.kde.org/applications/education/marble/"><b>Marble</b></a>, наш віртуальний глобус, тепер може планувати маршрути і звантажувати дані карт так, щоб ви могли скористатися картою у подорожі, де у вас не буде доступу до Інтернету. Ви зможете скористатися атласами та можливостями з подання картографічних даних Marble, наданими службами OpenStreetmap і OpenRouteService. Докладніше про <a href="http://nienhueser.de/blog/?p=137">роботу з картами та маршрутами у автономному режимі у Marble</a>.
    </li>

</ul>

</p>

<div class="text-center">
	<a href="/announcements/4/4.5.0/marble-routing.png">
	<img src="/announcements/4/4.5.0/thumbs/marble-routing.png" class="img-fluid" >
	</a> <br/>
	<em>The Marble Desktop Globe can now be used for routing as well</em>
</div>
<br/>

<p align="justify">

У Konqueror передбачено можливість автоматичного оновлення списків блокування реклами, що спростить вам перегляд Інтернету. Користувачі, які надають перевагу використанню WebKit для показу сторінок, зможуть скористатися модулем KDE-WebKit. Нова версія Gwenview може працювати без блокування інтерфейсу навіть у разі застосування ефектів, дуже вимогливих до можливостей процесора. У процесі редагування зображень за допомогою нової версії повністю зберігаються всі метадані EXIF. Крім того, у новій версії Gwenview передбачено кращі можливості з користування та розширені можливості з налаштування. Нарешті, KInfoCenter, програму, яка надає користувачеві огляд апаратної та програмної конфігурації системи, у новій версії значно перероблено: тепер нею зручніше користуватися, покращено також стабільність роботи програми.

</p>

<h3>Інші знімки вікон…</h3>

<div class="text-center">
	<a href="/announcements/4/4.5.0/plasma-infocenter.png">
	<img src="/announcements/4/4.5.0/thumbs/plasma-infocenter.png" class="img-fluid">
	</a> <br/>
	<em>Повністю перероблений інтерфейс програми KInfoCenter, призначеної для показу відомостей щодо вашої системи</em>
</div>
<br/>

<div class="text-center">
	<a href="/announcements/4/4.5.0/dolphin-metadata.png">
	<img src="/announcements/4/4.5.0/thumbs/dolphin-metadata.png" class="img-fluid">
	</a> <br/>
	<em>Можливості семантичної стільниці у Dolphin надають доступ до корисних метаданих</em>
</div>
<br/>

<div class="text-center">
	<a href="/announcements/4/4.5.0/gwenview-flickrexport.png">
	<img src="/announcements/4/4.5.0/thumbs/gwenview-flickrexport.png" class="img-fluid">
	</a> <br/>
	<em>За допомогою Gwenview вам простіше буде оприлюднити ваші фотографії у мережі на одній з ваших улюблених служб зберігання зображень.</em>
</div>
<br/>

<h4>Встановлення програм KDE</h4>

<p align="justify">
KDE software, including all its libraries and its applications, is available for free under Open Source licenses. KDE software runs on various hardware configurations, operating systems and works with any kind of window manager or desktop environment. Besides Linux and other UNIX based operating systems you can find Microsoft Windows versions of most KDE applications on the <a href="http://windows.kde.org">KDE software on Windows</a> site and Apple Mac OS X versions on the <a href="http://mac.kde.org/">KDE software on Mac site</a>. Experimental builds of KDE applications for various mobile platforms like MeeGo, MS Windows Mobile and Symbian can be found on the web but are currently unsupported.
<br />
KDE software can be obtained in source and various binary formats from <a
href="http://download.kde.org/stable/4.5.0/">http://download.kde.org</a> and can
also be obtained on <a href="http://www.kde.org/download/cdrom.">CD-ROM</a>
or with any of the <a href="http://www.kde.org/download/distributions.">major
GNU/Linux and UNIX systems</a> shipping today.
</p>
<p align="justify">
<a id="packages"><em>Packages</em></a>.
Some Linux/UNIX OS vendors have kindly provided binary packages of 4.5.0
for some versions of their distribution, and in other cases community volunteers
have done so. <br />
  Some of these binary packages are available for free download from KDE's <a
href="http://download.kde.org/binarydownload.html?url=/stable/4.5.0/">http://download.kde.org</a>.
  Additional binary packages, as well as updates to the packages now available,
will become available over the coming weeks.
</p>

<p align="justify">
  <a id="package_locations"><em>Package Locations</em></a>.
  For a current list of available binary packages of which the KDE's Release Team has
been informed, please visit the <a href="/info/4.5.0.">4.5 Info
Page</a>.
</p>

<h4>
  Compiling 4.5.0
</h4>
<p align="justify">
  <a id="source_code"></a>
  The complete source code for 4.5.0 may be <a
href="http://download.kde.org/stable/4.5.0/src/">freely downloaded</a>.
Instructions on compiling and installing KDE software 4.5.0
  are available from the <a href="/info/4.5.0.#binary">4.5.0Info Page</a>.
</p>

<h4>
System Requirements
</h4>
<p align="justify">
In order to get the most out of these releases, we strongly recommend to use the latest version of Qt, as of today 4.6.3. This is necessary in order to assure a stable experience, as some improvements made to KDE software have actually been done in the underlying Qt framework.<br />
Graphics drivers may under certain conditions fall back to XRender rather than OpenGL for compositing. If you have issues with notably slow graphics performance, switching off desktop effects is likely to help, depending on the graphics driver and setup used. In order to make full use of the capabilities of KDE's software, we also recommend to use the latest graphics drivers for your system, as this can improve the user experience substantially, both in optional functionality, and in overall performance and stability. 
</p>




<h2>Крім того, сьогодні випущено:</h2>

<a href="../platform">
<img src="/announcements/4/4.5.0/images/platform.png" class="app-icon float-left m-3" alt="Платформа розробки KDE 4.5.0"/>
</a>

<h2>У новій Платформі розробки KDE 4.5.0 ви зможете скористатися покращеною швидкодією, стабільністю, новим високошвидкісним кешем та підтримкою WebKit</h2>
<p align="justify">
10 серпня 2010 року. Сьогодні командою KDE було випущено Платформу розробки KDE версії 4.5.0. Цей випуск характеризується покращеннями у швидкодії та стабільності. Нову версію <b>KSharedDataCache</b> оптимізовано для пришвидшення доступу до даних, що зберігаються на диску, зокрема піктограм. У новій версії бібліотеки <b>WebKit</b> передбачено інтеграцію з параметрами мережі, системою зберігання паролів та іншими можливостями Konqueror. <a href="../platform"><b>Ознайомитися з повним текстом повідомлення</b></a>
</p>

<a href="../plasma">
<img src="/announcements/4/4.5.0/images/plasma.png" class="app-icon float-left m-3" alt="Робочі простори Плазми KDE 4.5.0" />
</a>

<h2>Плазма для стаціонарних та портативних комп’ютерів, версія 4.5.0: зручніше користування</h2>
<p align="justify">
 Сьогодні командою KDE випущено робочі простори Плазми для стаціонарних та портативних комп’ютерів версії 4.5.0. Користування Плазмою для стаціонарних комп’ютерів стало набагато зручнішим. Спрощено прийоми роботи з сповіщеннями та завданнями. З області сповіщень було усунуто зайві візуальні елементи, обробка сповіщень від програм стала одноріднішою за рахунок ширшого використання протоколу сповіщення Freedesktop.org, вперше реалізованого у попередній версії Плазми. <a href="../plasma"><b>Ознайомитися з повним текстом повідомлення</b></a>
</p>
