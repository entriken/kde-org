---
date: 2022-09-06
changelog: 5.25.4-5.25.5
layout: plasma
video: false
asBugfix: true
draft: false
---

+ Plasma Desktop: Fix KCM duplicates in landingpage. [Commit.](http://commits.kde.org/plasma-desktop/44821ceddc9bd14f72bf1e9e4c5f22119f657262) Fixes bug [#449563](https://bugs.kde.org/449563)
+ Fix emojier displaying a blank window with ibus 1.5.26. [Commit.](http://commits.kde.org/plasma-desktop/58f34488426f041f0b185bfd0fbcf049b2119e64) Fixes bug [#457521](https://bugs.kde.org/457521)
+ Fix profile switching in the brightness actions. [Commit.](http://commits.kde.org/powerdevil/0f548c53dd177235eaf5ce8452c9b44839c12e21) Fixes bug [#394945](https://bugs.kde.org/394945)
