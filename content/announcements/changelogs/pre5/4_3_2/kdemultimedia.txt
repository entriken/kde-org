------------------------------------------------------------------------
r1017563 | gulino | 2009-08-30 22:09:56 +0000 (Sun, 30 Aug 2009) | 2 lines

Backport commit 1017562

------------------------------------------------------------------------
r1019176 | scripty | 2009-09-03 04:10:48 +0000 (Thu, 03 Sep 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1021086 | scripty | 2009-09-08 03:00:00 +0000 (Tue, 08 Sep 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1022150 | dfaure | 2009-09-10 22:21:23 +0000 (Thu, 10 Sep 2009) | 2 lines

Backport fix for session-management bug that made kscd unusable until killed by hand.

------------------------------------------------------------------------
r1022184 | darioandres | 2009-09-11 02:14:09 +0000 (Fri, 11 Sep 2009) | 4 lines

Backport to 4.3:
- Fix a small memory leak by clearing the vorbis variables on destruction


------------------------------------------------------------------------
r1022194 | darioandres | 2009-09-11 02:26:35 +0000 (Fri, 11 Sep 2009) | 5 lines

- Do not use deprecated members:
  - Use QByteArray::fromRawData instead of QByteArray.setRawData
  - On KDoubleNumInput use setDecimals instead of setPrecision


------------------------------------------------------------------------
r1024444 | scripty | 2009-09-16 16:13:34 +0000 (Wed, 16 Sep 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1027830 | lueck | 2009-09-24 21:14:17 +0000 (Thu, 24 Sep 2009) | 1 line

use lowercase catalogname to load the translations
------------------------------------------------------------------------
r1030279 | scripty | 2009-10-02 02:57:30 +0000 (Fri, 02 Oct 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
