---
date: 2022-03-08
changelog: 5.24.2-5.24.3
layout: plasma
video: false
asBugfix: true
draft: false
---

+ Kcms/colors: fix spacing between radio buttons and content. [Commit.](http://commits.kde.org/plasma-workspace/d7469d570c5d331419f64293d86b7fa06e215b55)
+ SDDM theme: stop eliding people's names so aggressively. [Commit.](http://commits.kde.org/plasma-workspace/9b307559ef19a773576c745d65b337ff287d475f) Fixes bug [#450673](https://bugs.kde.org/450673)
+ Powerdevil: Improved backlight devices selection. [Commit.](http://commits.kde.org/powerdevil/c6b7c714af1d25d04bc820bc5ce554241999901f) Fixes bug [#399646](https://bugs.kde.org/399646)
