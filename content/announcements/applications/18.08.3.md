---
aliases:
- ../announce-applications-18.08.3
changelog: true
date: 2018-11-08
description: KDE Ships KDE Applications 18.08.3
layout: application
title: KDE Ships KDE Applications 18.08.3
version: 18.08.3
---

{{% i18n_var "November 8, 2018. Today KDE released the third stability update for <a href='%[1]s'>KDE Applications 18.08</a>. This release contains only bugfixes and translation updates, providing a safe and pleasant update for everyone." "../18.08.0" %}}

About 20 recorded bugfixes include improvements to Kontact, Ark, Dolphin, KDE Games, Kate, Okular, Umbrello, among others.

Improvements include:

- HTML viewing mode in KMail is remembered, and again loads external images if allowed
- Kate now remembers meta information (including bookmarks) between editing sessions
- Automatic scrolling in the Telepathy text UI was fixed with newer QtWebEngine versions