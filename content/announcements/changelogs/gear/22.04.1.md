---
aliases:
- ../../fulllog_releases-22.04.1
title: KDE Gear 22.04.1 Full Log Page
type: fulllog
gear: true
hidden: true
---
{{< details id="artikulate" title="artikulate" link="https://commits.kde.org/artikulate" >}}
+ Remove %i parameter from Exec line. [Commit.](http://commits.kde.org/artikulate/ff1aa582ec4237cfb39e3dfdfb4f3f0e93863372) 
{{< /details >}}
{{< details id="calendarsupport" title="calendarsupport" link="https://commits.kde.org/calendarsupport" >}}
+ Use standaloneMonthName in the printed month's header. [Commit.](http://commits.kde.org/calendarsupport/2fd753e34b5ee3882af8d4cc7a8aa74414d02253) Fixes bug [#453292](https://bugs.kde.org/453292)
{{< /details >}}
{{< details id="cantor" title="cantor" link="https://commits.kde.org/cantor" >}}
+ Fix includes. [Commit.](http://commits.kde.org/cantor/064c0ba1526c5cb1bfcf353aea4f84d8ca6393d6) 
{{< /details >}}
{{< details id="dolphin" title="dolphin" link="https://commits.kde.org/dolphin" >}}
+ Fix icon resize animation. [Commit.](http://commits.kde.org/dolphin/94bbf13ff7fe45cb4a847ed7906fa69d5a39748b) 
+ Fix terminal panel not keeping up with dir changes. [Commit.](http://commits.kde.org/dolphin/e70e12e3bdf3ce4e9cca4c8f003655ea10b21d7e) Fixes bug [#391380](https://bugs.kde.org/391380). Fixes bug [#416690](https://bugs.kde.org/416690)
{{< /details >}}
{{< details id="elisa" title="elisa" link="https://commits.kde.org/elisa" >}}
+ Correct last commit. [Commit.](http://commits.kde.org/elisa/25167bf1e30cdcb2c8693f3a82552ec03776c866) 
+ Improve UX for buttons that open menus. [Commit.](http://commits.kde.org/elisa/fcde91570f35ebb5726580a2cdcff55c0d93130d) Fixes bug [#453399](https://bugs.kde.org/453399)
+ Fix Open and Save Playlist global menu entries. [Commit.](http://commits.kde.org/elisa/a5f6f0b613ae26610ba5421c7b27f7e38299ad06) 
+ Emit change signals for various config changes. [Commit.](http://commits.kde.org/elisa/4ff13b1942380d9f2b86fc59abb58934063ce006) Fixes bug [#436424](https://bugs.kde.org/436424)
{{< /details >}}
{{< details id="eventviews" title="eventviews" link="https://commits.kde.org/eventviews" >}}
+ Edit start dates in the Todo view. [Commit.](http://commits.kde.org/eventviews/6f859a64eacda84e56a793b370a3efd42f9f32cf) 
+ Use standaloneMonthName in the Month View's header. [Commit.](http://commits.kde.org/eventviews/6a6d73274f088194ae77765854d7a06aa19d5806) Fixes bug [#453292](https://bugs.kde.org/453292)
+ Remove some SIGNAL and SLOT macros. [Commit.](http://commits.kde.org/eventviews/d1f78490130965fa9086e536419092d4f6a1b0d8) 
+ Keep dtStart valid when moving a todo. [Commit.](http://commits.kde.org/eventviews/ef3e58f27b7b11fb358ab4f959861a3420b94577) 
{{< /details >}}
{{< details id="filelight" title="filelight" link="https://commits.kde.org/filelight" >}}
+ Rebuild the iteration tech using better architecture. [Commit.](http://commits.kde.org/filelight/e4c9db692acf2969ef14a927a842fa5edc657887) Fixes bug [#442299](https://bugs.kde.org/442299)
{{< /details >}}
{{< details id="incidenceeditor" title="incidenceeditor" link="https://commits.kde.org/incidenceeditor" >}}
+ Separate time order tests from time validation tests. [Commit.](http://commits.kde.org/incidenceeditor/250c2de6576007c2c20a3c01ecaac04da5983b99) 
+ Compute locale-independant strings. [Commit.](http://commits.kde.org/incidenceeditor/d4d479a26e2a895390bb2aa5cda6c0e264c3de0b) 
+ Simplify KTimeZoneComboBox. [Commit.](http://commits.kde.org/incidenceeditor/c9ad2907d6f4cd2fa359bffc22ff154614348bc3) 
+ Test initialization and display of todo datetime GUI elements. [Commit.](http://commits.kde.org/incidenceeditor/070ccf338b9d09098cd37643b1bbf528f0b725a5) 
{{< /details >}}
{{< details id="itinerary" title="itinerary" link="https://commits.kde.org/itinerary" >}}
+ Add 22.04.1 release notes. [Commit.](http://commits.kde.org/itinerary/9d0484c9ca0713bfb670311582eb11a297c8402f) 
+ Don't attempt to resolve ISO 3166-1 country codes for the address formatter. [Commit.](http://commits.kde.org/itinerary/b2bc9af04be59f949012bbc709f9c1bed1ad42f4) Fixes bug [#452979](https://bugs.kde.org/452979)
+ Support all barcode formats specified in pkpass files. [Commit.](http://commits.kde.org/itinerary/21db26241a3fcb93dd97f59ad61debe76ed20ea1) 
{{< /details >}}
{{< details id="kajongg" title="kajongg" link="https://commits.kde.org/kajongg" >}}
+ Install missing file. [Commit.](http://commits.kde.org/kajongg/8716aa796f819f0374e3d13b2b5ddb154f960d93) 
{{< /details >}}
{{< details id="kalarm" title="kalarm" link="https://commits.kde.org/kalarm" >}}
+ Don't show volume fade controls if Phonon backend doesn't support fade. [Commit.](http://commits.kde.org/kalarm/38bc9f5a2c10c57a374991f1e39ce72e41ded125) 
+ Fix sound files not playing after previously playing with fade. [Commit.](http://commits.kde.org/kalarm/f648997e7675990c0dfffbac420e126048e03014) 
+ Bug 453193: Fix crash in font chooser after deselecting default font. [Commit.](http://commits.kde.org/kalarm/1e44f031537711733a38fe487fee78e0be41c698) Fixes bug [#453193](https://bugs.kde.org/453193)
+ Fix Stop Play button not working for display alarm with audio file. [Commit.](http://commits.kde.org/kalarm/7a6348bd49b9e0f5b5c3ad85403e1b450cb0c772) 
+ Bug 452962: Fix crash when Try clicked to stop audio alarm with fade. [Commit.](http://commits.kde.org/kalarm/30d5a943393d647bb7471d571d25a326e217ec04) 
+ Fix time spin boxes being displayed in the wrong position. [Commit.](http://commits.kde.org/kalarm/158ad4413f1609aff6a16d2254f05ecf72722483) 
+ Fix checkboxes being disabled in Preferences dialog. [Commit.](http://commits.kde.org/kalarm/4565ff433d5785298c9f548b1cf7486e6c915864) 
{{< /details >}}
{{< details id="kalendar" title="kalendar" link="https://commits.kde.org/kalendar" >}}
+ Install locally downloaded translations. [Commit.](http://commits.kde.org/kalendar/daf3c0bea9dd7479785541725a3e012d504961d1) 
+ Use IncidenceTreeModel from akonadi-calendar. [Commit.](http://commits.kde.org/kalendar/3966f45c3e8d3a71ffbc3b845e6753a105ad31c9) 
+ Fix tasks view highlight colours. [Commit.](http://commits.kde.org/kalendar/237167b3017b701478dc5865c77d088c62175cec) 
+ Ensure Kirigami Theme colourset pulls from properties in AbstractTreeItem. [Commit.](http://commits.kde.org/kalendar/8b3eda5e5b24e455406a070e2b6e20e49f9cd59e) 
+ Fix bad property access. [Commit.](http://commits.kde.org/kalendar/6d9ce81caecca524b18e8add0649fa654973351f) 
+ Parity between kirigami abstractlistitem and our treeview abstracttreeitem, fix highlight of list items. [Commit.](http://commits.kde.org/kalendar/c82d595f097ce8e427b1b1e5b223f6c48bee2777) 
+ Ensure delete dialog properly wraps label. [Commit.](http://commits.kde.org/kalendar/b5af9475e20cad46a42987bcbf9f48e4806b3144) 
+ Remove unnecessary key press handlers. [Commit.](http://commits.kde.org/kalendar/b3c9c136ad390746ed74732b62f3573e8619f355) 
+ Fix 'next week' button in the week's view. [Commit.](http://commits.kde.org/kalendar/aba5daa965e4ed61ec1ee728abd6963bbdc7e093) 
+ Ensure incidence summary text in incidence info drawer is properly escaped. [Commit.](http://commits.kde.org/kalendar/d937483fb680cd3edf34a862114f6acb32dc397b) 
{{< /details >}}
{{< details id="kate" title="kate" link="https://commits.kde.org/kate" >}}
+ Use both highlightingModeAt and highlightingMode. [Commit.](http://commits.kde.org/kate/7d1c9096ebe60b4acda6c4ae2711328b39a62257) 
+ Fix snippets not showing up in completion. [Commit.](http://commits.kde.org/kate/cab17fbeb81f40b1f03801e6b5a0a947f5221028) 
+ No delayed readSessionConfig. [Commit.](http://commits.kde.org/kate/3414b6269a616a0dee4237f66e77772669ee59ff) Fixes bug [#453152](https://bugs.kde.org/453152)
+ Fix crash on file browser activation. [Commit.](http://commits.kde.org/kate/a28624c877cfe6db6ae6ab257f95af36213b25e0) Fixes bug [#453234](https://bugs.kde.org/453234)
{{< /details >}}
{{< details id="kbackup" title="kbackup" link="https://commits.kde.org/kbackup" >}}
+ Saving profile needs to reset the backup cycle. [Commit.](http://commits.kde.org/kbackup/ca33bb7d2d7c435e7f71b114965db05d9b8c9788) 
{{< /details >}}
{{< details id="kcalutils" title="kcalutils" link="https://commits.kde.org/kcalutils" >}}
+ Display year of exception to yearly recurrence. [Commit.](http://commits.kde.org/kcalutils/8d05102db40c04e753f0970511ffec377d7d5f79) 
+ Fix Bug 452480: Kmail doesn't display calendar invites. [Commit.](http://commits.kde.org/kcalutils/494331a28bb722b4ab4c38a2520f305b8f7ad8fa) Fixes bug [#452480](https://bugs.kde.org/452480)
{{< /details >}}
{{< details id="kdeconnect-kde" title="kdeconnect-kde" link="https://commits.kde.org/kdeconnect-kde" >}}
+ Kcm: Fix showing plugins' configuration. [Commit.](http://commits.kde.org/kdeconnect-kde/a0b9a2131c2fa30c041448f87fa927128785cea0) 
+ Fix PlaySound with correct QUrl. [Commit.](http://commits.kde.org/kdeconnect-kde/27da388c74e5eebca05251de471057486612f69b) Fixes bug [#452591](https://bugs.kde.org/452591)
{{< /details >}}
{{< details id="kdenlive" title="kdenlive" link="https://commits.kde.org/kdenlive" >}}
+ Add 'reverse' parameter to transition 'mix'. [Commit.](http://commits.kde.org/kdenlive/f7bc263e620657957726f5887a07ab913525a31a) 
+ Fix custom effect type sometimes incorrect. [Commit.](http://commits.kde.org/kdenlive/89cdeb163db3392ce5cbc6094974e8f4b83c11b2) 
+ Fix drag incorrectly terminating in icon view. [Commit.](http://commits.kde.org/kdenlive/c6f537bea43ec6fb88153752dd8c9ccf106f66e3) 
+ Fix freeze cause by incorrect duplicate entry in thumbnail cache. [Commit.](http://commits.kde.org/kdenlive/077f2f290a4f42ca7bacae1cc05c87db16c3f35c) 
+ Fix crash trying to drag in empty space in Bin icon view. [Commit.](http://commits.kde.org/kdenlive/ae1e79a176067ed3f78ff473fd5063972c26f1d2) 
+ Update kdenliveeffectscategory.rc new mlt's box_blur added to the 'Blur and Sharpen' category. [Commit.](http://commits.kde.org/kdenlive/7431f170cefc5424a5bf21fc2188161d18a6e266) 
+ Update CMakeLists.txt adding the new mlt's Box_Blur. [Commit.](http://commits.kde.org/kdenlive/02cd900aa5e72ed17f13585434aaa1576fa87dd2) 
+ Add new mlt's Box_Blur ui. It was not working with the automatic one. [Commit.](http://commits.kde.org/kdenlive/42c8bd8c555acd470563cfcab94197112857f1d7) 
+ Update secondary_color_correction.xml fixing Transparency default value error. [Commit.](http://commits.kde.org/kdenlive/6ba4c16f48d9ef64c26ea2c2744c066fee52e1ef) 
+ Fix titler text alignment. [Commit.](http://commits.kde.org/kdenlive/4685d1353cf0b098049cdf31828720241c9f026a) 
+ Fix potential deadlock, maybe related to #1380. [Commit.](http://commits.kde.org/kdenlive/3c59d78541e124c4a639537bbe1f696ebd563d40) 
+ Small refactoring of cache get thumbnail. [Commit.](http://commits.kde.org/kdenlive/a61a8f94657883e781e405e6db30a9b2a25b7a6c) 
+ Fix timeline preview failing when creating a new project. [Commit.](http://commits.kde.org/kdenlive/9f51aeb7ca86b074284782accfb993c626aa1707) 
+ Timeline preview profiles - remove unused audio parameters, fix interlaced nvenc. [Commit.](http://commits.kde.org/kdenlive/e725bcfdcddda0753232fef78e87636c3865c881) 
+ Another set of minor improvements for monitor audio level. [Commit.](http://commits.kde.org/kdenlive/18f77f07afb37011473e3fef211d42ba6434d648) 
+ Minor fix in audio levels look. [Commit.](http://commits.kde.org/kdenlive/1ba4dedcfe7d59504815d5c237c4605586d69ed4) 
+ Ensure all color clips use the RGBA format. [Commit.](http://commits.kde.org/kdenlive/8ab8f8e53e1935190521470ba306bf0c6b664352) 
+ Show dB in mixer tooltip. [Commit.](http://commits.kde.org/kdenlive/a5690290305ac7c096b8b3a3bdd49745913d813a) 
+ Fix audio levels showing incorrect values, and not impacted by master effects. [Commit.](http://commits.kde.org/kdenlive/637661db295e18f623f439b090e384f34fb29100) 
{{< /details >}}
{{< details id="kdevelop" title="kdevelop" link="https://commits.kde.org/kdevelop" >}}
+ ContextBrowserPlugin: remove redundant DUChain read locks. [Commit.](http://commits.kde.org/kdevelop/5dbeef5fa857f1643f8b47cf2b025ca1325a38b9) 
+ Fix a crash in the "update signature action". [Commit.](http://commits.kde.org/kdevelop/a947074f0872ad3245b8c73679143998a88e3753) Fixes bug [#416714](https://bugs.kde.org/416714). See bug [#358787](https://bugs.kde.org/358787)
{{< /details >}}
{{< details id="khangman" title="khangman" link="https://commits.kde.org/khangman" >}}
+ Remove %i parameter from Exec line. [Commit.](http://commits.kde.org/khangman/db7e987c2e83578ff1d230ecc3ad1ca40bf6cd06) 
{{< /details >}}
{{< details id="kio-extras" title="kio-extras" link="https://commits.kde.org/kio-extras" >}}
+ Fish: don't overwrite dir mimetype with extension based one. [Commit.](http://commits.kde.org/kio-extras/afe11f248c663e4038f0bab99c1bcb9fd1345b69) Fixes bug [#452940](https://bugs.kde.org/452940)
{{< /details >}}
{{< details id="kitinerary" title="kitinerary" link="https://commits.kde.org/kitinerary" >}}
+ Add PDF extractor stage for SNCF discount cards. [Commit.](http://commits.kde.org/kitinerary/d62695c849ed806f4d1cdf571432d32ec734577e) See bug [#453322](https://bugs.kde.org/453322)
+ Extract SNCF discount card barcodes. [Commit.](http://commits.kde.org/kitinerary/c7e4dee7d0f6d4fb5c3499bcbd8fcc18642dd021) See bug [#453322](https://bugs.kde.org/453322)
+ Extract SNCF discount program information from tickets. [Commit.](http://commits.kde.org/kitinerary/141af23a9b0a8b1ddf92d432b0a2b910650dfd91) See bug [#453322](https://bugs.kde.org/453322)
+ Fix reservation number propagation from barcode to PDF extracted data. [Commit.](http://commits.kde.org/kitinerary/982732660dfc94355d92d4627138ad3536558e9d) 
+ Handle minor variations in the phone number part from simplebooking.it. [Commit.](http://commits.kde.org/kitinerary/0a475e3fe444410eaee60e4fb329aea8296d5ea1) 
+ Extract onepagebooking.com pkpass files as well. [Commit.](http://commits.kde.org/kitinerary/8b09a8ebe23ad91f7e1aeeeca1fee82f418a9080) 
+ Make the onepagebooking.com email extractor slightly more tolerant. [Commit.](http://commits.kde.org/kitinerary/20b287eabca8a7f82346c7ffec24c2798bdf8d59) 
+ Add extractor for alternative booking.com HTML emails. [Commit.](http://commits.kde.org/kitinerary/c014509147dd15bce483f3cb1a5b8d6ff458e7cd) 
+ Also post-process stand-alone program membership elements. [Commit.](http://commits.kde.org/kitinerary/f85ea284528b01b547bcceb320bbfd6a7e03d739) 
+ Update eurowings plaintext extractor. [Commit.](http://commits.kde.org/kitinerary/71b0372bd67223fce21fa0b877e84196da2911d8) 
{{< /details >}}
{{< details id="kmail" title="kmail" link="https://commits.kde.org/kmail" >}}
+ Fix crash when we delete several identities. [Commit.](http://commits.kde.org/kmail/aa500afa6358b742cd8f609a716e6cd3d2582bee) 
{{< /details >}}
{{< details id="kmailtransport" title="kmailtransport" link="https://commits.kde.org/kmailtransport" >}}
+ Add separator. [Commit.](http://commits.kde.org/kmailtransport/a753314885c642b58f53198bf8870348631d5917) 
{{< /details >}}
{{< details id="knights" title="knights" link="https://commits.kde.org/knights" >}}
+ Remove %i parameter from Exec line. [Commit.](http://commits.kde.org/knights/494ac5a4e4dc3f58a5c242cff6e60d4c64a0e1fd) 
{{< /details >}}
{{< details id="kolourpaint" title="kolourpaint" link="https://commits.kde.org/kolourpaint" >}}
+ Recalculate toolbar size on layout requests. [Commit.](http://commits.kde.org/kolourpaint/859b4455e37464a8d6e8167f2581abf237f45bd4) 
{{< /details >}}
{{< details id="konsole" title="konsole" link="https://commits.kde.org/konsole" >}}
+ Fix cursor position after drawing Sixel graphics. [Commit.](http://commits.kde.org/konsole/9ae57f4e1de920d1a30b8f64448fd6c186a36730) Fixes bug [#452476](https://bugs.kde.org/452476)
{{< /details >}}
{{< details id="kopeninghours" title="kopeninghours" link="https://commits.kde.org/kopeninghours" >}}
+ Add Dutch month abbreviations and weekdays. Remove Ma conversion. [Commit.](http://commits.kde.org/kopeninghours/4ef92d5fa3e20bb5bd2d3534b31bdd2f2a81d866) 
{{< /details >}}
{{< details id="kosmindoormap" title="kosmindoormap" link="https://commits.kde.org/kosmindoormap" >}}
+ Fix removing the country prefix from the country subdivision code. [Commit.](http://commits.kde.org/kosmindoormap/97824328deacda5d4f8f014bb8d623bf9afa1745) 
{{< /details >}}
{{< details id="kpmcore" title="kpmcore" link="https://commits.kde.org/kpmcore" >}}
+ Fix location of GPT header on 4K LBA devices. [Commit.](http://commits.kde.org/kpmcore/82a6351bae64b99616249e44492259ec31a3b34e) Fixes bug [#453333](https://bugs.kde.org/453333)
{{< /details >}}
{{< details id="krfb" title="krfb" link="https://commits.kde.org/krfb" >}}
+ Remove %i parameter from Exec line. [Commit.](http://commits.kde.org/krfb/76ba4d038b33784bbfb91fa64ceb0c816c1cbc90) 
{{< /details >}}
{{< details id="libkleo" title="libkleo" link="https://commits.kde.org/libkleo" >}}
+ Include &lt;iterator&gt; to fix building with GCC 12 [Commit.](http://commits.kde.org/libkleo/5c4b8edb6d0b142a1d76276509e5252cacdecbe0) 
{{< /details >}}
{{< details id="marble" title="marble" link="https://commits.kde.org/marble" >}}
+ Explicitly define plugin id for thumbnail plugins. [Commit.](http://commits.kde.org/marble/59fb712062005c17c52755a9d7450eb61ccf0c34) 
{{< /details >}}
{{< details id="okular" title="okular" link="https://commits.kde.org/okular" >}}
+ Fix part of the Welcome Screen not being translatable. [Commit.](http://commits.kde.org/okular/afc0d8ed07a645f2013ec6f3094e36f70ab5edce) 
{{< /details >}}
{{< details id="pim-data-exporter" title="pim-data-exporter" link="https://commits.kde.org/pim-data-exporter" >}}
+ Show all headers in qtc6. [Commit.](http://commits.kde.org/pim-data-exporter/bef4da5087214bb5112611b28634615a08ab1b89) [Commit.](http://commits.kde.org/pim-data-exporter/003bb0cc14bfc367b4d2fcc52aed42a76c5ee32c) [Commit.](http://commits.kde.org/pim-data-exporter/9afa8d15ccddacb783cc414c61c2418a2f6c43b4) 
+ Use auto directly. [Commit.](http://commits.kde.org/pim-data-exporter/38640527b8dd1e072690d4f253d6f1de78e724f6) 
+ USe auto foo directly. [Commit.](http://commits.kde.org/pim-data-exporter/42e9b063ae1f928605f25784c6c3fc2fd0d9372b) 
+ Fix compile warnings. [Commit.](http://commits.kde.org/pim-data-exporter/f290d7eae3c39b7c556e54a075398228a7b9d064) [Commit.](http://commits.kde.org/pim-data-exporter/d475027ac9b37254ade73b11ad24da12ac1b7931) [Commit.](http://commits.kde.org/pim-data-exporter/ff25429742a31a89680e83364357f116a5cc4db7) [Commit.](http://commits.kde.org/pim-data-exporter/77ab9619695e9621330c014760d60ac58e7eb283) 
+ Const'ify variables. [Commit.](http://commits.kde.org/pim-data-exporter/766a54ffa2e260fbe49cef82f118e9741c6ab082) [Commit.](http://commits.kde.org/pim-data-exporter/04ba0f6ea6dfa5a79c78fb31b982b2ca9692f7bd) [Commit.](http://commits.kde.org/pim-data-exporter/7da217c878f542dbcb6e5b23d1ac62605cb9aa36) 
+ Const'ify pointer. [Commit.](http://commits.kde.org/pim-data-exporter/a7a5563837355e2637176970a9516d35a3b9b966) 
+ Remove it. [Commit.](http://commits.kde.org/pim-data-exporter/dd0fc50af0f0055c3ee50c782b097051dffe6ba0) 
+ Fix install variable. [Commit.](http://commits.kde.org/pim-data-exporter/b6068df9c898137365f9a195e2cc4da02dd49621) 
+ Prepare to use custom install dir PIMDATAINSTALL_DIR. [Commit.](http://commits.kde.org/pim-data-exporter/c9bbae9d96214fd05a60420f5fa6d8def4b2f9d5) 
+ Fix export reminder agent config. [Commit.](http://commits.kde.org/pim-data-exporter/95a11b035d9e4b35bc01ec603481e9868be53997) 
+ Fix update buttons. [Commit.](http://commits.kde.org/pim-data-exporter/f600b003b341a4e908bb8ea69ff9aa76bf3a41b0) 
+ Fix import/export colors. [Commit.](http://commits.kde.org/pim-data-exporter/9161494a1fa2cdbd611bd531d94d18c950bd3ed2) 
+ Fix import kalendar config. [Commit.](http://commits.kde.org/pim-data-exporter/6e73de4eb8dd2288fec69929472b8e82b3d90df8) 
+ Add more autotest (kalendar). [Commit.](http://commits.kde.org/pim-data-exporter/c3ef8d26b460d77763ff9a2664f425691c154779) 
+ Start to import/export Kalendar config. [Commit.](http://commits.kde.org/pim-data-exporter/79c8d7209d2859da84148ea94ed3321fc8d4f34b) 
+ Fix autotest. [Commit.](http://commits.kde.org/pim-data-exporter/5bbe4dd7dc83b6841e4551b3dbda3ea10b2a983b) 
+ Fix import kalendar reminder agent. [Commit.](http://commits.kde.org/pim-data-exporter/e19f04c9e7116df3fc0d7f11be490662fc01a8d0) 
+ Now korgac is remplaced by kalendaracrc. [Commit.](http://commits.kde.org/pim-data-exporter/966e0241d0a04cdf53c94043b34659959b94c63e) 
+ Make sure to synchronize resources. [Commit.](http://commits.kde.org/pim-data-exporter/926d000d939ce534423428c0dc329527a082e820) 
+ Change the logic. [Commit.](http://commits.kde.org/pim-data-exporter/7c3a5d87369110397b2b765458c3579313d9fa3c) 
+ Fix sync tree folder. [Commit.](http://commits.kde.org/pim-data-exporter/caf36588c9ba654645e12c3e9cdacf5efa73d0ab) 
{{< /details >}}
{{< details id="spectacle" title="spectacle" link="https://commits.kde.org/spectacle" >}}
+ Canceling a screenshot shall not disable buttons if previous screenshot is visible. [Commit.](http://commits.kde.org/spectacle/21bd2a92431fe9039a6833ec1402c6559eb82011) 
{{< /details >}}
{{< details id="yakuake" title="yakuake" link="https://commits.kde.org/yakuake" >}}
+ Fix screen index preference resetting unexpectedly. [Commit.](http://commits.kde.org/yakuake/06cd7566630c2652f06b5059cc427f95f033e194) Fixes bug [#445106](https://bugs.kde.org/445106)
{{< /details >}}
