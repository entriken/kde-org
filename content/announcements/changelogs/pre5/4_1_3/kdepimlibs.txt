2008-09-27 00:03 +0000 [r865199]  djarvie <djarvie@localhost>:

	* branches/KDE/4.1/kdepimlibs/kcal/icalformat_p.cpp: Fix a weeks
	  value being mixed with days/time in ical duration values,
	  contrary to RFC2445. Libical errors when it tries to read a
	  duration containing weeks and days/time, so that a value written
	  can't be read back again.

2008-09-27 00:35 +0000 [r865203]  djarvie <djarvie@localhost>:

	* branches/KDE/4.1/kdepimlibs/kcal/icalformat_p.cpp: Fix mixing
	  weeks and days/time in ical durations (2nd attempt)

2008-10-02 14:36 +0000 [r866971]  tmcguire <tmcguire@localhost>:

	* branches/KDE/4.1/kdepimlibs/kcal/incidenceformatter.cpp: Backport
	  r866396 by tmcguire from trunk to the 4.1 branch: Merged
	  revisions 861651 via svnmerge from
	  svn+ssh://tmcguire@svn.kde.org/home/kde/branches/kdepim/enterprise4/kdepimlibs
	  ........ r861651 | vkrause | 2008-09-16 20:29:28 +0200 (Tue, 16
	  Sep 2008) | 2 lines Fix alignment. ........

2008-10-05 16:26 +0000 [r868165]  winterz <winterz@localhost>:

	* branches/KDE/4.1/kdepimlibs/kioslave/imap4/imap4.cpp: backport
	  SVN commit 868092 by kloecker: A signal handler that calls for
	  example waitpid has to save errno before and restore it
	  afterwards.

2008-10-06 06:16 +0000 [r868356]  scripty <scripty@localhost>:

	* branches/KDE/4.1/kdepimlibs/kresources/kresources_manager.desktop,
	  branches/KDE/4.1/kdepimlibs/kresources/kresources_plugin.desktop:
	  SVN_SILENT made messages (.desktop file)

2008-10-17 06:08 +0000 [r872396]  scripty <scripty@localhost>:

	* branches/KDE/4.1/kdepimlibs/kresources/kresources_manager.desktop,
	  branches/KDE/4.1/kdepimlibs/kresources/kresources_plugin.desktop:
	  SVN_SILENT made messages (.desktop file)

2008-10-19 18:54 +0000 [r873590]  osterfeld <osterfeld@localhost>:

	* branches/KDE/4.1/kdepimlibs/syndication/tools.cpp: backport:
	  don't require < count match > count to consider string as HTML
	  CCBUG:172845 CCBUG:170318 CCBUG:168864

2008-10-21 06:51 +0000 [r874288]  scripty <scripty@localhost>:

	* branches/KDE/4.1/kdepimlibs/kabc/plugins/dir/dir.desktop,
	  branches/KDE/4.1/kdepimlibs/kabc/plugins/net/net.desktop,
	  branches/KDE/4.1/kdepimlibs/kcal/kcal_manager.desktop,
	  branches/KDE/4.1/kdepimlibs/kabc/formats/binary.desktop,
	  branches/KDE/4.1/kdepimlibs/kabc/plugins/file/file.desktop,
	  branches/KDE/4.1/kdepimlibs/kcal/localdir.desktop,
	  branches/KDE/4.1/kdepimlibs/kcal/local.desktop,
	  branches/KDE/4.1/kdepimlibs/mailtransport/kcm_mailtransport.desktop,
	  branches/KDE/4.1/kdepimlibs/kabc/kabc_manager.desktop,
	  branches/KDE/4.1/kdepimlibs/kresources/kresources.desktop:
	  SVN_SILENT made messages (.desktop file)

2008-10-21 18:58 +0000 [r874510]  tmcguire <tmcguire@localhost>:

	* branches/KDE/4.1/kdepimlibs/kcal/incidence.cpp: Backport r874506
	  by tmcguire from trunk to the 4.1 branch: Merged revisions 874469
	  via svnmerge from
	  svn+ssh://tmcguire@svn.kde.org/home/kde/branches/kdepim/enterprise4/kdepimlibs
	  ........ r874469 | tilladam | 2008-10-21 19:08:48 +0200 (Tue, 21
	  Oct 2008) | 1 line Do not honor read only state of incidences in
	  setRelatedTo and setRelatedToUid, otherwise the internal
	  parent/child structures are not built properly, and thus not
	  properly cleaned up, which leads to crashes. Whatever this was
	  supposed to prevent, it will have to be caught at a higher level.
	  ........

2008-10-23 21:49 +0000 [r875281]  kloecker <kloecker@localhost>:

	* branches/KDE/4.1/kdepimlibs/kioslave/sieve/sieve.cpp: Merging
	  revision 875279 from trunk: Fix recognition of Cyrus < 2.3.11.
	  Patch by Martin Fahrendorf.

2008-10-28 17:30 +0000 [r877112-877106]  tmcguire <tmcguire@localhost>:

	* branches/KDE/4.1/kdepimlibs/gpgme++/editinteractor.cpp: Backport
	  r851234 by tmcguire from trunk to the 4.1 branch: Merged
	  revisions 843671 via svnmerge from
	  svn+ssh://tmcguire@svn.kde.org/home/kde/branches/kdepim/enterprise4/kdepimlibs
	  ........ r843671 | osterfeld | 2008-08-07 17:25:04 +0200 (Thu, 07
	  Aug 2008) | 5 lines _write is wrong, WriteFile must be used on
	  windows. Also, try hard to really write the whole string for
	  sure. Makes edit interactor operations start, but still they
	  don't make it through completely CCMAIL:166140 ........

	* branches/KDE/4.1/kdepimlibs/gpgme++/editinteractor.cpp: Backport
	  r851235 by tmcguire from trunk to the 4.1 branch: Merged
	  revisions 844028 via svnmerge from
	  svn+ssh://tmcguire@svn.kde.org/home/kde/branches/kdepim/enterprise4/kdepimlibs
	  ........ r844028 | osterfeld | 2008-08-08 14:51:13 +0200 (Fri, 08
	  Aug 2008) | 1 line check for write errors ........

	* branches/KDE/4.1/kdepimlibs/gpgme++/editinteractor.cpp: Backport
	  r851236 by tmcguire from trunk to the 4.1 branch: Merged
	  revisions 844044 via svnmerge from
	  svn+ssh://tmcguire@svn.kde.org/home/kde/branches/kdepim/enterprise4/kdepimlibs
	  ........ r844044 | osterfeld | 2008-08-08 16:24:13 +0200 (Fri, 08
	  Aug 2008) | 1 line fix stupid error, use size_t ........

	* branches/KDE/4.1/kdepimlibs/gpgme++/key.cpp,
	  branches/KDE/4.1/kdepimlibs/gpgme++/CMakeLists.txt: Backport
	  r877056 by tmcguire from trunk to the 4.1 branch: Merged
	  revisions 875493 via svnmerge from
	  svn+ssh://tmcguire@svn.kde.org/home/kde/branches/kdepim/enterprise4/kdepimlibs
	  ........ r875493 | mutz | 2008-10-24 17:39:36 +0200 (Fri, 24 Oct
	  2008) | 1 line Use gpgme_subkey_t::keyid for keyId() and
	  shortKeyId(), not the fingerprint. Let's hope this was a mistake
	  and not a workaround for something... :) ........

