---
aliases:
- ../../plasma-5.18.6
changelog: 5.18.5-5.18.6
date: 2020-09-29
layout: plasma
peertube: cda402b5-2bcb-4c0c-b232-0fa5a4dacaf5
figure:
  src: /announcements/plasma/5/5.18.0/plasma-5.18.png
---

{{< i18n_date >}}. {{< i18n "annc-plasma-bugfix-intro" "5" "5.18.6" >}}

{{% i18n "annc-plasma-bugfix-minor-release-2" "5.18" "/announcements/plasma/5/5.18.0" "2020" %}}

{{< i18n "annc-plasma-bugfix-worth-8" >}} {{< i18n "annc-plasma-bugfix-last" >}}

+ Kcm_fonts: Make the font selection dialog select the correct Regular-like style. <a href="https://commits.kde.org/plasma-desktop/e5e5f5ed51aadfac99bfbdf3d2db5be16a12443b">Commit.</a> See bug <a href="https://bugs.kde.org/420287">#420287</a>
+ Fix calendar events not being shown at first. <a href="https://commits.kde.org/plasma-workspace/97648f015cff39e46e39ee7b150515d1d3bce5f7">Commit.</a>
+ Discover: Confirm reboot action with the user. <a href="https://commits.kde.org/discover/f2dce150e6d32810d1deae08378a136fc13e9988">Commit.</a>
